/**
 * 
 */
package com.appletech.bean.center.performance;

/**
 * the third exam in a term 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class CycleThreeScore extends Performance{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3693358655050283049L;

	/**
	 * 
	 */
	public CycleThreeScore() {
		super();
	}

	@Override
	public String toString() {
		return "CycleThreeScore [getCenterId()=" + getCenterId() + ", getStudentId()=" + getStudentId()
				+ ", getSubjectId()=" + getSubjectId() + ", getClassroomId()=" + getClassroomId() + ", getStreamId()="
				+ getStreamId() + ", getCycleThreeScore()=" + getCycleThreeScore() + ", getTerm()=" + getTerm()
				+ ", getYear()=" + getYear() + ", getUuid()=" + getUuid() + "]";
	}


}

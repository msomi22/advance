/**
 * 
 */
package com.appletech.bean.center.performance;

/**
 * the second exam in a term
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class CycleTwoScore extends Performance{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3683150959100888509L;

	/**
	 * 
	 */
	public CycleTwoScore() {
		super();
	}

	@Override
	public String toString() {
		return "CycleTwoScore [getCenterId()=" + getCenterId() + ", getStudentId()=" + getStudentId()
				+ ", getSubjectId()=" + getSubjectId() + ", getClassroomId()=" + getClassroomId() + ", getStreamId()="
				+ getStreamId() + ", getCycleTwoScore()=" + getCycleTwoScore() + ", getTerm()=" + getTerm()
				+ ", getYear()=" + getYear() + ", getUuid()=" + getUuid() + "]";
	}

	
}

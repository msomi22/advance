/**
 * 
 */
package com.appletech.bean.center.performance;

import java.util.*;

/**
 * @author peter
 *
 */
public class Result {
	
	private String studentId;
	private String streamId;
	private int totalPoints;
	private int cycle1totalPoints;
	private int cycle2totalPoints;
	private int cycle3totalPoints;
	private int avgmeanScore;
	private int cycle1meanScore;
	private int cycle2meanScore;
	private int cycle3meanScore;
	private String avgmeanGrade;
	private String cycle1meanGrade;
	private String cycle2meanGrade;
	private String cycle3meanGrade;
	private String division;
	private String iscomplete;
	private String isabsent;
	private String hasPassed;
	private Map<String,Integer> subScore;
	private Map<String,Integer> cycle1scoreMap;
	private Map<String,Integer> cycle2scoreMap;
	private Map<String,Integer> cycle3scoreMap;
	
	/**
	 * @param studentId
	 * @param totalPoints
	 * @param division
	 * @param iscomplete
	 * @param hasPassed
	 * @param subScore
	 */
	public Result() {
		studentId = "";
		streamId = "";
		totalPoints = 0;
		cycle1totalPoints = 0;
		cycle2totalPoints = 0;
		cycle3totalPoints = 0;
		avgmeanScore = 0;
		cycle1meanScore = 0;
		cycle2meanScore = 0;
		cycle3meanScore = 0;
		avgmeanGrade = "";
		cycle1meanGrade = "";
		cycle2meanGrade = "";
		cycle3meanGrade = "";
		division = "";
		iscomplete = "Complete";
		isabsent = "Present";
		hasPassed = "";
		subScore = new HashMap<>(); 
		cycle1scoreMap = new HashMap<>();
		cycle2scoreMap = new HashMap<>();
		cycle3scoreMap = new HashMap<>();
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public int getTotalPoints() {
		return (int) totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public int getCycle1totalPoints() {
		return cycle1totalPoints;
	}

	public void setCycle1totalPoints(int cycle1totalPoints) {
		this.cycle1totalPoints = cycle1totalPoints;
	}

	public int getCycle2totalPoints() {
		return cycle2totalPoints;
	}

	public void setCycle2totalPoints(int cycle2totalPoints) {
		this.cycle2totalPoints = cycle2totalPoints;
	}

	public int getCycle3totalPoints() {
		return cycle3totalPoints;
	}

	public void setCycle3totalPoints(int cycle3totalPoints) {
		this.cycle3totalPoints = cycle3totalPoints;
	}

	public int getAvgmeanScore() {
		return avgmeanScore;
	}

	public void setAvgmeanScore(int avgmeanScore) {
		this.avgmeanScore = avgmeanScore;
	}

	public int getCycle1meanScore() {
		return cycle1meanScore;
	}

	public void setCycle1meanScore(int cycle1meanScore) {
		this.cycle1meanScore = cycle1meanScore;
	}

	public int getCycle2meanScore() {
		return cycle2meanScore;
	}

	public void setCycle2meanScore(int cycle2meanScore) {
		this.cycle2meanScore = cycle2meanScore;
	}

	public int getCycle3meanScore() {
		return cycle3meanScore;
	}

	public void setCycle3meanScore(int cycle3meanScore) {
		this.cycle3meanScore = cycle3meanScore;
	}

	public String getAvgmeanGrade() {
		return avgmeanGrade;
	}

	public void setAvgmeanGrade(String avgmeanGrade) {
		this.avgmeanGrade = avgmeanGrade;
	}

	public String getCycle1meanGrade() {
		return cycle1meanGrade;
	}

	public void setCycle1meanGrade(String cycle1meanGrade) {
		this.cycle1meanGrade = cycle1meanGrade;
	}

	public String getCycle2meanGrade() {
		return cycle2meanGrade;
	}

	public void setCycle2meanGrade(String cycle2meanGrade) {
		this.cycle2meanGrade = cycle2meanGrade;
	}

	public String getCycle3meanGrade() {
		return cycle3meanGrade;
	}

	public void setCycle3meanGrade(String cycle3meanGrade) {
		this.cycle3meanGrade = cycle3meanGrade;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getIscomplete() {
		return iscomplete;
	}

	public void setIscomplete(String iscomplete) {
		this.iscomplete = iscomplete;
	}

	public String getIsabsent() {
		return isabsent;
	}

	public void setIsabsent(String isabsent) {
		this.isabsent = isabsent;
	}

	public String getHasPassed() {
		return hasPassed;
	}

	public void setHasPassed(String hasPassed) {
		this.hasPassed = hasPassed;
	}

	public Map<String, Integer> getSubScore() {
		return subScore;
	}

	public void setSubScore(Map<String, Integer> subScore) {
		this.subScore = subScore;
	}

	public Map<String, Integer> getCycle1scoreMap() {
		return cycle1scoreMap;
	}

	public void setCycle1scoreMap(Map<String, Integer> cycle1scoreMap) {
		this.cycle1scoreMap = cycle1scoreMap;
	}

	public Map<String, Integer> getCycle2scoreMap() {
		return cycle2scoreMap;
	}

	public void setCycle2scoreMap(Map<String, Integer> cycle2scoreMap) {
		this.cycle2scoreMap = cycle2scoreMap;
	}

	public Map<String, Integer> getCycle3scoreMap() {
		return cycle3scoreMap;
	}

	public void setCycle3scoreMap(Map<String, Integer> cycle3scoreMap) {
		this.cycle3scoreMap = cycle3scoreMap;
	}

	@Override
	public String toString() {
		return "Result [studentId=" + studentId + ", streamId=" + streamId + ", totalPoints=" + totalPoints
				+ ", cycle1totalPoints=" + cycle1totalPoints + ", cycle2totalPoints=" + cycle2totalPoints
				+ ", cycle3totalPoints=" + cycle3totalPoints + ", avgmeanScore=" + avgmeanScore + ", cycle1meanScore="
				+ cycle1meanScore + ", cycle2meanScore=" + cycle2meanScore + ", cycle3meanScore=" + cycle3meanScore
				+ ", avgmeanGrade=" + avgmeanGrade + ", cycle1meanGrade=" + cycle1meanGrade + ", cycle2meanGrade="
				+ cycle2meanGrade + ", cycle3meanGrade=" + cycle3meanGrade + ", division=" + division + ", iscomplete="
				+ iscomplete + ", isabsent=" + isabsent + ", hasPassed=" + hasPassed + ", subScore=" + subScore
				+ ", cycle1scoreMap=" + cycle1scoreMap + ", cycle2scoreMap=" + cycle2scoreMap + ", cycle3scoreMap="
				+ cycle3scoreMap + "]";
	}

	
	
}

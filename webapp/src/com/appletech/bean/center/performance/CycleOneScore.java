/**
 * 
 */
package com.appletech.bean.center.performance;

/**
 * The first exam in a term 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class CycleOneScore extends Performance{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2518279033337616555L;

	public CycleOneScore() {
		super();
	}

	@Override
	public String toString() {
		return "CycleOneScore [getCenterId()=" + getCenterId() + ", getStudentId()=" + getStudentId()
				+ ", getSubjectId()=" + getSubjectId() + ", getClassroomId()=" + getClassroomId() + ", getStreamId()="
				+ getStreamId() + ", getCycleOneScore()=" + getCycleOneScore() + ", getTerm()=" + getTerm()
				+ ", getYear()=" + getYear() + ", getUuid()=" + getUuid() + "]";
	}

	

}


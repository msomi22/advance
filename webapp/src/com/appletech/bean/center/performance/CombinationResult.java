/**
 * 
 */
package com.appletech.bean.center.performance;

import java.util.HashMap;
import java.util.Map;

/**
 * @author peter
 *
 */
public class CombinationResult {
	
	private String studentId;
	private String combinationId;
	private int totalPoints;
	private String division;
	private Map<String,Integer> subScore;

	/**
	 * 
	 */
	public CombinationResult() {
		studentId = "";
		combinationId = "";
		totalPoints = 0;
		division = "";
		subScore = new HashMap<>(); 
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(String combinationId) {
		this.combinationId = combinationId;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Map<String, Integer> getSubScore() {
		return subScore;
	}

	public void setSubScore(Map<String, Integer> subScore) {
		this.subScore = subScore;
	}

	@Override
	public String toString() {
		return "CombinationResult [studentId=" + studentId + ", combinationId=" + combinationId + ", totalPoints="
				+ totalPoints + ", division=" + division + ", subScore=" + subScore + "]";
	}
	
	
	

}

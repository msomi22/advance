package com.appletech.bean.center.performance;

import com.appletech.bean.StorableBean;

/** 
 * Perfomance ---The systems output for marks
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class Performance extends StorableBean {

	private String centerId;
	private String studentId;
	private String subjectId;
	private String classroomId;
	private String streamId;
	private int cycleOneScore;
	private int cycleTwoScore;
	private int cycleThreeScore;
	private String term;
	private String year;
	//centerId,classroomId/streamId,term,year,(cycle)
	//studentId,subjectId

	public Performance() {

		centerId = "";
		studentId = "";
		subjectId = "";
		classroomId = "";
		streamId = "";
		cycleOneScore = 0;
		cycleTwoScore = 0;
		cycleThreeScore = 0;
		term = "";
		year = "";
	}
	
	

	public String getCenterId() {
		return centerId;
	}



	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}



	public String getStudentId() {
		return studentId;
	}



	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}



	public String getSubjectId() {
		return subjectId;
	}



	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}



	public String getClassroomId() {
		return classroomId;
	}



	public void setClassroomId(String classroomId) {
		this.classroomId = classroomId;
	}



	public String getStreamId() {
		return streamId;
	}



	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}



	public int getCycleOneScore() {
		return cycleOneScore;
	}



	public void setCycleOneScore(int cycleOneScore) {
		this.cycleOneScore = cycleOneScore;
	}



	public int getCycleTwoScore() {
		return cycleTwoScore;
	}



	public void setCycleTwoScore(int cycleTwoScore) {
		this.cycleTwoScore = cycleTwoScore;
	}



	public int getCycleThreeScore() {
		return cycleThreeScore;
	}



	public void setCycleThreeScore(int cycleThreeScore) {
		this.cycleThreeScore = cycleThreeScore;
	}



	public String getTerm() {
		return term;
	}



	public void setTerm(String term) {
		this.term = term;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	@Override
	public String toString() {
		return "Performance [centerId=" + centerId + ", studentId=" + studentId + ", subjectId=" + subjectId
				+ ", classroomId=" + classroomId + ", streamId=" + streamId + ", cycleOneScore=" + cycleOneScore
				+ ", cycleTwoScore=" + cycleTwoScore + ", cycleThreeScore=" + cycleThreeScore + ", term=" + term
				+ ", year=" + year + ", getUuid()=" + getUuid() + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1065024866918728963L;

}

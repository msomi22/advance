package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * Rank divisions
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class Division extends StorableBean {

	private String centerId;
	private String levelId;
	private String lowerMark;
	private String upperMark;
	private String description;
	private String grade;
	
	public Division() {
		centerId = "";
		levelId = "";
		lowerMark = "";
		upperMark = "";
		description = "";
		grade = "";
	}
	
	
	/**
	 * @return the centerId
	 */
	public String getCenterId() {
		return centerId;
	}


	/**
	 * @param centerId the centerId to set
	 */
	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}


	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}


	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}


	/**
	 * @return the lowerMark
	 */
	public String getLowerMark() {
		return lowerMark;
	}


	/**
	 * @param lowerMark the lowerMark to set
	 */
	public void setLowerMark(String lowerMark) {
		this.lowerMark = lowerMark;
	}


	/**
	 * @return the upperMark
	 */
	public String getUpperMark() {
		return upperMark;
	}


	/**
	 * @param upperMark the upperMark to set
	 */
	public void setUpperMark(String upperMark) {
		this.upperMark = upperMark;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}


	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}


	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Division [centerId=" + centerId + ", levelId=" + levelId + ", lowerMark=" + lowerMark + ", upperMark="
				+ upperMark + ", description=" + description + ", grade=" + grade + ", getUuid()=" + getUuid() + "]";
	}


	/**
	* 
	*/
	private static final long serialVersionUID = -5574434792841878378L;

}

package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * Config---Will load configuration parameters specific to a school
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class Config extends StorableBean {

	private String centerId;
	private String cycleId;
	private String term;
	private String year;
	private String closingdate;
	private String oppeningdate;
	private String headTeacherComment;
    

	public Config() {

		centerId = "";
		cycleId = "";
		term = "";
		year = "";
		closingdate = "";
		oppeningdate = "";
		headTeacherComment = "";

	}

	

	public String getCenterId() {
		return centerId;
	}



	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}



	public String getCycleId() {
		return cycleId;
	}



	public void setCycleId(String cycleId) {
		this.cycleId = cycleId;
	}



	public String getTerm() {
		return term;
	}



	public void setTerm(String term) {
		this.term = term;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	public String getClosingdate() {
		return closingdate;
	}



	public void setClosingdate(String closingdate) {
		this.closingdate = closingdate;
	}



	public String getOppeningdate() {
		return oppeningdate;
	}



	public void setOppeningdate(String oppeningdate) {
		this.oppeningdate = oppeningdate;
	}



	public String getHeadTeacherComment() {
		return headTeacherComment;
	}



	public void setHeadTeacherComment(String headTeacherComment) {
		this.headTeacherComment = headTeacherComment;
	}



	@Override
	public String toString() {
		return "Config [centerId=" + centerId + ", cycleId=" + cycleId + ", term=" + term + ", year=" + year
				+ ", closingdate=" + closingdate + ", oppeningdate=" + oppeningdate + ", headTeacherComment="
				+ headTeacherComment + ", getUuid()=" + getUuid() + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = -1355321225628040544L;

}

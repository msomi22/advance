package com.appletech.bean.center;

import java.sql.Timestamp;
import java.util.Date;

import com.appletech.bean.StorableBean;

/** 
 * @author dennis
 *
 */
public class Center extends StorableBean {

	private String centerNo;
	private String centerRegion;
	private String centerName;
	private String email;
	private String website;
	private String centerCode;
	private String postalCode;
	private String postalAddress;
	private String town;
	private String logoUri;
	private String signatureUri;
	private String isActive;
	private Timestamp dateRegistred;
	

	public Center() {
		email= "";
		website= "";
		centerCode= "";
		postalCode= "";
		postalAddress= "";
		town= "";
		logoUri= "";
		signatureUri= "";
		centerNo = "";
		centerRegion = "";
		centerName = "";
		isActive = "0";
		dateRegistred = new Timestamp(new Date().getTime());
	}



	public String getCenterNo() {
		return centerNo;
	}



	public void setCenterNo(String centerNo) {
		this.centerNo = centerNo;
	}



	public String getCenterRegion() {
		return centerRegion;
	}



	public void setCenterRegion(String centerRegion) {
		this.centerRegion = centerRegion;
	}



	public String getCenterName() {
		return centerName;
	}



	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getWebsite() {
		return website;
	}



	public void setWebsite(String website) {
		this.website = website;
	}



	public String getCenterCode() {
		return centerCode;
	}



	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}



	public String getPostalCode() {
		return postalCode;
	}



	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}



	public String getPostalAddress() {
		return postalAddress;
	}



	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}



	public String getTown() {
		return town;
	}



	public void setTown(String town) {
		this.town = town;
	}



	public String getLogoUri() {
		return logoUri;
	}



	public void setLogoUri(String logoUri) {
		this.logoUri = logoUri;
	}



	public String getSignatureUri() {
		return signatureUri;
	}



	public void setSignatureUri(String signatureUri) {
		this.signatureUri = signatureUri;
	}



	public String getIsActive() {
		return isActive;
	}



	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}



	public Timestamp getDateRegistred() {
		return dateRegistred;
	}



	public void setDateRegistred(Timestamp dateRegistred) {
		this.dateRegistred = dateRegistred;
	}



	@Override
	public String toString() {
		return "Center [centerNo=" + centerNo + ", centerRegion=" + centerRegion + ", centerName=" + centerName
				+ ", email=" + email + ", website=" + website + ", centerCode=" + centerCode + ", postalCode="
				+ postalCode + ", postalAddress=" + postalAddress + ", town=" + town + ", logoUri=" + logoUri
				+ ", signatureUri=" + signatureUri + ", isActive=" + isActive + ", dateRegistred=" + dateRegistred
				+ ", getUuid()=" + getUuid() + "]";
	}



	/**
	* 
	*/
	private static final long serialVersionUID = -5672207796072326724L;

}

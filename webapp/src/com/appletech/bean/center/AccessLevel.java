package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * AccessLevel ---
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class AccessLevel extends StorableBean {

	private String centerId;
	private String description;

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AccessLevel [uuid=" + getUuid() + ", centerId=" + centerId + ", description=" + description + "]";
	}

	public AccessLevel() {

		centerId = "";
		description = "";

	}

	/**
	* 
	*/
	private static final long serialVersionUID = 3434920470152664289L;

}

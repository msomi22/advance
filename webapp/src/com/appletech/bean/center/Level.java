/**
 * 
 */
package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * @author peter
 *
 */
public class Level  extends StorableBean {
	
	private String centerId;
	private String levelId;
	private String description;
	 
	/**
	 * 
	 */
	public Level() {
		centerId = "";
		levelId = "";
		description = "";
	}

	/**
	 * @return the centerId
	 */
	public String getCenterId() {
		return centerId;
	}

	/**
	 * @param centerId the centerId to set
	 */
	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Level [centerId=" + centerId + ", levelId=" + levelId + ", description=" + description + "]";
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2458761030200864213L;

}

package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/** 
 * A classroom is made up of streams or just one stream
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Classroom extends StorableBean{
	
	private String centerId ;
	private String description;
	
	public Classroom(){
		centerId = "";
		description = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Classroom [centerId=" + centerId + ", description=" + description + ", getUuid()=" + getUuid() + "]";
	}
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 6305850986015029499L;
}

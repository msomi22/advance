package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/**
 * A stream is a group of students that are teachable 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Stream extends StorableBean{
	
	private String centerId;
	private String classroomId;
	private String description;
	
	
	public Stream(){
		centerId = "";
		classroomId = "";
		description = "";
	}


	public String getCenterId() {
		return centerId;
	}


	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}


	public String getClassroomId() {
		return classroomId;
	}


	public void setClassroomId(String classroomId) {
		this.classroomId = classroomId;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "Stream [centerId=" + centerId + ", classroomId=" + classroomId + ", description=" + description
				+ ", getUuid()=" + getUuid() + "]";
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 8395582270086269648L;

}

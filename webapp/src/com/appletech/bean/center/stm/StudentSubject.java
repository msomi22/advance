package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/** 
 * A subject that a student takes
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class StudentSubject extends StorableBean{
	
	private String centerId;
	private String studentId;
	private String subjectId;
	
	public StudentSubject(){
		centerId = "";
		studentId = "";
		subjectId = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}




	@Override
	public String toString() {
		return "StudentSubject [centerId=" + centerId + ", studentId=" + studentId + ", subjectId=" + subjectId
				+ ", getUuid()=" + getUuid() + "]";
	}




	/**
	 * 
	 */
	private static final long serialVersionUID = 6057139696975152365L;
	

}

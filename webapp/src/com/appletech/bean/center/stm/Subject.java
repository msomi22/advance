package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/**  
 * A subject in a center/school
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Subject extends StorableBean{
	
	private String centerId;
	private String subjectCode;
	private String subjectNumCode;
	private String subjectDescr;
	
	public Subject(){
		centerId = "";
		subjectCode = "";
		subjectNumCode = "";
		subjectDescr = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	
	

	public String getSubjectNumCode() {
		return subjectNumCode;
	}

	public void setSubjectNumCode(String subjectNumCode) {
		this.subjectNumCode = subjectNumCode;
	}

	public String getSubjectDescr() {
		return subjectDescr;
	}

	public void setSubjectDescr(String subjectDescr) {
		this.subjectDescr = subjectDescr;
	}

	

	@Override
	public String toString() {
		return "Subject [centerId=" + centerId + ", subjectCode=" + subjectCode + ", subjectNumCode=" + subjectNumCode
				+ ", subjectDescr=" + subjectDescr + ", getUuid()=" + getUuid() + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = -3533040741395946138L;

}

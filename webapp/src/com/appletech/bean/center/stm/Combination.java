/**
 * 
 */
package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/** 
 * @author peter
 *
 */
public class Combination extends StorableBean{
	
	 private String centerId;
	 private String description;

	/**
	 * 
	 */
	public Combination() {
		centerId = "";
		description = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Combination [centerId=" + centerId + ", description=" + description + ", getUuid()=" + getUuid() + "]";
	}
	
	 /**
		 * 
		 */
		private static final long serialVersionUID = -8816910620794662865L;

}

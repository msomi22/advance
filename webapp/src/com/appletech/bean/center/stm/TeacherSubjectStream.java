package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/** 
 * Map the subject and a class that is allocated to a teacher 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class TeacherSubjectStream extends StorableBean{
	
	private String centerId;
	private String teacherId;
	private String subjectId;
	private String streamId;
	
	public TeacherSubjectStream(){
		centerId = "";
		teacherId = "";
		subjectId = "";
		streamId = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	@Override
	public String toString() {
		return "TeacherSubjectStream [centerId=" + centerId + ", teacherId=" + teacherId + ", subjectId=" + subjectId
				+ ", streamId=" + streamId + ", getUuid()=" + getUuid() + "]";
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4082732942497100923L;
}

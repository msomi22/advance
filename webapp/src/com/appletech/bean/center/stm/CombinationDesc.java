/**
 * 
 */
package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;

/** 
 * @author peter
 *
 */
public class CombinationDesc extends StorableBean{
	
	
	 private String centerId;
	 private String combinationId;
	 private String subjectId;

	/**
	 * 
	 */
	public CombinationDesc() {
		centerId = "";
		combinationId = "";
		subjectId = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(String combinationId) {
		this.combinationId = combinationId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	@Override
	public String toString() {
		return "CombinationDesc [centerId=" + centerId + ", combinationId=" + combinationId + ", subjectId=" + subjectId
				+ ", getUuid()=" + getUuid() + "]";
	}
	
	 /**
		 * 
		 */
		private static final long serialVersionUID = 8178610985560176156L;

}

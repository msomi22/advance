package com.appletech.bean.center.stm;

import com.appletech.bean.StorableBean;
/** 
 * Map a stream allocated to a teacher 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class TeacherStream extends StorableBean{
	
	private String centerId;
	private String teacherId;
	private String streamId;
	
	public TeacherStream(){
		centerId = "";
		teacherId = "";
		streamId = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	@Override
	public String toString() {
		return "TeacherStream [centerId=" + centerId + ", teacherId=" + teacherId + ", streamId=" + streamId
				+ ", getUuid()=" + getUuid() + "]";
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2909767859275311494L;

}

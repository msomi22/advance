package com.appletech.bean.center.staff;

import java.sql.Timestamp;
import java.util.Date;

import com.appletech.bean.StorableBean;


/** 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Staff extends StorableBean{
	
	private String centerId;
	private String accessLevelId ;
	private String name;
	private String gender;
	private String mobile;
	private String password;
	private String email;
	private String idNumber;
	private String nationality;
	private String isActive;
	private Timestamp dateRegistered;
	 
	public Staff(){
		 centerId = "";
		 accessLevelId ="";
		 password = "";
		 name = "";
		 gender = "";
		 mobile = "";
		 email = "";
		 idNumber = "";
		 nationality = "";
		 isActive = "1";//active 
		 dateRegistered = new Timestamp(new Date().getTime()); 
	}
    
	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getAccessLevelId() {
		return accessLevelId;
	}

	public void setAccessLevelId(String accessLevelId) {
		this.accessLevelId = accessLevelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Timestamp getDateRegistered() {
		return dateRegistered;
	}

	public void setDateRegistered(Timestamp dateRegistered) {
		this.dateRegistered = dateRegistered;
	}
	
	

	@Override
	public String toString() {
		return "Staff [centerId=" + centerId + ", accessLevelId=" + accessLevelId + ", name=" + name + ", gender="
				+ gender + ", mobile=" + mobile + ", password=" + password + ", email=" + email + ", idNumber="
				+ idNumber + ", nationality=" + nationality + ", isActive=" + isActive + ", dateRegistered="
				+ dateRegistered + ", getUuid()=" + getUuid() + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = -216574474239293976L;
}

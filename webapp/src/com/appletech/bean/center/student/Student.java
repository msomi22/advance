package com.appletech.bean.center.student;

import java.sql.Timestamp;
import java.util.Date;

import com.appletech.bean.StorableBean;


/** 
 * A student in a school/center
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Student extends StorableBean implements Comparable<Student>{

	private String centerId;
	private String admmitedStreamId;
	private String currentStreamId;
	private String combinationId;
	private String indexNo;
	private String firstname;
	private String middlename;
	private String lastname;
	private String gender;
	private String levelId;
	private String isActive;
	private String isAlumnus;
	private String photoUri;
	private Timestamp admissionDate;

	public Student(){
		centerId = "";
		admmitedStreamId = "";
		currentStreamId = "";
		combinationId = "";
		indexNo = "";
		firstname = "";
		middlename = "";
		lastname = "";
		gender = "";
		levelId = "";
		isActive = "1";//Active by default 
		isAlumnus = "0";//Not alumnus by default 
		photoUri = "";
		admissionDate = new Timestamp(new Date().getTime());
	}

	




	/**
	 * @return the centerId
	 */
	public String getCenterId() {
		return centerId;
	}

	/**
	 * @param centerId the centerId to set
	 */
	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	/**
	 * @return the admmitedStreamId
	 */
	public String getAdmmitedStreamId() {
		return admmitedStreamId;
	}

	/**
	 * @param admmitedStreamId the admmitedStreamId to set
	 */
	public void setAdmmitedStreamId(String admmitedStreamId) {
		this.admmitedStreamId = admmitedStreamId;
	}

	/**
	 * @return the currentStreamId
	 */
	public String getCurrentStreamId() {
		return currentStreamId;
	}

	/**
	 * @param currentStreamId the currentStreamId to set
	 */
	public void setCurrentStreamId(String currentStreamId) {
		this.currentStreamId = currentStreamId;
	}

	/**
	 * @return the combinationId
	 */
	public String getCombinationId() {
		return combinationId;
	}

	/**
	 * @param combinationId the combinationId to set
	 */
	public void setCombinationId(String combinationId) {
		this.combinationId = combinationId;
	}

	/**
	 * @return the indexNo
	 */
	public String getIndexNo() {
		return indexNo;
	}

	/**
	 * @param indexNo the indexNo to set
	 */
	public void setIndexNo(String indexNo) {
		this.indexNo = indexNo;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}

	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the isAlumnus
	 */
	public String getIsAlumnus() {
		return isAlumnus;
	}

	/**
	 * @param isAlumnus the isAlumnus to set
	 */
	public void setIsAlumnus(String isAlumnus) {
		this.isAlumnus = isAlumnus;
	}

	/**
	 * @return the photoUri
	 */
	public String getPhotoUri() {
		return photoUri;
	}

	/**
	 * @param photoUri the photoUri to set
	 */
	public void setPhotoUri(String photoUri) {
		this.photoUri = photoUri;
	}

	/**
	 * @return the admissionDate
	 */
	public Timestamp getAdmissionDate() {
		return admissionDate;
	}

	/**
	 * @param admissionDate the admissionDate to set
	 */
	public void setAdmissionDate(Timestamp admissionDate) {
		this.admissionDate = admissionDate;
	}


	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Student [centerId=" + centerId + ", admmitedStreamId=" + admmitedStreamId + ", currentStreamId="
				+ currentStreamId + ", combinationId=" + combinationId + ", indexNo=" + indexNo + ", firstname="
				+ firstname + ", middlename=" + middlename + ", lastname=" + lastname + ", gender=" + gender
				+ ", levelId=" + levelId + ", isActive=" + isActive + ", isAlumnus=" + isAlumnus + ", photoUri="
				+ photoUri + ", admissionDate=" + admissionDate + ", getUuid()=" + getUuid() + "]";
	}






	/**
	 * 
	 */
	private static final long serialVersionUID = 4676614320448501980L;

	@Override
	public int compareTo(Student student) {
		return getIndexNo().compareTo(((Student) student).getIndexNo());
	}

}

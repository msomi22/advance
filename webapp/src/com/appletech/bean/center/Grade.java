package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * Grade --- A, B ---
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class Grade extends StorableBean {

	private String centerId;
	private String lowerMark;
	private String upperMark;
	private String description;
	private String point;

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getLowerMark() {
		return lowerMark;
	}

	public void setLowerMark(String lowerMark) {
		this.lowerMark = lowerMark;
	}

	public String getUpperMark() {
		return upperMark;
	}

	public void setUpperMark(String upperMark) {
		this.upperMark = upperMark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "Grade [centerId=" + centerId + ", lowerMark=" + lowerMark + ", upperMark=" + upperMark
				+ ", description=" + description + ", point=" + point + ", getUuid()=" + getUuid() + "]";
	}

	public Grade() {

		centerId = "";
		lowerMark = "";
		upperMark = "";
		description = "";
		point = "";

	}

	/**
	* 
	*/
	private static final long serialVersionUID = -7160530289362349694L;

}

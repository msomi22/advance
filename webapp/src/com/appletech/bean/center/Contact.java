/**
 * 
 */
package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * @author peter
 *
 */
public class Contact extends StorableBean{

	private String centerId;
	private String mobile;
	private String description;

	/**
	 * 
	 */
	public Contact() {
		centerId = "";
	    mobile = "";
	    description = "";
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

	
	@Override
	public String toString() {
		return "Contact [centerId=" + centerId + ", mobile=" + mobile + ", description=" + description + ", getUuid()="
				+ getUuid() + "]";
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

package com.appletech.bean.center;

import com.appletech.bean.StorableBean;

/** 
 * Cycle ---similar to term
 * 
 * @author <a href="mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */
public class Cycle extends StorableBean {

	private String centerId;
	private String description;

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Cycle [centerId=" + centerId + ", description=" + description + ", getUuid()=" + getUuid() + "]";
	}

	public Cycle() {

		centerId = "";
		description = "";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6834973371997145895L;

}

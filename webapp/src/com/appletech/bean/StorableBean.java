/**
 * 
 * 
 */
package com.appletech.bean;

import java.io.Serializable;
import java.util.UUID;

/**
 * Common to all objects 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class StorableBean implements Serializable {
	
	private String uuid;
	/**
	 * 
	 */
	public StorableBean() {
		uuid = UUID.randomUUID().toString();
	} 
	
	 /**
	  * 
	  * @return the uuid
	  */
	public String getUuid() {
		return uuid;
	}
	/**
	 * 
	 * @param uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6756966885024490524L;
}

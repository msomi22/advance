/**
 * 
 * 
 */
package com.appletech.server.session;

/** 
 * A class that manages all the sessions in the users side
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class SessionConstants {
	
	//session management
	final public static int SESSION_TIMEOUT = 500; 
	
	public static final String CENTER_SIGN_IN_ERROR= "Error Login";
	public static final String CENTER_SIGN_IN_ID = "Center Signin CenterId";
	public static final String CENTER_SIGN_IN_CENTERNO= "Center Signin Key";
	public static final String CENTER_SIGN_IN_TIME = "Center Signin Time";
	public static final String CENTER_SIGN_SUCCESS = "Center Signin Success";
	
	public static final String CENTER_STAFF_SIGN_ID = "Center_Staff Signin Id";
	public static final String CENTER_STAFF_SIGN_MOBILE = "Center_Staff Signin Moblie";
	public static final String CENTER_STAFF_SIGN_ACCESS_LEVEL = "Center_Staff Signin AccessLevel";
	
	public static final String STAFF_CENTER_UPDATE_ERROR = "Staff Center update error.";
	public static final String STAFF_CENTER_UPDATE_SUCCESS = "Staff Center update success.";
	
	public static final String STUDENT_UPDATE_ERROR = "Student update error";
	public static final String STUDENT_UPDATE_SUCCESS = "Student update success";
	public static final String STUDENT_PARAM = "student param";
	
	public static final String STUDENT_ADD_ERROR = "Student add error";
	public static final String STUDENT_ADD_SUCCESS = "Student add success";
	
	public static final String CONFIG_UPDATE_ERROR = "Config update error";
	public static final String CONFIG_UPDATE_SUCCESS = "Config update success";
	public static final String CONFIG_PARAM = "Config param";
	
	public static final String STAFF_ADD_ERROR = "Staff add error";
	public static final String STAFF_ADD_SUCCESS = "Staff add success";
	
	public static final String STAFF_RESET_PASSWORD_ERROR = "Staff reset password error";
	public static final String STAFF_RESET_PASSWORD_SUCCESS = "Staff reset password success";
	
	public static final String USER_FORGOT_PASSWORD_ERROR = "User forgot password error";
	public static final String USER_FORGOT_PASSWORD_SUCCESS = "User forgot password success";

	
	public static final String STAFF_UPDATE_ERROR = "Staff update error";
	public static final String STAFF_UPDATE_SUCCESS = "Staff update success";
	public static final String STAFF_ADD_PARAM = "staff param";
	public static final String STAFF_UPDATE_PARAM = "staff param";

	
	
	
	

}

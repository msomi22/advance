/**
 *
 * 
 */
package com.appletech.server.session;

/** 
 * A class that manages all the sessions in the admin side
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */

public class AdminSessionConstants {

	   //session management
		final public static int SESSION_TIMEOUT = 500; 
		
		final public static String ADMIN_SIGN_IN_KEY = "Admin sign-in  username";
		final public static String ADMIN_SIGN_IN_TIME = "Admin sign-in time";
		
		final public static String ADMIN_LOGIN_ERROR = "admin login error"; 
		
		final public static String SCHOOL_UPDATE_ERROR = "School update error"; 
		final public static String SCHOOL_UPDATE_SUCCESS = "School update success"; 
		final public static String SCHOOL_UPDATE_PARAM = "School Update param"; 
		
		final public static String SCHOOL_ADD_ERROR = "School add error"; 
		final public static String SCHOOL_ADD_SUCCESS = "School add success"; 
		final public static String SCHOOL_PARAM = "School param"; 
		
		
		

}

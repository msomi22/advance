/**
 * 
 */
package com.appletech.server.servlet.admin;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.appletech.server.session.AdminSessionConstants;


/**
 * @author peter
 *
 */
public class AdminLogout  extends HttpServlet{
	

	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


        HttpSession session = request.getSession(true);
        String username = (String) session.getAttribute(AdminSessionConstants.ADMIN_SIGN_IN_KEY);

        response.sendRedirect("index.jsp");

        if (session != null) {
            session.invalidate();
        } 
        
    
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 4084498609555048211L;

}

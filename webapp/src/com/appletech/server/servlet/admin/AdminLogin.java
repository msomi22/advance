/**
 * 
 */
package com.appletech.server.servlet.admin;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.server.servlet.util.PropertiesConfig;
import com.appletech.server.session.AdminSessionConstants;

/** 
 * @author peter
 *
 */
public class AdminLogin  extends HttpServlet{
	
    final String ERROR_WRONG_USER_DETAIL = "Incorrect username or password.";
	
	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);//current session
		
		if (session != null) {
			session.invalidate();  
		}
		session = request.getSession(true);
		
		String username = StringUtils.trimToEmpty(request.getParameter("username"));
		String password = StringUtils.trimToEmpty(request.getParameter("password"));
		
         if (!StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			session.setAttribute(AdminSessionConstants.ADMIN_LOGIN_ERROR, ERROR_WRONG_USER_DETAIL); 
            response.sendRedirect("index.jsp");
         
         }
         else if (!StringUtils.equals(password, PropertiesConfig.getConfigValue("ADMIN_PASSWORD"))) {
        	  session.setAttribute(AdminSessionConstants.ADMIN_LOGIN_ERROR, ERROR_WRONG_USER_DETAIL); 
              response.sendRedirect("index.jsp");

         }else{
              session.setAttribute(AdminSessionConstants.ADMIN_SIGN_IN_KEY, username);
              session.setAttribute(AdminSessionConstants.ADMIN_SIGN_IN_TIME, new Date());
             
              response.sendRedirect("adminIndex.jsp");
			 
			}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 4084498609555048211L;

}

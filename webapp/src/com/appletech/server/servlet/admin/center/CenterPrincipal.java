/**
 * 
 */
package com.appletech.server.servlet.admin.center;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import com.appletech.bean.center.staff.Staff;
import com.appletech.persistence.staff.StaffDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class CenterPrincipal extends HttpServlet{

	private static StaffDAO staffDAO;
	private EmailValidator emailValidator;
	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		staffDAO = StaffDAO.getInstance();
		emailValidator = EmailValidator.getInstance();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OutputStream out = response.getOutputStream();
		response.setContentType("application/json;charset=UTF-8");

		String name = StringUtils.trimToEmpty(request.getParameter("name"));
		String mobile = StringUtils.trimToEmpty(request.getParameter("mobile"));
		String email = StringUtils.trimToEmpty(request.getParameter("email"));
		String idNumber = StringUtils.trimToEmpty(request.getParameter("idNumber"));
		String nationality = StringUtils.trimToEmpty(request.getParameter("nationality"));
		String gender = StringUtils.trimToEmpty(request.getParameter("gender"));
		String password = StringUtils.trimToEmpty(request.getParameter("password"));
		String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
/*
		System.out.println("name : " + name );
		System.out.println("mobile : " + mobile );
		System.out.println("email : " + email );
		System.out.println("idNumber : " + idNumber );
		System.out.println("nationality : " + nationality );
		System.out.println("gender : " + gender );
		System.out.println("password : " + password );
		System.out.println("centerId : " + centerId );
		System.out.println("******************************************************");

*/
		Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();
		
		out.write(gson.toJson(updatePrincipal(name,mobile,email,idNumber,nationality,gender,password,centerId)).getBytes());
		out.flush();
		out.close();
			

	}




	/**
	 * @param name
	 * @param mobile
	 * @param email
	 * @param idNumber
	 * @param nationality
	 * @param gender
	 * @param password
	 * @param centerId
	 * @return
	 */
	private JsonElement updatePrincipal(String name, String mobile, String email, String idNumber, String nationality,
			String gender, String password, String centerId) {

		JsonObject jsonObject = new JsonObject();
		String accessLevelId = "AF066A82-B397-4213-AA1A-D9BF03FE0152";

		if(StringUtils.isBlank(name)){
			jsonObject.addProperty("messageResponse", "Principal's Name can't be blank.");

		}else if(!Wordlength(name)){
			jsonObject.addProperty("messageResponse", "Principal's Name is not valid.");

		}else if(!lengthValid(mobile)){
			jsonObject.addProperty("messageResponse", "Principal's Mobile is not valid.");

		}else if(StringUtils.isBlank(mobile)){
			jsonObject.addProperty("messageResponse", "Principal's Mobile can't be blank.");

		}else if(!isNumeric(mobile)){ 
			jsonObject.addProperty("messageResponse", "Principal's Mobile is not valid.");

		}else if(StringUtils.isBlank(email)){
			jsonObject.addProperty("messageResponse", "Principal's Email can't be blank.");

		}else if (!emailValidator.isValid(email)) {
			jsonObject.addProperty("messageResponse", "Principal's Email is not valid.");

		}else if(StringUtils.isBlank(idNumber)){
			jsonObject.addProperty("messageResponse", "Principal's Id Number can't be blank.");

		}/*else if(StringUtils.isBlank(password)){
			jsonObject.addProperty("messageResponse", "Principal's Password can't be blank.");

		}*/else{
			
			
			   Staff staff;
			   boolean exist = true;
			  if(staffDAO.getStaff(centerId, accessLevelId) == null){
				   staff = new Staff();
				   staff.setPassword(password); 
				   staff.setCenterId(centerId);
				   staff.setIsActive("1");  
				   staff.setAccessLevelId(accessLevelId);
				   
				   staff.setName(name);
				   staff.setGender(gender);
				   staff.setMobile(removeLeadingZeroes(mobile));
				   staff.setEmail(email);
				   staff.setIdNumber(idNumber);
				   staff.setNationality(nationality);
				   
				   exist = false;
			  }else{
				  staff = staffDAO.getStaff(centerId, accessLevelId);
				  
				   staff.setName(name);
				   staff.setGender(gender);
				   staff.setMobile(removeLeadingZeroes(mobile));
				   staff.setEmail(email);
				   staff.setIdNumber(idNumber);
				   staff.setNationality(nationality);
			  }
			
			  
			  
			   
			   if(exist){
				   
				   if(staffDAO.updateStaff(staff)){
						jsonObject.addProperty("messageResponse", "Principal updated successfully.");
						
					}else{
						jsonObject.addProperty("messageResponse", "An Error occured while adding Principal.");
						
					}
				   
			   }else{
				  
				   if(staffDAO.putStaff(staff)){
						jsonObject.addProperty("messageResponse", "Principal added successfully.");
						
					}else{
						jsonObject.addProperty("messageResponse", "An Error occured while adding Principal.");
						
					}
			   }
			   
		}

		return jsonObject;
	}

	/**
	 * @param value
	 * @return
	 */
	private static String removeLeadingZeroes(String value) {
		return new Integer(value).toString();
	}


	/**
	 * @param mystring
	 * @return
	 */
	private static boolean Wordlength(String mystring) {
		boolean isvalid = true;
		int length = 0;
		length = mystring.length();
		if(length<3){
			isvalid = false;
		}
		return isvalid;
	}



	/**
	 * @param mystring
	 * @return
	 */
	private static boolean lengthValid(String mystring) {
		boolean isvalid = true;
		int length = 0;
		length = mystring.length();
		if(length<9 ||length>10){
			isvalid = false;
		}
		return isvalid;
	}

	/**
	 * @param str
	 * @return
	 */
	private static boolean isNumeric(String str) {  
		try  
		{  
			double d = Double.parseDouble(str);  

		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}



	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 4702759518590729390L;
}

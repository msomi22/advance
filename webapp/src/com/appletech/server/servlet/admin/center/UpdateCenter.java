/**
 * 
 */
package com.appletech.server.servlet.admin.center;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.Center;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.server.session.AdminSessionConstants;

/** 
 * @author peter
 *
 */
public class UpdateCenter extends HttpServlet{
	
	private static CenterDAO centerDAO;

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       centerDAO = CenterDAO.getInstance();
      
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
       
       String centerNo = StringUtils.trimToEmpty(request.getParameter("centerNo"));
       String centerRegion = StringUtils.trimToEmpty(request.getParameter("centerRegion"));
       String centerName = StringUtils.trimToEmpty(request.getParameter("centerName"));
       String email = StringUtils.trimToEmpty(request.getParameter("email"));
       String website = StringUtils.trimToEmpty(request.getParameter("website"));
       String centerCode = StringUtils.trimToEmpty(request.getParameter("centerCode"));
       String postalCode = StringUtils.trimToEmpty(request.getParameter("postalCode"));
       String postalAddress = StringUtils.trimToEmpty(request.getParameter("postalAddress"));
       String town = StringUtils.trimToEmpty(request.getParameter("town"));
       String isActive = StringUtils.trimToEmpty(request.getParameter("isActive"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
       
       Map<String, String> centerMap = new HashMap<>(); 
	   centerMap.put("centerId", centerId); 
      /* 
       System.out.println("centerNo : " + centerNo );
       System.out.println("centerRegion : " + centerRegion );
       System.out.println("centerName : " + centerName );
       System.out.println("email : " + email );
       System.out.println("website : " + website );
       System.out.println("centerCode : " + centerCode );
       System.out.println("postalCode : " + postalCode );
       System.out.println("postalAddress : " + postalAddress );
       System.out.println("town : " + town );
       System.out.println("isActive : " + isActive );
       System.out.println("centerId : " + centerId );
       System.out.println("******************************************************");
       */
       if(StringUtils.isBlank(centerNo)){
    	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "CenterNo can't be blank."); 
		      
	   }else if(StringUtils.isBlank(centerRegion)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Region can't be blank."); 
		   
	   }else if(StringUtils.isBlank(centerName)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Name can't be blank."); 
		   
	   }else if(StringUtils.isBlank(email)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Email can't be blank."); 
		   
	   }else if(StringUtils.isBlank(website)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Website can't be blank."); 
		   
	   }else if(StringUtils.isBlank(centerCode)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Code can't be blank."); 
		   
	   }else if(StringUtils.isBlank(postalCode)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Postal Code can't be blank."); 
		   
	   }else if(StringUtils.isBlank(postalAddress)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Postal Address can't be blank."); 
		   
	   }else if(StringUtils.isBlank(town)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Center Town can't be blank."); 
		   
	   }else{
		   
		   Center center = centerDAO.getCenterById(centerId);
		   center.setCenterNo(centerNo);
		   center.setCenterRegion(centerRegion);
		   center.setCenterName(centerName);
		   center.setEmail(email);
		   center.setWebsite(website);
		   center.setCenterCode(centerCode);
		   center.setPostalCode(postalCode);
		   center.setPostalAddress(postalAddress);
		   center.setTown(town);
		   center.setLogoUri("some path"); 
		   center.setSignatureUri("some path");
		   center.setIsActive(isActive); 
		   
		   if(centerDAO.updateCenter(center)){
			   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_SUCCESS, "The Center was updated successfully."); 
			   
		   }else{
			   session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, "Something went wrong while updating the Center."); 
			   
		   }
		   
		   
	   }
       
       session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_PARAM, centerMap); 
       response.sendRedirect("schoolProfile.jsp");   
	   return;  
   }
   
   
   

 /**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

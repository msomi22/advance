/**
 * 
 */
package com.appletech.server.servlet.admin.center;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.Contact;
import com.appletech.persistence.center.ContactDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class CenterContact extends HttpServlet{
	
	private static ContactDAO contactDAO;

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       contactDAO = ContactDAO.getInstance();
      
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
	   
	   String mobile = StringUtils.trimToEmpty(request.getParameter("mobile"));
	   String description = StringUtils.trimToEmpty(request.getParameter("description"));
	   String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
	   String decision = StringUtils.trimToEmpty(request.getParameter("decision"));
	   
	  /* System.out.println("mobile : " + mobile );
	   System.out.println("description : " + description );
       System.out.println("centerId : " + centerId );
       System.out.println("decision : " + decision );
       System.out.println("******************************************************");
	   */
	   Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();
    

		if(StringUtils.equalsIgnoreCase(decision, "addContact")){

			out.write(gson.toJson(addContact(mobile,description,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "updateContact")){

			out.write(gson.toJson(updateContact(mobile,description,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "removeContact")){

			out.write(gson.toJson(removeContact(mobile,centerId)).getBytes());
			out.flush();
			out.close();

		}
       
       
       
   }
   
   
   

 /**
 * @param mobile
 * @param description
 * @param centerId
 * @return
 */
private JsonElement removeContact(String mobile, String centerId) {
	 JsonObject jsonObject = new JsonObject();
	 
	 if(contactDAO.deleteContact(centerId, removeLeadingZeroes(mobile))){
		 jsonObject.addProperty("messageResponse", "The contact has been removed successfully.");
		 
	 }else{
		 jsonObject.addProperty("messageResponse", "Something went wrong while removing the contact.");
		 
	 }
	 
	return jsonObject;
}

/**
 * @param mobile
 * @param description
 * @param centerId
 * @return
 */
private JsonElement updateContact(String mobile, String description, String centerId) {
	 JsonObject jsonObject = new JsonObject();

	 if(!isNumeric(mobile)){ 
	    	jsonObject.addProperty("messageResponse", "The mobile \"" + mobile + "\" is no valid.");
	    	
	  }else if(!lengthValid(mobile)){
		  jsonObject.addProperty("messageResponse", "The mobile \"" + mobile + "\" is no valid.");
		  
	  }else{
		  
		     Contact contact;
			 if(contactDAO.getContactByMbile(centerId, mobile) == null){
				 contact = new Contact();
				 contact.setCenterId(centerId);
				 contact.setMobile(removeLeadingZeroes(mobile));
				 contact.setDescription(description);
			 }else{
				 contact = contactDAO.getContactByMbile(centerId, mobile);
				 contact.setDescription(description);
			 }
		 
		 if(contactDAO.putContact(contact, centerId, removeLeadingZeroes(mobile))){
			 jsonObject.addProperty("messageResponse", "The contact has been updated successfully.");
			 
		 }else{
			 jsonObject.addProperty("messageResponse", "Something went wrong while updating the contact.");
			 
		 }
		 
	  }
	return jsonObject;
}

/**
 * @param mobile
 * @param description
 * @param centerId
 * @return
 */
private JsonElement addContact(String mobile, String description, String centerId) {
	 JsonObject jsonObject = new JsonObject();
	 if(!isNumeric(mobile)){ 
	    	jsonObject.addProperty("messageResponse", "The mobile \"" + mobile + "\" is no valid.");
	    	
	  }else if(!lengthValid(mobile)){
		  jsonObject.addProperty("messageResponse", "The mobile \"" + mobile + "\" is no valid.");
		  
	  }else{
		 Contact contact = new Contact();
		 contact.setCenterId(centerId);
		 contact.setDescription(description);
		 contact.setMobile(removeLeadingZeroes(mobile));
		 
		 if(contactDAO.putContact(contact, centerId, removeLeadingZeroes(mobile))){
			 jsonObject.addProperty("messageResponse", "The contact has been added successfully.");
			 
		 }else{
			 jsonObject.addProperty("messageResponse", "Something went wrong while adding the contact.");
			 
		 }
		 
	  }
	 
	return jsonObject;
}




/**
 * @param value
 * @return
 */
private static String removeLeadingZeroes(String value) {
     return new Integer(value).toString();
}



/**
 * @param mystring
 * @return
 */
private static boolean lengthValid(String mystring) {
	boolean isvalid = true;
	int length = 0;
	length = mystring.length();
	if(length<9 ||length>10){
		isvalid = false;
	}
	return isvalid;
}

/**
 * @param str
 * @return
 */
private static boolean isNumeric(String str) {  
  try  
  {  
    double d = Double.parseDouble(str);  
    
  }  
  catch(NumberFormatException nfe)  
  {  
    return false;  
  }  
  return true;  
}






/**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

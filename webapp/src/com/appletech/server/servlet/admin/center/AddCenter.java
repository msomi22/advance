/**
 * 
 */
package com.appletech.server.servlet.admin.center;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.AccessLevel;
import com.appletech.bean.center.Center;
import com.appletech.bean.center.Config;
import com.appletech.bean.center.Cycle;
import com.appletech.bean.center.Division;
import com.appletech.bean.center.Grade;
import com.appletech.bean.center.stm.Classroom;
//import com.appletech.bean.center.stm.Combination;
//import com.appletech.bean.center.stm.CombinationDesc;
import com.appletech.bean.center.stm.Subject;
import com.appletech.persistence.center.AccessLevelDAO;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.center.CycleDAO;
import com.appletech.persistence.center.DivisionDAO;
import com.appletech.persistence.center.GradeDAO;
import com.appletech.persistence.stm.ClassroomDAO;
//import com.appletech.persistence.stm.CombinationDAO;
//import com.appletech.persistence.stm.CombinationDescDAO;
import com.appletech.persistence.stm.SubjectDAO;
import com.appletech.server.session.AdminSessionConstants;

/** 
 * @author peter
 *
 */
public class AddCenter extends HttpServlet{
	
	private static CenterDAO centerDAO;
	private static ConfigDAO configDAO;
	private static CycleDAO cycleDAO;
	private static GradeDAO gradeDAO;
	private static DivisionDAO divisionDAO;
	
	private static ClassroomDAO classroomDAO;
	//private static CombinationDAO combinationDAO;
	//private static CombinationDescDAO combinationDescDAO;
	private static SubjectDAO subjectDAO;
	private static AccessLevelDAO accessLevelDAO;

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       centerDAO = CenterDAO.getInstance();
       configDAO = ConfigDAO.getInstance();
       cycleDAO = CycleDAO.getInstance();
       gradeDAO = GradeDAO.getInstance();
       divisionDAO = DivisionDAO.getInstance();
       classroomDAO = ClassroomDAO.getInstance();
       subjectDAO = SubjectDAO.getInstance();
       accessLevelDAO = AccessLevelDAO.getInstance();
       
       //combinationDAO = CombinationDAO.getInstance();
       //combinationDescDAO = CombinationDescDAO.getInstance();
       
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
       
       String centerNo = StringUtils.trimToEmpty(request.getParameter("centerNo"));
       String centerRegion = StringUtils.trimToEmpty(request.getParameter("centerRegion"));
       String centerName = StringUtils.trimToEmpty(request.getParameter("centerName"));
       String email = StringUtils.trimToEmpty(request.getParameter("email"));
       String website = StringUtils.trimToEmpty(request.getParameter("website"));
       String centerCode = StringUtils.trimToEmpty(request.getParameter("centerCode"));
       String postalCode = StringUtils.trimToEmpty(request.getParameter("postalCode"));
       String postalAddress = StringUtils.trimToEmpty(request.getParameter("postalAddress"));
       String town = StringUtils.trimToEmpty(request.getParameter("town"));
       
      /* System.out.println("centerNo : " + centerNo );
       System.out.println("centerRegion : " + centerRegion );
       System.out.println("centerName : " + centerName );
       System.out.println("email : " + email );
       System.out.println("website : " + website );
       System.out.println("centerCode : " + centerCode );
       System.out.println("postalCode : " + postalCode );
       System.out.println("postalAddress : " + postalAddress );
       System.out.println("town : " + town );
       System.out.println("******************************************************");
      */
       
       Map<String, String> centerMap = new HashMap<>(); 
	   centerMap.put("centerNo", centerNo); 
	   centerMap.put("centerRegion", centerRegion); 
	   centerMap.put("centerName", centerName); 
	   centerMap.put("email", email); 
	   centerMap.put("website", website); 
	   centerMap.put("centerCode", centerCode); 
	   centerMap.put("postalCode", postalCode); 
	   centerMap.put("postalAddress", postalAddress); 
	   centerMap.put("town", town); 
       
       if(StringUtils.isBlank(centerNo)){
    	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "CenterNo can't be blank."); 
		      
	   }else if(centerDAO.getCenterBycenterNo(centerNo) != null){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "The CenterNo is not available."); 
		   
	   }else if(StringUtils.isBlank(centerCode)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Code can't be blank."); 
		   
	   }else if(centerDAO.getCenterBycenterCode(centerCode) != null){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "The Center Code is not available."); 
		   
	   }else if(StringUtils.isBlank(centerName)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Name can't be blank."); 
		   
	   }else if(centerDAO.getCenterBycenterName(centerName) != null){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "The Center Name is not available."); 
		   
	   }else if(StringUtils.isBlank(centerRegion)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Region can't be blank."); 
		   
	   }else if(StringUtils.isBlank(email)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Email can't be blank."); 
		   
	   }else if(centerDAO.getCenterByemail(email) != null){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "The Center Email is not available."); 
		   
	   }else if(StringUtils.isBlank(website)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Website can't be blank."); 
		   
	   }else if(centerDAO.getCenterBywebsite(website) != null){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "The Center Web site is not available."); 
		   
	   }else if(StringUtils.isBlank(postalCode)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Postal Code can't be blank."); 
		   
	   }else if(StringUtils.isBlank(postalAddress)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Postal Address can't be blank."); 
		   
	   }else if(StringUtils.isBlank(town)){
	 	   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Center Town can't be blank."); 
		   
	   }else{
		   
		   Center center = new Center();
		   center.setCenterNo(centerNo);
		   center.setCenterRegion(centerRegion);
		   center.setCenterName(centerName);
		   center.setEmail(email);
		   center.setWebsite(website);
		   center.setCenterCode(centerCode);
		   center.setPostalCode(postalCode);
		   center.setPostalAddress(postalAddress);
		   center.setTown(town);
		   center.setLogoUri("some path"); 
		   center.setSignatureUri("some path");
		   
		   
		   
		   
		   if(centerDAO.putCenter(center)){
			   
			 //AccessLevel
			 String[] acesslevelIds = {"AF066A82-B397-4213-AA1A-D9BF03FE0152",
					   "CD3533AC-14EC-48E4-B841-725D83976D10",
					   "2E1ABC0E-1532-4AC1-87E1-8BDBBE8EB501",
					   "8491F8F8-C09F-4EAA-AF90-E6AA389CB123"};
			
			String[] acessDescription = {"Principal","Deputy Principal","Academic Master","Teacher"};
			
			for(int i=0;i<acesslevelIds.length;i++){
			AccessLevel accessLevel = new AccessLevel();
			accessLevel.setUuid(acesslevelIds[i]); 
			accessLevel.setCenterId(center.getUuid()); 
			accessLevel.setDescription(acessDescription[i]); 
			accessLevelDAO.putAccessLevel(accessLevel);
					   
			}

			   
			 
			 //Cycle
			   String lastcycleId = "";
			   String[] cycles = {"One","Two","Three"};
			   for(int i=0;i<cycles.length;i++){
				   Cycle cycle = new Cycle();
				   cycle.setCenterId(center.getUuid());
				   cycle.setDescription(cycles[i]); 
				   cycleDAO.putCycle(cycle);
				   lastcycleId = cycle.getUuid();
			   }
			   
			   //Config
			   Config config = new Config();
			   config.setCenterId(center.getUuid()); 
			   config.setCycleId(lastcycleId);    
			   config.setTerm("1");
			   config.setYear("2017");
			   config.setClosingdate("05/05/17");
			   config.setOppeningdate("05/05/17");
			   config.setHeadTeacherComment("HeadTeacher's Comment");
			   configDAO.putConfig(config, center.getUuid());
			   
			   
			   //Division
			   String[] lMarks = {"1","10","13","18","21"};
			   String[] uMarks = {"9","12","17","20","21"};
			   String[] descriptions = {"1","2","3","4","5"}; 
			   for(int i=0;i<descriptions.length;i++){
				   Division division = new Division();
				   division.setCenterId(center.getUuid()); 
				   division.setLowerMark(lMarks[i]);
				   division.setUpperMark(uMarks[i]); 
				   division.setDescription(descriptions[i]);
				   divisionDAO.putDivision(division, center.getUuid(), descriptions[i]);
			   }
			  
			   
			   //Grade
			   String[] glMarks= {"80","70","60","50","40","35","0"};
			   String[] guMarks= {"100","79","69","59","49","39","34"};
			   String[] points= {"1","2","3","4","5","6","7"};
			   String[] gdescs= {"A","B","C","D","E","S","F"};
			   for(int i=0;i<gdescs.length;i++){
				   Grade grade = new Grade();
				   grade.setCenterId(center.getUuid()); 
				   grade.setLowerMark(glMarks[i]);
				   grade.setUpperMark(guMarks[i]);
				   grade.setDescription(gdescs[i]);
				   grade.setPoint(points[i]);
				   gradeDAO.putGrade(grade, center.getUuid(), points[i]); 
			   }
			
			  //Classroom
			   String[] classes = {"FORM 5","FORM 6"};
			   for(int i=0;i<classes.length;i++){
				   Classroom classroom = new Classroom();
				   classroom.setCenterId(center.getUuid());
				   classroom.setDescription(classes[i]); 
				   classroomDAO.putClassroom(classroom);
			   }
			  
			   //Combination
			   
			
			   
			   
			  
			   
			  /*

			   String[] combinationIds = {"1641480B-25A9-4170-999D-84F2D2B663F2","2963792C-AC68-4048-92CA-5B1869FA565B",
										   "299E6F62-8684-4EC4-A3E6-56491ABEC7C6","AB2A276A-8CB3-4378-A47A-BAAA4921E707",
										   "2F232B11-191E-4800-B86C-5C3F06A91FE1","23AC459A-8BF8-4059-A92D-F0E71293E1CF",
										   "7D15768E-1BB7-4139-B50A-CF8385F76052","F214B63E-7FA0-472A-B8CA-0AA916B9E311"};
			   String[] combinations = {"CBM","ECA","HGE","HGL","PGM","EGM","HKL","PCM"};
			   for(int i=0;i<combinations.length;i++){
				   Combination combination = new Combination();
				   combination.setUuid(combinationIds[i]); 
				   combination.setCenterId(center.getUuid()); 
				   combination.setDescription(combinations[i]);
				   combinationDAO.putCombination(combination);
			   }
			   */
			   
			   //Subject
			   String[] subjectsIds = {"8717A263-EF1F-4042-81F5-714003F37BB8","54015F57-F935-4157-8DE7-0D2C1ECAAE31","DEE651EC-CD80-4DC6-81B8-6DEBF58F335A","0947F521-03CC-4815-8536-3E66837B0C99","592B480E-BBD7-4010-B835-2213D65CD845","B5B8319F-19EC-4EE6-AE15-3D15F4FB9D63","8E731019-1479-46EC-ACDF-67C0D7BB9031","12E96C96-4C28-4253-86B1-A7A0D284613F","9B5EE6F2-782F-4A86-A680-6BCE10C00FE5","5999E599-F469-41AE-BF55-6C1AC812D8B9","978D89E5-9419-40D6-8669-4AD0E1DA55BD","6D1DEC06-39F6-4A7B-AE15-6FBE434DFCF2","DE86BC64-D3ED-4B9D-B8FB-789C1CB913CC"};
			   String[] subjectsCodes = {"G.S","HIST","GEO","KISW","ENG","PHY","CHEM","BIO","ECO","P/MATH","COMM","ACC","BAM"};
			   String[] subjectsMCodes = {"102","103","104","105","106","107","108","109","110","111","112","113","114"};
			   String[] subjectsDescr = {"Unkwon","History","Geography","Kiswahili","English","Physics","Chemistry","Biology","Unkwon","Pure Mathematics","Computer Studies","Unkwon","Unkwon"};
			   for(int i=0;i<subjectsMCodes.length;i++){
				   Subject subject = new Subject();
				   subject.setUuid(subjectsIds[i]); 
				   subject.setCenterId(center.getUuid());
				   subject.setSubjectCode(subjectsCodes[i]);
				   subject.setSubjectNumCode(subjectsMCodes[i]);
				   subject.setSubjectDescr(subjectsDescr[i]);
				   subjectDAO.putSubject(subject);
			   }
			   
			   /*
			   //CombinationDesc
			   for(int i=0;i<subjectsIds.length;i++){
				   if(i<=2){
					   CombinationDesc combinationDesc = new CombinationDesc();
					   combinationDesc.setCenterId(center.getUuid());
					   combinationDesc.setCombinationId(combinationIds[0]);
					   combinationDesc.setSubjectId(subjectsIds[i]); 
					   combinationDescDAO.putCombinationDesc(combinationDesc);
				   }
				  
				   if(i>2 && i<=5){
					   CombinationDesc combinationDesc = new CombinationDesc();
					   combinationDesc.setCenterId(center.getUuid());
					   combinationDesc.setCombinationId(combinationIds[1]);
					   combinationDesc.setSubjectId(subjectsIds[i]); 
					   combinationDescDAO.putCombinationDesc(combinationDesc);
				   }
				   
				   if(i>5 && i<=8){
					   CombinationDesc combinationDesc = new CombinationDesc();
					   combinationDesc.setCenterId(center.getUuid());
					   combinationDesc.setCombinationId(combinationIds[2]);
					   combinationDesc.setSubjectId(subjectsIds[i]); 
					   combinationDescDAO.putCombinationDesc(combinationDesc);
				   }
				   
				   if(i>8 && i<=11){
					   CombinationDesc combinationDesc = new CombinationDesc();
					   combinationDesc.setCenterId(center.getUuid());
					   combinationDesc.setCombinationId(combinationIds[3]); 
					   combinationDesc.setSubjectId(subjectsIds[i]); 
					   combinationDescDAO.putCombinationDesc(combinationDesc);
				   }
				  
			   }//end 
			   
			   
			   */
			   
			   centerMap.clear();
			   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_SUCCESS, "The Center was added successfully."); 
			   
		   }else{
			   session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, "Something went wrong while adding the Center."); 
			   
		   }
	
		   
	   }
       
       session.setAttribute(AdminSessionConstants.SCHOOL_PARAM, centerMap); 
       response.sendRedirect("newSchool.jsp");   
	   return; 
   }
   
   
   

 /**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

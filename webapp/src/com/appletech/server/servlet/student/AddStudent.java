/**
 * 
 */
package com.appletech.server.servlet.student;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.stm.StudentSubject;
import com.appletech.bean.center.student.Student;
import com.appletech.persistence.stm.StudentSubjectDAO;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class AddStudent extends HttpServlet{
	
	
	private  String[] subjects = {};
	public static StudentDAO studentDAO;
	public static StudentSubjectDAO studentSubjectDAO;


	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       studentDAO = StudentDAO.getInstance();
       studentSubjectDAO = StudentSubjectDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
	   
	   HttpSession session = request.getSession(true);
	   
	   String indexNo = StringUtils.trimToEmpty(request.getParameter("IndexNo"));
       String firstname = StringUtils.trimToEmpty(request.getParameter("Firstname"));
       String middlename = StringUtils.trimToEmpty(request.getParameter("Middlename"));
       String lastname = StringUtils.trimToEmpty(request.getParameter("Lastname"));
       String gender = StringUtils.trimToEmpty(request.getParameter("Gender"));
       String classroom = StringUtils.trimToEmpty(request.getParameter("Classroom"));
       String combination = StringUtils.trimToEmpty(request.getParameter("Combination"));
       subjects = request.getParameterValues("subjects[]");  
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
      
	  
	   Map<String, String> studentHash = new HashMap<>(); 
	   studentHash.put("indexNo", indexNo); 
	   studentHash.put("firstname", firstname); 
	   studentHash.put("middlename", middlename); 
	   studentHash.put("lastname", lastname); 
	   studentHash.put("gender", gender); 
	   studentHash.put("classroom", classroom); 
	   studentHash.put("combination", combination); 
	   studentHash.put("centerId", centerId); 

	   if(subjects != null){
		   studentHash.put("subjects", subjects.toString()); 
	   }
	   
	   
      
       if(StringUtils.isBlank(indexNo)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Please provide an index number."); 

		}else if(StringUtils.isBlank(firstname)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Firstname can't be empty."); 

		}else if(StringUtils.isBlank(middlename)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Middlename can't be empty."); 

		}else if(StringUtils.isBlank(lastname)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Lastname can't be empty."); 

		}else if(StringUtils.isBlank(gender)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Please select a gender."); 

		}else if(StringUtils.isBlank(classroom)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Please select a classroom."); 

		}else if(StringUtils.isBlank(combination)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Please select a combination."); 

		}else if(subjects == null){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Please select a subject."); 

		}else if(StringUtils.isBlank(centerId)){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Something went wrong, try again later."); 

		}else if(studentDAO.getStudentByindexNo(centerId, indexNo) != null){
			session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "The index number already exist."); 

		}else{
			Student student = new Student();
			student.setCenterId(centerId);
			student.setAdmmitedStreamId(classroom);
			student.setCurrentStreamId(classroom);
			student.setCombinationId(combination);
			student.setIndexNo(indexNo);
			student.setFirstname(firstname);
			student.setMiddlename(middlename);
			student.setLastname(lastname);
			student.setGender(gender);
			student.setPhotoUri("path");
			
			 //logger.info(""); 
			

			if(studentDAO.putStudent(student)){

				for(String subjectId : subjects){
					StudentSubject studentSubject = new StudentSubject();
					studentSubject.setCenterId(centerId);
					studentSubject.setStudentId(student.getUuid());
					studentSubject.setSubjectId(subjectId);
					studentSubjectDAO.putStudentSubject(studentSubject);
				}
				
				session.setAttribute(SessionConstants.STUDENT_ADD_SUCCESS, "Student was added successfully."); 
				studentHash.clear();
				
			}else{
				session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, "Something went wrong, try again later."); 
				
			}
		
		}
       session.setAttribute(SessionConstants.STUDENT_PARAM, studentHash); 
	   response.sendRedirect("newstudent.jsp");  
	   return;
   
   }
   
   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
       doPost(request, response);
   }
   
   /**
	 * 
	 */
	private static final long serialVersionUID = 2530323567857571160L;


}

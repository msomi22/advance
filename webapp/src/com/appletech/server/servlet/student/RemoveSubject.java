/**
 * 
 */
package com.appletech.server.servlet.student;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.appletech.persistence.stm.StudentSubjectDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class RemoveSubject  extends HttpServlet{
	
	private final String ERROR_MESSAGE = "Something went wrong while removing the subjects."; 
	private final String SUCCESS_SCORE_SUBMITTED = "The subject was removed successfully."; 
	
	public static StudentSubjectDAO studentSubjectDAO;

	
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       studentSubjectDAO = StudentSubjectDAO.getInstance();
       
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String subid = StringUtils.trimToEmpty(request.getParameter("subid"));
       String stuid = StringUtils.trimToEmpty(request.getParameter("stuid"));
       String centerid = StringUtils.trimToEmpty(request.getParameter("centerid"));
       
       
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(removeSubject(subid,stuid,centerid)).getBytes()); 
		

		out.flush();
		out.close();
       
       
       
   }
   
   
   

/**
 * @param subid
 * @param stuid
 * @param centerid
 * @return
 */
private JsonElement removeSubject(String subid, String stuid, String centerid) {
	JsonObject jsonObject = new JsonObject(); 
	

	if(StringUtils.isEmpty(subid)){

		jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

	}else if(StringUtils.isEmpty(stuid)){

		jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

	}else if(StringUtils.isEmpty(centerid)){

		jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

	}else{
	
		if(studentSubjectDAO.deleteStudentSubject(centerid, stuid, subid)){
			
			 //logger.info(""); 
			
			jsonObject.addProperty("responseMessage", SUCCESS_SCORE_SUBMITTED);  
		}else{
			
			jsonObject.addProperty("responseMessage", ERROR_MESSAGE);  
		}
		 
		
		
	}
	
	return jsonObject;
}

@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
      doPost(request, response);
  }

/**
 * 
 */
private static final long serialVersionUID = -9098298827508194017L;

}

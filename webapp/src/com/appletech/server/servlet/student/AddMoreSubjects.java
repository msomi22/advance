/**
 * 
 */
package com.appletech.server.servlet.student;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.stm.StudentSubject;
import com.appletech.persistence.stm.StudentSubjectDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class AddMoreSubjects  extends HttpServlet{
	
	private final String ERROR_MESSAGE = "Something went wrong while adding the subjects."; 
	private final String ERROR_EMPTY_SUB = "Please select a subject.";
	private final String ERROR_SUB_EXIST = "This subject is already assigned.";
	private final String SUCCESS_SCORE_SUBMITTED = "The subject(s) was added successfully.";
	
	public static StudentSubjectDAO studentSubjectDAO;
    

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       studentSubjectDAO = StudentSubjectDAO.getInstance();
      
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String subids = StringUtils.trimToEmpty(request.getParameter("subids"));
       String stuid = StringUtils.trimToEmpty(request.getParameter("stuid"));
       String centerid = StringUtils.trimToEmpty(request.getParameter("centerid"));
       
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(addSubject(subids,stuid,centerid)).getBytes()); 
		
		out.flush();
		out.close();
       
       
   }
   
   
   

/**
 * @param subids
 * @param stuid
 * @param centerid
 * @return
 */
private JsonElement addSubject(String subids, String stuid, String centerid) {
	JsonObject jsonObject = new JsonObject(); 
	
	subids = removeChar(subids);
	String [] subjects = subids.split(",");

	if(StringUtils.isEmpty(subids)){

		jsonObject.addProperty("responseMessage", ERROR_EMPTY_SUB); 

	}else if(StringUtils.isEmpty(stuid)){

		jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

	}else if(StringUtils.isEmpty(centerid)){

		jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

	}else{
		for(String subjectId : subjects){
			 //System.out.println("subjectId - "+ subjectId);   
			 
			 if(studentSubjectDAO.getStudentSubject(centerid, stuid, subjectId) != null){
				 jsonObject.addProperty("responseMessage", ERROR_SUB_EXIST);
			 }else{
				 StudentSubject studentSubject = new StudentSubject();
				 studentSubject.setCenterId(centerid);
				 studentSubject.setStudentId(stuid);
				 studentSubject.setSubjectId(subjectId);
				 studentSubjectDAO.putStudentSubject(studentSubject);
				 
				 //logger.info(""); 
				 
				 jsonObject.addProperty("responseMessage", SUCCESS_SCORE_SUBMITTED);  
			 }
		}
		
	}
	return jsonObject;
}

/**
 * @param subids
 * @return
 */
private String removeChar(String subids) {
	subids = subids.replace("[", "");
	subids = subids.replace("]", "");
	subids = subids.replace("\"", "");
	return subids;
}

@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
      doPost(request, response);
  }

/**
 * 
 */
private static final long serialVersionUID = 9130701189193337485L;



}

/**
 * 
 */
package com.appletech.server.servlet.student;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.student.Student;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.session.SessionConstants;


/** 
 * @author peter
 *
 */
public class UpdateStudent  extends HttpServlet{
	
	
	private static StudentDAO studentDAO;
	
	private final String ERROR_BLANK_INDEXNO = "Index Number can't be blank.";
	private final String ERROR_BLANK_FN = "Firstname can't be blank.";
	private final String ERROR_BLANK_MN = "Middlename can't be blank.";
	private final String ERROR_BLANK_LN = "Lastname can't be blank.";
	
	private final String ERROR_BLANK_CMB = "Please select a combination.";
	private final String ERROR_BLANK_GDR = "Please select a gender.";
	private final String ERROR_BLANK_CLS = "Please select a class.";
	
	
	private final String ERROR = "An error occured, try again later.";
	private final String SUCCESS = "The student was updated successfully.";
	

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       studentDAO = StudentDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
       
       String indexNo = StringUtils.trimToEmpty(request.getParameter("IndexNo"));
       String firstname = StringUtils.trimToEmpty(request.getParameter("Firstname"));
       String middlename = StringUtils.trimToEmpty(request.getParameter("Middlename"));
       String lastname = StringUtils.trimToEmpty(request.getParameter("Lastname"));
       String combination = StringUtils.trimToEmpty(request.getParameter("Combination"));
       String streamId = StringUtils.trimToEmpty(request.getParameter("streamId"));
       String gender = StringUtils.trimToEmpty(request.getParameter("gender"));
       String isActive = StringUtils.trimToEmpty(request.getParameter("IsActive"));
       String isAlumni = StringUtils.trimToEmpty(request.getParameter("IsAlumni"));
       String studentId = StringUtils.trimToEmpty(request.getParameter("studentId"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
     
       
       Map<String, String> studentIdHash = new HashMap<>(); 
       studentIdHash.put("studentId", studentId); 
       
       if(StringUtils.isBlank(indexNo)){
    	   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_INDEXNO); 
		      
	   }else if(StringUtils.isBlank(firstname)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_FN); 
		   
	   }else if(StringUtils.isBlank(middlename)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_MN); 
		   
	   }else if(StringUtils.isBlank(lastname)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_LN); 
		   
	   }else if(StringUtils.isBlank(combination)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_CMB); 
		   
	   }else if(StringUtils.isBlank(streamId)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_CLS); 
		   
	   }else if(StringUtils.isBlank(gender)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR_BLANK_GDR); 
		   
	   }else if(StringUtils.isBlank(isActive)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR); 
		   
	   }else if(StringUtils.isBlank(isAlumni)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR); 
		   
	   }else if(StringUtils.isBlank(studentId)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR); 
		   
	   }else if(StringUtils.isBlank(centerId)){
		   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR); 
		   
	   }else{
		   
		   Student student = studentDAO.getStudentByUUID(centerId, studentId); 
		   student.setCombinationId(combination);
		   student.setCurrentStreamId(streamId);
		   student.setFirstname(firstname);
		   student.setMiddlename(middlename);
		   student.setLastname(lastname);
		   student.setGender(gender);
		   student.setIsActive(isActive);
		   student.setIsAlumnus(isAlumni); 
		   student.setIndexNo(indexNo); 
		   
		   //logger.info(""); 
		   
		   if(studentDAO.updateStudent(student)){
			   session.setAttribute(SessionConstants.STUDENT_UPDATE_SUCCESS, SUCCESS); 
			   
		   }else{
			   session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, ERROR); 
			   
		   }
		   
	   }
      
       session.setAttribute(SessionConstants.STUDENT_PARAM, studentIdHash); 
       response.sendRedirect("studentProfile.jsp");  
	   return;
   }
   
   

@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
      doPost(request, response);
  }
  
/**
 * 
 */
private static final long serialVersionUID = 947278544686138709L;
}

/**
 * 
 */
package com.appletech.server.servlet.student.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.appletech.bean.center.student.Student;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.stm.ClassroomDAO;
import com.appletech.persistence.stm.CombinationDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.appletech.persistence.student.StudentDAO;

/**
 * @author peter
 *
 */
public class ExcelUtil {


	private String[] genderArray;
	private List<String> genderList;

	/**
	 * 
	 */
	public ExcelUtil() {
		genderArray = new String[] {"M", "F", "m", "f"};
		genderList = Arrays.asList(genderArray);
	}

	/**
	 * @param uploadedFile
	 * @param centerId
	 * @param studentDAO
	 * @param streamDAO
	 * @param combinationDAO
	 * @param classroomDAO
	 * @return
	 * @throws IOException 
	 */
	public String processUploadedFiles(File uploadedFile, String centerId, StudentDAO studentDAO, StreamDAO streamDAO,
			CombinationDAO combinationDAO, ClassroomDAO classroomDAO) throws IOException { 
		String feedback = StudentExcel.UPLOAD_SUCCESS;

		if(uploadedFile != null){


			FileInputStream myInput =  new FileInputStream(uploadedFile);

			try{




				Workbook myWorkBook = WorkbookFactory.create(myInput);
				XSSFSheet mySheet = (XSSFSheet) myWorkBook.getSheetAt(0);
				int totalRow = mySheet.getLastRowNum();
				
				String indexNo = "";
				String firstname = "";
				String middlename = "";
				String gender = "";
				String combination = "";
				String classroom = "";
				String classroom_str = "";
				String stream_str = "";

				int count = 1;
				int totalColumn = 0;
				for(int i=0; i<=totalRow; i++){
					
					XSSFRow row = mySheet.getRow(i);
					if(row !=null){
						totalColumn = row.getLastCellNum();
						
						if(totalColumn > 6){
							return ("Invalid Number of comlumns on line \"" + count + "\""); 
						}
						
					}
					

					for(int j=0; j<totalColumn; j++){

						if(i==0){
							XSSFCell classroomCell = row.getCell((short)0);
							classroom = classroomCell+""; 
							
							String[] parts = classroom.split("\\s+");
							  classroom_str = StringUtils.trimToEmpty(parts[0]);
							  stream_str = StringUtils.trimToEmpty(parts[1]); 
							  

						}else if(i > 1){

							indexNo = row.getCell(0)+"";
							firstname =  row.getCell(1)+"";
							middlename =  row.getCell(2)+"";
							gender =  row.getCell(4)+"";
							combination = row.getCell(5)+"";
						}
						
					}
					
					
					if(count > 2){
						
						if(StringUtils.endsWith(indexNo, ".0")){
							indexNo = StringUtils.remove(indexNo, ".0"); 
						}
						
						if (classroom == null || StringUtils.equals(classroom, "null")){
							return ("Invalid or blank Classroom  on line \"" + count + "\" ."); 
						}
						
						if(classroomDAO.getClassroom(centerId, "FORM "+classroom_str) == null){
							return ("Classroom \"" + classroom_str + "\" on line \"" + count + "\"  doesn't exist ."); 
							
						}
						
						if(streamDAO.getStreamByDescription(centerId, stream_str) == null){
							return ("Stream \"" + stream_str + "\" on line \"" + count + "\"  doesn't exist ."); 
							
						}
						
						if (indexNo == null || StringUtils.equals(indexNo, "null")){
							return ("Invalid or blank indexNo  on line \"" + count + "\" ."); 
						}
						
						if(studentDAO.getStudentByindexNo(centerId, indexNo) != null){
							return ("IndexNo \"" + indexNo + "\" on line \"" + count + "\"  already exist."); 
						}
						
						if (firstname == null || StringUtils.equals(firstname, "null")){   
							return ("Invalid or blank Firstname  on line \"" + count + "\" ."); 
						}
						
						if (middlename == null || StringUtils.equals(middlename, "null")){   
							return ("Invalid or blank Middlename  on line \"" + count + "\" ."); 
						}
						
						if (gender == null || StringUtils.equals(gender, "null")){   
							return ("Invalid or blank Gender  on line \"" + count + "\" ."); 
						}
						

						if(!genderList.contains(gender)) {
							return ("Invalid or blank Gender " + gender + " on line " + count + "\" .");
						}
						
						if (combination == null || StringUtils.equals(combination, "null")){   
							return ("Invalid or blank Combination  on line \"" + count + "\" ."); 
						}
						
						if(combinationDAO.getCombination(centerId, combination.toUpperCase()) == null){
							return ("Combination \"" + combination + "\" on line \"" + count + "\"  doesn't exist ."); 
						}
						
						
					}
					
					count++;
				}
				
				


			}


			catch (Exception e){

			}finally {
				myInput.close();

			}


		}

		return feedback;
	}



	/**
	 * @param uploadedFile
	 * @param centerId
	 * @param studentDAO
	 * @param streamDAO
	 * @param combinationDAO
	 * @param classroomDAO
	 * @param configDAO
	 * @throws IOException 
	 */
	public void saveResults(File uploadedFile, String centerId, StudentDAO studentDAO, StreamDAO streamDAO,
			CombinationDAO combinationDAO, ClassroomDAO classroomDAO, ConfigDAO configDAO) throws IOException {

		FileInputStream myInput =  new FileInputStream(uploadedFile);

		try{




			Workbook myWorkBook = WorkbookFactory.create(myInput);
			XSSFSheet mySheet = (XSSFSheet) myWorkBook.getSheetAt(0);
			int totalRow = mySheet.getLastRowNum();
			
			String indexNo = "";
			String firstname = "";
			String middlename = "";
			String lastname = "";
			String gender = "";
			String combination = "";
			String classroom = "";
			String classroom_str = "";
			String stream_str = "";

			int count = 1;
			int totalColumn = 0;
			for(int i=0; i<=totalRow; i++){
				
				XSSFRow row = mySheet.getRow(i);
				if(row !=null){
					totalColumn = row.getLastCellNum();
					
				}
				

				for(int j=0; j<totalColumn; j++){

					if(i==0){
						XSSFCell classroomCell = row.getCell((short)0);
						classroom = classroomCell+""; 
						
						String[] parts = classroom.split("\\s+");
						  classroom_str = StringUtils.trimToEmpty(parts[0]);
						  stream_str = StringUtils.trimToEmpty(parts[1]); 
						 
					}else if(i > 1){

						indexNo = row.getCell(0)+"";
						firstname =  row.getCell(1)+"";
						middlename =  row.getCell(2)+"";
						lastname =  row.getCell(3)+"";
						gender =  row.getCell(4)+"";
						combination = row.getCell(5)+"";
					}
					
				}
				
				
				if(count > 2){
					
					if(StringUtils.endsWith(indexNo, ".0")){
						indexNo = StringUtils.remove(indexNo, ".0"); 
					}
					
					//Add Student in the database
					String classroomId = classroomDAO.getClassroom(centerId, "FORM "+classroom_str).getUuid();
					String combinationId = combinationDAO.getCombination(centerId, combination.toUpperCase()).getUuid();
					String streamId = streamDAO.getStream(centerId, classroomId, stream_str).getUuid();
					
					if (lastname == null || StringUtils.equals(lastname, "null")){   
						lastname = "";
					}
					
					
					Student student = new Student();
					student.setCenterId(centerId);
					student.setAdmmitedStreamId(streamId);
					student.setCurrentStreamId(streamId);
					student.setCombinationId(combinationId);
					student.setIndexNo(indexNo);
					student.setFirstname(firstname);
					student.setMiddlename(middlename);
					student.setLastname(lastname);
					student.setGender(gender);
					student.setPhotoUri("path"); 
					
					studentDAO.putStudent(student);
					
					
					
				}
				
				count++;
			}
			
			


		}


		catch (Exception e){

		}finally {
			myInput.close();

		}


	



	}

}

/**
 * 
 */
package com.appletech.server.servlet.student.upload;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Center;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.stm.ClassroomDAO;
import com.appletech.persistence.stm.CombinationDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class StudentExcel  extends HttpServlet{
	
	
	public final static String UPLOAD_FEEDBACK = "UploadFeedback";
	public final static String UPLOAD_SUCCESS = "Student's successfully uploaded.";
	
	private String USER = "";
	private String UPLOAD_DIR = "";
	
	private ExcelUtil excelUtil;
	
	private String schoolCode = "";
	
	public static StudentDAO studentDAO;
	public static StreamDAO streamDAO;
	public static CombinationDAO combinationDAO;
	public static ClassroomDAO classroomDAO;
	private static ConfigDAO configDAO;
	private static CenterDAO centerDAO;
	
	private Logger logger;
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       
       studentDAO = StudentDAO.getInstance();
       streamDAO = StreamDAO.getInstance();
       combinationDAO = CombinationDAO.getInstance();
       classroomDAO = ClassroomDAO.getInstance();
       configDAO = ConfigDAO.getInstance();
       centerDAO = CenterDAO.getInstance();
       
       
       // Create a factory for disk-based file items
       DiskFileItemFactory factory = new DiskFileItemFactory();
       File repository = FileUtils.getTempDirectory();
       factory.setRepository(repository);
       USER = System.getProperty("user.name");
       UPLOAD_DIR =  "/home/"+USER+"/school/students"; 
      
       excelUtil = new ExcelUtil();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
	   
	   File uploadedFile = null;
	   HttpSession session = request.getSession(false);
	  
	   
	   String centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
	   Center center = centerDAO.getCenterById(centerId);
	   schoolCode = center.getCenterCode();
	   
	   String staff = ""; 
	   staff = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_MOBILE);
	   //logger.info(""); 
	  
	// Create a factory for disk-based file items
       DiskFileItemFactory factory = new DiskFileItemFactory();

       // Set up where the files will be stored on disk
       File repository = new File(UPLOAD_DIR);
       FileUtils.forceMkdir(repository); 
       factory.setRepository(repository);
       
       // Create a new file upload handler
       ServletFileUpload upload = new ServletFileUpload(factory);
       // Parse the request    
       
       try {
    	
		List<FileItem> items = upload.parseRequest(request);
		Iterator<FileItem> iter = items.iterator();
		
		FileItem item;
		
		while (iter.hasNext()) {
		    item = iter.next();

		    if (!item.isFormField()) {
		    	if(item!=null){
		    		  uploadedFile = processUploadedFiles(item);
		    		  String feedback = "";
		    		  //System.out.println("************** ");
		    	      feedback = excelUtil.processUploadedFiles(uploadedFile,centerId,studentDAO,streamDAO,combinationDAO,classroomDAO);
		    		  session.setAttribute(UPLOAD_FEEDBACK,"<p class='error'>"+feedback+"<p>");
		    		    // Process the file into the database if it is ok
		    	       if(StringUtils.equals(feedback, UPLOAD_SUCCESS)) {
		    	    	   excelUtil.saveResults(uploadedFile,centerId,studentDAO,streamDAO,combinationDAO,classroomDAO,configDAO);
		    	         }
		    	       
		    	       
		    	      
		    	}	
		    } 
		}// end 'while (iter.hasNext())'
    
	    } catch (FileUploadException e) {
	    	logger.error("FileUploadException while getting File Items.");
			logger.error(e);
	   } 
	 
      
	   response.sendRedirect("importStudent.jsp");
       return;
	  
   }
   
   

	/**
	 * @param item
	 * @return the file handle
	 */
	private File processUploadedFiles(FileItem item) {
		// A specific folder in the system
		String folder = UPLOAD_DIR + File.separator + schoolCode;
		File file = null;
		
       try {
			FileUtils.forceMkdir(new File(folder));
			file = new File(folder + File.separator + item.getName());
			item.write(file); 
			
		} catch (IOException e) {
			logger.error("IOException while processUploadedFile: " + item.getName());
			logger.error(e);
			
		} catch (Exception e) {
			logger.error("Exception while processUploadedFile: " + item.getName());
			logger.error(e);
		} 
   
       return file;
	}

   
   

@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
      doPost(request, response);
  }



/**
 * 
 */
private static final long serialVersionUID = -8520768915446715589L;


}

/**
 * 
 */
package com.appletech.server.servlet.performance;

import java.util.Comparator;

import com.appletech.bean.center.performance.Result;

/**
 * @author peter
 *
 */
public class ResultComparator implements Comparator<Result>{

	@Override
	public int compare(Result result1, Result result2) {
		if(result1.getTotalPoints() > result2.getTotalPoints()){
			return 1;
		}else if(result1.getTotalPoints() < result2.getTotalPoints()){
			return -1;
		}else{
			return 0;
		}
		//return ( result1.getTotalPoints() > result2.getTotalPoints()     ) ? 1: -1 ;
	} 
	
}

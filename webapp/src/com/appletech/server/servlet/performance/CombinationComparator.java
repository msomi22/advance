/**
 * 
 */
package com.appletech.server.servlet.performance;

import java.util.Comparator;

import com.appletech.bean.center.performance.CombinationResult;

/**
 * @author peter
 *
 */
public class CombinationComparator implements Comparator<CombinationResult> {

	@Override
	public int compare(CombinationResult comb1, CombinationResult comb2) {
		return (comb1.getTotalPoints() > comb2.getTotalPoints()) ? 1: -1 ;
	}

	
}

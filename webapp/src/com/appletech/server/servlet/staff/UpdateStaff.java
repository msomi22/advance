/**
 * 
 */
package com.appletech.server.servlet.staff;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;

import com.appletech.bean.center.staff.Staff;
import com.appletech.bean.center.stm.TeacherStream;
import com.appletech.persistence.staff.StaffDAO;
import com.appletech.persistence.stm.TeacherStreamDAO;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class UpdateStaff extends HttpServlet{
	
	private final String ERROR_BLANK_NAME = "Name can't be blank.";
	private final String ERROR_SHORT_NAME = "Name too short.";
	
	private final String ERROR_BLANK_MOBILE = "Mobile Number can't be blank.";
	private final String ERROR_INVALID_MOBILE = "Mobile Number is not valid.";
	//private final String ERROR_EXIST_MOBILE = "Mobile Number already exist.";
	
	private final String ERROR_BLANK_EMAIL = "Email can't be blank.";
	private final String ERROR_INVALID_EMAIL = "Email is not valid.";
	//private final String ERROR_EXIST_EMAIL = "Email already exist.";
	
	private final String ERROR_BLANK_ID = "Id Number can't be blank.";
	//private final String ERROR_EXIST_ID = "Id Number already exist.";
	
	private final String ERROR_CLASS_OCCUPIED = "The selected classroom is already assigned another teacher.";
	
	//private final String ERROR_NOT_ALLOWED = "You are not allowed to update staff.";
	//private final String ERROR_NOT_POSIBLE = "This is an illegal operation.";
	
	private final String SUCCESS = "The staff details was updated successfully.";
	private final String ERROR = "Something went wrong, staff details not updated.";
	
	
	private EmailValidator emailValidator;
	private static StaffDAO staffDAO;
	private static TeacherStreamDAO teacherStreamDAO;
	
	String a_l_Principal = "AF066A82-B397-4213-AA1A-D9BF03FE0152";
	String a_l_DPrincipal = "CD3533AC-14EC-48E4-B841-725D83976D10";
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       emailValidator = EmailValidator.getInstance();
       staffDAO = StaffDAO.getInstance();
       teacherStreamDAO = TeacherStreamDAO.getInstance();
      
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
      
       String name = StringUtils.trimToEmpty(request.getParameter("name"));
       String mobile = StringUtils.trimToEmpty(request.getParameter("mobile"));
       String email = StringUtils.trimToEmpty(request.getParameter("email"));
       String idNumber = StringUtils.trimToEmpty(request.getParameter("idNumber"));
       String accessLevelId = StringUtils.trimToEmpty(request.getParameter("accessLevel"));
       String nationality = StringUtils.trimToEmpty(request.getParameter("nationality"));
       String gender = StringUtils.trimToEmpty(request.getParameter("gender"));
       String isActive = StringUtils.trimToEmpty(request.getParameter("isActive"));
       String streamId = StringUtils.trimToEmpty(request.getParameter("classroom"));
       String teacherId = StringUtils.trimToEmpty(request.getParameter("teacherId"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
       
       String accessLevelLoggedIn = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_ACCESS_LEVEL); 
       
       /*System.out.println("name : " + name );
       System.out.println("mobile : " + mobile );
       System.out.println("email : " + email );
       System.out.println("idNumber : " + idNumber );
       System.out.println("accessLevelId : " + accessLevelId );
       System.out.println("accessLevelLoggedIn : " + accessLevelLoggedIn );
       System.out.println("nationality : " + nationality );
       System.out.println("gender : " + gender );
       System.out.println("isActive : " + isActive );
       System.out.println("teacherId : " + teacherId );
       System.out.println("centerId : " + centerId );
       System.out.println("******************************************************");*/
       
       String dbteacherId = teacherId;
       if(teacherStreamDAO.getTeacherStreamBystreamId(centerId, streamId) != null){
    	   dbteacherId = teacherStreamDAO.getTeacherStreamBystreamId(centerId, streamId).getTeacherId();
       }
       
       String dbteacherAcesslevel = "";
       if(staffDAO.getStaffByuuid(centerId, teacherId) != null){
    	   dbteacherAcesslevel = staffDAO.getStaffByuuid(centerId, teacherId).getAccessLevelId();
       }
       
       String dbdeputyId = teacherId;
       if( staffDAO.getStaff(centerId, a_l_DPrincipal) != null){
    	   dbdeputyId = staffDAO.getStaff(centerId, a_l_DPrincipal).getUuid();
       }
       
       Map<String, String> staffIdHash = new HashMap<>(); 
       staffIdHash.put("teacherId", teacherId); 
       
       if(StringUtils.isBlank(name)){
    	   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_BLANK_NAME); 
		      
	   }else if(!Wordlength(name)){
	 	   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_SHORT_NAME); 
		   
	   }else if(StringUtils.isBlank(mobile)){
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_BLANK_MOBILE); 
		   
	   }else if(!isNumeric(mobile)){
    	   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_INVALID_MOBILE); 
    	   
       }else if(!lengthValid(mobile)){
	 	   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_INVALID_MOBILE); 
		   
       }else if(StringUtils.isBlank(email)){
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_BLANK_EMAIL); 
		   
	   }else if (!emailValidator.isValid(email)) {
           session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_INVALID_EMAIL);

       }else if(StringUtils.isBlank(idNumber)){
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_BLANK_ID); 
		   
	   }
	   /**
	    * @param teacherId: the teacher_id for the teacher to be updated 
	    * @param dbteacherId: get teacher_id given (centerId, streamId) 
	    * 
	    * AIM: do not create a duplicate when updating teacher-stream 
	    */
	   else if(!StringUtils.equals(teacherId, dbteacherId)){
		   
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR_CLASS_OCCUPIED); 
		   
	   }else if(StringUtils.isBlank(accessLevelLoggedIn) || accessLevelLoggedIn == null){
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR); 
		   
	   }
	   
	   else if(!StringUtils.equals(a_l_Principal, accessLevelLoggedIn) &&
			   (StringUtils.equals(dbteacherAcesslevel, a_l_Principal))){ 
		   //not logged as principal, and selected access level is principal 
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, "You can not alter the principal's details."); 
	   }
	   
	   else if( !(StringUtils.equals(a_l_Principal, accessLevelLoggedIn) ||
			      StringUtils.equals(a_l_DPrincipal, accessLevelLoggedIn)  ) 
			     && (StringUtils.equals(accessLevelId, a_l_DPrincipal))){ 
		   //not logged as principal, and selected access level is deputy principal 
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, "You can only update Deputy when you are either the Principal or Deputy"); 
	   }
      
       
	   //accessLevelLoggedIn
	   else if(StringUtils.equals(a_l_Principal, accessLevelId) &&
			     staffDAO.getStaff(centerId, a_l_Principal) != null &&
			       !StringUtils.equals(a_l_Principal, accessLevelLoggedIn)){
		   //you can't have two principals
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, "A center can't have two principals."); 
		   
	   }else if(!StringUtils.equals(teacherId, dbdeputyId) && StringUtils.equals(a_l_DPrincipal, accessLevelId) &&
			   (StringUtils.equals(a_l_Principal, accessLevelLoggedIn)  || StringUtils.equals(a_l_DPrincipal, accessLevelLoggedIn))){
		   //you can't have two deputy principals
		   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, "A center can't have two Deputy principals."); 
		   
	   }else{
		   
		   String cteacherId = teacherId; 
		   if(teacherStreamDAO.getTeacherStreamBystreamId(centerId, streamId) != null){
			   cteacherId = teacherStreamDAO.getTeacherStreamBystreamId(centerId, streamId).getTeacherId();
		   }
		   
		   if(!StringUtils.equals(cteacherId, teacherId)){
			   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, "The class you selected is already in use."); 
		   }else{
			   
			   Staff staff = staffDAO.getStaffByuuid(centerId, teacherId);
			   staff.setAccessLevelId(accessLevelId);
			   staff.setName(name);
			   staff.setGender(gender);
			   staff.setMobile(removeLeadingZeroes(mobile));
			   staff.setEmail(email);
			   staff.setIdNumber(idNumber);
			   staff.setNationality(nationality);
			   staff.setIsActive(isActive);
			  
			   boolean teacherstreamUpate = false;
			   
			  
			   
			   if(!StringUtils.isBlank(streamId)){
				   
				   if(teacherStreamDAO.getTeacherStream(centerId, teacherId) != null){
					   TeacherStream teacherStream = teacherStreamDAO.getTeacherStream(centerId, teacherId);
					   teacherStream.setStreamId(streamId);
					   
					   if(teacherStreamDAO.updateTeacherStream(teacherStream)){
						   teacherstreamUpate = true;
					   }
					   
				   }else{
					   
					   TeacherStream teacherStream = new TeacherStream();
					   teacherStream.setCenterId(centerId);
					   teacherStream.setStreamId(streamId);
					   teacherStream.setTeacherId(dbteacherId);
					   if(teacherStreamDAO.putTeacherStream(teacherStream)){
						   teacherstreamUpate = true;
					   }
					   
				   }
				   
				   
			   }else{
				   
				   teacherstreamUpate = true;
				   
			   }
			   
			   
			   
			  
			   
			   
			   if(staffDAO.updateStaff(staff) && teacherstreamUpate){
				   
				   //accessLevelLoggedIn
				   if(StringUtils.equals(accessLevelLoggedIn, a_l_DPrincipal) && !StringUtils.equals(accessLevelId, a_l_DPrincipal)){
					   response.sendRedirect("../index.jsp");
				   }
				   
				   logger.info(staff + " updated staff" );
				   
				   session.setAttribute(SessionConstants.STAFF_UPDATE_SUCCESS, SUCCESS); 
				   
			   }else{
				   session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, ERROR); 
				   
			   }
			   
		   }
		   
	   }
       
       session.setAttribute(SessionConstants.STAFF_UPDATE_PARAM, staffIdHash); 
       response.sendRedirect("staffProfile.jsp");   
	   return;
   }
   
   

	/**
	 * @param value
	 * @return
	 */
	public static String removeLeadingZeroes(String value) {
	     return new Integer(value).toString();
	}
   

	/**
	 * @param mystring
	 * @return
	 */
	private static boolean Wordlength(String mystring) {
		boolean isvalid = true;
		int length = 0;
		length = mystring.length();
		if(length<3){
			isvalid = false;
		}
		return isvalid;
	}
	
	

	/**
	 * @param mystring
	 * @return
	 */
	private static boolean lengthValid(String mystring) {
		boolean isvalid = true;
		int length = 0;
		length = mystring.length();
		if(length<9 ||length>10){
			isvalid = false;
		}
		return isvalid;
	}
	
	/**
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	    
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
   

 /**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

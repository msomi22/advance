/**
 * 
 */
package com.appletech.server.servlet.staff;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.staff.Staff;
import com.appletech.persistence.staff.StaffDAO;
import com.appletech.server.servlet.util.SecurityUtil;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class ResetPassword extends HttpServlet{
	
	private static StaffDAO staffDAO;
	private Logger logger = Logger.getLogger(this.getClass());

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       staffDAO = StaffDAO.getInstance();
      
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
     
       String staffId = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_ID);
       String centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
       
       String currentPassword = StringUtils.trimToEmpty(request.getParameter("currentPassword"));
       String newPassword = StringUtils.trimToEmpty(request.getParameter("newPassword"));
       String confirmPassword = StringUtils.trimToEmpty(request.getParameter("confirmPassword"));
       
      /* System.out.println(currentPassword);
       System.out.println(newPassword);
       System.out.println(confirmPassword);*/
       

       if(StringUtils.isBlank(currentPassword)){
    	   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Please provide your current password."); 
		      
	   }else if(StringUtils.isBlank(newPassword)){
		   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Please provide your new password."); 
		   
	   }else if(StringUtils.isBlank(confirmPassword)){
		   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Please confirm your new password."); 
		   
	   }else if(!StringUtils.equals(newPassword, confirmPassword)){
		   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Password doesn't match."); 
		   
	   }
	   else if(staffDAO.getStaffByuuid(centerId, staffId) == null){
		   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Something went wrong, try again later."); 
		   
	   }else{
		   
		   Staff staff = staffDAO.getStaffByuuid(centerId, staffId);
		   if(!StringUtils.equals(staff.getPassword(), SecurityUtil.getMD5Hash(currentPassword))){
			   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "The provided Current Password is incorrect.");
		   }else{
			   
			   if(staffDAO.resetStaffPassword(centerId, staffId, newPassword)){
				   
				   logger.info(staff.getMobile() + ", changed password");
				   
				   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_SUCCESS, "The password was reset successfully.");
				   
			   }else{
				   session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, "Something went wrong, try again later.");
				   
			   }
			   
		   }
		   
	   }
      
       response.sendRedirect("userProfile.jsp");   
	   return;
   }
   
   
   

 /**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

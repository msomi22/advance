/**
 * 
 */
package com.appletech.server.servlet.staff.login;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.appletech.server.session.SessionConstants;

/**
 * staff sign out
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Logout extends HttpServlet {
	
	private Logger logger = Logger.getLogger(this.getClass());


	/**
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
		
	}
    
    /**
    *
    * @param request
    * @param response
    * @throws ServletException, IOException
    * @throws java.io.IOException
    */
   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
       doPost(request, response);
   }
   
   /**
    * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
    */
   @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);

       response.sendRedirect("index.jsp");
       
       String mobile = ""; 
       mobile = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_MOBILE);
	   logger.info(mobile + " has logged out"); 

       if (session != null) {
           //destroy the session
           session.invalidate();
           
           
           
       }
       
   }
  


   /**
	 * 
	 */
	private static final long serialVersionUID = -5357835068628480751L;

}

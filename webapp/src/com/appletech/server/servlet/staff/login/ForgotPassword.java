/**
 * 
 */
package com.appletech.server.servlet.staff.login;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.staff.Staff;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.staff.StaffDAO;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class ForgotPassword extends HttpServlet{
	
	
	private static StaffDAO staffDAO;
	private static CenterDAO centerDAO;
	private Logger logger = Logger.getLogger(this.getClass());

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       staffDAO = StaffDAO.getInstance();
       centerDAO = CenterDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

       HttpSession session = request.getSession(true);
       
       String centerCredential = StringUtils.trimToEmpty(request.getParameter("centerCredential"));
       String mobileORemailOrIdNo = StringUtils.trimToEmpty(request.getParameter("mobileORemail"));
       
      /* System.out.println(centerCredential);
       System.out.println(mobileORemailOrIdNo);*/
       
       if(StringUtils.isBlank(centerCredential)){
    	   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "Please provide Center Credentials."); 
		      
	   }else if(centerDAO.getCenter(centerCredential) == null){
		   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "Incorrect Center Credentials."); 
		   
	   }else if(StringUtils.isBlank(mobileORemailOrIdNo)){
		   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "Please provide your Mobile, Email or IdNumber."); 
		   
	   }else if(staffDAO.getStaffBymobileORemail(centerDAO.getCenter(centerCredential).getUuid(), mobileORemailOrIdNo) == null){
		   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "The staff was not found in the given center, confirm your staff Credentials."); 
		   
	   }else if(StringUtils.equals(staffDAO.getStaffBymobileORemail(centerDAO.getCenter(centerCredential).getUuid(), mobileORemailOrIdNo).getIsActive(),"0")){
		   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "Oh! Am sorry but you have to contact the Admin."); 
		   
	   }else{
		   
		  
		   
		   Staff staff = staffDAO.getStaffBymobileORemail(centerDAO.getCenter(centerCredential).getUuid(), mobileORemailOrIdNo);
		   String password = RandomStringUtils.randomAlphabetic(5);
		   if(staffDAO.resetStaffPassword(centerDAO.getCenter(centerCredential).getUuid(), staff.getUuid(), password)){
			   
			   logger.info(staff.getMobile() + " has reset their password"); 
			  
			   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_SUCCESS, "Your password has been retset, your new password is " + password); 
		   }else{
			   
			   session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, "An error occured, please try again later."); 
		   }
	   }
       
       response.sendRedirect("resetPassword.jsp");   
	   return;
   }
   
   
   

 /**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

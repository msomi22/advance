/**
 * 
 */
package com.appletech.server.servlet.staff.login;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Center;
import com.appletech.bean.center.staff.Staff;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.staff.StaffDAO;
import com.appletech.server.session.SessionConstants;



/**
 * staff sign in 
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Login extends HttpServlet{
	
	private final String ERROR_WRONG_CENTER_DETAIL = "Incorrect center credentials.";
	private final String ERROR_WRONG_STAFF_DETAIL = "Incorrect staff credentials.";
	
	private final String ERROR_SCHOOL_INACTIVE = "You can't login to your school account, call +254718953974 for help.";
	
	private static StaffDAO staffDAO;
	private static CenterDAO centerDAO;
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		staffDAO = StaffDAO.getInstance();
		centerDAO = CenterDAO.getInstance();
	}
	
	/**
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);//current session
		
		String centerCredential = StringUtils.trimToEmpty(request.getParameter("centerCredential"));
		//email,centerCode,centerName,centerNo,website
		
		String mobileORemail = StringUtils.trimToEmpty(request.getParameter("mobileORemail"));
		String password = StringUtils.trimToEmpty(request.getParameter("password"));
		
		String userPublicIpAddress = request.getHeader("X-Forwarded-For");
		String localAddress =  request.getRemoteAddr();
		String userAgent = request.getHeader("User-Agent");
		
		if(centerDAO.getCenter(centerCredential) == null){
			session.setAttribute(SessionConstants.CENTER_SIGN_IN_ERROR, ERROR_WRONG_CENTER_DETAIL); 
			response.sendRedirect("index.jsp");

		}else if(centerDAO.getCenter(centerCredential,"1") == null){
			session.setAttribute(SessionConstants.CENTER_SIGN_IN_ERROR, ERROR_SCHOOL_INACTIVE); 
			response.sendRedirect("index.jsp");

		}else if(staffDAO.StaffLogin(centerDAO.getCenter(centerCredential,"1").getUuid(), mobileORemail, password) == null){ 
			session.setAttribute(SessionConstants.CENTER_SIGN_IN_ERROR, ERROR_WRONG_STAFF_DETAIL); 
			response.sendRedirect("index.jsp");

		}else if(StringUtils.equals(staffDAO.getStaffBymobileORemail(centerDAO.getCenter(centerCredential).getUuid(), mobileORemail).getIsActive(),"0")){
			session.setAttribute(SessionConstants.CENTER_SIGN_IN_ERROR, "You are not allowed to access this system, please contact the Admin.");
			response.sendRedirect("index.jsp");
			
		}else{
			
			Center center = centerDAO.getCenter(centerCredential,"1");
			Staff staff  = staffDAO.StaffLogin(centerDAO.getCenter(centerCredential,"1").getUuid(), mobileORemail, password);
			
				logger.info("Mobile: " + staff.getMobile() + ", "
						+ " userPublicIpAddress: " + userPublicIpAddress + ", "
								+ "localAddress : " + localAddress  + ", userAgent : " +  userAgent + " login"); 
			
				session.setAttribute(SessionConstants.CENTER_SIGN_IN_ID, center.getUuid());
				session.setAttribute(SessionConstants.CENTER_SIGN_IN_CENTERNO, center.getCenterNo());
				session.setAttribute(SessionConstants.CENTER_SIGN_SUCCESS, SessionConstants.CENTER_SIGN_SUCCESS); 
				session.setAttribute(SessionConstants.CENTER_SIGN_IN_TIME, String.valueOf(new Date().getTime()));
				request.getSession().setAttribute(SessionConstants.CENTER_STAFF_SIGN_ID, staff.getUuid()); 
				request.getSession().setAttribute(SessionConstants.CENTER_STAFF_SIGN_MOBILE, staff.getMobile());
				request.getSession().setAttribute(SessionConstants.CENTER_STAFF_SIGN_ACCESS_LEVEL, staff.getAccessLevelId());
				response.sendRedirect("school/schoolIndex.jsp"); 
				
		}
		
	   }
	

	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


}

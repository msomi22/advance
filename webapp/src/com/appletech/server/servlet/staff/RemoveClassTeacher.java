/**
 * 
 */
package com.appletech.server.servlet.staff;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.persistence.stm.TeacherStreamDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class RemoveClassTeacher extends HttpServlet{
	
	private static TeacherStreamDAO teacherStreamDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       teacherStreamDAO = TeacherStreamDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String uuid = StringUtils.trimToEmpty(request.getParameter("uuid"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
      
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(removeTeacher(centerId,uuid)).getBytes()); 
		

		out.flush();
		out.close();
       
   }
   
   
   

 /**
 * @param classrooms
 * @param subjectId
 * @param teacherId
 * @param centerId
 * @return
 */
private JsonElement removeTeacher(String centerId,  String uuid) {
		JsonObject jsonObject = new JsonObject(); 
		
		if(teacherStreamDAO.deleteTeacherStream(centerId, uuid)){
			
			 logger.info("remove class teacher with id: " + uuid);
			 
			jsonObject.addProperty("responseMessage", "ClassTeacher removed successfully."); 

		}else{
			jsonObject.addProperty("responseMessage", "Something went wrong, try again later."); 

		}
		
		return jsonObject;
}
 


/**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }

/**
 * 
 */
private static final long serialVersionUID = 491324163248836080L;


}

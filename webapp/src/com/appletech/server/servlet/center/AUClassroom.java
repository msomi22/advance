/**
 * 
 */
package com.appletech.server.servlet.center;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Stream;
import com.appletech.persistence.stm.ClassroomDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class AUClassroom extends HttpServlet{
	
	public static StreamDAO streamDAO;
	public static ClassroomDAO classroomDAO;
	private String [] A_Z;
	private List<String> A_ZList;
	
	private Logger logger = Logger.getLogger(this.getClass());

	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       streamDAO = StreamDAO.getInstance();
       classroomDAO = ClassroomDAO.getInstance();
       A_Z = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
       A_ZList = Arrays.asList(A_Z);
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String classroom = StringUtils.trimToEmpty(request.getParameter("classroomId"));
       String newstream = StringUtils.trimToEmpty(request.getParameter("newstream"));
       String streamId = StringUtils.trimToEmpty(request.getParameter("streamId"));
       String newstreamName = StringUtils.trimToEmpty(request.getParameter("newstreamName"));
       String newstreamId = StringUtils.trimToEmpty(request.getParameter("newstreamId"));
       
       
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
       
       String decision = StringUtils.trimToEmpty(request.getParameter("decision"));
      /* 
       System.out.println("classroom : " + classroom );
       System.out.println("newstream : " + newstream );
       System.out.println("streamId : " + streamId );
       System.out.println("newstreamName : " + newstreamName );
       System.out.println("newstreamId : " + newstreamId );
       
       System.out.println("decision : " + decision );
       System.out.println("centerId : " + centerId );
       System.out.println("******************************************************");*/
       
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();
   

		if(StringUtils.equalsIgnoreCase(decision, "addClassroom")){

			out.write(gson.toJson(addClassroom(classroom,newstream,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "updateClassroom")){

			out.write(gson.toJson(updateClassroom(newstreamName,newstreamId,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "removeClassroom")){

			out.write(gson.toJson(removeClassroom(streamId,centerId)).getBytes());
			out.flush();
			out.close();

		}
      
       
       
       
   }
   
   
   

 /**
 * @param classId
 * @param streamId
 * @param stream
 * @param centerId
 * @return
 */
private JsonElement removeClassroom(String streamId,String centerId) {
	 JsonObject jsonObject = new JsonObject();
	 
	 if(streamDAO.deleteStream(centerId, streamId)){
		 
		 logger.info("Stream " + streamId + " removed successfully." );
		 jsonObject.addProperty("messageResponse", "Stream removed successfully.");
		 
	 }else{
		 jsonObject.addProperty("messageResponse", "Something went wrong, stream not removed.");
		 
	 }
	
	return jsonObject;
}

/**
 * @param classId
 * @param streamId
 * @param stream
 * @param centerId
 * @return
 */
private JsonElement updateClassroom(String newstreamName,String newstreamId,String centerId) {
	 JsonObject jsonObject = new JsonObject();
	 
	 Stream stream = streamDAO.getStreambyUuid(centerId, newstreamId);
	 
     if(!A_ZList.contains(newstreamName.toUpperCase())){ 
    	 
    	jsonObject.addProperty("messageResponse", "Stream \"" + newstreamName + "\" Is not valid, try an Alphabet.");
    	
	 }else if(StringUtils.equalsIgnoreCase(stream.getDescription(), newstreamName)){
		 
		 jsonObject.addProperty("messageResponse", "Nothing to update.");
		 
	 }else if(streamDAO.getStreamByDescription(centerId, newstreamName.toUpperCase()) != null){
		  jsonObject.addProperty("messageResponse", "Stream \"" + newstreamName.toUpperCase() + "\" is already in use.");
		  
	  }else{
		 
		 stream.setDescription(newstreamName.toUpperCase()); 
		 
		 if(streamDAO.updateStream(stream)){
			 
			 logger.info("Stream " + newstreamId + " updated to " + newstreamName);
			 jsonObject.addProperty("messageResponse", "Stream updated successfully.");
			 
		 } else{
			 jsonObject.addProperty("messageResponse", "Something went wrong, stream not updated.");
			 
		 }
		 
		 
	 }
	 
	return jsonObject;
}

/**
 * @param classId
 * @param streamId
 * @param stream
 * @param centerId
 * @return
 */
private JsonElement addClassroom(String classroomId,String description,String centerId) {
	 JsonObject jsonObject = new JsonObject();
	 
	 if(!A_ZList.contains(description.toUpperCase())){ 
	    	jsonObject.addProperty("messageResponse", "Stream \"" + description.toUpperCase() + "\" Is not valid, try an Alphabet.");
	    	
	  }else if(streamDAO.getStreamByDescription(centerId, description.toUpperCase()) != null){
		  jsonObject.addProperty("messageResponse", "Stream \"" + description.toUpperCase() + "\" is already in use.");
		  
	  }else{
		 
		 Stream stream = new Stream();
		 stream.setClassroomId(classroomId);
		 stream.setDescription(description.toUpperCase()); 
		 stream.setCenterId(centerId); 
		 
		 if(streamDAO.putStream(stream)){
			 logger.info("new stream " + stream + " added");
			 jsonObject.addProperty("messageResponse", "Stream aded successfully.");
			 
		 }else{
			 jsonObject.addProperty("messageResponse", "Something went wrong, stream not added.");
			 
		 }
		 
			 
		 }
	 
	return jsonObject;
}


/**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }



/**
 * 
 */
private static final long serialVersionUID = 4702759518590729390L;
}

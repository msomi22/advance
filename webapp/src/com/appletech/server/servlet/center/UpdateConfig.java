/**
 * 
 */
package com.appletech.server.servlet.center;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Config;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.server.session.SessionConstants;

/** 
 * @author peter
 *
 */
public class UpdateConfig extends HttpServlet{

	private static ConfigDAO configDAO;
	
	String[] allawableTerms;
	List<String> terms = new ArrayList<String>(); 
	private Logger logger = Logger.getLogger(this.getClass());
	

	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		configDAO = ConfigDAO.getInstance();
		allawableTerms = new String[] {"1","2","3"};  
		terms = Arrays.asList(allawableTerms);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		 HttpSession session = request.getSession(true);

		String cycleId = StringUtils.trimToEmpty(request.getParameter("currentCycle"));
		String term = StringUtils.trimToEmpty(request.getParameter("term"));
		String year = StringUtils.trimToEmpty(request.getParameter("year"));
		String closingdate = StringUtils.trimToEmpty(request.getParameter("closingDate"));//05/25/2017
		String oppeningdate = StringUtils.trimToEmpty(request.getParameter("openingDate"));//05/25/2017
		String headTeacherComment = StringUtils.trimToEmpty(request.getParameter("headTeacherComment"));
		String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
	
		
		Calendar calendar = Calendar.getInstance();
		final int YEAR = calendar.get(Calendar.YEAR);

		Map<String, String> centerIdHash = new HashMap<>(); 
		centerIdHash.put("centerId", centerId); 

		if(StringUtils.isBlank(cycleId)){//terms
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Please select a cycle."); 

		}else if(StringUtils.isBlank(term)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Term can't be empty."); 

		}else if(!terms.contains(term)){ 
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Term can only be either 1, 2 or 3."); 

		}else if(StringUtils.isBlank(year)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Year can't be empty."); 

		}else if(!isNumeric(year)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "The year is not valid"); 

		}else if(Integer.parseInt(year) > (YEAR + 1)){ 
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "The year is not allowed"); 

		}else if(StringUtils.isBlank(closingdate)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Closing date can't be empty."); 

		}else if(StringUtils.isBlank(oppeningdate)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Opening date date can't be empty."); 

		}else if(isDate1GraterDate2(stringToDate(closingdate), stringToDate(oppeningdate))){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Opening Date can't come before Closing Date.");
			
		}else if(StringUtils.isBlank(headTeacherComment)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "HeadTeacher's comment can't be empty."); 

		}else if(StringUtils.isBlank(centerId)){
			session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Something went wrong, try again later."); 

		}else{
			
			Config config = configDAO.getConfig(centerId);
			config.setCenterId(centerId);
			config.setClosingdate(closingdate);
			config.setCycleId(cycleId);
			config.setHeadTeacherComment(headTeacherComment);
			config.setOppeningdate(oppeningdate);
			config.setTerm(term);
			config.setYear(year);
			
			if(configDAO.putConfig(config, centerId)){
				 //logger.info(""); 
				session.setAttribute(SessionConstants.CONFIG_UPDATE_SUCCESS, "Config updates successfully."); 
			}else{
				session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, "Something went wrong, try again later."); 
			}
			
		}
		
		session.setAttribute(SessionConstants.CONFIG_PARAM, centerIdHash); 
		response.sendRedirect("cpanel.jsp");  
		return;

	}
	
	/**
	 * @param date1
	 * @param date2
	 */
	private static boolean isDate1GraterDate2(Date date1, Date date2) {
		boolean greater = false;
		if (date1.after(date2)) {
			greater = true;
			} 
		return greater;
	}


	/**
	 * @param dateString
	 */
	private static Date stringToDate(String dateString) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		 Date date = null;
        try {
        	 date = formatter.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
        return date;
	}
	
	
	/**
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {  
		try  
		{  
			Double.parseDouble(str);  

		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}



	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -658159825555241543L;

}

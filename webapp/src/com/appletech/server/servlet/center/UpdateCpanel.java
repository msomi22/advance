/**
 * 
 */
package com.appletech.server.servlet.center;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Division;
import com.appletech.bean.center.Grade;
import com.appletech.persistence.center.DivisionDAO;
import com.appletech.persistence.center.GradeDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author peter
 *
 */
public class UpdateCpanel extends HttpServlet{
	
	private static GradeDAO gradeDAO;
	private static DivisionDAO divisionDAO;
	private Logger logger = Logger.getLogger(this.getClass());

	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		gradeDAO = GradeDAO.getInstance();
		divisionDAO = DivisionDAO.getInstance();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		OutputStream out = response.getOutputStream();
		response.setContentType("application/json;charset=UTF-8");


		String lowerMark = StringUtils.trimToEmpty(request.getParameter("lowerMark"));
		String upperMark = StringUtils.trimToEmpty(request.getParameter("upperMark"));
		String description = StringUtils.trimToEmpty(request.getParameter("description"));
		String points = StringUtils.trimToEmpty(request.getParameter("points"));
		String uuid = StringUtils.trimToEmpty(request.getParameter("uuid"));
		String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
		String toUpdate = StringUtils.trimToEmpty(request.getParameter("toUpdate"));
		
		
		Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();
     

		if(StringUtils.equalsIgnoreCase(toUpdate, "division")){

			out.write(gson.toJson(updateDivision(lowerMark,upperMark,points,uuid,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(toUpdate, "grade")){

			out.write(gson.toJson(updateGrade(lowerMark,upperMark,description,points,uuid,centerId)).getBytes());
			out.flush();
			out.close();

		}
		
	}
	


	
	/**
	 * @param lowerMark
	 * @param upperMark
	 * @param description
	 * @param points
	 * @param uuid
	 * @param centerId
	 * @return
	 */
	private JsonElement updateGrade(String lowerMark, String upperMark, String description, String points, String uuid,
			String centerId) {
		
		JsonObject jsonObject = new JsonObject();
		
		if(StringUtils.isBlank(lowerMark)){
			jsonObject.addProperty("messageResponse", "Error: LowerMark can't be empty.");

		}else if(StringUtils.isBlank(upperMark)){
			jsonObject.addProperty("messageResponse", "Error: UpperMark can't be empty.");
			
		}else if(StringUtils.isBlank(description)){
			jsonObject.addProperty("messageResponse", "Error: Description can't be empty.");

		}else if(StringUtils.isBlank(points)){
			jsonObject.addProperty("messageResponse", "Error: Points can't be empty.");

		}else if(StringUtils.isBlank(uuid)){
			jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");

		}else if(StringUtils.isBlank(centerId)){
			jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");

		}else{
			
			Grade grade = gradeDAO.getGrade(centerId, uuid);
			grade.setCenterId(centerId);
			grade.setDescription(description);
			grade.setLowerMark(lowerMark);
			grade.setPoint(points);
			grade.setUpperMark(upperMark);
			if(gradeDAO.putGrade(grade, centerId, points)){
				 //logger.info(""); 
				jsonObject.addProperty("messageResponse", "Grade Updated successfully.");
				
			}else{
				jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");
				
			}
			
			
		}
		
		return jsonObject;
	}

	/**
	 * @param lowerMark
	 * @param upperMark
	 * @param points
	 * @param uuid
	 * @param centerId
	 * @return
	 */
	private JsonElement updateDivision(String lowerMark, String upperMark, String points, String uuid,
			String centerId) {
		
		JsonObject jsonObject = new JsonObject();
		
		if(StringUtils.isBlank(lowerMark)){
			jsonObject.addProperty("messageResponse", "Error: LowerMark can't be empty.");

		}else if(StringUtils.isBlank(upperMark)){
			jsonObject.addProperty("messageResponse", "Error: UpperMark can't be empty.");
			
		}else if(StringUtils.isBlank(points)){
			jsonObject.addProperty("messageResponse", "Error: Points can't be empty.");

		}else if(StringUtils.isBlank(uuid)){
			jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");

		}else if(StringUtils.isBlank(centerId)){
			jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");

		}else{
			Division division = divisionDAO.getDivision(centerId, uuid);
			division.setCenterId(centerId);
			division.setDescription(points);
			division.setLowerMark(lowerMark);
			division.setUpperMark(upperMark);
			if(divisionDAO.putDivision(division, centerId, points)){
				 //logger.info(""); 
				jsonObject.addProperty("messageResponse", "Division Updated successfully.");
				
			}else{
				jsonObject.addProperty("messageResponse", "Error: Something went wrong, try again.");
				
			}
			
			
		}
		
		return jsonObject;
	}

	
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


}

/**
 * 
 */
package com.appletech.server.servlet.ajax;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Combination;
import com.appletech.bean.center.stm.CombinationDesc;
import com.appletech.bean.center.stm.Subject;
import com.appletech.persistence.stm.CombinationDAO;
import com.appletech.persistence.stm.CombinationDescDAO;
import com.appletech.persistence.stm.SubjectDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class GestSubjects  extends HttpServlet{

	private static CombinationDescDAO combinationDescDAO;
	private static CombinationDAO combinationDAO;
	private static SubjectDAO subjectDAO;
	
	private Logger logger = Logger.getLogger(this.getClass());

	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		combinationDescDAO = CombinationDescDAO.getInstance();
		combinationDAO = CombinationDAO.getInstance();
		subjectDAO = SubjectDAO.getInstance();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OutputStream out = response.getOutputStream();
		response.setContentType("application/json;charset=UTF-8");

		String combinationId = StringUtils.trimToEmpty(request.getParameter("combinationId"));
		String subjectsId = StringUtils.trimToEmpty(request.getParameter("subjectsId"));
		String description = StringUtils.trimToEmpty(request.getParameter("description"));
		String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
		String decision = StringUtils.trimToEmpty(request.getParameter("decision"));

		/* System.out.println("subjectsId : " + subjectsId);
       System.out.println("description : " + description);
       System.out.println("combinationId : " + combinationId);
       System.out.println("centerId : " + centerId);
       System.out.println("decision : " + decision);*/

		Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		if(StringUtils.equalsIgnoreCase(decision, "loadSubjects")){
			out.write(gson.toJson(loadSubjects(centerId, combinationId)).getBytes());
			out.flush();
			out.close();
		}

		if(StringUtils.equalsIgnoreCase(decision, "removeCombination")){
			out.write(gson.toJson(removeCombination(centerId, combinationId)).getBytes());
			out.flush();
			out.close();
		}

		if(StringUtils.equalsIgnoreCase(decision, "addCombination")){
			out.write(gson.toJson(addCombination(centerId, subjectsId,description)).getBytes());
			out.flush();
			out.close();
		}
		
		if(StringUtils.equalsIgnoreCase(decision, "combinationUpdate")){
			out.write(gson.toJson(combinationUpdate(centerId, description,combinationId)).getBytes());
			out.flush();
			out.close();
		}



	}




	/**
	 * @param centerId
	 * @param description
	 * @param combinationId
	 * @return
	 */
	private JsonElement combinationUpdate(String centerId, String description, String combinationId) {
		JsonObject jsonObject = new JsonObject();
		Combination combination = combinationDAO.getCombinationBYUUID(centerId, combinationId);
		combination.setDescription(description.toUpperCase()); 
		
			if(combinationDAO.updateCombination(combination)){
				
				 logger.info(combination + " updated" );
				jsonObject.addProperty("updateMessage", "Combination has been updated successfully.");

			}else{
				jsonObject.addProperty("updateMessage", "Combination not updated, try again later.");

			}
		return jsonObject;
	}

	/**
	 * @param centerId
	 * @param subjectsId
	 * @param description
	 * @return
	 */
	private JsonElement addCombination(String centerId, String subjectsId, String description) {
		JsonObject jsonObject = new JsonObject();

		subjectsId = removeChar(subjectsId);
		String [] subjects = subjectsId.split(",");


		if(subjects.length != 3){
			jsonObject.addProperty("addMessage", "Please select only 3 subjects."); 

		}else if(StringUtils.isEmpty(description)){
			jsonObject.addProperty("addMessage", "Please select a description."); 

		}else if(combinationDAO.getCombination(centerId, description) != null){
			jsonObject.addProperty("addMessage", "The description is already in use."); 

		}else{

			Combination combination = new Combination();
			combination.setCenterId(centerId);
			combination.setDescription(description.toUpperCase()); 

			boolean cominationAdded = false;
			boolean descriptionAdded = false;
			
			if(combinationDAO.putCombination(combination)){
				cominationAdded = true;
			}

            boolean canAddSubject = true;
            
            if(cominationAdded){
			for(String subjectId : subjects){
				if(combinationDescDAO.getCombinationDescBySubjectId(centerId, combination.getUuid(), subjectId) != null){
					canAddSubject = false;
				}
			  }
			}
		
			
			
			if(cominationAdded && canAddSubject){
				for(String subjectId : subjects){
					CombinationDesc combinationDesc = new CombinationDesc();
					combinationDesc.setCenterId(centerId);
					combinationDesc.setCombinationId(combination.getUuid());
					combinationDesc.setSubjectId(subjectId);

					if(combinationDescDAO.putCombinationDesc(combinationDesc)){
						descriptionAdded = true;
						logger.info(combinationDesc +" added" );
					}



				}
			}
			
			
			

			if(cominationAdded && descriptionAdded){
				
				logger.info(combination +" added" );
				jsonObject.addProperty("addMessage", "Combination added successfully."); 

			}else{
				combinationDescDAO.deleteCombination(centerId, combination.getUuid()); 
				combinationDAO.deleteCombination(centerId, combination.getUuid()); 
				jsonObject.addProperty("addMessage", "Something went wrong, try again later."); 

			}

		}


		return jsonObject;
	}

	/**
	 * @param centerId
	 * @param combinationId
	 * @return
	 */
	private JsonElement removeCombination(String centerId, String combinationId) {
		JsonObject jsonObject = new JsonObject();
		if(combinationDescDAO.deleteCombination(centerId, combinationId)){


			if(combinationDAO.deleteCombination(centerId, combinationId)){
				jsonObject.addProperty("removeMessage", "Combination has been removed successfully.");

			}else{
				jsonObject.addProperty("removeMessage", "Combination not removed, try again later.");

			}

		}else{

			jsonObject.addProperty("removeMessage", "Combination not removed, ensure that it's not referenced anywhere.");
		}
		return jsonObject;
	}

	/**
	 * @param centerId
	 * @param combinationId
	 * @return
	 */
	private JsonElement loadSubjects(String centerId, String combinationId) {
		JsonObject jsonObject = new JsonObject();
		List<CombinationDesc> list = new ArrayList<CombinationDesc>();
		List<Subject> subjectList  = new ArrayList<Subject>();
		Map<String,Subject> subjectMap = new HashMap<>();

		String comibation = "";

		if(combinationDAO.getCombinationBYUUID(centerId, combinationId) != null){
			comibation = combinationDAO.getCombinationBYUUID(centerId, combinationId).getDescription(); 
		}


		if(subjectDAO.getAllSubjects(centerId) != null){
			subjectList = subjectDAO.getAllSubjects(centerId);
		}


		for(Subject subject : subjectList){
			subjectMap.put(subject.getUuid(), subject); 
		}



		List<Subject> subjects  = new ArrayList<Subject>();
		list  =  combinationDescDAO.getCombinationDesc(centerId, combinationId);
		CombinationSub combinationSub = new CombinationSub();
		for(CombinationDesc subdescr : list){
			Subject subject = subjectMap.get(subdescr.getSubjectId());
			subjects.add(subject);
		}

		combinationSub.setComibation(comibation);
		combinationSub.setSubjects(subjects);

		Gson gson = new Gson();
		jsonObject.addProperty("subjects", gson.toJson(combinationSub));
		return jsonObject;
	}


	/**
	 * @param subids
	 * @return
	 */
	private String removeChar(String subids) {
		subids = subids.replace("[", "");
		subids = subids.replace("]", "");
		subids = subids.replace("\"", "");
		return subids;
	}

	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 4702759518590729390L;
}

/**
 * 
 */
package com.appletech.server.servlet.ajax;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.student.Student;
import com.appletech.persistence.student.StudentDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class LoadStudents extends HttpServlet{

	private static StudentDAO studentDAO;
	private List<Student> studentList = new ArrayList<Student>(); 

	private Logger logger = Logger.getLogger(this.getClass());

	/**  
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		studentDAO = StudentDAO.getInstance();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OutputStream out = response.getOutputStream();
		response.setContentType("application/json;charset=UTF-8");

		String streamId = StringUtils.trimToEmpty(request.getParameter("streamId"));
		String tostreamId = StringUtils.trimToEmpty(request.getParameter("tostreamId"));
		String studentIds = StringUtils.trimToEmpty(request.getParameter("studentIds"));
		String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
		String decision = StringUtils.trimToEmpty(request.getParameter("decision"));

		/*System.out.println("streamId : " + streamId); 
		System.out.println("tostreamId : " + tostreamId); 
		System.out.println("studentIds : " + studentIds); 
		System.out.println("centerId : " + centerId);
		System.out.println("decision : " + decision); 

		System.out.println("************************************");*/


		Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();


		if(StringUtils.equalsIgnoreCase(decision, "loadActive")){

			out.write(gson.toJson(loadActive(centerId, streamId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "loadInactive")){

			out.write(gson.toJson(loadInactive(centerId, streamId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "moveStudent")){

			out.write(gson.toJson(moveStudent(studentIds,centerId,tostreamId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "isNotAlumni")){

			out.write(gson.toJson(isNotAlumni(studentIds,centerId)).getBytes());
			out.flush();
			out.close();

		}else if(StringUtils.equalsIgnoreCase(decision, "isAlumni")){

			out.write(gson.toJson(isAlumni(studentIds,centerId)).getBytes());
			out.flush();
			out.close();

		}




	}


	/**
	 * @param studentIds
	 * @param centerId
	 * @return
	 */
	private JsonElement isAlumni(String studentIds, String centerId) {
		JsonObject jsonObject = new JsonObject();

		studentIds = removeChar(studentIds);
		String [] students = studentIds.split(",");

		if(StringUtils.isEmpty(studentIds)){

			jsonObject.addProperty("responseMessage", "Something went wrong while moving students to Alumni Register.");  

		}

		int affectedCount = 0;
		boolean hasWorked = false;
		for(String studentId : students){
			Student student = studentDAO.getStudentByUUID(centerId, studentId);
			student.setIsActive("0"); 
			student.setIsAlumnus("1"); 
			if(studentDAO.updateStudent(student)){
				affectedCount++;
				hasWorked = true;
			}
		}

		if(hasWorked){

			logger.info(affectedCount +" Students moved to Alumni Register." );
			jsonObject.addProperty("responseMessage", affectedCount + " Students moved to Alumni Register.");  

		}else{
			jsonObject.addProperty("responseMessage", "Something went wrong however,  " + affectedCount +" moved to Alumni Register.");  

		}


		return jsonObject;
	}


	/**
	 * @param studentIds
	 * @param centerId
	 * @return
	 */
	private JsonElement isNotAlumni(String studentIds, String centerId) {
		JsonObject jsonObject = new JsonObject();

		studentIds = removeChar(studentIds);
		String [] students = studentIds.split(",");

		if(StringUtils.isEmpty(studentIds)){

			jsonObject.addProperty("responseMessage", "Something went wrong while deleting students from Alumni Register.");  

		}

		int affectedCount = 0;
		boolean hasWorked = false;
		for(String studentId : students){
			Student student = studentDAO.getStudentByUUID(centerId, studentId);
			student.setIsActive("1"); 
			student.setIsAlumnus("0"); 
			if(studentDAO.updateStudent(student)){
				affectedCount++;
				hasWorked = true;
			}
		}

		if(hasWorked){
			logger.info(affectedCount +" Students deleted from Alumni Register." );
			jsonObject.addProperty("responseMessage", affectedCount + " Students deleted from Alumni Register.");  

		}else{
			jsonObject.addProperty("responseMessage", "Something went wrong however,  " + affectedCount +" deleted from Alumni Register.");  

		}


		return jsonObject;
	}


	/**
	 * @param studentIds
	 * @param centerId
	 * @return
	 */
	private JsonElement moveStudent(String studentIds, String centerId, String tostreamId) {
		JsonObject jsonObject = new JsonObject();

		studentIds = removeChar(studentIds);
		String [] students = studentIds.split(",");

		if(StringUtils.isEmpty(studentIds)){
			jsonObject.addProperty("responseMessage", "Something went wrong, students not moved.");  

		}else if(StringUtils.isEmpty(tostreamId)){
			jsonObject.addProperty("responseMessage", "Select a class to move students to.");  

		}

		int movedCount = 0;
		boolean hasMoved = false;
		for(String studentId : students){
			Student student = studentDAO.getStudentByUUID(centerId, studentId);
			student.setCurrentStreamId(tostreamId); 
			if(studentDAO.updateStudent(student)){
				movedCount++;
				hasMoved = true;
			}
		}

		if(hasMoved){

			logger.info(movedCount +"  Students moved successfully to stream " + tostreamId);
			jsonObject.addProperty("responseMessage", movedCount + " Students moved successfully.");  

		}else{
			jsonObject.addProperty("responseMessage","Something went wrong however, " + movedCount + " moved.");  

		}


		return jsonObject;
	}


	/**
	 * @param centerId
	 * @param streamId
	 * @return
	 */
	private JsonElement loadActive(String centerId, String currentStreamId) {
		JsonObject jsonObject = new JsonObject();

		if(studentDAO.getStudentByStreamId(centerId, currentStreamId) != null){
			studentList = studentDAO.getStudentByStreamId(centerId, currentStreamId);
		}

		AjaxStudent ajaxStudent = new AjaxStudent();
		List<Student> studentList2 = new ArrayList<>();

		for(Student student : studentList){
			if(StringUtils.equals(student.getIsActive(), "1")) { 
				studentList2.add(student);
			}

		}

		ajaxStudent.setStudents(studentList2);
		Gson gson = new Gson();
		jsonObject.addProperty("students", gson.toJson(ajaxStudent));

		return jsonObject;
	}

	/**
	 * @param centerId
	 * @param streamId
	 * @return
	 */
	private JsonElement loadInactive(String centerId, String currentStreamId) {
		JsonObject jsonObject = new JsonObject();

		if(studentDAO.getStudentByStreamId(centerId, currentStreamId) != null){
			studentList = studentDAO.getStudentByStreamId(centerId, currentStreamId);
		}

		AjaxStudent ajaxStudent = new AjaxStudent();
		List<Student> studentList2 = new ArrayList<>();

		for(Student student : studentList){
			if(StringUtils.equals(student.getIsActive(), "0")) {
				studentList2.add(student);
			}

		}

		ajaxStudent.setStudents(studentList2);
		Gson gson = new Gson();
		jsonObject.addProperty("students", gson.toJson(ajaxStudent));

		return jsonObject;
	}


	/**
	 * @param subids
	 * @return
	 */
	private String removeChar(String subids) {
		subids = subids.replace("[", "");
		subids = subids.replace("]", "");
		subids = subids.replace("\"", "");
		//StringUtils.stripEnd(names, ",");
		return subids.replaceAll("on,", ""); 
	}



	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}







	/**
	 * 
	 */
	private static final long serialVersionUID = 3016819006909898421L;

}

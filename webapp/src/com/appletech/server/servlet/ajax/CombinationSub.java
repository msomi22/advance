/**
 * 
 */
package com.appletech.server.servlet.ajax;

import java.util.ArrayList;
import java.util.List;

import com.appletech.bean.center.stm.Subject;

/**
 * @author peter
 *
 */
public class CombinationSub {
	
 private String comibation;	
 private List<Subject> subjects;

	/**
	 * 
	 */
	public CombinationSub() {
		comibation = "";
		subjects = new ArrayList<>();
	}

	public String getComibation() {
		return comibation;
	}

	public void setComibation(String comibation) {
		this.comibation = comibation;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "CombinationSub [comibation=" + comibation + ", subjects=" + subjects + "]";
	}
	

}

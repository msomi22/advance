package com.appletech.server.servlet.ajax;

import java.util.ArrayList;
import java.util.List;

import com.appletech.bean.center.student.Student;

public class AjaxStudent {
	
	private List<Student> students;
	
	public AjaxStudent(){
		students = new ArrayList<>();
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "AjaxStudent [students=" + students + "]";
	}
	
	

}

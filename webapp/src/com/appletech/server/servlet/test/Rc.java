/**
 * 
 */
package com.appletech.server.servlet.test;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Center;
import com.appletech.bean.center.Config;
import com.appletech.bean.center.Contact;
import com.appletech.bean.center.Grade;
import com.appletech.bean.center.performance.Performance;
import com.appletech.bean.center.performance.Result;
import com.appletech.bean.center.stm.Stream;
import com.appletech.bean.center.stm.Subject;
import com.appletech.bean.center.student.Student;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.center.ContactDAO;
import com.appletech.persistence.center.GradeDAO;
import com.appletech.persistence.performance.PerformanceDAO;
import com.appletech.persistence.stm.ClassroomDAO;
import com.appletech.persistence.stm.CombinationDAO;
import com.appletech.persistence.stm.CombinationDescDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.appletech.persistence.stm.SubjectDAO;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.servlet.performance.ResultComparator;
import com.appletech.server.session.SessionConstants;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/** 
 * @author cisco
 *
 */
public class Rc extends HttpServlet {

	private Font smallfont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
	Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
	
	private static final String USER_SYSTEM = System.getProperty("user.name");
	private static final String LOGO_PATH = "/home/"+USER_SYSTEM+"/school/logo/logo.png";

	private Document document;
	private PdfWriter writer;

	private static PerformanceDAO performanceDAO;
	public static CombinationDescDAO combinationDescDAO;
	public static StudentDAO studentDAO;
	public static StreamDAO streamDAO;
	public static SubjectDAO subjectDAO;
	public static CombinationDAO combinationDAO; 
	private static ConfigDAO configDAO;
	private static CenterDAO centerDAO;
	public static ClassroomDAO classroomDAO;
	private static ContactDAO contactDAO;
	private static GradeDAO gradeDAO;

	private Logger logger;

	private Map<String, Student> studentMap = new HashMap<String, Student>();
	private Map<String, Stream> streamMap = new HashMap<String, Stream>();
	

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		performanceDAO = PerformanceDAO.getInstance();
		combinationDescDAO = CombinationDescDAO.getInstance();
		studentDAO = StudentDAO.getInstance();
		streamDAO = StreamDAO.getInstance();
		subjectDAO = SubjectDAO.getInstance();
		combinationDAO = CombinationDAO.getInstance();
		configDAO = ConfigDAO.getInstance();
		centerDAO = CenterDAO.getInstance();
		classroomDAO = ClassroomDAO.getInstance();
		contactDAO = ContactDAO.getInstance();
		gradeDAO = GradeDAO.getInstance();

		logger = Logger.getLogger(this.getClass());

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ServletContext context = getServletContext();
		HttpSession session = request.getSession(true);
		response.setContentType("application/pdf");
		
		String centerId = ""; 
		centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
		String classroomId = StringUtils.trimToEmpty(request.getParameter("ClassroomId"));
		
		// logger.info("classroomId : " + classroomId); 
		
		List<Student> studentList = new ArrayList<>();
		List<Stream> streams = streamDAO.getStreamPerClass(centerId, classroomId);
		
		for (Stream strm : streams) {
			List<Student> studentListStream = studentDAO.getStudentByStreamId(centerId, strm.getUuid());
			studentList.addAll(studentListStream);
		}

		List<Student> students = new ArrayList<>();
		students = studentDAO.getAllStudent(centerId);
		for (Student student : students) {
			studentMap.put(student.getUuid(), student);
		}

		List<Stream> streamlist = new ArrayList<>();
		streamlist = streamDAO.getAllStreams(centerId);
		for (Stream stream : streamlist) {
			streamMap.put(stream.getUuid(), stream);
		}
		
		String fileName = "file.pdf"; 
		response.setHeader("Content-Disposition", "inline; filename=\""+fileName);

		document = new Document(PageSize.A4, 46, 46, 64, 64);

		try {
			writer = PdfWriter.getInstance(document, response.getOutputStream());
			PdfUtil event = new PdfUtil();

			writer.setBoxSize("art", new Rectangle(46, 64, 559, 788));
			writer.setPageEvent(event);

			populatePDFDocument(studentList,centerId,classroomId);

		} catch (DocumentException e) {
			logger.error("DocumentException while writing into the document");
			logger.error(ExceptionUtils.getStackTrace(e));
		}

	}

	private void populatePDFDocument(List<Student> studentList, String centerId, String classroomId) {

		try {
			Config config = configDAO.getConfig(centerId);
			String term = config.getTerm();
			String year = config.getYear();
			String closingDate = config.getClosingdate();
			String openingdate = config.getOppeningdate();
			String headteacherRemarks = config.getHeadTeacherComment();
			String teacherRemarks = "";
			Center center = centerDAO.getCenterById(centerId);
			String class_str = classroomDAO.getClassroomById(centerId, classroomId).getDescription(); 
			List<Contact> contacts = contactDAO.getContactList(centerId);
			String form = "";
			
			String mobiles = "";
			int i = 1;
			for(Contact contact : contacts){
				mobiles += " +255 " + contact.getMobile();
				if(i > 3 || i >= contacts.size() ){
					break;
				}
				i++;
				mobiles += " /";
			}
			
			
			// step 1

			// step 2
			PdfWriter.getInstance(document, new FileOutputStream("res.pdf"));
			// step 3
			document.open();
			// step 4
			
			document.add(new Paragraph(",")); 

			String topContent = "    P.O Box: " + center.getPostalAddress() + " " + center.getTown()+" "
					+ ",Cell:" + mobiles.toString() + "\n"
					+ "   Website:" + center .getWebsite() + "             EMAIL: " + center.getEmail(); 
		
			
			

			Image img = Image.getInstance(LOGO_PATH);
			img.scaleAbsolute(65, 65);
			img.setAbsolutePosition(250f,773f);
			
			
			

			List<Result> list = new ArrayList<>();
			list = ComputePerformanceCycles(studentList,centerId);
			//System.out.println("list : " + list);
			if(list !=null){
			  list = getListComplete(list);
			}
			if(list !=null){
			  Collections.sort(list, new ResultComparator());
			}
			
			int stucount = 0;
			for (Result result : list) {
				if (!StringUtils.isEmpty(result.getStudentId())) {
					if (result.getIscomplete().equalsIgnoreCase("Complete")) {
						stucount++;
					}
				}
			}
			
			

			int position = 1;
			for (Result result : list) {
				
				//System.out.println("result : " + result.getCycle1meanGrade());

				// Populate student details
				if (!StringUtils.isEmpty(result.getStudentId())) {
					if (result.getIscomplete().equalsIgnoreCase("Complete")) {
						

						Student student = studentMap.get(result.getStudentId());
						String name = "";
						name = student.getFirstname() + " " + student.getMiddlename() + " " + student.getLastname();
						Stream stream = streamDAO.getStreambyUuid(centerId,student.getCurrentStreamId());
						String streamStr = "";
						String division = result.getDivision(); 
						
						
						if (stream != null) {
							streamStr = stream.getDescription();
						}
						
						String form_str = streamDAO.getStreambyUuid(centerId, student.getCurrentStreamId()).getDescription();
						
						form = class_str + " : " + form_str;
						
						document.add(img);
						document.add(new Paragraph("                                                     "
								+ center.getCenterName().toUpperCase(),
								boldFont));   
						document.add(new Paragraph("                                                                                "
								+ "("+center.getCenterCode()+")",
								smallfont));

						document.add(new Paragraph(topContent, smallfont));

						document.add(new Paragraph(
								"__________________________________________________________________________"));

						document.add(new Paragraph(
								"                                                        "
								+ "SCHOOL PROGRESS REPORT", boldFont));
						
					

						
						PdfPTable frontContentTable = new PdfPTable(2);  
						frontContentTable.setWidthPercentage(100); 
						frontContentTable.setWidths(new int[]{140,140}); 
						
						Phrase studentPhrase = new Phrase();
						studentPhrase.add(new Chunk("Student Name : ",  boldFont));
						studentPhrase.add(new Chunk(name,  smallfont));
						studentPhrase.add(new Chunk("\n"));
						
						studentPhrase.add(new Chunk("Index No          : ",  boldFont));
						studentPhrase.add(new Chunk(student.getIndexNo(),  smallfont));
						studentPhrase.add(new Chunk("\n"));
						
						studentPhrase.add(new Chunk("Form                : ",  boldFont));
						studentPhrase.add(new Chunk(form,  smallfont));
						studentPhrase.add(new Chunk("\n"));
						
						studentPhrase.add(new Chunk("Stream             : ",  boldFont));
						studentPhrase.add(new Chunk(streamStr,  smallfont));
						studentPhrase.add(new Chunk("\n"));
						
						studentPhrase.add(new Chunk("Division           : ",  boldFont));
						studentPhrase.add(new Chunk(division,  smallfont));
						studentPhrase.add(new Chunk("\n"));
						
						
						Phrase termPhrase = new Phrase();
						termPhrase.add(new Chunk("Term                   : ",  boldFont));
						termPhrase.add(new Chunk(term,  smallfont));
						termPhrase.add(new Chunk("\n"));
						
						termPhrase.add(new Chunk("Year                    : ",  boldFont));
						termPhrase.add(new Chunk(year,  smallfont));
						termPhrase.add(new Chunk("\n"));
						
						termPhrase.add(new Chunk("Overall position : ",  boldFont));
						termPhrase.add(new Chunk(String.valueOf(position),  smallfont));
						termPhrase.add(new Chunk("                   Out of : ",  boldFont));
						termPhrase.add(new Chunk(String.valueOf(stucount),  smallfont)); 
						termPhrase.add(new Chunk("\n"));
						
						termPhrase.add(new Chunk("Stream position : ",  boldFont));
						termPhrase.add(new Chunk("",  smallfont));
						termPhrase.add(new Chunk("                      Out of : ",  boldFont));
						termPhrase.add(new Chunk("",  smallfont));
						termPhrase.add(new Chunk("\n"));
						
						Phrase outofPhrase = new Phrase();
						outofPhrase.add(new Chunk("\n\n"));
						
						PdfPCell cellOne = new PdfPCell(studentPhrase);
						PdfPCell cellTwo = new PdfPCell(termPhrase);
						
						
						cellOne.setBorder(Rectangle.NO_BORDER);
						cellTwo.setBorder(Rectangle.NO_BORDER);
						
						
						frontContentTable.addCell(cellOne);
						frontContentTable.addCell(cellTwo);

						
						document.add(frontContentTable);
						
						document.add(new Paragraph("\n"));


						PdfPTable table = new PdfPTable(7);
						table.setWidthPercentage(100);

						PdfPCell c1 = new PdfPCell(new Phrase("SUBJECT", smallfont));
						table.addCell(c1);

						PdfPCell c2 = new PdfPCell(new Phrase("Cycle 1 (%) ", smallfont));
						table.addCell(c2);
						PdfPCell c3 = new PdfPCell(new Phrase("Cycle 2 (%)", smallfont));
						table.addCell(c3);
						PdfPCell cext = new PdfPCell(new Phrase("Cycle 3 (%)", smallfont));
						table.addCell(cext);
						PdfPCell c4 = new PdfPCell(new Phrase("AVERAGE (%)", smallfont));
						table.addCell(c4);
						PdfPCell c5 = new PdfPCell(new Phrase("REMARKS", smallfont));
						table.addCell(c5);
						PdfPCell c6 = new PdfPCell(new Phrase("INITIALS", smallfont));
						table.addCell(c6);

						List<Subject> subjectsList = subjectDAO.getAllSubjects(centerId);

						Map<String, Integer> subScore = result.getSubScore();
						Map<String, Integer> cycle1scoresMap = result.getCycle1scoreMap();
						Map<String, Integer> cycle2scoresMap = result.getCycle2scoreMap();
						Map<String, Integer> cycle3scoresMap = result.getCycle3scoreMap();

						for (Subject subject : subjectsList) {
							String grade = "";
							String points = "";
							String remarks = "";
							
							String gradecycle1 = "";
							String pointscycle1 = "";
							
							
							String gradecycle2 = "";
							String pointscycle2 = "";
							
							
							String gradecycle3 = "";
							String pointscycle3 = "";
							
							

							String score = String.valueOf(subScore.get(subject.getUuid()));
							String cycle1score = String.valueOf(cycle1scoresMap.get(subject.getUuid()));
							String cycle2score = String.valueOf(cycle2scoresMap.get(subject.getUuid()));
							String cycle3score = String.valueOf(cycle3scoresMap.get(subject.getUuid()));
							
							if(StringUtils.equals(cycle1score, "0")){
								cycle1score = "";
							}
							if(StringUtils.equals(cycle2score, "0")){
								cycle2score = "";
							}
							if(StringUtils.equals(cycle3score, "0")){
								cycle3score = "";
							}

							

							if (score.equalsIgnoreCase("null") || cycle1score.equalsIgnoreCase("null") || cycle2score.equalsIgnoreCase("null") || cycle3score.equalsIgnoreCase("null") ) {
								score = "";
								cycle1score = "";
								cycle2score = "";
								cycle3score = "";
							} else {
								grade = getGrade(subScore.get(subject.getUuid()));
								gradecycle1 = getGrade(cycle1scoresMap.get(subject.getUuid()));
								gradecycle2 = getGrade(cycle2scoresMap.get(subject.getUuid()));
								gradecycle3 = getGrade(cycle3scoresMap.get(subject.getUuid()));
								
								if(StringUtils.equals(gradecycle1, "0")){
									gradecycle1 = "";
								}
								if(StringUtils.equals(gradecycle2, "0")){
									gradecycle2 = "";
								}
								if(StringUtils.equals(gradecycle3, "0")){
									gradecycle3 = "";
								}
								
								points = String.valueOf(getPoints(subScore.get(subject.getUuid())));
								pointscycle1 = String.valueOf(getPoints(cycle1scoresMap.get(subject.getUuid())));
								pointscycle2 = String.valueOf(getPoints(cycle2scoresMap.get(subject.getUuid())));
								pointscycle3 = String.valueOf(getPoints(cycle3scoresMap.get(subject.getUuid())));
								
								if(StringUtils.equals(pointscycle1, "0")){
									pointscycle1 = "";
								}
								if(StringUtils.equals(pointscycle2, "0")){
									pointscycle2 = "";
								}
								if(StringUtils.equals(pointscycle3, "0")){
									pointscycle3 = "";
								}
								
								remarks = getRemarks(subScore.get(subject.getUuid()));
							}

							
							
							
						
							PdfPCell r0 = new PdfPCell(new Phrase(subject.getSubjectCode(), smallfont));
							table.addCell(r0);

							PdfPCell r2 = new PdfPCell(new Phrase("   " + cycle1score + " " + gradecycle1 + " " + pointscycle1, smallfont));
							table.addCell(r2);
							PdfPCell r3 = new PdfPCell(new Phrase("   " + cycle2score + " " + gradecycle2 + " " + pointscycle2, smallfont));
							table.addCell(r3);

							PdfPCell cycle3 = new PdfPCell(new Phrase("   " + cycle3score + " " + gradecycle3 + " " + pointscycle3, smallfont));
							table.addCell(cycle3);

							PdfPCell r4 = new PdfPCell(new Phrase("   " + score + " " + grade + " " + points, smallfont));
							table.addCell(r4);
							PdfPCell r5 = new PdfPCell(new Phrase(remarks, smallfont));
							table.addCell(r5);
							PdfPCell r6 = new PdfPCell(new Phrase("", smallfont));
							table.addCell(r6);

						}

						String[] grades = { "TOTAL", "OUT OF", "MEAN SCORE", "MEAN GRADE" };
						int count = 0;
						for (String grade1 : grades) {
							
							String grade2 = "";
							if(!StringUtils.equals(grade1, "0")){
								grade2 = grade1;
							}
							

							PdfPCell r0 = new PdfPCell(new Phrase(grade2, boldFont));
							table.addCell(r0);
							

							if (count == 0) {
								
								String c1totalPoints = "";
								String c2totalPoints = "";
								String c3totalPoints = "";
								String totalPoints = "";
								
								if(result.getCycle1totalPoints() > 0){
									c1totalPoints = String.valueOf(result.getCycle1totalPoints());
								}
								
								if(result.getCycle2totalPoints() > 0){
									c2totalPoints = String.valueOf(result.getCycle2totalPoints());
								}
								
								if(result.getCycle3totalPoints() > 0){
									c3totalPoints = String.valueOf(result.getCycle3totalPoints());
								}
								
								if(result.getTotalPoints() > 0){
									totalPoints = String.valueOf(result.getTotalPoints());
								}
								
								
								PdfPCell r2 = new PdfPCell(new Phrase("   " + c1totalPoints, smallfont));
								table.addCell(r2);
								
								PdfPCell r3 = new PdfPCell(new Phrase("   " + c2totalPoints, smallfont));
								table.addCell(r3);

								PdfPCell cycle3 = new PdfPCell(new Phrase("   " + c3totalPoints, smallfont));
								table.addCell(cycle3);
								
								PdfPCell r4 = new PdfPCell(new Phrase(String.valueOf("   " + totalPoints), smallfont));
								table.addCell(r4);
								
								
							} else if(count == 2) {
								
								String c1meanscore = "";
								String c2meanscore = "";
								String c3meanscore = "";
								String avgmeanscore = "";
								
								if(result.getCycle1meanScore() > 0){
									c1meanscore = String.valueOf(result.getCycle1meanScore());
								}
								
								if(result.getCycle2meanScore() > 0){
									c2meanscore = String.valueOf(result.getCycle2meanScore());
								}
								
								if(result.getCycle3meanScore() > 0){
									c3meanscore = String.valueOf(result.getCycle3meanScore());
								}
								
								if(result.getAvgmeanScore() > 0){
									avgmeanscore = String.valueOf(result.getAvgmeanScore());
								}
								

								PdfPCell r2 = new PdfPCell(new Phrase("   " + c1meanscore, smallfont));
								table.addCell(r2);
								PdfPCell r3 = new PdfPCell(new Phrase("   " + c2meanscore, smallfont));
								table.addCell(r3);

								PdfPCell cycle3 = new PdfPCell(new Phrase("   " + c3meanscore, smallfont));
								table.addCell(cycle3);
								PdfPCell r4 = new PdfPCell(new Phrase("   " + avgmeanscore, smallfont));
								table.addCell(r4);
								teacherRemarks = getRemarks(result.getAvgmeanScore());
								
								
							} else if(count == 3){
								
								PdfPCell r2 = new PdfPCell(new Phrase("   " + result.getCycle1meanGrade(), smallfont));
								table.addCell(r2);
								PdfPCell r3 = new PdfPCell(new Phrase("   " + result.getCycle2meanGrade(), smallfont));
								table.addCell(r3);

								PdfPCell cycle3 = new PdfPCell(new Phrase("   " + result.getCycle3meanGrade(), smallfont));
								table.addCell(cycle3);
								PdfPCell r4 = new PdfPCell(new Phrase("   " + result.getAvgmeanGrade(), smallfont));
								table.addCell(r4);
								
								
							}else{
								PdfPCell r2 = new PdfPCell(new Phrase("", smallfont));
								table.addCell(r2);
								PdfPCell r3 = new PdfPCell(new Phrase("", smallfont));
								table.addCell(r3);

								PdfPCell cycle3 = new PdfPCell(new Phrase("", smallfont));
								table.addCell(cycle3);
								PdfPCell r4 = new PdfPCell(new Phrase("", smallfont));
								table.addCell(r4);
								
							}
							

							PdfPCell r5 = new PdfPCell(new Phrase("", smallfont));
							table.addCell(r5);
							PdfPCell r6 = new PdfPCell(new Phrase("", smallfont));
							table.addCell(r6);
							count++;

						}

						document.add(table);

						Chunk underline = new Chunk("GENERAL COMMENTS. ", boldFont);
						underline.setUnderline(0.1f, -2f); // 0.1 thick, -2
															// y-location

						document.add(underline);
						
						Phrase teacherremarkphrase = new Phrase();
						teacherremarkphrase.add(new Chunk("CLASS TEACHER'S REMARKS:",  boldFont));
						teacherremarkphrase.add(new Chunk("  " + teacherRemarks,  smallfont));
						teacherremarkphrase.add(new Chunk("\n"));
						
						Phrase headteacherremarkphrase = new Phrase();
						headteacherremarkphrase.add(new Chunk("Head TEACHER'S REMARKS:",  boldFont));
						headteacherremarkphrase.add(new Chunk("  " + headteacherRemarks,  smallfont));
						headteacherremarkphrase.add(new Chunk("\n"));
						
						Phrase datesphrase = new Phrase();
						datesphrase.add(new Chunk("SCHOOL CLOSES ON:",  boldFont));
						datesphrase.add(new Chunk("  " + closingDate,  smallfont));
						datesphrase.add(new Chunk("               NEXT TERM BEGINS ",  boldFont));
						datesphrase.add(new Chunk(" " + openingdate,  smallfont));
						datesphrase.add(new Chunk("\n"));
						
						Phrase signaturephrase = new Phrase();
						signaturephrase.add(new Chunk("SIGNATURE:",  boldFont));
						signaturephrase.add(new Chunk("__________________",  smallfont));
						signaturephrase.add(new Chunk("              DATE AND STAMP",  boldFont));
						signaturephrase.add(new Chunk("__________________",  smallfont));
						signaturephrase.add(new Chunk("\n"));


						document.add(new Paragraph(teacherremarkphrase));

						document.add(new Paragraph(headteacherremarkphrase));
						
						document.add(new Paragraph(datesphrase));
						
						document.add(new Paragraph(signaturephrase));


						
						
						PdfPTable gradesTable = new PdfPTable(3);
						PdfPCell rangecell = new PdfPCell(new Phrase("Mark Range" , boldFont));
						gradesTable.addCell(rangecell);
						PdfPCell gradescell = new PdfPCell(new Phrase("Grades" , boldFont));
						gradesTable.addCell(gradescell);
						PdfPCell remarksscell = new PdfPCell(new Phrase("Points" , boldFont));
						gradesTable.addCell(remarksscell);
						
						
						List<Grade> gradeScale = new ArrayList<>();
						if(gradeDAO.getGradeList(centerId) != null){
							gradeScale = gradeDAO.getGradeList(centerId);
						}
						for(Grade grade : gradeScale){
							
							rangecell = new PdfPCell(new Phrase(grade.getLowerMark() + " - " + grade.getUpperMark(), smallfont));
							gradesTable.addCell(rangecell);
							
							gradescell = new PdfPCell(new Phrase(grade.getDescription() + "" , smallfont));
							gradesTable.addCell(gradescell);
							
							remarksscell = new PdfPCell(new Phrase(grade.getPoint() + " " , smallfont));
							gradesTable.addCell(remarksscell);
						}
						
						document.add(new Paragraph("\n"));
						
						document.add(gradesTable);
						

						document.newPage();

						position++;

					}

				}

			} 
			// last step
			document.close();

		} catch (DocumentException e) {
			logger.error("DocumentException while writing into the document");
			logger.error(ExceptionUtils.getStackTrace(e));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param studentList
	 * @return
	 */
	public static List<Result> ComputePerformance(List<Student> studentList,String centerId) {

		List<Result> resultList = new ArrayList<>();
		
		String term = configDAO.getConfig(centerId).getTerm();
		String year = configDAO.getConfig(centerId).getYear();

		int totalpoint = 0;
		
		String isComplete = "";
		String isAbsent = "";

		if (studentList != null) {
			List<Performance> studentperformancelist = null;
			for (Student student : studentList) {

				studentperformancelist = performanceDAO.getStudentPerformance(centerId, student.getUuid(), term, year);
				Result result = new Result();
				Map<String, Integer> subscoreMap = new HashMap<>();

				if (!studentperformancelist.isEmpty()) {

					int isaMainCount = 0;

					for (Performance performance : studentperformancelist) {

						int point = getPoints(performance.getCycleOneScore());

						if (combinationDescDAO.getCombinationDescBySubjectId(centerId, student.getCombinationId(),
								performance.getSubjectId()) != null) {
							totalpoint += point;
							isaMainCount++;
						}

						subscoreMap.put(performance.getSubjectId(), performance.getCycleOneScore());
						result.setSubScore(subscoreMap);
					}
					if (isaMainCount < 3) {
						isComplete = "Incomplete";
						result.setIscomplete(isComplete);
					}

					result.setDivision(getDivision(totalpoint));
					result.setHasPassed("status");
					result.setStudentId(student.getUuid());
					result.setTotalPoints(totalpoint);
					totalpoint = 0;

				} else {
					isAbsent = "Absent";
					result.setIsabsent(isAbsent);
				}

				resultList.add(result);
			}
		}
		return resultList;
	}

	/**
	 * @param studentList
	 * @return
	 */
	public static List<Result> ComputePerformanceCycles(List<Student> studentList,String centerId) {

		List<Result> resultList = new ArrayList<>();
		
		String term = configDAO.getConfig(centerId).getTerm();
		String year = configDAO.getConfig(centerId).getYear();

		int totalpoint = 0;
		int totalpointcycle1 = 0;
		int totalpointcycle2 = 0;
		int totalpointcycle3 = 0;
		
		int avgtotalMarks = 0;
		int cycle1totalMarks = 0;
		int cycle2totalMarks = 0;
		int cycle3totalMarks = 0;
		
		
		String isComplete = "";
		String isAbsent = "";

		if (studentList != null) {
			List<Performance> studentperformancelist = null;
			for (Student student : studentList) {

				studentperformancelist = performanceDAO.getStudentPerformance(centerId, student.getUuid(), term, year);
				Result result = new Result();
				Map<String, Integer> subscoreMap = new HashMap<>();
				Map<String, Integer> cycle1scoreMap = new HashMap<>();
				Map<String, Integer> cycle2scoreMap = new HashMap<>();
				Map<String, Integer> cycle3scoreMap = new HashMap<>();

				if (!studentperformancelist.isEmpty()) {

					int isaMainCount = 0;

					for (Performance performance : studentperformancelist) {
						
						int cycle1score = performance.getCycleOneScore();
						int cycle2score = performance.getCycleTwoScore();
						int cycle3score = performance.getCycleThreeScore();
						
						int value = cycle1score + cycle2score + cycle3score;
						
						double dvalue = value / 3.0;
						
						int Averagescore = (int) Math.round(dvalue);
						
						int point = getPoints(Averagescore);
						int cycle1point = getPoints(cycle1score);
						int cycle2point = getPoints(cycle2score);
						int cycle3point = getPoints(cycle3score);
						
						if (combinationDescDAO.getCombinationDescBySubjectId(centerId, student.getCombinationId(),
								performance.getSubjectId()) != null) {
							totalpoint += point;
							
							if(cycle1point > 0){
								totalpointcycle1 += cycle1point;
							}
							if(cycle2point > 0){
								totalpointcycle2 += cycle2point;
							}
							if(cycle3point > 0){
								totalpointcycle3 += cycle3point;
							}
							
							
							avgtotalMarks += Averagescore;
							if(cycle1score > 0){
								cycle1totalMarks += cycle1score;
							}
							if(cycle2score > 0){
								cycle2totalMarks += cycle2score;
							}
							if(cycle3score > 0){
								cycle3totalMarks += cycle3score;
							}
							
							isaMainCount++;
							
						}

						

						subscoreMap.put(performance.getSubjectId(), Averagescore);
						cycle1scoreMap.put(performance.getSubjectId(), cycle1score);
						cycle2scoreMap.put(performance.getSubjectId(), cycle2score);
						cycle3scoreMap.put(performance.getSubjectId(), cycle3score);
						result.setSubScore(subscoreMap);
						result.setCycle1scoreMap(cycle1scoreMap);
						result.setCycle2scoreMap(cycle2scoreMap);
						result.setCycle3scoreMap(cycle3scoreMap);
					}
					if (isaMainCount < 3) {
						isComplete = "Incomplete";
						result.setIscomplete(isComplete);
					}

					result.setDivision(getDivision(totalpoint));
					result.setHasPassed("status");
					result.setStudentId(student.getUuid());
					result.setTotalPoints(totalpoint);
					result.setCycle1totalPoints(totalpointcycle1);
					result.setCycle2totalPoints(totalpointcycle2);
					result.setCycle3totalPoints(totalpointcycle3);
					
					result.setAvgmeanScore(avgtotalMarks/3);
					result.setCycle1meanScore(cycle1totalMarks/3);
					result.setCycle2meanScore(cycle2totalMarks/3);
					result.setCycle3meanScore(cycle3totalMarks/3);
					
					result.setAvgmeanGrade(getGrade(avgtotalMarks/3));
					result.setCycle1meanGrade(getGrade(cycle1totalMarks/3));
					result.setCycle2meanGrade(getGrade(cycle2totalMarks/3));
					result.setCycle3meanGrade(getGrade(cycle3totalMarks/3));
					
					
					totalpoint = 0;
					totalpointcycle1 = 0;
					totalpointcycle2 = 0;
					totalpointcycle3 = 0;
					
					avgtotalMarks = 0;
					cycle1totalMarks = 0;
					cycle2totalMarks = 0;
					cycle3totalMarks = 0;

				} else {
					isAbsent = "Absent";
					result.setIsabsent(isAbsent);
				}
				//System.out.println("result: " + result.getCycle1meanGrade() + " -- " +result.getCycle1meanScore() );
				resultList.add(result);
				
			}
		}
		
		//System.out.println("resultList: " + resultList);
		return resultList;
	}

	
	/**
	 * @param img
	 * @param newW
	 * @param newH
	 * @return
	 */
	public static BufferedImage resize(BufferedImage img, int newW, int newH) {
		java.awt.Image tmp = img.getScaledInstance(newW, newH, java.awt.Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return dimg;
	}

	/**
	 * @param score
	 * @return
	 */
	private static String getGrade(int score) {

		if (score > 100) {
			score = 0;
		}

		String grade = "";

		if (score >= 80) {
			// A 1
			grade = "A";
		} else if (score >= 70) {
			// B 2
			grade = "B";
		} else if (score >= 60) {
			// C 3
			grade = "C";
		} else if (score >= 50) {
			// D 4
			grade = "D";
		} else if (score >= 40) {
			// E 5
			grade = "E";
		} else if (score >= 35) {
			// S 6
			grade = "S";
		} else if (score >= 1) {
			// S 6
			grade = "F";
		} else {
			// F 7
			grade = "";
		}
		return grade;
	}

	/**
	 * @param score
	 * @return
	 */
	private static int getPoints(int score) {
		int point = 0;
		if (score >= 80) {
			// A 1
			point = 1;
		} else if (score >= 70) {
			// B 2
			point = 2;
		} else if (score >= 60) {
			// C 3
			point = 3;
		} else if (score >= 50) {
			// D 4
			point = 4;
		} else if (score >= 40) {
			// E 5
			point = 5;
		} else if (score >= 35) {
			// S 6
			point = 6;
		} else if(score > 0) {
			// F 7
			point = 7;
		}
		return point;
	}

	/**
	 * @param points
	 * @return
	 */
	public static String getDivision(int points) {
		String division = "";
		if (points >= 21) {
			// O
			division = "O";
		} else if (points >= 18) {
			// IV
			division = "IV";
		} else if (points >= 13) {
			// III
			division = "III";
		} else if (points >= 10) {
			// II
			division = "II";
		} else if (points >= 1) {
			// I
			division = "I";
		}
		return division;
	}

	/**
	 * @param getRemarks
	 * @return
	 */
	public static String getRemarks(int score) {
		String remark = "";
		if (score >= 80) {
			// A 1
			remark = "Excellent";
		} else if (score >= 70) {
			// B 2
			remark = "Very good";
		} else if (score >= 60) {
			// C 3
			remark = "Relatively good";
		} else if (score >= 50) {
			// D 4
			remark = "Good";
		} else if (score >= 40) {
			// E 5
			remark = "Fair";
		} else if (score >= 35) {
			// S 6
			remark = "Poor";
		} else if  (score > 0){
			// F 7
			remark = "Very poor";
		}
		return remark;
	}

	/**
	 * Get the student who did a complete exams
	 * 
	 * @param list
	 * @return
	 */
	public static List<Result> getListComplete(List<Result> list) {
		List<Result> resultList = new ArrayList<>();
		for (Result result : list) {
			if (result.getIscomplete().equalsIgnoreCase("Complete")) {
				resultList.add(result);
			}
		}

		return resultList;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3167332773123573788L;

}

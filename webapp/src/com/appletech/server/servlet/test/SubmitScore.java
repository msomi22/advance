/**
 * 
 */
package com.appletech.server.servlet.test;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.performance.CycleOneScore;
import com.appletech.bean.center.performance.CycleThreeScore;
import com.appletech.bean.center.performance.CycleTwoScore;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.center.CycleDAO;
import com.appletech.persistence.performance.PerformanceDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.session.SessionConstants;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class SubmitScore  extends HttpServlet{

	private static PerformanceDAO performanceDAO;
	public static StudentDAO studentDAO;
	public static StreamDAO streamDAO;
	private static ConfigDAO configDAO;
	private static CycleDAO cycleDAO;

	private final String ERROR_MESSAGE = "Something went wrong, score not submited.";
	private final String ERROR_EMPTY_SCORE = "Please provide te score.";
	private final String ERROR_INVALID_SCORE = "The score you entered is not valid.";
	//private final String SUCCESS_SCORE_SUBMITTED = "The score was submitted successfully.";


	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		performanceDAO = PerformanceDAO.getInstance();
		studentDAO = StudentDAO.getInstance();
		streamDAO = StreamDAO.getInstance();
		configDAO = ConfigDAO.getInstance();
		cycleDAO = CycleDAO.getInstance();

	}


	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		OutputStream out = response.getOutputStream();
		response.setContentType("application/json;charset=UTF-8");


		String indexNo =  StringUtils.trimToEmpty(request.getParameter("indexNo"));
		String subjectId =  StringUtils.trimToEmpty(request.getParameter("subjectId"));
		String score =  StringUtils.trimToEmpty(request.getParameter("score"));
		String streamId =  StringUtils.trimToEmpty(request.getParameter("streamId"));

		String centerId = ""; 
		centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

		Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(submitScore(indexNo,subjectId,streamId,score,centerId)).getBytes()); 


		out.flush();
		out.close();


	}

	private JsonElement submitScore(String indexNo, String subjectId, String streamId, String score,String centerId) {

		JsonObject jsonObject = new JsonObject(); 

		if(StringUtils.isEmpty(indexNo)){

			jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(subjectId)){

			jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(streamId)){

			jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(score)){

			jsonObject.addProperty("errorMessage", ERROR_EMPTY_SCORE); 

		}else if(!StringUtils.isNumeric(score)){ 

			jsonObject.addProperty("errorMessage", ERROR_INVALID_SCORE + " (" + score + " )"); 

		}else if(Integer.parseInt(score) < 0){ 

			jsonObject.addProperty("errorMessage", ERROR_INVALID_SCORE + " (" + score + " )"); 

		}else if(Integer.parseInt(score) > 100){ 

			jsonObject.addProperty("errorMessage", ERROR_INVALID_SCORE + " (" + score + " )"); 

		}else{


			String term = configDAO.getConfig(centerId).getTerm();
			String year = configDAO.getConfig(centerId).getYear();
			String cycleId = configDAO.getConfig(centerId).getCycleId(); 
			String cycle = cycleDAO.getCycleByid(centerId, cycleId).getDescription(); 

			String studentId = studentDAO.getStudentByindexNo(centerId, indexNo).getUuid();
			String classroomid = streamDAO.getStreambyUuid(centerId, streamId).getClassroomId();

			
			
			

			if(StringUtils.equalsIgnoreCase(cycle, "One")){
				CycleOneScore cycleOneScore = new CycleOneScore();
				cycleOneScore.setCenterId(centerId);
				cycleOneScore.setClassroomId(classroomid); 
				cycleOneScore.setCycleOneScore(Integer.parseInt(score));
				cycleOneScore.setStreamId(streamId);
				cycleOneScore.setStudentId(studentId);
				cycleOneScore.setSubjectId(subjectId);
				cycleOneScore.setTerm(term);
				cycleOneScore.setYear(year);
				
				if(performanceDAO.putScore(cycleOneScore)){

					jsonObject.addProperty("succesMessage", "Cycle One  score was submitted successfully."); 

				}else{

					jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 
				}

			}
			else if(StringUtils.equalsIgnoreCase(cycle, "Two")){
				CycleTwoScore cycleTwoScore = new CycleTwoScore();
				cycleTwoScore.setCenterId(centerId);
				cycleTwoScore.setClassroomId(classroomid);
				cycleTwoScore.setCycleTwoScore(Integer.parseInt(score));
				cycleTwoScore.setStreamId(streamId);
				cycleTwoScore.setStudentId(studentId);
				cycleTwoScore.setSubjectId(subjectId);
				cycleTwoScore.setTerm(term);
				cycleTwoScore.setYear(year);
				
				if(performanceDAO.putScore(cycleTwoScore)){
					jsonObject.addProperty("succesMessage", "Cycle Two  score was submitted successfully."); 

				}else{

					jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 
				}

			}
			else if(StringUtils.equalsIgnoreCase(cycle, "Three")){
				CycleThreeScore cycleThreeScore = new CycleThreeScore();
				cycleThreeScore.setCenterId(centerId);
				cycleThreeScore.setClassroomId(classroomid);
				cycleThreeScore.setCycleThreeScore(Integer.parseInt(score));
				cycleThreeScore.setStreamId(streamId);
				cycleThreeScore.setStudentId(studentId);
				cycleThreeScore.setSubjectId(subjectId);
				cycleThreeScore.setTerm(term);
				cycleThreeScore.setYear(year);
				
				if(performanceDAO.putScore(cycleThreeScore)){
					jsonObject.addProperty("succesMessage", "Cycle Three score was submitted successfully."); 

				}else{

					jsonObject.addProperty("errorMessage", ERROR_MESSAGE); 
				}

			}else{
				//do nothing 
			}
			


			

		}

		//System.out.println("jsonObject : " + jsonObject); 


		return jsonObject; 
	}


	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1434307272969210301L;

}

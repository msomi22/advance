/**
 * 
 */
package com.appletech.server.servlet.test;

import java.awt.Graphics2D; 
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Center;
import com.appletech.bean.center.Config;
import com.appletech.bean.center.performance.CombinationResult;
import com.appletech.bean.center.performance.Performance;
import com.appletech.bean.center.performance.Result;
import com.appletech.bean.center.stm.Combination;
import com.appletech.bean.center.stm.Stream;
import com.appletech.bean.center.stm.Subject;
import com.appletech.bean.center.student.Student;
import com.appletech.persistence.center.CenterDAO;
import com.appletech.persistence.center.ConfigDAO;
import com.appletech.persistence.performance.PerformanceDAO;
import com.appletech.persistence.stm.ClassroomDAO;
import com.appletech.persistence.stm.CombinationDAO;
import com.appletech.persistence.stm.CombinationDescDAO;
import com.appletech.persistence.stm.StreamDAO;
import com.appletech.persistence.stm.SubjectDAO;
import com.appletech.persistence.student.StudentDAO;
import com.appletech.server.servlet.performance.CombinationComparator;
import com.appletech.server.servlet.performance.CombinationResultComparator;
import com.appletech.server.servlet.performance.ResultComparator;
import com.appletech.server.session.SessionConstants;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**  
 * @author peter
 *
 */
public class Rpc extends HttpServlet{

	private Font timesRomanNarmal8 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
	private Font timesRomanNormal7 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
	private Font timesRomanNormal0 = new Font(Font.FontFamily.TIMES_ROMAN,10, Font.NORMAL);

	private Document document;
	private PdfWriter writer;

	private String PDF_SUBTITLE ="";


	private static PerformanceDAO performanceDAO;
	public static CombinationDescDAO combinationDescDAO;
	public static StudentDAO studentDAO;
	public static StreamDAO streamDAO;
	public static SubjectDAO subjectDAO;
	public static CombinationDAO combinationDAO; 
	private static ConfigDAO configDAO;
	private static CenterDAO centerDAO;
	public static ClassroomDAO classroomDAO;
	
	private Logger logger;

	private static final String USER_SYSTEM = System.getProperty("user.name");
	private static final String LOGO_PATH = "/home/"+USER_SYSTEM+"/school/logo/logo.png";

	private Map<String,Student> studentMap = new HashMap<String,Student>();
	private Map<String,Stream> streamMap = new HashMap<String,Stream>();
	

	String gsid = "8717A263-EF1F-4042-81F5-714003F37BB8";
	String histid = "54015F57-F935-4157-8DE7-0D2C1ECAAE31";
	String geoid = "DEE651EC-CD80-4DC6-81B8-6DEBF58F335A";
	String kiswid = "0947F521-03CC-4815-8536-3E66837B0C99";
	String engid = "592B480E-BBD7-4010-B835-2213D65CD845";
	String phyid = "B5B8319F-19EC-4EE6-AE15-3D15F4FB9D63";
	String chemid = "8E731019-1479-46EC-ACDF-67C0D7BB9031";
	String bioid = "12E96C96-4C28-4253-86B1-A7A0D284613F";
	String ecoid = "9B5EE6F2-782F-4A86-A680-6BCE10C00FE5";
	String pmathid = "5999E599-F469-41AE-BF55-6C1AC812D8B9";
	String commid = "978D89E5-9419-40D6-8669-4AD0E1DA55BD";
	String accid = "6D1DEC06-39F6-4A7B-AE15-6FBE434DFCF2";
	String bamid = "DE86BC64-D3ED-4B9D-B8FB-789C1CB913CC";

	int gs=0,his=0,geo=0,kisw=0,eng=0,phy=0,chem=0,bio=0,eco=0,pmath=0,comm=0,acc=0,bam=0;
	

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		performanceDAO = PerformanceDAO.getInstance();
		combinationDescDAO = CombinationDescDAO.getInstance();
		studentDAO = StudentDAO.getInstance();
		streamDAO = StreamDAO.getInstance();
		subjectDAO = SubjectDAO.getInstance();
		combinationDAO = CombinationDAO.getInstance();
		configDAO = ConfigDAO.getInstance();
		centerDAO = CenterDAO.getInstance();
		classroomDAO = ClassroomDAO.getInstance();
				
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		response.setContentType("application/pdf");
		
		 String centerId = ""; 
		 centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
		 String classroomId = StringUtils.trimToEmpty(request.getParameter("classroomId"));
		 String cycle = StringUtils.trimToEmpty(request.getParameter("cycle"));
		 
		// System.out.println("classroomId : " + classroomId);
		// System.out.println("cycle : " + cycle);
		
		List<Student> studentList = new ArrayList<>();

		List<Stream> streams = streamDAO.getStreamPerClass(centerId, classroomId);
		for(Stream stream : streams){
			List<Student> studentListStream = studentDAO.getStudentByStreamId(centerId, stream.getUuid()); 
			studentList.addAll(studentListStream);
		}
		
		List<Student> students = new ArrayList<>();
		students = studentDAO.getAllStudent(centerId); 
		for(Student student : students){
			studentMap.put(student.getUuid(), student);
		}

		List<Stream> streamlist = new ArrayList<>();
		streamlist = streamDAO.getAllStreams(centerId);
		for(Stream stream : streamlist){
			streamMap.put(stream.getUuid(), stream);
		}
		
		String fileName = "file.pdf"; 
		response.setHeader("Content-Disposition", "inline; filename=\""+fileName);

		document = new Document(PageSize.A4.rotate(), 46, 46, 64, 64);

		try {
			writer = PdfWriter.getInstance(document, response.getOutputStream());           
			PdfUtil event = new PdfUtil();



			writer.setBoxSize("art", new Rectangle(46, 64, 559, 788));
			writer.setPageEvent(event);

			populatePDFDocument(studentList,centerId,classroomId,cycle); 


		} catch (DocumentException e) {
			logger.error("DocumentException while writing into the document");
			logger.error(ExceptionUtils.getStackTrace(e));
		}



	}





	/**
	 * @param studentList
	 * @param centerId
	 */
	private void populatePDFDocument(List<Student> studentList,String centerId,String ClassroomId, String cycle) {

		try {
			document.open();

			//***********************************************************
			//**** 	PREFACE START HERE
			
			Config config = configDAO.getConfig(centerId);
			
			String term = config.getTerm();
			String year = config.getYear();
			
			Center center = centerDAO.getCenterById(centerId);
			String form = classroomDAO.getClassroomById(centerId, ClassroomId).getDescription(); 
			
			
			PDF_SUBTITLE = center.getCenterName().toUpperCase() + "\n"
					+form + " CYCLE " + cycle + " EXAMINATION RESULT - " + year + " TERM " + term +"\n"
					+ "CENTER NUMBER: " + center.getCenterNo() + "       REGION : " + center.getCenterRegion();

			String logopath = LOGO_PATH;

			PdfPTable prefaceTable = new PdfPTable(2);  
			prefaceTable.setWidthPercentage(100); 
			prefaceTable.setWidths(new int[]{70,130}); 

			Paragraph topHeader = new Paragraph();
			topHeader.add(new Paragraph((PDF_SUBTITLE) , timesRomanNormal0));
			topHeader.setAlignment(Element.ALIGN_LEFT);

			PdfPCell topHeaderCell = new PdfPCell(topHeader);
			topHeaderCell.setBorder(Rectangle.NO_BORDER); 
			topHeaderCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell logo = new PdfPCell();
			logo.addElement(createImage(logopath)); 
			logo.setBorder(Rectangle.NO_BORDER); 
			logo.setHorizontalAlignment(Element.ALIGN_LEFT);

			prefaceTable.addCell(logo); 
			prefaceTable.addCell(topHeaderCell);

			//*********** PREFACE END  HERE
			//***********************************************************

			//*********  RESULT SUMMARY START

			PdfPTable outerTable = new PdfPTable(2);  
			outerTable.setWidthPercentage(87);  
			outerTable.setWidths(new int[]{40,50});     
			outerTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			//Remove boarder from the outer table 
			outerTable.getDefaultCell().setBorder(0); 


			BaseColor baseColorWhite = new BaseColor(255,255,255);//while
			BaseColor baseColor = new BaseColor(117,229,210);//#75e5d2
			BaseColor baseColorShadow = new BaseColor(0,255,119);//#00FF77


			PdfPCell subject_header = new PdfPCell(new Paragraph("Subject",timesRomanNarmal8));
			subject_header.setBackgroundColor(baseColor);
			subject_header.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header2 = new PdfPCell(new Paragraph("A",timesRomanNarmal8));
			header2.setBackgroundColor(baseColor);
			header2.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header3 = new PdfPCell(new Paragraph("B",timesRomanNarmal8));
			header3.setBackgroundColor(baseColor);
			header3.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header4 = new PdfPCell(new Paragraph("C",timesRomanNarmal8));
			header4.setBackgroundColor(baseColor);
			header4.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header5 = new PdfPCell(new Paragraph("D",timesRomanNarmal8));
			header5.setBackgroundColor(baseColor);
			header5.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header6 = new PdfPCell(new Paragraph("E",timesRomanNarmal8));
			header6.setBackgroundColor(baseColor);
			header6.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header7 = new PdfPCell(new Paragraph("S",timesRomanNarmal8));
			header7.setBackgroundColor(baseColor);
			header7.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell header8 = new PdfPCell(new Paragraph("F",timesRomanNarmal8));
			header8.setBackgroundColor(baseColor);
			header8.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell totalHeader = new PdfPCell(new Paragraph("Total",timesRomanNarmal8));
			totalHeader.setBackgroundColor(baseColor);
			totalHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell passHeader = new PdfPCell(new Paragraph("Pass",timesRomanNarmal8));
			passHeader.setBackgroundColor(baseColor);
			passHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell passHeader2 = new PdfPCell(new Paragraph("% Pass",timesRomanNarmal8));
			passHeader2.setBackgroundColor(baseColor);
			passHeader2.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell failHeader = new PdfPCell(new Paragraph("Fail",timesRomanNarmal8));
			failHeader.setBackgroundColor(baseColor);
			failHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell failHeader2 = new PdfPCell(new Paragraph("% Fail",timesRomanNarmal8));
			failHeader2.setBackgroundColor(baseColor);
			failHeader2.setHorizontalAlignment(Element.ALIGN_LEFT);



			float minCellHeight = 15f;
			PdfPCell devision_header = new PdfPCell(new Paragraph("Division",timesRomanNarmal8));
			devision_header.setBackgroundColor(baseColor);
			devision_header.setHorizontalAlignment(Element.ALIGN_LEFT);
			devision_header.setFixedHeight(minCellHeight); 

			PdfPCell desc_header = new PdfPCell(new Paragraph("***",timesRomanNarmal8));
			desc_header.setBackgroundColor(baseColor);
			desc_header.setHorizontalAlignment(Element.ALIGN_LEFT);
			desc_header.setFixedHeight(minCellHeight); 

			PdfPCell empty_header = new PdfPCell(new Paragraph(" ",timesRomanNarmal8));
			empty_header.setBackgroundColor(baseColorWhite);
			empty_header.setHorizontalAlignment(Element.ALIGN_LEFT);
			empty_header.setBorder(PdfPCell.NO_BORDER); 
			empty_header.setFixedHeight(minCellHeight); 


			PdfPTable performanceSumaryTable = new PdfPTable(3);  
			performanceSumaryTable.setWidthPercentage(40); 
			performanceSumaryTable.setWidths(new int[]{10,5,25});      
			performanceSumaryTable.setHorizontalAlignment(Element.ALIGN_LEFT);


			PdfPTable resultSumaryTable = new PdfPTable(13);  
			resultSumaryTable.setWidthPercentage(47); 
			resultSumaryTable.setWidths(new int[]{5,3,3,3,3,3,3,3,5,3,5,3,5});     
			resultSumaryTable.setHorizontalAlignment(Element.ALIGN_RIGHT);

			resultSumaryTable.addCell(subject_header);
			resultSumaryTable.addCell(header2);
			resultSumaryTable.addCell(header3);
			resultSumaryTable.addCell(header4);
			resultSumaryTable.addCell(header5);
			resultSumaryTable.addCell(header6);
			resultSumaryTable.addCell(header7);
			resultSumaryTable.addCell(header8);
			resultSumaryTable.addCell(totalHeader);
			resultSumaryTable.addCell(passHeader);
			resultSumaryTable.addCell(passHeader2);
			resultSumaryTable.addCell(failHeader);
			resultSumaryTable.addCell(failHeader2);




			performanceSumaryTable.addCell(devision_header);
			performanceSumaryTable.addCell(desc_header);
			performanceSumaryTable.addCell(empty_header);
			performanceSumaryTable.setHeaderRows(1); 


			/**
		    Subjects
			 */
			PdfPCell countHeader = new PdfPCell(new Paragraph("**",timesRomanNarmal8));
			countHeader.setBackgroundColor(baseColor);
			countHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell indexNoHeader = new PdfPCell(new Paragraph("IndexNo",timesRomanNarmal8));
			indexNoHeader.setBackgroundColor(baseColor);
			indexNoHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell nameHeader = new PdfPCell(new Paragraph("Name",timesRomanNarmal8));
			nameHeader.setBackgroundColor(baseColor);
			nameHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell genderHeader = new PdfPCell(new Paragraph("Sex",timesRomanNarmal8));
			genderHeader.setBackgroundColor(baseColor);
			genderHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			/*PdfPCell streamHeader = new PdfPCell(new Paragraph("Stream",timesRomanNarmal8));
			streamHeader.setBackgroundColor(baseColor);
			streamHeader.setHorizontalAlignment(Element.ALIGN_LEFT);*/

			//SUBJECTS
			PdfPCell subject1 = new PdfPCell(new Paragraph("G.S",timesRomanNarmal8));
			subject1.setBackgroundColor(baseColor);
			subject1.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject2 = new PdfPCell(new Paragraph("HIST",timesRomanNarmal8));
			subject2.setBackgroundColor(baseColor);
			subject2.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject3 = new PdfPCell(new Paragraph("GEO",timesRomanNarmal8));
			subject3.setBackgroundColor(baseColor);
			subject3.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject4 = new PdfPCell(new Paragraph("KISW",timesRomanNarmal8));
			subject4.setBackgroundColor(baseColor);
			subject4.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject5 = new PdfPCell(new Paragraph("ENG",timesRomanNarmal8));
			subject5.setBackgroundColor(baseColor);
			subject5.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject6 = new PdfPCell(new Paragraph("PHY",timesRomanNarmal8));
			subject6.setBackgroundColor(baseColor);
			subject6.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject7 = new PdfPCell(new Paragraph("CHEM",timesRomanNarmal8));
			subject7.setBackgroundColor(baseColor);
			subject7.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject8 = new PdfPCell(new Paragraph("BIO",timesRomanNarmal8));
			subject8.setBackgroundColor(baseColor);
			subject8.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject9 = new PdfPCell(new Paragraph("ECO",timesRomanNarmal8));
			subject9.setBackgroundColor(baseColor);
			subject9.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject10 = new PdfPCell(new Paragraph("P/MATH",timesRomanNarmal8));
			subject10.setBackgroundColor(baseColor);
			subject10.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject11 = new PdfPCell(new Paragraph("COMM",timesRomanNarmal8));
			subject11.setBackgroundColor(baseColor);
			subject11.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject12 = new PdfPCell(new Paragraph("ACC",timesRomanNarmal8));
			subject12.setBackgroundColor(baseColor);
			subject12.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell subject13 = new PdfPCell(new Paragraph("BAM",timesRomanNarmal8));
			subject13.setBackgroundColor(baseColor);
			subject13.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell divisionHeader = new PdfPCell(new Paragraph("Div",timesRomanNarmal8));
			divisionHeader.setBackgroundColor(baseColor);
			divisionHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell positionHeader = new PdfPCell(new Paragraph("Pos",timesRomanNarmal8));
			positionHeader.setBackgroundColor(baseColor);
			positionHeader.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell totalsHeader = new PdfPCell(new Paragraph("Points",timesRomanNarmal8));
			totalsHeader.setBackgroundColor(baseColor);
			totalsHeader.setHorizontalAlignment(Element.ALIGN_LEFT);


			PdfPTable resultTable = new PdfPTable(19);  
			PdfPTable resultIcompleteTable = new PdfPTable(19);  
			

			resultTable.setWidthPercentage(100); 
			resultTable.setWidths(new int[]{3,5,15,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5});      
			resultTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			resultTable.setHeaderRows(1); 
			resultTable.isSkipFirstHeader();

			resultIcompleteTable.setWidthPercentage(100); 
			resultIcompleteTable.setWidths(new int[]{3,5,15,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5});      
			resultIcompleteTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			resultIcompleteTable.setHeaderRows(1); 
			resultIcompleteTable.isSkipFirstHeader();
			

			resultTable.addCell(countHeader);
			resultTable.addCell(indexNoHeader);
			resultTable.addCell(nameHeader);
			resultTable.addCell(genderHeader);

			resultTable.addCell(subject1);
			resultTable.addCell(subject2);
			resultTable.addCell(subject3);
			resultTable.addCell(subject4);
			resultTable.addCell(subject5);
			resultTable.addCell(subject6);
			resultTable.addCell(subject7);
			resultTable.addCell(subject8);
			resultTable.addCell(subject9);
			resultTable.addCell(subject10);
			resultTable.addCell(subject11);
			resultTable.addCell(subject12);
			resultTable.addCell(subject13);

			resultTable.addCell(divisionHeader);
			resultTable.addCell(totalsHeader);
			//******************************************
			
			



			//loop
			List<Subject> subjects = subjectDAO.getAllSubjects(centerId);
			List<Performance> subjectPerformanceList = null;

			for(Subject subject : subjects){
				
				subjectPerformanceList = performanceDAO.getSubjectPerformance(centerId, subject.getUuid(), ClassroomId,"", term, year); 

				int passCount = 0;
				int subCount = 0;

				int aCount=0,bCount=0,cCount=0,dCount=0,eCount=0,sCount=0,fCount=0;

				if(!subjectPerformanceList.isEmpty()){
					for(Performance performance : subjectPerformanceList){

						String grade = getGrade(performance.getCycleOneScore());

						if(StringUtils.equalsIgnoreCase(grade, "A")){
							aCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "B")){
							bCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "C")){
							cCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "D")){
							dCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "E")){
							eCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "S")){
							sCount++;
						}else if(StringUtils.equalsIgnoreCase(grade, "F")){
							fCount++;
						}


						if(performance.getCycleOneScore() >= 50){
							passCount++;
						}
						subCount++;
					}
				}

				int total = 0;
				int failCount = 0;
				double percentagePass = 0;
				double percentageFail = 0;

				total = subCount;
				failCount = (total - passCount);

				if(passCount !=0 ){
					percentagePass = ((double)passCount / (double)total) * (double)100;
					percentagePass = Math.floor(percentagePass);
				}

				if(failCount !=0 ){
					percentageFail = ((double)failCount / (double)total) * (double)100;
					percentageFail = Math.floor(percentageFail);
				}

				resultSumaryTable.addCell(new Paragraph(subject.getSubjectCode(),timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(aCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(bCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(cCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(dCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(eCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(sCount) + "",timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZero(fCount) + "",timesRomanNormal7));

				resultSumaryTable.addCell(new Paragraph(toStringNoZero(total) + "",timesRomanNormal7));

				resultSumaryTable.addCell(new Paragraph(toStringNoZero(passCount) + "" , timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZeroDouble(percentagePass) + "" , timesRomanNormal7));

				resultSumaryTable.addCell(new Paragraph(toStringNoZero(failCount) + "", timesRomanNormal7));
				resultSumaryTable.addCell(new Paragraph(toStringNoZeroDouble(percentageFail) + "" , timesRomanNormal7));


			}

			//***************************************************************************************************************************
			//****  RESULT SUMMARY  END


			//*****************************************************************************************************************************
			//*************** STUDENT RESULT ANALYSIS START

			
			BaseColor baseColorRed = new BaseColor(255,0,0);//#FF0000
			
			countHeader.setBackgroundColor(baseColorRed);
			indexNoHeader.setBackgroundColor(baseColorRed);
			nameHeader.setBackgroundColor(baseColorRed);
			genderHeader.setBackgroundColor(baseColorRed);
			subject1.setBackgroundColor(baseColorRed);
			subject2.setBackgroundColor(baseColorRed);
			subject3.setBackgroundColor(baseColorRed);
			subject4.setBackgroundColor(baseColorRed);
			subject5.setBackgroundColor(baseColorRed);
			subject6.setBackgroundColor(baseColorRed);
			subject7.setBackgroundColor(baseColorRed);
			subject8.setBackgroundColor(baseColorRed);
			subject9.setBackgroundColor(baseColorRed);
			subject10.setBackgroundColor(baseColorRed);
			subject11.setBackgroundColor(baseColorRed);
			subject12.setBackgroundColor(baseColorRed);
			subject13.setBackgroundColor(baseColorRed);
			divisionHeader.setBackgroundColor(baseColorRed);
			totalsHeader.setBackgroundColor(baseColorRed);
			
			resultIcompleteTable.addCell(countHeader);
			resultIcompleteTable.addCell(indexNoHeader);
			resultIcompleteTable.addCell(nameHeader);
			resultIcompleteTable.addCell(genderHeader);

			resultIcompleteTable.addCell(subject1);
			resultIcompleteTable.addCell(subject2);
			resultIcompleteTable.addCell(subject3);
			resultIcompleteTable.addCell(subject4);
			resultIcompleteTable.addCell(subject5);
			resultIcompleteTable.addCell(subject6);
			resultIcompleteTable.addCell(subject7);
			resultIcompleteTable.addCell(subject8);
			resultIcompleteTable.addCell(subject9);
			resultIcompleteTable.addCell(subject10);
			resultIcompleteTable.addCell(subject11);
			resultIcompleteTable.addCell(subject12);
			resultIcompleteTable.addCell(subject13);

			resultIcompleteTable.addCell(divisionHeader);
			resultIcompleteTable.addCell(totalsHeader);
			//********************************************





			List<Result> list = new ArrayList<>();
			list = ComputePerformance(studentList,centerId,cycle);
			
			if(list != null){
				Collections.sort(list, new ResultComparator()); 
			} 
			
			
			int isAbsentCount = 0;
			int isCompleteCount = 0;

			int div1count = 0;
			int div2count = 0;
			int div3count = 0;
			int div4count = 0;
			int div5count = 0;

			int completeCount = 0;
			int inCompleteCount = 0;

			for(Result result : list){

				if(StringUtils.equalsIgnoreCase(result.getIscomplete(), "Incomplete")){

					isCompleteCount++;

				}
				if(StringUtils.equalsIgnoreCase(result.getIsabsent(), "Absent")){

					isAbsentCount++;

				}



				//Populate student details 
				if(!StringUtils.isEmpty(result.getStudentId())){ 

					String name = "";
					Student student = new Student();
					if(studentMap.get(result.getStudentId()) != null){
					    student = studentMap.get(result.getStudentId());
						name = student.getFirstname() + " " + student.getMiddlename() + " " + student.getLastname();
					}
					
					
					

					/**
					 * 
					 */
					if(!StringUtils.equalsIgnoreCase(result.getIscomplete(), "Incomplete")){
						completeCount++;

						if(StringUtils.equalsIgnoreCase(result.getDivision(), "I")){
							div1count++;
						}if(StringUtils.equalsIgnoreCase(result.getDivision(), "II")){
							div2count++;
						}if(StringUtils.equalsIgnoreCase(result.getDivision(), "III")){
							div3count++;
						}if(StringUtils.equalsIgnoreCase(result.getDivision(), "IV")){
							div4count++;
						}if(StringUtils.equalsIgnoreCase(result.getDivision(), "O")){
							div5count++;
						}



						resultTable.addCell(new Paragraph(completeCount + "",timesRomanNormal7));

						resultTable.addCell(new Paragraph(student.getIndexNo() + "",timesRomanNormal7));//indexNoHeader
						resultTable.addCell(new Paragraph(name + "",timesRomanNormal7));//name
						resultTable.addCell(new Paragraph(student.getGender() + "",timesRomanNormal7));//sex
						
						Chunk gschunk = new Chunk("",timesRomanNormal7);
						Chunk histchunk = new Chunk("",timesRomanNormal7);
						Chunk geochunk = new Chunk("",timesRomanNormal7);
						Chunk kiswchunk = new Chunk("",timesRomanNormal7);
						Chunk engchunk = new Chunk("",timesRomanNormal7);
						Chunk phychunk = new Chunk("",timesRomanNormal7);
						Chunk chemchunk = new Chunk("",timesRomanNormal7);
						Chunk biochunk = new Chunk("",timesRomanNormal7);
						Chunk ecochunk = new Chunk("",timesRomanNormal7);
						Chunk pmathchunk = new Chunk("",timesRomanNormal7);
						Chunk commchunk = new Chunk("",timesRomanNormal7);
						Chunk accchunk = new Chunk("",timesRomanNormal7);
						Chunk bamchunk = new Chunk("",timesRomanNormal7);


						Map<String,Integer> subScoreMap = result.getSubScore();
						
                        //13 subjects
						for (Subject subject : subjects) {

							int score = 0;
							if(subScoreMap.get(subject.getUuid()) != null){
								score = subScoreMap.get(subject.getUuid());  
							}

							
							
							if(StringUtils.equals(subject.getUuid(), gsid)){
								gs = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									gschunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), histid)){
								his = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									histchunk.setBackground(baseColorShadow);   
								}
							}
							
							if(StringUtils.equals(subject.getUuid(), geoid)){
								geo = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									geochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), kiswid)){
								kisw = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									kiswchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), engid)){
								eng = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									engchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), phyid)){
								phy = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									phychunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), chemid)){
								 chem = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									chemchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bioid)){
								bio = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									biochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), ecoid)){
								eco = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									ecochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), pmathid)){
								pmath = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									pmathchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), commid)){
								comm = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									commchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), accid)){
								acc = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									accchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bamid)){
								bam = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									bamchunk.setBackground(baseColorShadow);   
								}
							}
							
						}

						gschunk.append(toStringNoZero(gs) + " " + getGrade(gs)); 
						histchunk.append(toStringNoZero(his) + " " + getGrade(his));
						geochunk.append(toStringNoZero(geo) + " " + getGrade(geo));
						kiswchunk.append(toStringNoZero(kisw) + " " + getGrade(kisw));
						engchunk.append(toStringNoZero(eng) + " " + getGrade(eng));
						phychunk.append(toStringNoZero(phy) + " " + getGrade(phy));
						chemchunk.append(toStringNoZero(chem) + " " + getGrade(chem));
						biochunk.append(toStringNoZero(bio) + " " + getGrade(bio));
						ecochunk.append(toStringNoZero(eco) + " " + getGrade(eco));
						pmathchunk.append(toStringNoZero(pmath) + " " + getGrade(pmath));
						commchunk.append(toStringNoZero(comm) + " " + getGrade(comm));
						accchunk.append(toStringNoZero(acc) + " " + getGrade(acc));
						bamchunk.append(toStringNoZero(bam) + " " + getGrade(bam));


						//subScoreMap
						resultTable.addCell(new Paragraph(gschunk));//subject1
						resultTable.addCell(new Paragraph(histchunk));
						resultTable.addCell(new Paragraph(geochunk));
						resultTable.addCell(new Paragraph(kiswchunk));
						resultTable.addCell(new Paragraph(engchunk));
						resultTable.addCell(new Paragraph(phychunk));
						resultTable.addCell(new Paragraph(chemchunk));
						resultTable.addCell(new Paragraph(biochunk));
						resultTable.addCell(new Paragraph(ecochunk));
						resultTable.addCell(new Paragraph(pmathchunk));
						resultTable.addCell(new Paragraph(commchunk));
						resultTable.addCell(new Paragraph(accchunk));
						resultTable.addCell(new Paragraph(bamchunk));//subject13

						resultTable.addCell(new Paragraph(result.getDivision() + "",timesRomanNormal7));
						resultTable.addCell(new Paragraph(result.getTotalPoints() +"",timesRomanNormal7));

					}//end if(result.getTotalPoints() > 0){


					/**
					 * 
					 */
					else{


						inCompleteCount++;
						
						


						resultIcompleteTable.addCell(new Paragraph(inCompleteCount + "",timesRomanNormal7));

						resultIcompleteTable.addCell(new Paragraph(student.getIndexNo() + "",timesRomanNormal7));
						resultIcompleteTable.addCell(new Paragraph(name + "",timesRomanNormal7));
						resultIcompleteTable.addCell(new Paragraph(student.getGender() + "",timesRomanNormal7));

						Chunk gschunk = new Chunk("",timesRomanNormal7);
						Chunk histchunk = new Chunk("",timesRomanNormal7);
						Chunk geochunk = new Chunk("",timesRomanNormal7);
						Chunk kiswchunk = new Chunk("",timesRomanNormal7);
						Chunk engchunk = new Chunk("",timesRomanNormal7);
						Chunk phychunk = new Chunk("",timesRomanNormal7);
						Chunk chemchunk = new Chunk("",timesRomanNormal7);
						Chunk biochunk = new Chunk("",timesRomanNormal7);
						Chunk ecochunk = new Chunk("",timesRomanNormal7);
						Chunk pmathchunk = new Chunk("",timesRomanNormal7);
						Chunk commchunk = new Chunk("",timesRomanNormal7);
						Chunk accchunk = new Chunk("",timesRomanNormal7);
						Chunk bamchunk = new Chunk("",timesRomanNormal7);
						
						gs=0;his=0;geo=0;kisw=0;eng=0;phy=0;chem=0;bio=0;eco=0;pmath=0;comm=0;acc=0;bam=0;

						Map<String,Integer> subScoreMap2 = result.getSubScore();
						for (Subject subject : subjects) {

							int score = 0;
							if(subScoreMap2.get(subject.getUuid()) != null){
								score = subScoreMap2.get(subject.getUuid());  
							}

							
							
							if(StringUtils.equals(subject.getUuid(), gsid)){
								gs = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									gschunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), histid)){
								his = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									histchunk.setBackground(baseColorShadow);   
								}
							}
							
							if(StringUtils.equals(subject.getUuid(), geoid)){
								geo = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									geochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), kiswid)){
								kisw = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									kiswchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), engid)){
								eng = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									engchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), phyid)){
								phy = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									phychunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), chemid)){
								 chem = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									chemchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bioid)){
								bio = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									biochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), ecoid)){
								eco = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									ecochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), pmathid)){
								pmath = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									pmathchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), commid)){
								comm = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									commchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), accid)){
								acc = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									accchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bamid)){
								bam = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									bamchunk.setBackground(baseColorShadow);   
								}
							}
							
						}

						gschunk.append(toStringNoZero(gs) + " " + getGrade(gs)); 
						histchunk.append(toStringNoZero(his) + " " + getGrade(his));
						geochunk.append(toStringNoZero(geo) + " " + getGrade(geo));
						kiswchunk.append(toStringNoZero(kisw) + " " + getGrade(kisw));
						engchunk.append(toStringNoZero(eng) + " " + getGrade(eng));
						phychunk.append(toStringNoZero(phy) + " " + getGrade(phy));
						chemchunk.append(toStringNoZero(chem) + " " + getGrade(chem));
						biochunk.append(toStringNoZero(bio) + " " + getGrade(bio));
						ecochunk.append(toStringNoZero(eco) + " " + getGrade(eco));
						pmathchunk.append(toStringNoZero(pmath) + " " + getGrade(pmath));
						commchunk.append(toStringNoZero(comm) + " " + getGrade(comm));
						accchunk.append(toStringNoZero(acc) + " " + getGrade(acc));
						bamchunk.append(toStringNoZero(bam) + " " + getGrade(bam));


						//subScoreMap
						resultIcompleteTable.addCell(new Paragraph(gschunk));//subject1
						resultIcompleteTable.addCell(new Paragraph(histchunk));
						resultIcompleteTable.addCell(new Paragraph(geochunk));
						resultIcompleteTable.addCell(new Paragraph(kiswchunk));
						resultIcompleteTable.addCell(new Paragraph(engchunk));
						resultIcompleteTable.addCell(new Paragraph(phychunk));
						resultIcompleteTable.addCell(new Paragraph(chemchunk));
						resultIcompleteTable.addCell(new Paragraph(biochunk));
						resultIcompleteTable.addCell(new Paragraph(ecochunk));
						resultIcompleteTable.addCell(new Paragraph(pmathchunk));
						resultIcompleteTable.addCell(new Paragraph(commchunk));
						resultIcompleteTable.addCell(new Paragraph(accchunk));
						resultIcompleteTable.addCell(new Paragraph(bamchunk));//subject13

						resultIcompleteTable.addCell(new Paragraph(result.getDivision() + "",timesRomanNormal7));
						resultIcompleteTable.addCell(new Paragraph(result.getTotalPoints() +"",timesRomanNormal7));


					}




				}

			}//end if !Incomplete	



			// START REAL STUDENT PERFORMANCE ANALYSIS




			//*******************************************************************************************************************************
			//******************** STUDENT RESULT ANALYSIS END


			//******************************************************************************************************************************
			//**************** PERFORMANCE ANALYSIS START
			int totals = div1count + div2count + div3count + div4count + div5count;
			for(int i=0;i<8;i++){

				if(i==0){

					performanceSumaryTable.addCell(new Paragraph("Divison I",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(div1count + "",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==1){

					performanceSumaryTable.addCell(new Paragraph("Divison II",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(div2count + "",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==2){

					performanceSumaryTable.addCell(new Paragraph("Divison III",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(div3count + "",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==3){

					performanceSumaryTable.addCell(new Paragraph("Divison IV",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(div4count + "",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==4){

					performanceSumaryTable.addCell(new Paragraph("Divison O",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(div5count + "",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==5){
					// The totals 
					PdfPCell totalsCell = new PdfPCell(new Paragraph("Total",timesRomanNarmal8));
					totalsCell.setBackgroundColor(baseColor);
					totalsCell.setHorizontalAlignment(Element.ALIGN_LEFT);

					PdfPCell totalsCell2 = new PdfPCell(new Paragraph(totals +"",timesRomanNarmal8));
					totalsCell2.setBackgroundColor(baseColor);
					totalsCell2.setHorizontalAlignment(Element.ALIGN_LEFT);

					performanceSumaryTable.addCell(totalsCell);//new Paragraph("Total",timesRomanNormal7)
					performanceSumaryTable.addCell(totalsCell2);//new Paragraph(totaldiv + " ",timesRomanNormal7)
					performanceSumaryTable.addCell(empty_header);

				}else if(i==6){

					performanceSumaryTable.addCell(new Paragraph("Incomplete",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(isCompleteCount + " ",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}else if(i==7){

					performanceSumaryTable.addCell(new Paragraph("Absent",timesRomanNormal7));
					performanceSumaryTable.addCell(new Paragraph(isAbsentCount + " ",timesRomanNormal7));
					performanceSumaryTable.addCell(empty_header);

				}

			}

			//******************************************************************************************************************************
			//**************** PERFORMANCE ANALYSIS END


			document.add(prefaceTable);

			outerTable.addCell(performanceSumaryTable);
			outerTable.addCell(resultSumaryTable);

			/**
			 * Titles for the two tables (Do not change this )
			 */
			// Do not change this 
			document.add(new Paragraph("PERFORMANCE SUMMARY                                   "
					+ "                                                                       "
					+ "   SUBJECT RESULT SUMMARY",timesRomanNormal0));

			// Titles end here 

			document.add(new Paragraph("\n"));//end empty line in an awkward way  

			document.add(outerTable);

			document.add(new Paragraph("\n"));//end empty line in an awkward way  

			document.add(new Paragraph("                                                       "
					+ "                                           "
					+ "STUDENTS PERFORMANCE SUMMARY",timesRomanNormal0));

			document.add(new Paragraph("\n"));//end empty line in an awkward way 

			document.add(resultTable);



			//***************************************************************************************************
			
			
			List<Combination> combinationList = combinationDAO.getCombinationList(centerId);
			Map<String,List<CombinationResult>> outMap = new HashMap<>();
			for(Combination comb : combinationList){

				List<CombinationResult> outList = new ArrayList<>();

				List<CombinationResult> combiList = ComputePerformanceCombination(studentList,centerId,cycle);
				Collections.sort(combiList, new CombinationResultComparator()); 

				for(CombinationResult combinations : combiList ){
					if(StringUtils.equals(comb.getUuid(), combinations.getCombinationId())){

						outList.add(combinations);

					}


				}
				Collections.sort(outList, new CombinationComparator()); 
				outMap.put(comb.getUuid(), outList);

			}

			java.util.Iterator<Entry<String,List<CombinationResult>>> iterator = outMap.entrySet().iterator(); 
			
						
			while(iterator.hasNext()){
				@SuppressWarnings("rawtypes")
				Map.Entry pair = (Map.Entry) iterator.next(); 
				@SuppressWarnings("unchecked")
				List<CombinationResult> cmblist = ( List<CombinationResult>) pair.getValue();

				if(!cmblist.isEmpty()){
					

					BaseColor baseColorCombi = new BaseColor(146,247,12);//#92F70C
					PdfPTable combinationTable = new PdfPTable(19);  
					combinationTable.setWidthPercentage(100); 
					combinationTable.setWidths(new int[]{3,5,15,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5});      
					combinationTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
					combinationTable.setHeaderRows(1); 
					combinationTable.isSkipFirstHeader();
					
					countHeader.setBackgroundColor(baseColorCombi);
					indexNoHeader.setBackgroundColor(baseColorCombi);
					nameHeader.setBackgroundColor(baseColorCombi);
					genderHeader.setBackgroundColor(baseColorCombi);
					subject1.setBackgroundColor(baseColorCombi);
					subject2.setBackgroundColor(baseColorCombi);
					subject3.setBackgroundColor(baseColorCombi);
					subject4.setBackgroundColor(baseColorCombi);
					subject5.setBackgroundColor(baseColorCombi);
					subject6.setBackgroundColor(baseColorCombi);
					subject7.setBackgroundColor(baseColorCombi);
					subject8.setBackgroundColor(baseColorCombi);
					subject9.setBackgroundColor(baseColorCombi);
					subject10.setBackgroundColor(baseColorCombi);
					subject11.setBackgroundColor(baseColorCombi);
					subject12.setBackgroundColor(baseColorCombi);
					subject13.setBackgroundColor(baseColorCombi);
					positionHeader.setBackgroundColor(baseColorCombi);
					totalsHeader.setBackgroundColor(baseColorCombi);
					
					
					combinationTable.addCell(countHeader);
					combinationTable.addCell(indexNoHeader);
					combinationTable.addCell(nameHeader);
					combinationTable.addCell(genderHeader);
					combinationTable.addCell(subject1);
					combinationTable.addCell(subject2);
					combinationTable.addCell(subject3);
					combinationTable.addCell(subject4);
					combinationTable.addCell(subject5);
					combinationTable.addCell(subject6);
					combinationTable.addCell(subject7);
					combinationTable.addCell(subject8);
					combinationTable.addCell(subject9);
					combinationTable.addCell(subject10);
					combinationTable.addCell(subject11);
					combinationTable.addCell(subject12);
					combinationTable.addCell(subject13);

					combinationTable.addCell(positionHeader);
					combinationTable.addCell(totalsHeader);
					//*******************************************
					
					int position = 1;
					int prevposition = 1;
					double total = 0;
					double prevtotal =0;
					String pos = "";
					
					int count = 1;
					
					for(CombinationResult combin : cmblist){
						
						total = combin.getTotalPoints();
						
						if(total == prevtotal){
							pos = String.valueOf(position-prevposition++);
						}else{
							prevposition = 1;
							pos =  String.valueOf(position);
						}
						
						
						//Populate PDF start
						
						Student student = studentMap.get(combin.getStudentId());
						String name = "";
						name = student.getFirstname() + " " + student.getMiddlename() + " " + student.getLastname();
						
						combinationTable.addCell(new Paragraph(count + "",timesRomanNormal7));

						combinationTable.addCell(new Paragraph(student.getIndexNo() + "",timesRomanNormal7));
						combinationTable.addCell(new Paragraph(name + "",timesRomanNormal7));
						combinationTable.addCell(new Paragraph(student.getGender() + "",timesRomanNormal7));
						
						Chunk gschunk = new Chunk("",timesRomanNormal7);
						Chunk histchunk = new Chunk("",timesRomanNormal7);
						Chunk geochunk = new Chunk("",timesRomanNormal7);
						Chunk kiswchunk = new Chunk("",timesRomanNormal7);
						Chunk engchunk = new Chunk("",timesRomanNormal7);
						Chunk phychunk = new Chunk("",timesRomanNormal7);
						Chunk chemchunk = new Chunk("",timesRomanNormal7);
						Chunk biochunk = new Chunk("",timesRomanNormal7);
						Chunk ecochunk = new Chunk("",timesRomanNormal7);
						Chunk pmathchunk = new Chunk("",timesRomanNormal7);
						Chunk commchunk = new Chunk("",timesRomanNormal7);
						Chunk accchunk = new Chunk("",timesRomanNormal7);
						Chunk bamchunk = new Chunk("",timesRomanNormal7);
						
						gs=0;his=0;geo=0;kisw=0;eng=0;phy=0;chem=0;bio=0;eco=0;pmath=0;comm=0;acc=0;bam=0;

						
                        Map<String,Integer> subScoreMap = combin.getSubScore();
						
						for (Subject subject : subjects) {

							int score = 0;
							if(subScoreMap.get(subject.getUuid()) != null){
								score = subScoreMap.get(subject.getUuid());  
							}

							
							
							if(StringUtils.equals(subject.getUuid(), gsid)){
								gs = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									gschunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), histid)){
								his = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									histchunk.setBackground(baseColorShadow);   
								}
							}
							
							if(StringUtils.equals(subject.getUuid(), geoid)){
								geo = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									geochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), kiswid)){
								kisw = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									kiswchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), engid)){
								eng = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									engchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), phyid)){
								phy = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									phychunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), chemid)){
								 chem = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									chemchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bioid)){
								bio = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									biochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), ecoid)){
								eco = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									ecochunk.setBackground(baseColorShadow);   
								}
							}
							
							
							
							if(StringUtils.equals(subject.getUuid(), pmathid)){
								pmath = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									pmathchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), commid)){
								comm = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									commchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), accid)){
								acc = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									accchunk.setBackground(baseColorShadow);   
								}
							}
							
							
							if(StringUtils.equals(subject.getUuid(), bamid)){
								bam = score;
								
								if(combinationDescDAO.getCombinationDescBySubjectId(centerId, 
										student.getCombinationId(), subject.getUuid()) != null){ 
									bamchunk.setBackground(baseColorShadow);   
								}
							}
							
						}
						
						gschunk.append(toStringNoZero(gs) + " " + getGrade(gs)); 
						histchunk.append(toStringNoZero(his) + " " + getGrade(his));
						geochunk.append(toStringNoZero(geo) + " " + getGrade(geo));
						kiswchunk.append(toStringNoZero(kisw) + " " + getGrade(kisw));
						engchunk.append(toStringNoZero(eng) + " " + getGrade(eng));
						phychunk.append(toStringNoZero(phy) + " " + getGrade(phy));
						chemchunk.append(toStringNoZero(chem) + " " + getGrade(chem));
						biochunk.append(toStringNoZero(bio) + " " + getGrade(bio));
						ecochunk.append(toStringNoZero(eco) + " " + getGrade(eco));
						pmathchunk.append(toStringNoZero(pmath) + " " + getGrade(pmath));
						commchunk.append(toStringNoZero(comm) + " " + getGrade(comm));
						accchunk.append(toStringNoZero(acc) + " " + getGrade(acc));
						bamchunk.append(toStringNoZero(bam) + " " + getGrade(bam));
						
						combinationTable.addCell(new Paragraph(gschunk));//subject1
						combinationTable.addCell(new Paragraph(histchunk));
						combinationTable.addCell(new Paragraph(geochunk));
						combinationTable.addCell(new Paragraph(kiswchunk));
						combinationTable.addCell(new Paragraph(engchunk));
						combinationTable.addCell(new Paragraph(phychunk));
						combinationTable.addCell(new Paragraph(chemchunk));
						combinationTable.addCell(new Paragraph(biochunk));
						combinationTable.addCell(new Paragraph(ecochunk));
						combinationTable.addCell(new Paragraph(pmathchunk));
						combinationTable.addCell(new Paragraph(commchunk));
						combinationTable.addCell(new Paragraph(accchunk));
						combinationTable.addCell(new Paragraph(bamchunk));//subject13
						
						combinationTable.addCell(new Paragraph(pos,timesRomanNormal7));
						combinationTable.addCell(new Paragraph((int)total +"",timesRomanNormal7));
						
						//Populate PDF end
						
						position++;
						prevtotal=total;
						count++;
						
						
						
					}
					
					String cmbin = combinationDAO.getCombinationBYUUID(centerId, (String) pair.getKey()).getDescription();
					
					document.add(new Paragraph("\n"));//end empty line in an awkward way 
					document.add(new Paragraph("                                                       "
							+ "                                           "
							+ cmbin.toUpperCase() + " -- COMBINATION PERFORMANCE SUMMARY" , timesRomanNormal0));
					
					document.add(new Paragraph("\n"));//end empty line in an awkward way 
					
					document.add(combinationTable);
					//do something 
					ReloadTable(countHeader, indexNoHeader, nameHeader, genderHeader, subject1, subject2, subject3,
							subject4, subject5, subject6, subject7, subject8, subject9, subject10, subject11, subject12,
							subject13, positionHeader, totalsHeader, baseColorCombi);
					//*******************************************
					
					document.add(new Paragraph("\n"));//end empty line in an awkward way 
					
				}
			}


			document.newPage();

			document.add(new Paragraph("                                                       "
					+ "                                           "
					+ "PERFORMANCE SUMMARY FOR INCOMPLETE EXAM",timesRomanNormal0));

			document.add(new Paragraph("\n"));//end empty line in an awkward way 

			document.add(resultIcompleteTable);


			document.close();

		}catch(DocumentException e) {
			logger.error("DocumentException while writing into the document");
			logger.error(ExceptionUtils.getStackTrace(e));
		}  
	}

	/**
	 * @param countHeader
	 * @param indexNoHeader
	 * @param nameHeader
	 * @param genderHeader
	 * @param subject1
	 * @param subject2
	 * @param subject3
	 * @param subject4
	 * @param subject5
	 * @param subject6
	 * @param subject7
	 * @param subject8
	 * @param subject9
	 * @param subject10
	 * @param subject11
	 * @param subject12
	 * @param subject13
	 * @param positionHeader
	 * @param totalsHeader
	 * @param baseColorCombi
	 * @throws DocumentException
	 */
	private void ReloadTable(PdfPCell countHeader, PdfPCell indexNoHeader, PdfPCell nameHeader, PdfPCell genderHeader,
			PdfPCell subject1, PdfPCell subject2, PdfPCell subject3, PdfPCell subject4, PdfPCell subject5,
			PdfPCell subject6, PdfPCell subject7, PdfPCell subject8, PdfPCell subject9, PdfPCell subject10,
			PdfPCell subject11, PdfPCell subject12, PdfPCell subject13, PdfPCell positionHeader, PdfPCell totalsHeader,
			BaseColor baseColorCombi) throws DocumentException {
		PdfPTable combinationTable;
		combinationTable = new PdfPTable(19); 
		combinationTable.setWidthPercentage(100); 
		combinationTable.setWidths(new int[]{3,5,15,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5});      
		combinationTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
		combinationTable.setHeaderRows(1); 
		combinationTable.isSkipFirstHeader();
		countHeader.setBackgroundColor(baseColorCombi);
		indexNoHeader.setBackgroundColor(baseColorCombi);
		nameHeader.setBackgroundColor(baseColorCombi);
		genderHeader.setBackgroundColor(baseColorCombi);
		subject1.setBackgroundColor(baseColorCombi);
		subject2.setBackgroundColor(baseColorCombi);
		subject3.setBackgroundColor(baseColorCombi);
		subject4.setBackgroundColor(baseColorCombi);
		subject5.setBackgroundColor(baseColorCombi);
		subject6.setBackgroundColor(baseColorCombi);
		subject7.setBackgroundColor(baseColorCombi);
		subject8.setBackgroundColor(baseColorCombi);
		subject9.setBackgroundColor(baseColorCombi);
		subject10.setBackgroundColor(baseColorCombi);
		subject11.setBackgroundColor(baseColorCombi);
		subject12.setBackgroundColor(baseColorCombi);
		subject13.setBackgroundColor(baseColorCombi);
		positionHeader.setBackgroundColor(baseColorCombi);
		totalsHeader.setBackgroundColor(baseColorCombi);
		
		
		combinationTable.addCell(countHeader);
		combinationTable.addCell(indexNoHeader);
		combinationTable.addCell(nameHeader);
		combinationTable.addCell(genderHeader);
		combinationTable.addCell(subject1);
		combinationTable.addCell(subject2);
		combinationTable.addCell(subject3);
		combinationTable.addCell(subject4);
		combinationTable.addCell(subject5);
		combinationTable.addCell(subject6);
		combinationTable.addCell(subject7);
		combinationTable.addCell(subject8);
		combinationTable.addCell(subject9);
		combinationTable.addCell(subject10);
		combinationTable.addCell(subject11);
		combinationTable.addCell(subject12);
		combinationTable.addCell(subject13);

		combinationTable.addCell(positionHeader);
		combinationTable.addCell(totalsHeader);
	}





	/**
	 * @param studentList
	 * @param centerId
	 * @return
	 */
	public static  List<Result> ComputePerformance(List<Student> studentList,String centerId, String cycle){

		List<Result> resultList = new ArrayList<>();

		int totalpoint = 0;
		String isComplete = "";
		String isAbsent = "";
		String term = configDAO.getConfig(centerId).getTerm();
		String year = configDAO.getConfig(centerId).getYear();
		

		if(studentList != null){
			List<Performance> studentperformancelist = null;  
			for(Student student : studentList){

				studentperformancelist = performanceDAO.getStudentPerformance(centerId, student.getUuid(), term, year);
				Result result = new Result();
				Map<String,Integer> subscoreMap = new HashMap<>();

				if(!studentperformancelist.isEmpty()){

					int isaMainCount = 0;

					for(Performance performance : studentperformancelist){

						 //TODO
						int point = 0;
						if(StringUtils.equalsIgnoreCase(cycle, "One")){
							 point = getPoints(performance.getCycleOneScore());
							 
						}else if(StringUtils.equalsIgnoreCase(cycle, "Two")){
							point = getPoints(performance.getCycleTwoScore());
							
						}else if(StringUtils.equalsIgnoreCase(cycle, "Three")){
							point = getPoints(performance.getCycleThreeScore()); 
							
						}
						
						if(combinationDescDAO.getCombinationDescBySubjectId(centerId, student.getCombinationId(), performance.getSubjectId()) != null){ 
							totalpoint += point;
							isaMainCount++;
						}

						if(StringUtils.equalsIgnoreCase(cycle, "One")){
							 subscoreMap.put(performance.getSubjectId(), performance.getCycleOneScore()); 
							 
						}else if(StringUtils.equalsIgnoreCase(cycle, "Two")){
							subscoreMap.put(performance.getSubjectId(), performance.getCycleTwoScore()); 
							
						}else if(StringUtils.equalsIgnoreCase(cycle, "Three")){
							subscoreMap.put(performance.getSubjectId(), performance.getCycleThreeScore()); 
							
						}
						result.setSubScore(subscoreMap);
					} 
					if(isaMainCount <3){
						isComplete = "Incomplete"; 
						result.setIscomplete(isComplete); 
					}

					result.setDivision(getDivision(totalpoint));
					result.setHasPassed("status");  
					result.setStudentId(student.getUuid()); 
					result.setTotalPoints(totalpoint); 
					totalpoint = 0;
					
				}else{
					isAbsent = "Absent";
					result.setIsabsent(isAbsent); 
				}

				resultList.add(result);
			}
		}
		return resultList;
	}





	/**
	 * @param studentList
	 * @return
	 */
	public static  List<CombinationResult> ComputePerformanceCombination(List<Student> studentList,String centerId, String cycle){

		List<CombinationResult> resultList = new ArrayList<>();

		int totalpoint = 0;
		String term = configDAO.getConfig(centerId).getTerm();
		String year = configDAO.getConfig(centerId).getYear();
		
		if(studentList != null){
			List<Performance> studentperformancelist = null;  
			for(Student student : studentList){

				studentperformancelist = performanceDAO.getStudentPerformance(centerId, student.getUuid(), term, year);

				Map<String,Integer> subscoreMap = new HashMap<>();

				if(!studentperformancelist.isEmpty()){

					CombinationResult result = new CombinationResult();
					
					int isaMainCount = 0;
					for(Performance performance : studentperformancelist){
                         //TODO
						int point = 0;
						if(StringUtils.equalsIgnoreCase(cycle, "One")){
							 point = getPoints(performance.getCycleOneScore());
							 
						}else if(StringUtils.equalsIgnoreCase(cycle, "Two")){
							point = getPoints(performance.getCycleTwoScore());
							
						}else if(StringUtils.equalsIgnoreCase(cycle, "Three")){
							point = getPoints(performance.getCycleThreeScore()); 
							
						}
						

						if(combinationDescDAO.getCombinationDescBySubjectId(centerId, student.getCombinationId(), performance.getSubjectId()) != null){ 
							totalpoint += point;
							isaMainCount++;
						}
                         
						if(StringUtils.equalsIgnoreCase(cycle, "One")){
							 subscoreMap.put(performance.getSubjectId(), performance.getCycleOneScore()); 
							 
						}else if(StringUtils.equalsIgnoreCase(cycle, "Two")){
							subscoreMap.put(performance.getSubjectId(), performance.getCycleTwoScore()); 
							
						}else if(StringUtils.equalsIgnoreCase(cycle, "Three")){
							subscoreMap.put(performance.getSubjectId(), performance.getCycleThreeScore()); 
							
						}
						
						result.setSubScore(subscoreMap);
					}
					
					if(isaMainCount == 3){

						result.setDivision(getDivision(totalpoint));
						result.setStudentId(student.getUuid()); 
						result.setTotalPoints(totalpoint); 
						result.setCombinationId(student.getCombinationId());
						resultList.add(result);
					}

					totalpoint = 0;

				}

			}

		}

		return resultList;
	}




	/**
	 * @param num
	 * @return
	 */
	private String toStringNoZero(int num){
		if(num <= 0){
			return " "; 
		}
		return String.valueOf(num); 
	}


	/**
	 * @param num
	 * @return
	 */
	private String toStringNoZeroDouble(double num){
		if(num <= 0){
			return " "; 
		}
		return String.valueOf(num); 
	}


	/**
	 * @param realPath
	 * @return
	 */
	private Element createImage(String realPath) {
		Image img = null;

		try {

			File file = new File(realPath);
			if(!file.exists()){
				realPath = getServletContext().getRealPath("/images/default.jpg");

			}

			BufferedImage bufferedImage = ImageIO.read(new File(realPath));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			ImageIO.write(resize(bufferedImage, 600,300), "png", baos);//w,h
			img = Image.getInstance(baos.toByteArray());
			img.scaleAbsolute(150f,70f); 
			img.setAlignment(Element.ALIGN_LEFT);


		} catch (BadElementException e) {
			logger.error("BadElementException Exception while creating an image");
			logger.error(ExceptionUtils.getStackTrace(e));

		} catch (MalformedURLException e) {
			logger.error("MalformedURLException for the path");
			logger.error(ExceptionUtils.getStackTrace(e));

		} catch (IOException e) {
			logger.error("IOException while creating an image");
			logger.error(ExceptionUtils.getStackTrace(e));
		}

		return img;
	}


	/**
	 * @param img
	 * @param newW
	 * @param newH
	 * @return
	 */
	public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
		java.awt.Image tmp = img.getScaledInstance(newW, newH, java.awt.Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return dimg;
	} 


	/**
	 * @param score
	 * @return
	 */
	private static String getGrade(int score) {

		if(score > 100){
			score = 0;
		}

		String grade = "";

		if(score >= 80){
			//A 1
			grade = "A";
		}else if(score >= 70){
			//B 2
			grade = "B";
		}else if(score >= 60){
			//C 3
			grade = "C";
		}else if(score >= 50){
			//D 4
			grade = "D";
		}else if(score >= 40){
			//E 5
			grade = "E";
		}else if(score >= 35){
			//S 6
			grade = "S";
		}else if(score >= 1){
			//S 6
			grade = "F";
		}else{
			//F 7
			grade = "";
		}
		return grade;
	}

	/**
	 * @param score
	 * @return
	 */
	private static int getPoints(int score) {
		int point = 0;
		if(score >= 80){
			//A 1
			point = 1;
		}else if(score >= 70){
			//B 2
			point = 2;
		}else if(score >= 60){
			//C 3
			point = 3;
		}else if(score >= 50){
			//D 4
			point = 4;
		}else if(score >= 40){
			//E 5
			point = 5;
		}else if(score >= 35){
			//S 6
			point = 6;
		}else{
			//F 7
			point = 7;
		}
		return point;
	}


	/**
	 * @param points
	 * @return
	 */
	public static String getDivision(int points){
		String division = "";
		if(points >= 21){
			//O
			division = "O";
		}else if(points >= 18){
			//IV
			division = "IV";
		}else if(points >= 13){
			//III
			division = "III";
		}else if(points >= 10){
			//II
			division = "II";
		}else if(points >= 1){
			//I
			division = "I";
		}
		return division;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = -3167332773123573788L;

}

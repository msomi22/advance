/**
 * 
 */
package com.appletech.server.servlet.teacher;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.appletech.persistence.stm.TeacherSubjectStreamDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class RemoveTSubject extends HttpServlet{
	
	private final String ERROR_MESSAGE = "Something went wrong while removing the subjects."; 
	private final String ERROR_NO_SUB = "No subject to delete."; 
	private final String SUCCESS_TSUB_DELETED = "The subject was removed successfully."; 

	private static TeacherSubjectStreamDAO teacherSubjectStreamDAO;
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       teacherSubjectStreamDAO = TeacherSubjectStreamDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String subjectId = StringUtils.trimToEmpty(request.getParameter("subjectId"));
       String streamId = StringUtils.trimToEmpty(request.getParameter("classroomId"));
       String teacherId = StringUtils.trimToEmpty(request.getParameter("teacherId"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
      
      /* System.out.println("subjectId : " + subjectId );
       System.out.println("streamId : " + streamId );
       System.out.println("teacherId : " + teacherId );
       System.out.println("centerId : " + centerId );
       System.out.println("******************************************************");*/
       
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(removeTSubject(subjectId,streamId,teacherId,centerId)).getBytes()); 
		

		out.flush();
		out.close();
       
   }
  

 /**
 * @param subjectId
 * @param streamId
 * @param teacherId
 * @param centerId
 * @return
 */
private JsonElement removeTSubject(String subjectId, String streamId, String teacherId, String centerId) {
	

		JsonObject jsonObject = new JsonObject(); 
		

		if(StringUtils.isEmpty(subjectId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(streamId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(teacherId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(centerId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(teacherSubjectStreamDAO.getTeacherSubStream(centerId, teacherId, subjectId, streamId) == null){
			jsonObject.addProperty("responseMessage", ERROR_NO_SUB); 
			
		}else{
		
			if(teacherSubjectStreamDAO.deleteTeacherSubjectStream(centerId, teacherId, subjectId, streamId)){
				
				 //logger.info(""); 
				
				jsonObject.addProperty("responseMessage", SUCCESS_TSUB_DELETED);  
			}else{
				
				jsonObject.addProperty("responseMessage", ERROR_MESSAGE);  
			}
			 
			
			
		}
		
		return jsonObject;

}

/**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }

/**
 * 
 */
private static final long serialVersionUID = 491324163248836080L;


}

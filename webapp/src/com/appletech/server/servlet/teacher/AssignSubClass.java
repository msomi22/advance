/**
 * 
 */
package com.appletech.server.servlet.teacher;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.appletech.bean.center.stm.TeacherSubjectStream;
import com.appletech.persistence.stm.TeacherSubjectStreamDAO;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/** 
 * @author peter
 *
 */
public class AssignSubClass extends HttpServlet{
	
	private final String ERROR_MESSAGE = "Something went wrong while assigning subjects/class."; 
	
	private final String ERROR_SUB_CLASS_EXIST = "The subjects/class already exist."; 
	
	private final String SUCCESS_TSUB_DELETED = "The subjects/class was assigned successfully."; 

	private static TeacherSubjectStreamDAO teacherSubjectStreamDAO;
	/**  
    *
    * @param config
    * @throws ServletException
    */
   @Override
   public void init(ServletConfig config) throws ServletException {
       super.init(config);
       teacherSubjectStreamDAO = TeacherSubjectStreamDAO.getInstance();
   }
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

	   OutputStream out = response.getOutputStream();
	   response.setContentType("application/json;charset=UTF-8");
       
       String classrooms = StringUtils.trimToEmpty(request.getParameter("classrooms"));
       String subjectId = StringUtils.trimToEmpty(request.getParameter("subjectId"));
       String teacherId = StringUtils.trimToEmpty(request.getParameter("teacherId"));
       String centerId = StringUtils.trimToEmpty(request.getParameter("centerId"));
       
     /*  System.out.println("classrooms : " + classrooms );
       System.out.println("subjectId : " + subjectId );
       System.out.println("teacherId : " + teacherId );
       System.out.println("centerId : " + centerId );
       System.out.println("******************************************************");*/
       
       Gson gson = new GsonBuilder().disableHtmlEscaping()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setPrettyPrinting().serializeNulls().create();

		out.write(gson.toJson(assignSubClass(classrooms,subjectId,teacherId,centerId)).getBytes()); 
		

		out.flush();
		out.close();
       
   }
   
   
   

 /**
 * @param classrooms
 * @param subjectId
 * @param teacherId
 * @param centerId
 * @return
 */
private JsonElement assignSubClass(String classrooms, String subjectId, String teacherId, String centerId) {
		JsonObject jsonObject = new JsonObject(); 
		
		classrooms = removeChar(classrooms);
		String [] streams = classrooms.split(",");

		if(StringUtils.isEmpty(classrooms)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(subjectId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(teacherId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else if(StringUtils.isEmpty(centerId)){

			jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 

		}else{
			
			for(String streamId : streams){
				
				// System.out.println("streamId : " + streamId );
				 //System.out.println("**********************************");
				 
				if(teacherSubjectStreamDAO.getTeacherSubStream(centerId, teacherId, subjectId, streamId) != null){
					jsonObject.addProperty("responseMessage", ERROR_SUB_CLASS_EXIST); 
				}else{
					
					TeacherSubjectStream teacherSubjectStream = new TeacherSubjectStream();
					teacherSubjectStream.setCenterId(centerId);
					teacherSubjectStream.setTeacherId(teacherId);
					teacherSubjectStream.setSubjectId(subjectId);
					teacherSubjectStream.setStreamId(streamId);
					
					 //logger.info(""); 
					
					if(teacherSubjectStreamDAO.getTeacherSubStream(centerId, teacherId, subjectId, streamId) == null){
						if(teacherSubjectStreamDAO.putTeacherSubjectStream(teacherSubjectStream)){
							 jsonObject.addProperty("responseMessage", SUCCESS_TSUB_DELETED);  
						}else{
							jsonObject.addProperty("responseMessage", ERROR_MESSAGE); 
						}
					}
					
				 }
				 
				 }
			
		}
		return jsonObject;
}
 

/**
 * @param subids
 * @return
 */
private String removeChar(String subids) {
	subids = subids.replace("[", "");
	subids = subids.replace("]", "");
	subids = subids.replace("\"", "");
	return subids;
}

/**
 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doPost(request, response);
     }

/**
 * 
 */
private static final long serialVersionUID = 491324163248836080L;


}

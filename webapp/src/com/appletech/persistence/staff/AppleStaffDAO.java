/**
 * 
 */
package com.appletech.persistence.staff;

import java.util.List;

import com.appletech.bean.center.staff.Staff;

/**
 * @author peter
 *
 */
public interface AppleStaffDAO {
	
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public Staff getStaffByuuid(String centerId,String uuid);
	
	/**
	 * @param centerId
	 * @param uuid
	 * @param accessLevelId
	 * @return
	 */
	public Staff getStaff(String centerId,String accessLevelId);
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Staff> getAllStaff(String centerId);
	
	/**
	 * 
	 * @param staff
	 * @return
	 */
	public boolean putStaff(Staff staff);
	
	/**
	 * 
	 * @param staff
	 * @return
	 */
	public boolean updateStaff(Staff staff);
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public boolean deleteStaff(String centerId,String uuid);
	
	/**
	 * 
	 * @param centerId
	 * @param mobileORemail
	 * @return
	 */
	public Staff getStaffBymobileORemail(String centerId,String mobileORemailOrIdNo);
	
	/**
	 * 
	 * @param centerId
	 * @param mobileORemail
	 * @param password
	 * @return
	 */
	public Staff StaffLogin(String centerId,String mobileORemailOrIdNo,String password);
	
	/**
	 * 
	 * @param centerId
	 * @param mobile
	 * @return
	 */
	public boolean resetStaffPassword(String centerId,String uuid,String password);
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public int staffCount(String centerId);
	/**
	 * 
	 * @param centerId
	 * @param isActive
	 * @return
	 */
	public int isActiveCount(String centerId,String isActive);
	
	/**
	 * 
	 * @param centerId
	 * @param accessLevelId
	 * @return
	 */
	public List<Staff> getStaffByaccessLevelId(String centerId,String accessLevelId);
	
	/**
	 * 
	 * @param centerId
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	public List<Staff> getStaffOffset(String centerId , int startIndex , int endIndex);
	

}

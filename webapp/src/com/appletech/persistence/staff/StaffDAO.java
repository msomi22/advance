package com.appletech.persistence.staff;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.staff.Staff;
import com.appletech.persistence.GenericDAO;
import com.appletech.server.servlet.util.SecurityUtil;

/** 
 * Persistence abstraction for {@link Staff}
 * 
 * @author <a href="mailto:mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */


public class StaffDAO extends GenericDAO implements AppleStaffDAO {
	
	private static StaffDAO staffDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return StaffDAO Instance
	 */
	public static StaffDAO getInstance() {

		if (staffDAO == null) {
			staffDAO = new StaffDAO();
		}
		return staffDAO;
	}

	/**
	 * 
	 */
	public StaffDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public StaffDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getStaffByuuid(java.lang.String, java.lang.String)
	 */
	@Override
	public Staff getStaffByuuid(String centerId, String uuid) {
		Staff staff = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND uuid = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				staff = beanProcessor.toBean(rset, Staff.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Staff for centerId " + centerId + "and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staff;
	}
	

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getStaffByuuid(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Staff getStaff(String centerId, String accessLevelId) {
		Staff staff = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND accessLevelId =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, accessLevelId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				staff = beanProcessor.toBean(rset, Staff.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Staff for centerId  " + centerId + " and accessLevelId" +accessLevelId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staff;
	}

	
	
	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#StaffLogin(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Staff StaffLogin(String centerId, String mobileORemailOrIdNo, String password) {
		Staff staff = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND "
						+ "(mobile = ? OR email =? OR idNumber =?) AND password =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, mobileORemailOrIdNo);
			pstmt.setString(3, mobileORemailOrIdNo);
			pstmt.setString(4, mobileORemailOrIdNo);
			pstmt.setString(5, SecurityUtil.getMD5Hash(password));
			rset = pstmt.executeQuery();
			while (rset.next()) {
				staff = beanProcessor.toBean(rset, Staff.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Staff with centerId " + centerId + " and mobileORemailOrIdNo" + mobileORemailOrIdNo);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staff;
	}



	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getStaffByemail(java.lang.String, java.lang.String)
	 */
	@Override
	public Staff getStaffBymobileORemail(String centerId,String mobileORemailOrIdNo) {
		Staff staff = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND (email = ? OR mobile = ? OR idNumber =?);");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, mobileORemailOrIdNo);
			pstmt.setString(3, mobileORemailOrIdNo);
			pstmt.setString(4, mobileORemailOrIdNo);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				staff = beanProcessor.toBean(rset, Staff.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Staff with centerId " + centerId + " and mobileORemailOrIdNo" + mobileORemailOrIdNo);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staff;
	}
	

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#staffCount(java.lang.String)
	 */
	@Override
	public int staffCount(String centerId) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ?;");    		   
 	    ) {
        	pstmt.setString(1, centerId);          
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting isActive staff count for centerId " + centerId);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#isActiveCount(java.lang.String, int)
	 */
	@Override
	public int isActiveCount(String centerId, String isActive) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND isActive =?;");    		   
 	    ) {
        	pstmt.setString(1, centerId);       
        	pstmt.setString(2, isActive);       
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting isActive staff count for centerId " + centerId + "and isActive " + isActive);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }

	
	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#resetStaffPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean resetStaffPassword(String centerId,String uuid,String password) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE staff SET password =?"
						+ " WHERE centerId = ? AND  uuid =? ");) {

			pstmt.setString(1, SecurityUtil.getMD5Hash(password));
			pstmt.setString(2, centerId);
			pstmt.setString(3, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to reset password for staff uuid " + uuid + " in centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}
	
	

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#putStaff(com.appletech.bean.center.staff.Staff)
	 */
	@Override
	public boolean putStaff(Staff staff) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO staff"
						+ "(uuid,centerId,accessLevelId,name,gender,mobile,email,password,idNumber,nationality,isActive,dateRegistered)" + 
						" VALUES (?,?,?,?,?,?,?,?,?,?,?,?);");) {

			pstmt.setString(1, staff.getUuid());
			pstmt.setString(2, staff.getCenterId());
			pstmt.setString(3, staff.getAccessLevelId());
			pstmt.setString(4, staff.getName());
			pstmt.setString(5, staff.getGender());
			pstmt.setString(6, staff.getMobile());
			pstmt.setString(7, staff.getEmail());
			pstmt.setString(8, SecurityUtil.getMD5Hash(staff.getPassword()));
			pstmt.setString(9, staff.getIdNumber());
			pstmt.setString(10, staff.getNationality());
			pstmt.setString(11, staff.getIsActive());
			pstmt.setTimestamp(12, staff.getDateRegistered());

			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to updating staff " + staff);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#updateStaff(com.appletech.bean.center.staff.Staff)
	 */
	@Override
	public boolean updateStaff(Staff staff) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE staff SET accessLevelId = ?,name = ?,"
						+ "gender= ?,mobile = ?,email = ?, idNumber =?, nationality =?,isActive =?"
						+ " WHERE centerId = ? AND uuid = ?");) {

			
			pstmt.setString(1, staff.getAccessLevelId());
			pstmt.setString(2, staff.getName());
			pstmt.setString(3, staff.getGender());
			pstmt.setString(4, staff.getMobile());
			pstmt.setString(5, staff.getEmail());
			pstmt.setString(6, staff.getIdNumber());
			pstmt.setString(7, staff.getNationality());
			pstmt.setString(8, staff.getIsActive());
			pstmt.setString(9, staff.getCenterId());
			pstmt.setString(10, staff.getUuid());

			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update staff " + staff);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#deleteStuff(java.lang.String)
	 */
	@Override
	public boolean deleteStaff(String centerId,String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM staff WHERE centerId =? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting staff with centerId " + centerId + " and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getStaffByaccessLevelId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Staff> getStaffByaccessLevelId(String centerId, String accessLevelId) {
		List<Staff> staffList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? AND accessLevelId = ?;");
				
				) {
			
			pstmt.setString(1, centerId);
			pstmt.setString(2, accessLevelId);
			ResultSet rset = pstmt.executeQuery();

			staffList = beanProcessor.toBeanList(rset, Staff.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all staffs");
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staffList;
	}

	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getStaffOffset(java.lang.String, int, int)
	 */
	@Override
	public List<Staff> getStaffOffset(String centerId, int startIndex, int endIndex) {
		List<Staff> staffList = null;
		try(   
				Connection conn = dbutils.getConnection();
				PreparedStatement  pstmt = conn.prepareStatement("SELECT * FROM staff LIMIT ? OFFSET ? WHERE centerId = ?;");   
				) {
			pstmt.setInt(1, endIndex - startIndex);
			pstmt.setInt(2, startIndex);
			pstmt.setString(3, centerId);
			try(ResultSet rset = pstmt.executeQuery();){
				staffList = beanProcessor.toBeanList(rset, Staff.class);
			}

		} catch(SQLException e){
			logger.error("SQL Exception when getting All end Staff from " + startIndex + " to " + endIndex + " for center with uuid " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e)); 
		}
		return staffList;
	}


	
	/**
	 * @see com.appletech.persistence.staff.AppleStaffDAO#getAllStaff(java.lang.String)
	 */
	@Override
	public List<Staff> getAllStaff(String centerId) {
		List<Staff> staffList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM staff WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			staffList = beanProcessor.toBeanList(rset, Staff.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all staffs");
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return staffList;
	}


}

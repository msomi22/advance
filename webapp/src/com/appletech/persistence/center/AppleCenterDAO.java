/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Center;

/**
 * @author peter
 *
 */
public interface AppleCenterDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public Center getCenter(String centerCredenttials);
	/**
	 * 
	 * @param centerCredenttials
	 * @param isActive
	 * @return
	 */
	public Center getCenter(String centerCredenttials,String isActive);
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public Center getCenterById(String uuid);
	/**
	 * 
	 * @param centerNo
	 * @return
	 */
	public Center getCenterBycenterNo(String centerNo);
	/**
	 * 
	 * @param centerName
	 * @return
	 */
	public Center getCenterBycenterName(String centerName);
	/**
	 * 
	 * @param email
	 * @return
	 */
	public Center getCenterByemail(String email);
	/**
	 * 
	 * @param website
	 * @return
	 */
	public Center getCenterBywebsite(String website);
	/**
	 * 
	 * @param centerCode
	 * @return
	 */
	public Center getCenterBycenterCode(String centerCode);
	/**
	 * 
	 * @param postalCode
	 * @return
	 */
	public Center getCenterBypostalCode(String postalCode);
	/**
	 * 
	 * @param center
	 * @return
	 */
	public boolean putCenter(Center center);
	/**
	 * 
	 * @param center
	 * @return
	 */
	public boolean updateCenter(Center center);
	/**
	 * 
	 * @return
	 */
	public List<Center> getAllCenters();

}

/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Level;

/**
 * @author peter
 *
 */
public interface AppleLevelDAO {
	
	public Level getLevel(String centerId, String uuid);
	
	public List<Level> getLevels(String centerId); 
	
	public boolean putLevel(Level level);
	
	public boolean updateLevel(Level level);
	
	
	

}

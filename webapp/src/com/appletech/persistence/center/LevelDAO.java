/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Level;
import com.appletech.persistence.GenericDAO;

/**  
 * @author peter
 *
 */
public class LevelDAO extends GenericDAO implements AppleLevelDAO {

	private static LevelDAO levelDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return LevelDAO Instance
	 */
	public static LevelDAO getInstance() {

		if (levelDAO == null) {
			levelDAO = new LevelDAO();
		}
		return levelDAO;
	}

	/**
	 * 
	 */
	public LevelDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public LevelDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.center.AppleLevelDAO#getLevel(java.lang.String, java.lang.String)
	 */
	@Override
	public Level getLevel(String centerId, String uuid) {
		Level level = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Level WHERE centerId = ? AND uuid =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				level = beanProcessor.toBean(rset, Level.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Level with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return level;
	}

	/**
	 * @see com.appletech.persistence.center.AppleLevelDAO#getLevels(java.lang.String)
	 */
	@Override
	public List<Level> getLevels(String centerId) {
		List<Level> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Level WHERE centerId = ?;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Level.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all Level");
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	/**
	 * @see com.appletech.persistence.center.AppleLevelDAO#putLevel(com.appletech.bean.center.Level)
	 */
	@Override
	public boolean putLevel(Level level) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO level"
						+ "(uuid,centerId,levelId,description) VALUES (?,?,?,?);");) { 
		
			pstmt.setString(1, level.getUuid());
			pstmt.setString(2, level.getCenterId());
			pstmt.setString(3, level.getLevelId());
			pstmt.setString(4, level.getDescription()); 
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to updating level " + level);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleLevelDAO#updateLevel(com.appletech.bean.center.Level)
	 */
	@Override
	public boolean updateLevel(Level level) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Level SET levelId = ?,description = ? "
						+ "WHERE centerId = ? AND uuid = ?");) {

			
			pstmt.setString(1, level.getLevelId());
			pstmt.setString(2, level.getDescription()); 
			pstmt.setString(3, level.getCenterId());
			pstmt.setString(4, level.getUuid());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update level " + level);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

}

/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Contact;

/**
 * @author peter
 *
 */
public interface AppleContactDAO {
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public Contact getContact(String centerId, String uuid);
	/**
	 * 
	 * @param centerId
	 * @param mobile
	 * @return
	 */
	public Contact getContactByMbile(String centerId, String mobile);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public boolean existContact(String centerId, String mobile);
	/**
	 * 
	 * @param contact
	 * @param centerId
	 * @return
	 */
	public boolean putContact(Contact contact , String centerId, String mobile);
	
	/**
	 * 
	 * @param centerId
	 * @param mobile
	 * @return
	 */
	public boolean deleteContact(String centerId, String mobile);
     /**
      * 
      * @param centerId
      * @return
      */
	public List<Contact> getContactList(String centerId);

}

/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Center;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class CenterDAO extends GenericDAO implements AppleCenterDAO {
	
	private static CenterDAO centerDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CenterDAO Instance
	 */
	public static CenterDAO getInstance() {

		if (centerDAO == null) {
			centerDAO = new CenterDAO();
		}
		return centerDAO;
	}

	/**
	 * 
	 */
	public CenterDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public CenterDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}
	
	

	
	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenter(java.lang.String)
	 */
	@Override
	public Center getCenter(String centerCredenttials) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE "
						+ "(email = ? OR centerCode =? OR centerName =? OR centerNo =? OR website =?);");) {

			pstmt.setString(1, centerCredenttials);
			pstmt.setString(2, centerCredenttials);
			pstmt.setString(3, centerCredenttials);
			pstmt.setString(4, centerCredenttials);
			pstmt.setString(5, centerCredenttials);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with centerCredenttials " + centerCredenttials);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}
	
	
	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenter(java.lang.String, java.lang.String)
	 */
	@Override
	public Center getCenter(String centerCredenttials,String isActive) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE "
						+ "(email = ? OR centerCode =? OR centerName =? OR centerNo =? OR website =?) AND isActive =?;");) {

			pstmt.setString(1, centerCredenttials);
			pstmt.setString(2, centerCredenttials);
			pstmt.setString(3, centerCredenttials);
			pstmt.setString(4, centerCredenttials);
			pstmt.setString(5, centerCredenttials);
			pstmt.setString(6, isActive);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with centerCredenttials " + centerCredenttials + " and isActive" + isActive);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterById(java.lang.String)
	 */
	@Override
	public Center getCenterById(String uuid) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE uuid = ?;");) {

			pstmt.setString(1, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}
	
	

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterBycenterNo(java.lang.String)
	 */
	@Override
	public Center getCenterBycenterNo(String centerNo) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE centerNo = ?;");) {

			pstmt.setString(1, centerNo);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with centerNo " + centerNo);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterBycenterName(java.lang.String)
	 */
	@Override
	public Center getCenterBycenterName(String centerName) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE centerName = ?;");) {

			pstmt.setString(1, centerName);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with centerName " + centerName);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterByemail(java.lang.String)
	 */
	@Override
	public Center getCenterByemail(String email) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE email = ?;");) {

			pstmt.setString(1, email);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with email " + email);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterBywebsite(java.lang.String)
	 */
	@Override
	public Center getCenterBywebsite(String website) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE website = ?;");) {

			pstmt.setString(1, website);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with website " + website);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterBycenterCode(java.lang.String)
	 */
	@Override
	public Center getCenterBycenterCode(String centerCode) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE centerCode = ?;");) {

			pstmt.setString(1, centerCode);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with centerCode " + centerCode);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getCenterBypostalCode(java.lang.String)
	 */
	@Override
	public Center getCenterBypostalCode(String postalCode) {
		Center center = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center WHERE postalCode = ?;");) {

			pstmt.setString(1, postalCode);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				center = beanProcessor.toBean(rset, Center.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Center with postalCode " + postalCode);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return center;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#putCenter(com.appletech.bean.center.Center)
	 */
	@Override
	public boolean putCenter(Center center) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Center"
						+ "(uuid,centerNo,centerRegion,centerName,email,website,centerCode,postalCode,postalAddress,"
						+ "town,logoUri,signatureUri,isActive,dateRegistered)" + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);");) {

			pstmt.setString(1, center.getUuid());
			pstmt.setString(2, center.getCenterNo());
			pstmt.setString(3, center.getCenterRegion());
			pstmt.setString(4, center.getCenterName());
			pstmt.setString(5, center.getEmail());
			pstmt.setString(6, center.getWebsite());
			pstmt.setString(7, center.getCenterCode());
			pstmt.setString(8, center.getPostalCode());
			pstmt.setString(9, center.getPostalAddress());
			pstmt.setString(10, center.getTown());
			pstmt.setString(11, center.getLogoUri());
			pstmt.setString(12, center.getSignatureUri());
			pstmt.setString(13, center.getIsActive());
			pstmt.setTimestamp(14, center.getDateRegistred());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to put center " + center);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#updateCenter(com.appletech.bean.center.Center)
	 */
	@Override
	public boolean updateCenter(Center center) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Center SET "
						+ "centerNo =?,centerRegion =?,centerName =?,email =?,website =?,centerCode =?,postalCode =?,"
						+ "postalAddress =?,town =?,logoUri =?,signatureUri =?,isActive =? WHERE uuid = ?;");) {

			
			pstmt.setString(1, center.getCenterNo());
			pstmt.setString(2, center.getCenterRegion());
			pstmt.setString(3, center.getCenterName());
			pstmt.setString(4, center.getEmail());
			pstmt.setString(5, center.getWebsite());
			pstmt.setString(6, center.getCenterCode());
			pstmt.setString(7, center.getPostalCode());
			pstmt.setString(8, center.getPostalAddress());
			pstmt.setString(9, center.getTown());
			pstmt.setString(10, center.getLogoUri());
			pstmt.setString(11, center.getSignatureUri());
			pstmt.setString(12, center.getIsActive());
			pstmt.setString(13, center.getUuid());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update center " + center);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCenterDAO#getAllCenters()
	 */
	@Override
	public List<Center> getAllCenters() {
		List<Center> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Center ;");
				ResultSet rset = pstmt.executeQuery();) {

			list = beanProcessor.toBeanList(rset, Center.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all Centers");
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}


}

/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Config;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class ConfigDAO extends GenericDAO implements AppleConfigDAO {

	private static ConfigDAO configDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return ConfigDAO Instance
	 */
	public static ConfigDAO getInstance() {

		if (configDAO == null) {
			configDAO = new ConfigDAO();
		}
		return configDAO;
	}

	/**
	 * 
	 */
	public ConfigDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public ConfigDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.center.AppleConfigDAO#getConfig(java.lang.String)
	 */
	@Override
	public Config getConfig(String centerId) {
		Config config = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Config WHERE centerId = ?;");) {

			pstmt.setString(1, centerId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				config = beanProcessor.toBean(rset, Config.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Config with centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return config;
	}

	/**
	 * @see com.appletech.persistence.center.AppleConfigDAO#existConfig(java.lang.String)
	 */
	@Override
	public boolean existConfig(String centerId) {
		boolean exist = false;
		
		String dbcenterId = "";
		ResultSet rset = null;
		try(    Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT centerId FROM Config WHERE centerId = ?;");
      		){

	            pstmt.setString(1, centerId);
	            rset = pstmt.executeQuery();
	            
	            if(rset.next()){
	            	dbcenterId = rset.getString("centerId");
	            	exist = (dbcenterId != centerId) ? true : false;
				}
			
			
		  }
		     catch(SQLException e){
			 logger.error("SQL Exception while getting Config for centerId  " + centerId);
             logger.error(ExceptionUtils.getStackTrace(e)); 
             System.out.println(ExceptionUtils.getStackTrace(e));
		 }
		return exist;
	}

	/**
	 * @see com.appletech.persistence.center.AppleConfigDAO#putConfig(com.appletech.bean.center.Config, java.lang.String)
	 */
	@Override
	public boolean putConfig(Config config, String centerId) {
		boolean success = true;
		if(!existConfig(centerId)) {
		try(   Connection conn = dbutils.getConnection();
				
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Config"
						+"(uuid, centerId, cycleId, term, year, closingdate , oppeningdate, headTeacherComment) "
						+ "VALUES (?,?,?,?,?,?,?,?);");
				
				){
			pstmt.setString(1, config.getUuid());
			pstmt.setString(2, centerId);
			pstmt.setString(3, config.getCycleId());
			pstmt.setString(4, config.getTerm());
			pstmt.setString(5, config.getYear());
			pstmt.setString(6, config.getClosingdate());
			pstmt.setString(7, config.getOppeningdate());
			pstmt.setString(8, config.getHeadTeacherComment());
			pstmt.executeUpdate();
			
		}catch(SQLException e){
			logger.error("SQL Exception trying to put config " + config);
			logger.error(ExceptionUtils.getStackTrace(e)); 
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}	
	
		
		
		} else { 
			
			      try(
					Connection conn = dbutils.getConnection();
					PreparedStatement pstmt = conn.prepareStatement("UPDATE Config SET cycleId =?, term = ?," 
							+"year =?, closingdate =?, oppeningdate = ?, headTeacherComment = ? "
							+ "WHERE centerId = ? AND uuid = ?;");	
			    	
					) {
						pstmt.setString(1, config.getCycleId());
						pstmt.setString(2, config.getTerm());
						pstmt.setString(3, config.getYear());
						pstmt.setString(4, config.getClosingdate());
						pstmt.setString(5, config.getOppeningdate());
						pstmt.setString(6, config.getHeadTeacherComment());
						pstmt.setString(7, centerId);
						pstmt.setString(8, config.getUuid());
							
						pstmt.executeUpdate();
				
										
			} catch(SQLException e) {
				logger.error("SQL Exception trying to update config " + config);
				logger.error(ExceptionUtils.getStackTrace(e));
				System.out.println(ExceptionUtils.getStackTrace(e));
				success = false;				
			} 
		}
		return success;
	}

}

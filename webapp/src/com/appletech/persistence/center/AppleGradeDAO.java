/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Grade;

/**
 * @author peter
 *
 */
public interface AppleGradeDAO {
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public Grade getGrade(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @param description
	 * @return
	 */
	public Grade getGradeBydescription(String centerId,String description);
	/**
	 * 
	 * @param centerId
	 * @param point
	 * @return
	 */
	public Grade getGradeBypoint(String centerId,String point);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public boolean existGrade(String centerId,String point);
	/**
	 * 
	 * @param grade
	 * @param centerId
	 * @return
	 */
	public boolean putGrade(Grade grade, String centerId,String point); 
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Grade> getGradeList(String centerId);

}

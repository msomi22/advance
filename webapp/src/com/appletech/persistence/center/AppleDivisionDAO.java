/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Division;

/**
 * @author peter
 *
 */
public interface AppleDivisionDAO {
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public Division getDivision(String centerId,String uuid);
	
	public Division getDivisionByLevelId(String centerId,String levelId);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public boolean existDivision(String centerId,String description);
	/**
	 * 
	 * @param division
	 * @param centerId
	 * @return
	 */
	public boolean putDivision(Division division, String centerId,String description);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Division> getDivisionList(String centerId, String levelId);
	
	public List<Division> getDivisionList(String centerId);
	
	

}

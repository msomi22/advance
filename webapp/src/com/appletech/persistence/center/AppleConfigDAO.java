/**
 * 
 */
package com.appletech.persistence.center;

import com.appletech.bean.center.Config;

/**
 * @author peter
 *
 */
public interface AppleConfigDAO {
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public Config getConfig(String centerId); 
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public boolean existConfig(String centerId); 
	/**
	 * 
	 * @param config
	 * @param centerId
	 * @return
	 */
	public boolean putConfig(Config config, String centerId);
	
	

}

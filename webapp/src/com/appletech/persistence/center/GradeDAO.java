/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Grade;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class GradeDAO extends GenericDAO implements AppleGradeDAO {

	private static GradeDAO gradeDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return GradeDAO Instance
	 */
	public static GradeDAO getInstance() {

		if (gradeDAO == null) {
			gradeDAO = new GradeDAO();
		}
		return gradeDAO;
	}

	/**
	 * 
	 */
	public GradeDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public GradeDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}
	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#getGrade(java.lang.String)
	 */
	@Override
	public Grade getGrade(String centerId,String uuid) {
		Grade grade = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Grade WHERE centerId = ? AND uuid = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				grade = beanProcessor.toBean(rset, Grade.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Grade with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return grade;
	}
	
	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#getGradeBydescription(java.lang.String, java.lang.String)
	 */
	@Override
	public Grade getGradeBydescription(String centerId, String description) {
		Grade grade = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Grade WHERE centerId = ? AND description = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				grade = beanProcessor.toBean(rset, Grade.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Grade with centerId " + centerId + " and description " + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return grade;
	}

	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#getGradeBypoint(java.lang.String, java.lang.String)
	 */
	@Override
	public Grade getGradeBypoint(String centerId, String point) {
		Grade grade = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Grade WHERE centerId = ? AND point = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, point);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				grade = beanProcessor.toBean(rset, Grade.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Grade with centerId " + centerId + " and point" + point);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return grade;
	}


	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#existGrade(java.lang.String)
	 */
	@Override
	public boolean existGrade(String centerId,String point) {
		boolean exist = false;
		
		String dbcenterId = "";
		String dbpoint = "";
		ResultSet rset = null;
		try(    Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT centerId, point FROM Grade WHERE "
						+ "centerId = ? AND point = ?;");
      		){

	            pstmt.setString(1, centerId);
	            pstmt.setString(2, point);
	            rset = pstmt.executeQuery();
	            
	            if(rset.next()){
	            	dbcenterId = rset.getString("centerId");
	            	dbpoint = rset.getString("point");
	            	exist = (dbcenterId != centerId && dbpoint != point) ? true : false; 
				}
			
			
		  }
		     catch(SQLException e){
			 logger.error("SQL Exception while getting Grade for centerId  " + centerId  + " and point " + point);
             logger.error(ExceptionUtils.getStackTrace(e)); 
             System.out.println(ExceptionUtils.getStackTrace(e));
		 }
		return exist;
	}

	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#putGrade(com.appletech.bean.center.Grade, java.lang.String)
	 */
	@Override
	public boolean putGrade(Grade grade, String centerId,String point) {
		boolean success = true;
		if(!existGrade(centerId,point)) {
		try(   Connection conn = dbutils.getConnection();
				
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Grade"
						+"(uuid, centerId, lowerMark, upperMark, description, point) "
						+ "VALUES (?,?,?,?,?,?);");
				
				){
			pstmt.setString(1, grade.getUuid());
			pstmt.setString(2, centerId);
			pstmt.setString(3, grade.getLowerMark());
			pstmt.setString(4, grade.getUpperMark());
			pstmt.setString(5, grade.getDescription());
			pstmt.setString(6, grade.getPoint()); 
			pstmt.executeUpdate();
			
		}catch(SQLException e){
			logger.error("SQL Exception trying to put Grade " + grade);
			logger.error(ExceptionUtils.getStackTrace(e)); 
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}	
	
		
		} else { 
			
			      try(
					Connection conn = dbutils.getConnection();
					PreparedStatement pstmt = conn.prepareStatement("UPDATE Grade SET lowerMark =?, upperMark=?, description = ?,point =?" 
							+"WHERE centerId = ? AND uuid = ?;");	
			    	
					) {
						pstmt.setString(1, grade.getLowerMark());
						pstmt.setString(2, grade.getUpperMark());
						pstmt.setString(3, grade.getDescription());
						pstmt.setString(4, grade.getPoint()); 
						pstmt.setString(5, centerId);
						pstmt.setString(6, grade.getUuid());
						pstmt.executeUpdate();
				
										
			} catch(SQLException e) {
				logger.error("SQL Exception trying to update Grade " + grade);
				logger.error(ExceptionUtils.getStackTrace(e));
				System.out.println(ExceptionUtils.getStackTrace(e));
				success = false;				
			} 
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleGradeDAO#getGradeList(java.lang.String)
	 */
	@Override
	public List<Grade> getGradeList(String centerId) {
		List<Grade> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Grade WHERE centerId = ? ORDER BY Point ASC;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Grade.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Grades for center with id " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	
}

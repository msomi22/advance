/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.AccessLevel;
import com.appletech.persistence.GenericDAO;

/**    
 * Persistence abstraction for {@link AccessLevel}
 * 
 * @author <a href="mailto:mutegison2@gmail.com">Dennis Mutegi</a>
 *
 */

public class AccessLevelDAO extends GenericDAO implements AppleAccessLevelDAO {
	
	private static AccessLevelDAO accessLevelDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return AccessLevelDAO Instance
	 */
	public static AccessLevelDAO getInstance() {

		if (accessLevelDAO == null) {
			accessLevelDAO = new AccessLevelDAO();
		}
		return accessLevelDAO;
	}

	/**
	 * 
	 */
	public AccessLevelDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public AccessLevelDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}


	/**
	 * @see com.appletech.persistence.center.AppleAccessLevelDAO#getAccessLevelByuuid(java.lang.String, java.lang.String)
	 */
	@Override
	public AccessLevel getAccessLevelByuuid(String centerId, String uuid) {
		AccessLevel accessLevel = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM accessLevel WHERE uuid = ? AND centerId = ?;");) {

			pstmt.setString(1, uuid);
			pstmt.setString(2, centerId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				accessLevel = beanProcessor.toBean(rset, AccessLevel.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting accessLevel with uuid " + uuid + "from centre with id -" + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return accessLevel;
	}

	/**
	 * @see com.appletech.persistence.center.AppleAccessLevelDAO#getAccessLevelsByCenter(java.lang.String)
	 */
	@Override
	public List<AccessLevel> getAccessLevelsByCenter(String centerId) {
		List<AccessLevel> accessLevelList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM accessLevel WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			accessLevelList = beanProcessor.toBeanList(rset, AccessLevel.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all accessLevels for center with id " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return accessLevelList;
	}

	/**
	 * @see com.appletech.persistence.center.AppleAccessLevelDAO#deleteAccessLevel(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteAccessLevel(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM accessLevel WHERE uuid = ? ;");

		) {
			pstmt.setString(1, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting accessLevel with uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleAccessLevelDAO#putAccessLevel(com.appletech.bean.center.AccessLevel)
	 */
	@Override
	public boolean putAccessLevel(AccessLevel accessLevel) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO accessLevel"
						+ "(uuid,centerId,description)" + " VALUES (?,?,?);");) {

			pstmt.setString(1, accessLevel.getUuid());
			pstmt.setString(2, accessLevel.getCenterId());
			pstmt.setString(3, accessLevel.getDescription());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to put accessLevel " + accessLevel);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleAccessLevelDAO#updateAccessLevel(com.appletech.bean.center.AccessLevel)
	 */
	@Override
	public boolean updateAccessLevel(AccessLevel accessLevel) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE accessLevel SET "
						+ "description = ? WHERE centerId = ? AND uuid = ?;");) {

			pstmt.setString(1, accessLevel.getDescription());
			pstmt.setString(2, accessLevel.getCenterId());
			pstmt.setString(3, accessLevel.getUuid());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update accessLevel " + accessLevel);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

}

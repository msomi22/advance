/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Cycle;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class CycleDAO extends GenericDAO implements AppleCycleDAO {
	
	private static CycleDAO cycleDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CycleDAO Instance
	 */
	public static CycleDAO getInstance() {

		if (cycleDAO == null) {
			cycleDAO = new CycleDAO();
		}
		return cycleDAO;
	}

	/**
	 * 
	 */
	public CycleDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public CycleDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}
	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#getCycleByid(java.lang.String, java.lang.String)
	 */
	@Override
	public Cycle getCycleByid(String centerId, String uuid) {
		Cycle cycle = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Cycle WHERE centerId = ? AND uuid =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				cycle = beanProcessor.toBean(rset, Cycle.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting cycle with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return cycle;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#getCycle(java.lang.String, java.lang.String)
	 */
	@Override
	public Cycle getCycle(String centerId, String description) {
		Cycle cycle = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Cycle WHERE centerId = ? AND description =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				cycle = beanProcessor.toBean(rset, Cycle.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting cycle with centerId " + centerId + " and description " + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return cycle;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#putCycle(com.appletech.bean.center.Cycle)
	 */
	@Override
	public boolean putCycle(Cycle cycle) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Cycle"
						+ "(uuid,centerId,description)" + " VALUES (?,?,?);");) {

			pstmt.setString(1, cycle.getUuid());
			pstmt.setString(2, cycle.getCenterId());
			pstmt.setString(3, cycle.getDescription());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to put Cycle " + cycle);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#updateCycle(com.appletech.bean.center.Cycle)
	 */
	@Override
	public boolean updateCycle(Cycle cycle) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Cycle SET "
						+ "description = ? WHERE centerId = ? AND uuid = ?;");) {
			
			pstmt.setString(1, cycle.getDescription());
			pstmt.setString(2, cycle.getCenterId());
			pstmt.setString(3, cycle.getUuid());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update Cycle " + cycle);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#deleteCycle(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteCycle(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM Cycle WHERE centerId = ? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Cycle with centerId " + centerId + " and " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleCycleDAO#getAllCyclesPerSchool(java.lang.String)
	 */
	@Override
	public List<Cycle> getAllCyclesPerSchool(String centerId) {
		List<Cycle> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Cycle WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Cycle.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all Cycles withcenterId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

}

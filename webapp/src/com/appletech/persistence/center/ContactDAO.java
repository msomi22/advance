/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Contact;
import com.appletech.persistence.GenericDAO;

/** 
 *  
 * @author peter
 *
 */
public class ContactDAO extends GenericDAO implements AppleContactDAO {

	private static ContactDAO contactDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return ContactDAO Instance
	 */
	public static ContactDAO getInstance() {

		if (contactDAO == null) {
			contactDAO = new ContactDAO();
		}
		return contactDAO;
	}

	/**
	 * 
	 */
	public ContactDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public ContactDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#getContact(java.lang.String)
	 */
	@Override
	public Contact getContact(String centerId,String uuid) {
		Contact contact = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Contact WHERE centerId = ? AND uuid =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				contact = beanProcessor.toBean(rset, Contact.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Contact with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return contact;
	}
	
	

	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#getContactByMbile(java.lang.String, java.lang.String)
	 */
	@Override
	public Contact getContactByMbile(String centerId, String mobile) {
		Contact contact = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Contact WHERE centerId = ? AND mobile =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, mobile);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				contact = beanProcessor.toBean(rset, Contact.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Contact with centerId " + centerId + " and mobile " + mobile);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return contact;
	}


	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#existContact(java.lang.String)
	 */
	@Override
	public boolean existContact(String centerId, String mobile) {
		boolean exist = false;
		
		String dbcenterId = "";
		String dbmobile = "";
		ResultSet rset = null;
		try(    Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT centerId,mobile FROM Contact WHERE centerId = ? AND mobile =?;");
      		){

	            pstmt.setString(1, centerId);
	            pstmt.setString(2, mobile);
	            rset = pstmt.executeQuery();
	            
	            if(rset.next()){
	            	dbcenterId = rset.getString("centerId");
	            	dbmobile = rset.getString("mobile");
	            	exist = (dbcenterId != centerId && dbmobile != mobile) ? true : false;
				}
			
			
		  }
		     catch(SQLException e){
			 logger.error("SQL Exception while getting Contact for centerId  " + centerId + " and mobile" + mobile);
             logger.error(ExceptionUtils.getStackTrace(e)); 
             System.out.println(ExceptionUtils.getStackTrace(e));
		 }
		return exist;
	}

	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#putContact(com.appletech.bean.center.Contact, java.lang.String)
	 */
	@Override
	public boolean putContact(Contact contact, String centerId, String mobile) {
		boolean success = true;
		if(!existContact(centerId,mobile)) {
		try(   Connection conn = dbutils.getConnection();
				
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Contact"
						+"(uuid, centerId, mobile, description) "
						+ "VALUES (?,?,?,?);");
				
				){
			pstmt.setString(1, contact.getUuid());
			pstmt.setString(2, centerId);
			pstmt.setString(3, contact.getMobile());
			pstmt.setString(4, contact.getDescription());
			pstmt.executeUpdate();
			
		}catch(SQLException e){
			logger.error("SQL Exception trying to put Contact " + contact);
			logger.error(ExceptionUtils.getStackTrace(e)); 
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}	
	
		
		
		} else { 
			
			      try(
					Connection conn = dbutils.getConnection();
					PreparedStatement pstmt = conn.prepareStatement("UPDATE Contact SET mobile =?, description = ?" 
							+"WHERE centerId = ? AND uuid = ?;");	
			    	
					) {
			    	  
						pstmt.setString(1, contact.getMobile());
						pstmt.setString(2, contact.getDescription());
						pstmt.setString(3, centerId);
						pstmt.setString(4, contact.getUuid());
						pstmt.executeUpdate();
				
										
			} catch(SQLException e) {
				logger.error("SQL Exception trying to update Contact " + contact);
				logger.error(ExceptionUtils.getStackTrace(e));
				System.out.println(ExceptionUtils.getStackTrace(e));
				success = false;				
			} 
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#getContactList(java.lang.String)
	 */
	@Override
	public List<Contact> getContactList(String centerId) {
		List<Contact> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Contact WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Contact.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Contacts for center with id " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	/**
	 * @see com.appletech.persistence.center.AppleContactDAO#deleteContact(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteContact(String centerId, String mobile) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM Contact WHERE centerId = ? AND mobile = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, mobile);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Contact with centerId " + centerId + " and mobile " + mobile);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

}

/**
 * 
 */
package com.appletech.persistence.center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.Division;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class DivisionDAO extends GenericDAO implements AppleDivisionDAO {

	private static DivisionDAO divisionDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return DivisionDAO Instance
	 */
	public static DivisionDAO getInstance() {

		if (divisionDAO == null) {
			divisionDAO = new DivisionDAO();
		}
		return divisionDAO;
	}

	/**
	 * 
	 */
	public DivisionDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public DivisionDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}
	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#getDivision(java.lang.String)
	 */
	@Override
	public Division getDivision(String centerId,String uuid) {
		Division division = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Division WHERE centerId = ? AND uuid =? ;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				division = beanProcessor.toBean(rset, Division.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Division with centerId " + centerId + "and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return division;
	}
	
	
	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#getDivisionByLevelId(java.lang.String, java.lang.String)
	 */
	@Override
	public Division getDivisionByLevelId(String centerId, String levelId) {
		Division division = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Division WHERE centerId = ? AND levelId =? ;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, levelId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				division = beanProcessor.toBean(rset, Division.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Division with centerId " + centerId + " and levelId " + levelId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return division;
	}


	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#existDivision(java.lang.String)
	 */
	@Override
	public boolean existDivision(String centerId,String description) {
		boolean exist = false;
		
		String dbcenterId = "";
		String dbdescription = "";
		ResultSet rset = null;
		try(    Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT centerId,description FROM Division WHERE centerId = ? AND description =?;");
      		){

	            pstmt.setString(1, centerId);
	            pstmt.setString(2, description);
	            rset = pstmt.executeQuery();
	            
	            if(rset.next()){
	            	dbcenterId = rset.getString("centerId");
	            	dbdescription = rset.getString("description");
	            	
	            	exist = (dbcenterId != centerId && dbdescription != description) ? true : false;
				}
			
			
		  }
		     catch(SQLException e){
			 logger.error("SQL Exception while getting Division for centerId  " + centerId);
             logger.error(ExceptionUtils.getStackTrace(e)); 
             System.out.println(ExceptionUtils.getStackTrace(e));
		 }
		return exist;
	}

	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#putDivision(com.appletech.bean.center.Division, java.lang.String)
	 */
	@Override
	public boolean putDivision(Division division, String centerId,String description) {
		boolean success = true;
		if(!existDivision(centerId,description)) {
		try(   Connection conn = dbutils.getConnection();
				
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Division"
						+"(uuid, centerId, levelId, lowerMark, upperMark, description, grade) "
						+ "VALUES (?,?,?,?,?,?,?);");
				
				
				
				){
			pstmt.setString(1, division.getUuid());
			pstmt.setString(2, centerId);
			pstmt.setString(3, division.getLevelId());
			pstmt.setString(4, division.getLowerMark());
			pstmt.setString(5, division.getUpperMark());
			pstmt.setString(6, division.getDescription());
			pstmt.setString(7, division.getGrade());
			pstmt.executeUpdate();
			
		}catch(SQLException e){
			logger.error("SQL Exception trying to put Division " + division);
			logger.error(ExceptionUtils.getStackTrace(e)); 
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}	
	
		
		
		} else { 
			
			      try(
					Connection conn = dbutils.getConnection();
					PreparedStatement pstmt = conn.prepareStatement("UPDATE Division SET lowerMark =?,upperMark=?, description = ?, grade =?" 
							+"WHERE levelId = ? AND centerId = ? AND uuid = ?;");	
			    	
					) {
						pstmt.setString(1, division.getLowerMark());
						pstmt.setString(2, division.getUpperMark());
						pstmt.setString(3, division.getDescription());
						pstmt.setString(4, division.getGrade());
						pstmt.setString(5, division.getLevelId());
						pstmt.setString(6, centerId);
						pstmt.setString(7, division.getUuid());
						pstmt.executeUpdate();
				
										
			} catch(SQLException e) {
				logger.error("SQL Exception trying to update Division " + division);
				logger.error(ExceptionUtils.getStackTrace(e));
				System.out.println(ExceptionUtils.getStackTrace(e));
				success = false;				
			} 
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#getDivisionList(java.lang.String)
	 */
	@Override
	public List<Division> getDivisionList(String centerId) {
		List<Division> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Division WHERE centerId = ? ORDER BY Description ASC;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Division.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Divisions for center with id " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	
	/**
	 * @see com.appletech.persistence.center.AppleDivisionDAO#getDivisionList(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Division> getDivisionList(String centerId, String levelId) {
		List<Division> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Division WHERE centerId = ? AND levelId =? ORDER BY Description ASC;");
				
				) {
			
			pstmt.setString(1, centerId);
			pstmt.setString(2, levelId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Division.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Divisions for center with id " + centerId + " and levelId " + levelId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

}

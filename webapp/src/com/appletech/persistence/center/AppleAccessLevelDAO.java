/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.AccessLevel;

/**
 * @author peter
 *
 */
public interface AppleAccessLevelDAO {
	
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public AccessLevel getAccessLevelByuuid(String centerId , String uuid);
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<AccessLevel> getAccessLevelsByCenter(String centerId);
	
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteAccessLevel(String centerId , String uuid);
	
	/**
	 * 
	 * @param accessLevel
	 * @return
	 */
	public boolean putAccessLevel(AccessLevel accessLevel);
	
	/**
	 * 
	 * @param accessLevel
	 * @return
	 */
	public boolean updateAccessLevel(AccessLevel accessLevel);

}

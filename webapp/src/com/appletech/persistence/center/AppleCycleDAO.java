/**
 * 
 */
package com.appletech.persistence.center;

import java.util.List;

import com.appletech.bean.center.Cycle;

/**
 * @author peter
 *
 */
public interface AppleCycleDAO {
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public Cycle getCycleByid(String centerId, String uuid); 
	/**
	 * 
	 * @param centerId
	 * @param description
	 * @return
	 */
	public Cycle getCycle(String centerId, String description); 
	/**
	 * 
	 * @param cycle
	 * @return
	 */
	public boolean putCycle(Cycle cycle);
	/**
	 * 
	 * @param cycle
	 * @return
	 */
	public boolean updateCycle(Cycle cycle);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteCycle(String centerId, String uuid);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Cycle> getAllCyclesPerSchool(String centerId);

}

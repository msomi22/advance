/**
 * 
 */
package com.appletech.persistence.performance;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.appletech.bean.center.performance.CycleOneScore;
import com.appletech.bean.center.performance.CycleThreeScore;
import com.appletech.bean.center.performance.CycleTwoScore;
import com.appletech.bean.center.performance.Performance;
import com.appletech.persistence.stm.StudentSubjectDAO;

/**
 * @author cisco
 *
 */
public class TestPerfomanceDAO {
	
	private final String databaseName = "advancedb";
	private final String Host = "localhost";
	private final String databaseUsername = "advance";
	private final String databasePassword = "xP*h12_^p12_";
	private final int databasePort = 5432;
	
	private PerformanceDAO store;
	
	
	private final String CENTER_ID = "6EB51B7A-A4C2-4600-8539-2AFACD2BE43A";
	private final String STUDENT_ID = "33A9CE81-11E6-494F-9E5F-4F2C6C89CCE7";
	private final String uuid = "18EC5D1D-8ED2-4A3E-A03E-16258709846A";

	    
	private final String subjectId = "DE86BC64-D3ED-4B9D-B8FB-789C1CB913CC";
	private final String classroomId = "94147C37-803A-4424-9528-5EE3F0EF3E2C";
	private final String streamId = "C3055C82-0E2F-4342-B765-FF84D81D18DA";
	private final int cycleOneScore = 0;
	private final int cycleTwoScore = 0;
	private final int cycleThreeScore = 0;
	private final String term = "1";
	private final String  year = "2017";

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#getPerformance(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetPerformance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#getStudentPerformance(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudentPerformance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#putPerformance(com.appletech.bean.center.performance.Performance)}.
	 */
	@Ignore
	@Test
	public void testPutPerformance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#deletePerformance(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testDeletePerformance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#getSubjectPerformance(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetSubjectPerformance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#perfomanceExists(com.appletech.bean.center.performance.Performance)}.
	 */
	@Test
	public void testPerfomanceExists() {
		Performance p = new Performance();
		p.setUuid(uuid);
		p.setCenterId(CENTER_ID);
		p.setStudentId(STUDENT_ID);
		p.setSubjectId(subjectId);
		p.setClassroomId(classroomId);
		p.setStreamId(streamId);
		p.setTerm(term);
		p.setYear(year);
		store = new PerformanceDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
		
		boolean exists = store.perfomanceExists(p);
		
		System.out.println("-------exists " + exists);
		
		assertTrue(exists);
		
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#updatePerfomance(com.appletech.bean.center.performance.Performance)}.
	 */

	@Ignore
	@Test
	public void testUpdatePerfomance() {
		Performance p = new Performance();
		p.setUuid(uuid);
		p.setCenterId(CENTER_ID);
		p.setStudentId(STUDENT_ID);
		p.setSubjectId(subjectId);
		p.setClassroomId(classroomId);
		p.setStreamId(streamId);
		p.setTerm(term);
		p.setYear(year);
		p.setCycleOneScore(55);
		p.setCycleTwoScore(65);
		p.setCycleThreeScore(70);
		store = new PerformanceDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
		
		boolean result = store.updatePerfomance(p);
		
		System.out.println("======== ---- result -----" + result);
	}

	/**
	 * Test method for {@link com.appletech.persistence.performance.PerformanceDAO#putScore(com.appletech.bean.center.performance.Performance)}.
	 */
	@Test
	public void testPutScore() {
		CycleTwoScore p = new CycleTwoScore();
		//CycleThreeScore p = new CycleThreeScore();
		//CycleOneScore p = new CycleOneScore();
		p.setUuid(uuid);
		p.setCenterId(CENTER_ID);
		p.setStudentId(STUDENT_ID);
		p.setSubjectId(subjectId);
		p.setClassroomId(classroomId);
		p.setStreamId(streamId);
		p.setTerm(term);
		p.setYear(year);
		//p.setCycleOneScore(57);
		p.setCycleTwoScore(67);
		//p.setCycleThreeScore(70);
		System.out.println(p.getUuid());
		store = new PerformanceDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
		
		boolean result = store.putScore(p);
		
		System.out.println("======== ---- result -----" + result);
	}

}

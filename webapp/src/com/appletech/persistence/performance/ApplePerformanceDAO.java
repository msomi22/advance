package com.appletech.persistence.performance;

import java.util.List;

import com.appletech.bean.center.performance.Performance;

/**
 * Implementation of {@Performance} Performance 
 * 
 * @author peter
 *
 */
public interface ApplePerformanceDAO {
	/**
	 * 
	 * @param studentId
	 * @param subjectId
	 * @param term
	 * @param year
	 * @return
	 */
	public Performance getPerformance(String centerId,String studentId,String subjectId,String term,String year); 
	
	/**
	 * 
	 * @param centerId
	 * @param studentId
	 * @param term
	 * @param year
	 * @return
	 */
	public List<Performance> getStudentPerformance(String centerId,String studentId,String term,String year); 
	
	/**
	 * 
	 * @param centerId
	 * @param subjectId
	 * @param ClassId
	 * @param streamId
	 * @param term
	 * @param year
	 * @return
	 */
	 
	public List<Performance> getSubjectPerformance(String centerId,String subjectId,String ClassId,String streamId,String term,String year); 
	
   /**
    * 
    * @param performance
    * @param studentId
    * @param subjectId
    * @param term
    * @param year
    * @return
    */
	public boolean putPerformance(Performance performance);
	
	/**
	 * 
	 * @param performance
	 * @return
	 */
	public boolean updatePerfomance(Performance performance);
	
	/**
	    * 
	    * @param performance
	    * @param studentId
	    * @param subjectId
	    * @param term
	    * @param year
	    * @return
	    */
		public boolean putScore(Performance performance);
	
	/**
	 * 
	 * @param studentId
	 * @param subjectId
	 * @param term
	 * @param year
	 * @return
	 */
	public boolean deletePerformance(String studentId,String subjectId,String term,String year); 
	
	/**
	 * 
	 * @param performance
	 * @return
	 */
	public boolean perfomanceExists(Performance performance);


}

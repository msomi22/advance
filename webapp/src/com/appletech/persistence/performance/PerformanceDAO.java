/**
 * 
 */
package com.appletech.persistence.performance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.performance.CycleOneScore;
import com.appletech.bean.center.performance.CycleThreeScore;
import com.appletech.bean.center.performance.CycleTwoScore;
import com.appletech.bean.center.performance.Performance;
import com.appletech.persistence.GenericDAO;


/**
 * Implementation of {@Performance} Performance

 * 
 * @author peter
 * 
 *
 */
public class PerformanceDAO extends GenericDAO implements ApplePerformanceDAO {

	public static PerformanceDAO performanceDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * @return performanceDAO Instance
	 */
	public static PerformanceDAO getInstance() {
		if (performanceDAO == null) {
			performanceDAO = new PerformanceDAO();
		}
		return performanceDAO;
	}

	/**
	 * 
	 */
	public PerformanceDAO() {
		super();
	}

	public PerformanceDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#getPerformance(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Performance getPerformance(String centerId, String studentId, String subjectId, String term, String year) {
		Performance performance = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(
						"SELECT * FROM performance WHERE centerId = ? AND studentId = ? AND subjectId = ?"
								+ " AND term = ? AND year = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, studentId);
			pstmt.setString(3, subjectId);
			pstmt.setString(4, term);
			pstmt.setString(5, year);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				performance = beanProcessor.toBean(rset, Performance.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Performance with for center id " + centerId + " ,Student id  -"
					+ studentId + "" + " subjectId " + subjectId + " term" + term + " year" + year);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return performance;
	}

	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#getStudentPerformance(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Performance> getStudentPerformance(String centerId, String studentId, String term, String year) {
		List<Performance> classperformanceList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Performance WHERE centerId = ?"
						+ " AND studentId = ? AND term = ? AND year = ?;");) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, studentId);
			pstmt.setString(3, term);
			pstmt.setString(4, year);
			try (ResultSet rset = pstmt.executeQuery();) {
				classperformanceList = beanProcessor.toBeanList(rset, Performance.class);

			}
		} catch (SQLException e) {
			logger.error("SQLException when getting class performance List for centerId " + centerId + " and studentId "
					+ studentId + " and term , " + term + " ,and year " + year);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return classperformanceList;
	}

	/**
	 * 
	 * 
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#
	 * deletePerformance(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean deletePerformance(String studentId, String subjectId, String term, String year) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#getSubjectPerformance(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public List<Performance> getSubjectPerformance(String centerId, String subjectId, String ClassId, String streamId,
			String term, String year) {
		List<Performance> classperformanceList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Performance WHERE centerId = ?"
						+ " AND subjectId = ? AND (classroomid = ? OR streamid = ?) AND term = ? AND year = ?;");) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, subjectId);
			pstmt.setString(3, ClassId);
			pstmt.setString(4, streamId);
			pstmt.setString(5, term);
			pstmt.setString(6, year);
			try (ResultSet rset = pstmt.executeQuery();) {
				classperformanceList = beanProcessor.toBeanList(rset, Performance.class);

			}
		} catch (SQLException e) {
			logger.error("SQLException when getting subject performance List for centerId " + centerId
					+ " and subjectId " + subjectId + " and ClassId" + ClassId + "and streamId " + streamId
					+ " and term , " + term + " ,and year " + year);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return classperformanceList;
	}

	
	
	
	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#perfomanceExists(com.appletech.bean.center.performance.Performance)
	 */
	@Override
	public boolean perfomanceExists(Performance performance) {
		Performance p = null;
		boolean result = false;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(
						"SELECT * FROM performance WHERE centerId = ? AND studentId = ? AND subjectId = ?"
								+ " AND term = ? AND year = ? AND classroomId = ? AND streamId = ?;");) {

			pstmt.setString(1, performance.getCenterId());
			pstmt.setString(2, performance.getStudentId());
			pstmt.setString(3, performance.getSubjectId());
			pstmt.setString(4, performance.getTerm());
			pstmt.setString(5, performance.getYear());
			pstmt.setString(6, performance.getClassroomId());
			pstmt.setString(7, performance.getStreamId());
			rset = pstmt.executeQuery();
			while (rset.next()) {
				p = beanProcessor.toBean(rset, Performance.class);
				//System.out.println(p);
				if (p != null) {
					result = true;
				}
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Performance  " + performance);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}

		return result;
	}
	
	

	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#putPerformance(com.appletech.bean.center.performance.Performance, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean putPerformance(Performance performance) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmtcycle1 = conn.prepareStatement("INSERT INTO performance"
						+ "(uuid,centerId,studentId,subjectId,classroomId,streamId,cycleOneScore,term,year)" + " VALUES (?,?,?,?,?,?,?,?,?);");
				PreparedStatement pstmtcycle2 = conn.prepareStatement("INSERT INTO performance"
						+ "(uuid,centerId,studentId,subjectId,classroomId,streamId,cycleTwoScore,term,year)" + " VALUES (?,?,?,?,?,?,?,?,?);");
				PreparedStatement pstmtcycle3 = conn.prepareStatement("INSERT INTO performance"
						+ "(uuid,centerId,studentId,subjectId,classroomId,streamId,cycleThreeScore,term,year)" + " VALUES (?,?,?,?,?,?,?,?,?);");) {

			 if(performance instanceof CycleOneScore){
				 pstmtcycle1.setString(1, performance.getUuid());
				 pstmtcycle1.setString(2, performance.getCenterId());
				 pstmtcycle1.setString(3, performance.getStudentId());
				 pstmtcycle1.setString(4, performance.getSubjectId());
				 pstmtcycle1.setString(5, performance.getClassroomId());
				 pstmtcycle1.setString(6, performance.getStreamId());
				 pstmtcycle1.setInt(7, performance.getCycleOneScore());
				 pstmtcycle1.setString(8, performance.getTerm());
				 pstmtcycle1.setString(9, performance.getYear());
					

				 pstmtcycle1.executeUpdate(); 
			 }
			 if(performance instanceof CycleTwoScore){
				 pstmtcycle2.setString(1, performance.getUuid());
				 pstmtcycle2.setString(2, performance.getCenterId());
				 pstmtcycle2.setString(3, performance.getStudentId());
				 pstmtcycle2.setString(4, performance.getSubjectId());
				 pstmtcycle2.setString(5, performance.getClassroomId());
				 pstmtcycle2.setString(6, performance.getStreamId());
				 pstmtcycle2.setInt(7, performance.getCycleTwoScore());
				 pstmtcycle2.setString(8, performance.getTerm());
				 pstmtcycle2.setString(9, performance.getYear());
					

				 pstmtcycle2.executeUpdate();
			 }
			 if(performance instanceof CycleThreeScore){
				 pstmtcycle3.setString(1, performance.getUuid());
				 pstmtcycle3.setString(2, performance.getCenterId());
				 pstmtcycle3.setString(3, performance.getStudentId());
				 pstmtcycle3.setString(4, performance.getSubjectId());
				 pstmtcycle3.setString(5, performance.getClassroomId());
				 pstmtcycle3.setString(6, performance.getStreamId());
				 pstmtcycle3.setInt(7, performance.getCycleThreeScore()); 
				 pstmtcycle3.setString(8, performance.getTerm());
				 pstmtcycle3.setString(9, performance.getYear());
					

				 pstmtcycle3.executeUpdate();
			 }
			

		} catch (SQLException e) {
			logger.error("SQL Exception trying to performance staff " + performance);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}


	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#updatePerfomance(com.appletech.bean.center.performance.Performance)
	 */
	@Override
	public boolean updatePerfomance(Performance performance) {
		boolean success = true;

		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmtcycleone = conn.prepareStatement("UPDATE performance SET "
						+ "cycleOneScore = ? WHERE term = ? AND year = ? AND studentId = ? AND subjectId = ?");
				PreparedStatement pstmtcycletwo = conn.prepareStatement("UPDATE performance SET "
						+ "cycleTwoScore = ? WHERE term = ? AND year = ? AND studentId = ? AND subjectId = ?");
				PreparedStatement pstmtcyclethree = conn.prepareStatement("UPDATE performance SET "
						+ "cycleThreeScore = ? WHERE term = ? AND year = ? AND studentId = ? AND subjectId = ?")) {

			if (performance instanceof CycleOneScore) {
				pstmtcycleone.setInt(1, performance.getCycleOneScore());
				pstmtcycleone.setString(2, performance.getTerm());
				pstmtcycleone.setString(3, performance.getYear());
				pstmtcycleone.setString(4, performance.getStudentId());
				pstmtcycleone.setString(5, performance.getSubjectId());

				pstmtcycleone.executeUpdate();
			}
			if (performance instanceof CycleTwoScore) {
				pstmtcycletwo.setInt(1, performance.getCycleTwoScore());
				pstmtcycletwo.setString(2, performance.getTerm());
				pstmtcycletwo.setString(3, performance.getYear());
				pstmtcycletwo.setString(4, performance.getStudentId());
				pstmtcycletwo.setString(5, performance.getSubjectId());

				pstmtcycletwo.executeUpdate();
			}
			if (performance instanceof CycleThreeScore) {
				pstmtcyclethree.setInt(1, performance.getCycleThreeScore());
				pstmtcyclethree.setString(2, performance.getTerm());
				pstmtcyclethree.setString(3, performance.getYear());
				pstmtcyclethree.setString(4, performance.getStudentId());
				pstmtcyclethree.setString(5, performance.getSubjectId());

				pstmtcyclethree.executeUpdate();
			}

		} catch (SQLException e) {
			logger.error("SQL Exception trying to  update  performance " + performance);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.performance.ApplePerformanceDAO#putScore(com.appletech.bean.center.performance.Performance)
	 */
	@Override
	public boolean putScore(Performance performance) {
		// check if performance exists
		if (perfomanceExists(performance)) {
			return updatePerfomance(performance);
		} else {
			return putPerformance(performance);
		}

	}

}

/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.*;

import com.appletech.bean.center.stm.Stream;

/**
 * @author peter
 *
 */
public interface AppleStreamDAO {
	/**
	 * 
	 * @param centerId
	 * @param classroomId
	 * @return
	 */
	public Stream getStream(String centerId,String classroomId,String description); 
	
	/**
	 * 
	 * @param centerId
	 * @param classroomId
	 * @return
	 */
	public Stream getStreambyUuid(String centerId, String uuid); 
	/**
	 * 
	 * @param centerId
	 * @param description
	 * @return
	 */
	public Stream getStreamByDescription(String centerId,String description);
	/**
	 * 
	 * @param centerId
	 * @param classroomId
	 * @return
	 */
	public List<Stream> getStreamPerClass(String centerId,String classroomId); 
	/**
	 * 
	 * @param stream
	 * @return
	 */
	public boolean putStream(Stream stream);
	/**
	 * 
	 * @param stream
	 * @return
	 */
	public boolean updateStream(Stream stream);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteStream(String centerId,String uuid); 
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Stream> getAllStreams(String centerId);

}

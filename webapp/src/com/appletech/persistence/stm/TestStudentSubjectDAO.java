/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.appletech.bean.center.stm.StudentSubject;

/**
 * @author peter
 *
 */
public class TestStudentSubjectDAO {
	

	private final String databaseName = "advancedb";
	private final String Host = "localhost";
	private final String databaseUsername = "advance";
	private final String databasePassword = "xP*h12_^p12_";
	private final int databasePort = 5432;
	
	private StudentSubjectDAO store;
	
	
	private final String CENTER_ID = "6EB51B7A-A4C2-4600-8539-2AFACD2BE43A";
	private final String STUDENT_ID = "33A9CE81-11E6-494F-9E5F-4F2C6C89CCE7";

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#getStudentSubject(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudentSubject() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#getStudentMainsCount(java.lang.String, java.lang.String, int)}.
	 */
	@Ignore
	@Test
	public void testGetStudentMainsCount() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#getSubjects(java.lang.String, java.lang.String)}.
	 */
	//@Ignore
	@Test
	public void testGetSubjects() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
		List<StudentSubject> list = store.getSubjects(CENTER_ID, STUDENT_ID);
		for(StudentSubject ss : list){
			System.out.println(ss); 
		}
		
	}

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#putStudentSubject(com.appletech.bean.center.stm.StudentSubject)}.
	 */
	@Ignore
	@Test
	public void testPutStudentSubject() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#updateStudentSubject(com.appletech.bean.center.stm.StudentSubject)}.
	 */
	@Ignore
	@Test
	public void testUpdateStudentSubject() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * Test method for {@link com.appletech.persistence.stm.StudentSubjectDAO#deleteStudentSubject(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testDeleteStudentSubject() {
		store = new StudentSubjectDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

}

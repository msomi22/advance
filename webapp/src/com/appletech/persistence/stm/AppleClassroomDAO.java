/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.Classroom;

/**
 * @author peter
 *
 */
public interface AppleClassroomDAO {
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public Classroom getClassroomById(String centerId, String uuid);
	/**
	 * 
	 * @param centerId
	 * @param description
	 * @return
	 */
	public Classroom getClassroom(String centerId, String description);
	/**
	 * 
	 * @param classroom
	 * @return
	 */
	public boolean putClassroom(Classroom classroom);
	/**
	 * 
	 * @param classroom
	 * @return
	 */
	public boolean updateClassroom(Classroom classroom);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteClassroom(String centerId, String uuid);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Classroom> getClassroomList(String centerId);

}

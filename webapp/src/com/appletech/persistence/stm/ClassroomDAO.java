/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Classroom;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class ClassroomDAO extends GenericDAO  implements AppleClassroomDAO {

	private static ClassroomDAO classroomDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	/**
	 * @return ClassroomDAO Instance
	 */
	public static ClassroomDAO getInstance(){
		if(classroomDAO == null){
			classroomDAO = new ClassroomDAO();
		}
		return classroomDAO;
	}

	/**
	 * 
	 */
	public ClassroomDAO() {
		super();
	}
	
    public ClassroomDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#getClassroomById(java.lang.String, java.lang.String)
	 */
	@Override
	public Classroom getClassroomById(String centerId, String uuid) {
		Classroom classroom = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Classroom WHERE centerId =? AND uuid = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				classroom = beanProcessor.toBean(rset, Classroom.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Classroom with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return classroom;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#getClassroom(java.lang.String, java.lang.String)
	 */
	@Override
	public Classroom getClassroom(String centerId, String description) {
		Classroom classroom = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Classroom WHERE centerId =? AND description = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				classroom = beanProcessor.toBean(rset, Classroom.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Classroom with centerId " + centerId + " and description " + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return classroom;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#putClassroom(com.appletech.bean.center.stm.Classroom)
	 */
	@Override
	public boolean putClassroom(Classroom classroom) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Classroom"
						+ "(uuid,centerId,description)" + " VALUES (?,?,?);");) {

			pstmt.setString(1, classroom.getUuid());
			pstmt.setString(2, classroom.getCenterId());
			pstmt.setString(3, classroom.getDescription());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to add Classroom " + classroom);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#updateClassroom(com.appletech.bean.center.stm.Classroom)
	 */
	@Override
	public boolean updateClassroom(Classroom classroom) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Classroom SET description = ?"
						+ " WHERE uuid = ? AND CenterId =?;");) {

			pstmt.setString(1, classroom.getDescription());
			pstmt.setString(2, classroom.getUuid());
			pstmt.setString(3, classroom.getCenterId());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update Classroom " + classroom);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#deleteClassroom(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteClassroom(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM Classroom WHERE centerId =? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Classroom with centerId " + centerId + " and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleClassroomDAO#getClassroomList(java.lang.String)
	 */
	@Override
	public List<Classroom> getClassroomList(String centerId) {
		List<Classroom> classList = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Classroom WHERE centerId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  classList = beanProcessor.toBeanList(rset, Classroom.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting Classroom List for centerId " + centerId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return classList;
	}

}

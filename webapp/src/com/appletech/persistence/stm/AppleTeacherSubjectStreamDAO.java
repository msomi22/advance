/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.TeacherSubjectStream;

/**
 * @author peter
 *
 */
public interface AppleTeacherSubjectStreamDAO {

	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public TeacherSubjectStream getTeacherSubStream(String centerId, String teacherId, String subjectId, String streamId);
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<TeacherSubjectStream> getTeacherSubjectStreamByCenterId(String centerId);
	
	/**
	 * 
	 * @param centerId
	 * @param subjectId
	 * @return
	 */
	public List<TeacherSubjectStream> getTeacherSubjectStreamBySubjectId(String centerId , String subjectId);
	
	
	/**
	 * 
	 * @param centerId
	 * @param streamId
	 * @return
	 */
	public List<TeacherSubjectStream> getTeacherSubjectStreamByStreamId(String centerId , String streamId);
	
	/**
	 * 
	 * @param centerId
	 * @param teacherId
	 * @return
	 */
	public List<TeacherSubjectStream> getTeacherSubjectStreamByTeacherId(String centerId , String teacherId);
	
	
	/**
	 * 
	 * @param teacherSubjectStream
	 * @return
	 */
	public boolean putTeacherSubjectStream(TeacherSubjectStream teacherSubjectStream);
	
	/**
	 * 
	 * @param teacherSubjectStream
	 * @return
	 */
	public boolean updateTeacherSubjectStream(TeacherSubjectStream teacherSubjectStream);
	
	/**
	 * 
	 * @param centerId
	 * @param teacherSubjectStreamId
	 * @return
	 */
	public boolean deleteTeacherSubjectStream(String centerId, String teacherId, String subjectId, String streamId);
}

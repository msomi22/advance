/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Combination;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class CombinationDAO  extends GenericDAO implements AppleCombinationDAO {

	private static CombinationDAO combinationDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return CombinationDAO Instance
	 */
	public static CombinationDAO getInstance(){
		if(combinationDAO == null){
			combinationDAO = new CombinationDAO();
		}
		return combinationDAO;
	}

	/**
	 * 
	 */
	public CombinationDAO() {
		super();
	}
	
    public CombinationDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#getCombinationBYUUID(java.lang.String, java.lang.String)
	 */
	@Override
	public Combination getCombinationBYUUID(String centerId, String uuid) {
		Combination combination = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Combination WHERE centerId =? AND uuid = ?;");       
        		){
        	
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, uuid);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 combination  = beanProcessor.toBean(rset,Combination.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a Combination with centerId  " + centerId + " and uuid " + uuid);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return combination; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#getCombination(java.lang.String, java.lang.String)
	 */
	@Override
	public Combination getCombination(String centerId, String description) {
		Combination combination = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Combination WHERE centerId =? AND description = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				combination = beanProcessor.toBean(rset, Combination.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Combination with centerId " + centerId + " and description " + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return combination;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#putCombination(com.appletech.bean.center.stm.Combination)
	 */
	@Override
	public boolean putCombination(Combination combination) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Combination"
						+ "(uuid,centerId,description)" + " VALUES (?,?,?);");) {

			pstmt.setString(1, combination.getUuid());
			pstmt.setString(2, combination.getCenterId());
			pstmt.setString(3, combination.getDescription());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to add Combination " + combination);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#updateCombination(com.appletech.bean.center.stm.Combination)
	 */
	@Override
	public boolean updateCombination(Combination combination) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Combination SET description = ?"
						+ " WHERE uuid = ? AND CenterId =?;");) {

			pstmt.setString(1, combination.getDescription());
			pstmt.setString(2, combination.getUuid());
			pstmt.setString(3, combination.getCenterId());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update Combination " + combination);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#deleteCombination(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteCombination(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE FROM Combination WHERE centerId =? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Combination with centerId " + centerId + " and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDAO#getCombinationList(java.lang.String)
	 */
	@Override
	public List<Combination> getCombinationList(String centerId) {
		List<Combination> list = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Combination WHERE centerId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  list = beanProcessor.toBeanList(rset, Combination.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting Combination List for centerId " + centerId ); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return list;
	}

}

/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.TeacherStream;

/**
 * @author peter
 *
 */
public interface AppleTeacherStreamDAO {
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public TeacherStream getTeacherStreamBystreamId(String centerId,String streamId);
	
	/**
	 * 
	 * @param centerId
	 * @param teacherId
	 * @return
	 */
	public TeacherStream getTeacherStream(String centerId,String teacherId);
	/**
	 * 
	 * @param centerId
	 * @param teacherId
	 * @param streamId
	 * @return
	 */
	public TeacherStream getTeacherStream(String centerId,String teacherId,String streamId);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<TeacherStream> getTeacherStream(String centerId);
	/**
	 * 
	 * @param teacherStream
	 * @return
	 */
	public boolean putTeacherStream(TeacherStream teacherStream);
	/**
	 * 
	 * @param teacherStream
	 * @return
	 */
	public boolean updateTeacherStream(TeacherStream teacherStream);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteTeacherStream(String centerId,String uuid);

}

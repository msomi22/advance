/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.StudentSubject;

/**
 * @author peter
 *
 */
public interface AppleStudentSubjectDAO {
	/**
	 * 
	 * @param centerId
	 * @param studentId
	 * @param subjectId
	 * @return
	 */
	public StudentSubject getStudentSubject(String centerId,String studentId,String subjectId);
	
	/**
	 * 
	 * @param centerId
	 * @param studentId
	 * @return
	 */
	 
	public StudentSubject getComibationId(String centerId,String studentId);
    /**
     * 
     * @param centerId
     * @param studentId
     * @return
     */
	public List<StudentSubject> getSubjects(String centerId,String studentId); 
	/**
	 * 
	 * @param studentSubject
	 * @return
	 */
	public boolean putStudentSubject(StudentSubject studentSubject);
	/**
	 * 
	 * @param studentSubject
	 * @return
	 */
	public boolean updateStudentSubject(StudentSubject studentSubject);
	/**
	 * 
	 * @param centerId
	 * @param studentId
	 * @param subjectId
	 * @return
	 */
	public boolean deleteStudentSubject(String centerId,String studentId,String subjectId);

}

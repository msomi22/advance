/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.Combination;

/**
 * @author peter
 *
 */
public interface AppleCombinationDAO {
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public Combination getCombinationBYUUID(String centerId,String uuid);
	/**
	 * 
	 * @param description
	 * @return
	 */
	public Combination getCombination(String centerId,String description);
	/**
	 * 
	 * @param combination
	 * @return
	 */
	public boolean putCombination(Combination combination);
	/**
	 * 
	 * @param combination
	 * @return
	 */
	public boolean updateCombination(Combination combination);
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public boolean deleteCombination(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Combination> getCombinationList(String centerId);

}

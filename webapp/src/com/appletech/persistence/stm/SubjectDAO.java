/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Subject;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class SubjectDAO extends GenericDAO  implements AppleSubjectDAO {
	
	private static SubjectDAO subjectDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return subjectDAO Instance
	 */
	public static SubjectDAO getInstance(){
		if(subjectDAO == null){
			subjectDAO = new SubjectDAO();
		}
		return subjectDAO;
	}

	/**
	 * 
	 */
	public SubjectDAO() {
		super();
	}
	
    public SubjectDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#getSubjectByUUID(java.lang.String, java.lang.String)
	 */
	@Override
	public Subject getSubjectByUUID(String centerId, String uuid) {
		Subject subject = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Subject WHERE uuid = ?;");       
        		){
        	
        	 pstmt.setString(1, uuid);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 subject  = beanProcessor.toBean(rset,Subject.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a Subject with uuid " + uuid);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return subject; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#getSubjectByCODE(java.lang.String, java.lang.String)
	 */
	@Override
	public Subject getSubjectByCODE(String centerId, String subjectCode) {
		Subject subject = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM subject WHERE subjectCode = ? AND centerId = ?;");) {

			pstmt.setString(1, subjectCode);
			pstmt.setString(2, centerId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				subject = beanProcessor.toBean(rset, Subject.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting subject with subjectCode " + subjectCode + "from centre with id -" + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return subject;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#putSubject(com.appletech.bean.center.stm.Subject)
	 */
	@Override
	public boolean putSubject(Subject subject) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO subject"
						+ "(uuid,centerId,subjectCode,subjectDescr)" + " VALUES (?,?,?,?);");) {

			pstmt.setString(1, subject.getUuid());
			pstmt.setString(2, subject.getCenterId());
			pstmt.setString(3, subject.getSubjectCode());
			pstmt.setString(4, subject.getSubjectDescr());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to putting subject " + subject);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#updateSubject(com.appletech.bean.center.stm.Subject)
	 */
	@Override
	public boolean updateSubject(Subject subject) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE subject SET centerId = ?,subjectCode = ?,subjectDescr = ?"
						+ " WHERE uuid = ?");) {

			pstmt.setString(1, subject.getCenterId());
			pstmt.setString(2, subject.getSubjectCode());
			pstmt.setString(3, subject.getSubjectDescr());
			pstmt.setString(4, subject.getUuid());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update subject " + subject);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#deleteSubject(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteSubject(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM subject WHERE uuid = ? AND centerId = ? ;");

		) {
			pstmt.setString(1, uuid);
			pstmt.setString(2, centerId);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting subject with uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleSubjectDAO#getAllSubjects(java.lang.String)
	 */
	@Override
	public List<Subject> getAllSubjects(String centerId) {
		List<Subject> subjectList = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Subject WHERE centerId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  subjectList = beanProcessor.toBeanList(rset, Subject.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting Stream List for centerId " + centerId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return subjectList;
	}

}

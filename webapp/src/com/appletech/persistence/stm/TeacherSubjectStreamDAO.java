/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.TeacherSubjectStream;
import com.appletech.persistence.GenericDAO;

/**
 * @author cisco
 *
 */
public class TeacherSubjectStreamDAO extends GenericDAO implements AppleTeacherSubjectStreamDAO{
	
	private static TeacherSubjectStreamDAO teacherSubjectStreamDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return TeacherSubjectStreamDAO Instance
	 */
	public static TeacherSubjectStreamDAO getInstance() {

		if (teacherSubjectStreamDAO == null) {
			teacherSubjectStreamDAO = new TeacherSubjectStreamDAO();
		}
		return teacherSubjectStreamDAO;
	}

	/**
	 * 
	 */
	public TeacherSubjectStreamDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public TeacherSubjectStreamDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}


	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#getTeacherSubjectStreamByuuid(java.lang.String, java.lang.String)
	 */
	@Override
	public TeacherSubjectStream getTeacherSubStream(String centerId, String teacherId, String subjectId, String streamId) {
		TeacherSubjectStream teacherSubjectStream = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM teacherSubjectStream WHERE centerId = ? "
           	   		+ "AND teacherId = ? AND subjectId = ? AND streamId = ?;");       
        		){
        	
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, teacherId);
        	 pstmt.setString(3, subjectId);
        	 pstmt.setString(4, streamId);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 teacherSubjectStream  = beanProcessor.toBean(rset,TeacherSubjectStream.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a teacherSubjectStream with centerId " + centerId + " and teacherId "+ teacherId + 
        			 " and subjectId" + subjectId + " and " + streamId);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return teacherSubjectStream; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#getTeacherSubjectStreamByCenterId(java.lang.String)
	 */
	@Override
	public List<TeacherSubjectStream> getTeacherSubjectStreamByCenterId(String centerId) {
		List<TeacherSubjectStream> teacherSubjectStreamList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM teacherSubjectStream WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			teacherSubjectStreamList = beanProcessor.toBeanList(rset, TeacherSubjectStream.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all teacherSubjectStreams for centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return teacherSubjectStreamList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#getTeacherSubjectStreamBySubjectId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<TeacherSubjectStream> getTeacherSubjectStreamBySubjectId(String centerId, String subjectId) {
		List<TeacherSubjectStream> teacherSubjectStreamList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM teacherSubjectStream WHERE centerId = ? AND subjectId = ?;");
				
				) {
			
			pstmt.setString(1, centerId);
			pstmt.setString(2, subjectId);
			ResultSet rset = pstmt.executeQuery();

			teacherSubjectStreamList = beanProcessor.toBeanList(rset, TeacherSubjectStream.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all teacherSubjectStreams from CenterId -" + centerId + " for subject with id - " + subjectId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return teacherSubjectStreamList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#getTeacherSubjectStreamByStreamId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<TeacherSubjectStream> getTeacherSubjectStreamByStreamId(String centerId, String streamId) {
		List<TeacherSubjectStream> teacherSubjectStreamList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM teacherSubjectStream WHERE centerId = ? AND streamId = ?;");
				
				) {
			
			pstmt.setString(1, centerId);
			pstmt.setString(2, streamId);
			ResultSet rset = pstmt.executeQuery();

			teacherSubjectStreamList = beanProcessor.toBeanList(rset, TeacherSubjectStream.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all teacherSubjectStreams from CenterId -" + centerId + " for stream with id - " + streamId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return teacherSubjectStreamList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#getTeacherSubjectStreamByTeacherId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<TeacherSubjectStream> getTeacherSubjectStreamByTeacherId(String centerId, String teacherId) {
		List<TeacherSubjectStream> teacherSubjectStreamList = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM teacherSubjectStream WHERE centerId = ? AND teacherId = ?;");
				
				) {
			
			pstmt.setString(1, centerId);
			pstmt.setString(2, teacherId);
			ResultSet rset = pstmt.executeQuery();

			teacherSubjectStreamList = beanProcessor.toBeanList(rset, TeacherSubjectStream.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all teacherSubjectStreams from CenterId -" + centerId + " for teacher with id - " + teacherId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return teacherSubjectStreamList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#putTeacherSubjectStream(com.appletech.bean.center.stm.TeacherSubjectStream)
	 */
	@Override
	public boolean putTeacherSubjectStream(TeacherSubjectStream teacherSubjectStream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO teacherSubjectStream"
						+ "(uuid,centerId,teacherId,subjectId,streamId)" + " VALUES (?,?,?,?,?);");) {

			pstmt.setString(1, teacherSubjectStream.getUuid());
			pstmt.setString(2, teacherSubjectStream.getCenterId());
			pstmt.setString(3, teacherSubjectStream.getTeacherId());
			pstmt.setString(4, teacherSubjectStream.getSubjectId());
			pstmt.setString(5, teacherSubjectStream.getStreamId());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to put teacherSubjectStream " + teacherSubjectStream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#updateTeacherSubjectStream(com.appletech.bean.center.stm.TeacherSubjectStream)
	 */
	@Override
	public boolean updateTeacherSubjectStream(TeacherSubjectStream teacherSubjectStream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE teacherSubjectStream SET subjectId = ?,streamId = ?"
						+ " WHERE uuid = ? AND TeacherId =? AND CenterId = ?");) {

			pstmt.setString(1, teacherSubjectStream.getSubjectId());
			pstmt.setString(2, teacherSubjectStream.getStreamId());
			pstmt.setString(3, teacherSubjectStream.getUuid());
			pstmt.setString(4, teacherSubjectStream.getTeacherId());
			pstmt.setString(5, teacherSubjectStream.getCenterId());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to updating teacherSubjectStream " + teacherSubjectStream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}


	/**
	 * @see com.appletech.persistence.stm.AppleTeacherSubjectStreamDAO#deleteTeacherSubjectStream(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteTeacherSubjectStream(String centerId, String teacherId, String subjectId, String streamId) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM teacherSubjectStream WHERE centerId = ? AND "
						+ "teacherId = ? AND subjectId = ? AND streamId = ?;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, teacherId);
			pstmt.setString(3, subjectId);
			pstmt.setString(4, streamId);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting teacher_Subject_Stream for centerId" + centerId +
					" and teacherId " + teacherId + " and subjectId " + subjectId + " and streamId" + streamId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

}

/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.TeacherStream;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class TeacherStreamDAO  extends GenericDAO  implements AppleTeacherStreamDAO {
	
	private static TeacherStreamDAO teacherStreamDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return TeacherStreamDAO Instance
	 */
	public static TeacherStreamDAO getInstance() {

		if (teacherStreamDAO == null) {
			teacherStreamDAO = new TeacherStreamDAO();
		}
		return teacherStreamDAO;
	}

	/**
	 * 
	 */
	public TeacherStreamDAO() {
		super();
	}

	/**
	 * 
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public TeacherStreamDAO(String databaseName, String Host, String databaseUsername, String databasePassword,
			int databasePort) {
		super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}


	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#getTeacherStreamById(java.lang.String, java.lang.String)
	 */
	@Override
	public TeacherStream getTeacherStreamBystreamId(String centerId, String streamId) {
		TeacherStream teacherStream = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM TeacherStream WHERE centerId = ? AND streamId = ?;");       
        		){
        	
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, streamId);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 teacherStream  = beanProcessor.toBean(rset,TeacherStream.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a TeacherStream with centerId " + centerId + " and streamId "+ streamId );
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return teacherStream; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#getTeacherStream(java.lang.String, java.lang.String)
	 */
	@Override
	public TeacherStream getTeacherStream(String centerId, String teacherId) {
		TeacherStream teacherStream = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM TeacherStream WHERE centerId = ? AND teacherId = ?;");       
        		){
        	
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, teacherId);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 teacherStream  = beanProcessor.toBean(rset,TeacherStream.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a TeacherStream with centerId " + centerId + " and teacherId " + teacherId );
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return teacherStream; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#getTeacherStream(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public TeacherStream getTeacherStream(String centerId, String teacherId, String streamId) {
		TeacherStream teacherStream = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM TeacherStream WHERE centerId = ? AND"
           	   		+ " teacherId = ? AND streamId = ?;");       
        		){
        	
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, teacherId);
        	 pstmt.setString(3, streamId);
	         rset = pstmt.executeQuery();
	         while(rset.next()){
	        	 teacherStream  = beanProcessor.toBean(rset,TeacherStream.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a TeacherStream with centerId " + centerId + 
        			 " and teacherId "+ teacherId + "and streamId" + streamId);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return teacherStream; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#getTeacherStream(java.lang.String)
	 */
	@Override
	public List<TeacherStream> getTeacherStream(String centerId) {
		List<TeacherStream> list = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM TeacherStream WHERE centerId = ? ;");
				
				) {
			
			pstmt.setString(1, centerId);
			ResultSet rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, TeacherStream.class);

		} catch (SQLException e) {
			logger.error("SQL Exception when getting all TeacherStream for centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#putTeacherStream(com.appletech.bean.center.stm.TeacherStream)
	 */
	@Override
	public boolean putTeacherStream(TeacherStream teacherStream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO TeacherStream"
						+ "(uuid,centerId,teacherId,streamId)" + " VALUES (?,?,?,?);");) {

			pstmt.setString(1, teacherStream.getUuid());
			pstmt.setString(2, teacherStream.getCenterId());
			pstmt.setString(3, teacherStream.getTeacherId());
			pstmt.setString(4, teacherStream.getStreamId());


			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to put teacherStream " + teacherStream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#updateTeacherStream(com.appletech.bean.center.stm.TeacherStream)
	 */
	@Override
	public boolean updateTeacherStream(TeacherStream teacherStream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE TeacherStream SET streamId = ?"
						+ " WHERE centerId = ? AND teacherId =? AND uuid = ?");) {

			pstmt.setString(1, teacherStream.getStreamId());
			pstmt.setString(2, teacherStream.getCenterId());
			pstmt.setString(3, teacherStream.getTeacherId());
			pstmt.setString(4, teacherStream.getUuid());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to updating TeacherStream " + teacherStream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleTeacherStreamDAO#deleteTeacherStream(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteTeacherStream(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM TeacherStream WHERE centerId = ? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deletting TeacherStream  for centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

}

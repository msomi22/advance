/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.Stream;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class StreamDAO extends GenericDAO  implements AppleStreamDAO {

	public static StreamDAO streamDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return StreamDAO Instance
	 */
	public static StreamDAO getInstance(){
		if(streamDAO == null){
			streamDAO = new StreamDAO();
		}
		return streamDAO;
	}

	/**
	 * 
	 */
	public StreamDAO() {
		super();
	}
	
    public StreamDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}
	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#getStream(java.lang.String, java.lang.String)
	 */
	@Override
	public Stream getStream(String centerId, String classroomId,String description) {
		Stream stream = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Stream WHERE centerId =? AND classroomId = ? AND description =?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, classroomId);
			pstmt.setString(3, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				stream = beanProcessor.toBean(rset, Stream.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Stream with centerId " + centerId + " and classroomId " + classroomId + " and description" + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return stream;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#getStreamByDescription(java.lang.String, java.lang.String)
	 */
	@Override
	public Stream getStreamByDescription(String centerId, String description) {
		Stream stream = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Stream WHERE centerId =? AND description = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, description);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				stream = beanProcessor.toBean(rset, Stream.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Stream with centerId " + centerId + " and description " + description);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return stream;
	}
	
	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#getStreambyUuid(java.lang.String, java.lang.String)
	 */
	@Override
	public Stream getStreambyUuid(String centerId,String uuid) {
		Stream stream = null;
		
		
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM stream WHERE centerId =? AND uuid = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				stream = beanProcessor.toBean(rset, Stream.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting Stream with uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		
		return stream;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#getStreamPerClass(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Stream> getStreamPerClass(String centerId, String classroomId) {
		List<Stream> streamList = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Stream WHERE centerId = ? AND classroomId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   pstmt.setString(2, classroomId); 
         	   
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  streamList = beanProcessor.toBeanList(rset, Stream.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting Stream List for centerId " + centerId +" and classroomId " +classroomId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return streamList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#putStream(com.appletech.bean.center.stm.Stream)
	 */
	@Override
	public boolean putStream(Stream stream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Stream"
						+ "(uuid,centerId,classroomId,description)" + " VALUES (?,?,?,?);");) {

			pstmt.setString(1, stream.getUuid());
			pstmt.setString(2, stream.getCenterId());
			pstmt.setString(3, stream.getClassroomId());
			pstmt.setString(4, stream.getDescription());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to add Stream " + stream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#updateStream(com.appletech.bean.center.stm.Stream)
	 */
	@Override
	public boolean updateStream(Stream stream) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE Stream SET classroomId = ?, description =?"
						+ " WHERE uuid = ? AND CenterId =?;");) {

			pstmt.setString(1, stream.getClassroomId());
			pstmt.setString(2, stream.getDescription());
			pstmt.setString(3, stream.getUuid());
			pstmt.setString(4, stream.getCenterId());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update stream " + stream);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#deleteStream(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteStream(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM Stream WHERE centerId =? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Stream with centerId " + centerId + " and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStreamDAO#getAllStream(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Stream> getAllStreams(String centerId) {
		List<Stream> list = null;
		try(   
				Connection conn = dbutils.getConnection();
				PreparedStatement  pstmt = conn.prepareStatement("SELECT * FROM Stream WHERE centerId = ?;");   
				) {
			pstmt.setString(1, centerId);
			try(ResultSet rset = pstmt.executeQuery();){
				list = beanProcessor.toBeanList(rset, Stream.class);
			}

		} catch(SQLException e){
			logger.error("SQL Exception when getting all Streams for centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e)); 
		}
		return list;
	}

	

}

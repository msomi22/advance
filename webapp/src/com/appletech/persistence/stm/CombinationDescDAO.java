/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.CombinationDesc;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class CombinationDescDAO  extends GenericDAO implements AppleCombinationDescDAO {

	private static CombinationDescDAO combinationDescDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return CombinationDescDAO Instance
	 */
	public static CombinationDescDAO getInstance(){
		if(combinationDescDAO == null){
			combinationDescDAO = new CombinationDescDAO();
		}
		return combinationDescDAO;
	}

	/**
	 * 
	 */
	public CombinationDescDAO() {
		super();
	}
	
    public CombinationDescDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#getCombinationDescByID(java.lang.String, java.lang.String)
	 */
	@Override
	public CombinationDesc getCombinationDescByID(String centerId, String uuid) {
		CombinationDesc combinationDesc = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM CombinationDesc WHERE centerId =? AND uuid = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				combinationDesc = beanProcessor.toBean(rset, CombinationDesc.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting CombinationDesc with centerId " + centerId + " and uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return combinationDesc;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#getCombinationDescBySubjectId(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public CombinationDesc getCombinationDescBySubjectId(String centerId, String combinationId, String subjectId) {
		CombinationDesc combinationDesc = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM CombinationDesc WHERE centerId = ? AND combinationId = ? AND subjectId =? ;");       
        		){
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, combinationId);
        	 pstmt.setString(3, subjectId);
        	 
	         rset = pstmt.executeQuery();
	          while(rset.next()){
	        	  combinationDesc  = beanProcessor.toBean(rset, CombinationDesc.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a CombinationDesc with centerId " + centerId + " and combinationId " + combinationId + " and subjectId" + subjectId);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return combinationDesc; 
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#CombinationDesCount(java.lang.String, java.lang.String)
	 */
	@Override
	public int CombinationDesCount(String centerId, String combinationId) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM CombinationDesc WHERE centerId = ? AND combinationId =?");    		   
 	    ) {
        	pstmt.setString(1, centerId);       
        	pstmt.setString(2, combinationId);       
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting count for centerId " + centerId + "and combinationId " + combinationId);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#getCombinationDesc(java.lang.String, java.lang.String)
	 */
	@Override
	public List<CombinationDesc> getCombinationDesc(String centerId, String combinationId) {
		List<CombinationDesc> list = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM CombinationDesc WHERE centerId = ? AND combinationId =?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId); 
         	   pstmt.setString(2, combinationId); 
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  list = beanProcessor.toBeanList(rset, CombinationDesc.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting CombinationDesc List for centerId " + centerId + " and combinationId " + combinationId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return list;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#putCombinationDesc(com.appletech.bean.center.stm.CombinationDesc)
	 */
	@Override
	public boolean putCombinationDesc(CombinationDesc combinationDesc) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO CombinationDesc"
						+ "(uuid,centerId,combinationId,subjectId)" + " VALUES (?,?,?,?);");) {

			pstmt.setString(1, combinationDesc.getUuid());
			pstmt.setString(2, combinationDesc.getCenterId());
			pstmt.setString(3, combinationDesc.getCombinationId());
			pstmt.setString(4, combinationDesc.getSubjectId());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to add CombinationDesc " + combinationDesc);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#updateCombinationDesc(com.appletech.bean.center.stm.CombinationDesc)
	 */
	@Override
	public boolean updateCombinationDesc(CombinationDesc combinationDesc) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE CombinationDesc SET subjectId = ?"
						+ " WHERE centerId = ? AND combinationId =?;");) {

			pstmt.setString(1, combinationDesc.getSubjectId());
			pstmt.setString(2, combinationDesc.getCenterId());
			pstmt.setString(3, combinationDesc.getCombinationId());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update CombinationDesc " + combinationDesc);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#deleteCombinationDesc(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteCombinationDesc(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM CombinationDesc WHERE centerId =? AND uuid = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, uuid);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting CombinationDesc with centerId " + centerId + " and uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#deleteCombination(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteCombination(String centerId, String combinationId) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE FROM CombinationDesc WHERE centerId =? AND combinationId = ? ;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, combinationId);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting CombinationDesc with centerId " + centerId + " and combinationId " + combinationId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleCombinationDescDAO#getCombinationDesc(java.lang.String)
	 */
	@Override
	public List<CombinationDesc> getCombinationDesc(String centerId) {
		List<CombinationDesc> list = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM CombinationDesc WHERE centerId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  list = beanProcessor.toBeanList(rset, CombinationDesc.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting CombinationDesc List for centerId " + centerId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return list;
	}

}

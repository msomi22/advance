/**
 * 
 */
package com.appletech.persistence.stm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.stm.StudentSubject;
import com.appletech.persistence.GenericDAO;

/**  
 * @author peter
 *
 */
public class StudentSubjectDAO extends GenericDAO implements AppleStudentSubjectDAO {

	public static StudentSubjectDAO studentSubjectDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return studentSubjectDAO Instance
	 */
	public static StudentSubjectDAO getInstance(){
		if(studentSubjectDAO == null){
			studentSubjectDAO = new StudentSubjectDAO();
		}
		return studentSubjectDAO;
	}

	/**
	 * 
	 */
	public StudentSubjectDAO() {
		super();
	}
	
    public StudentSubjectDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#getStudentSubject(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public StudentSubject getStudentSubject(String centerId, String studentId, String subjectId) {
		StudentSubject studentSubject = null;
        ResultSet rset = null;
           try(
        	   Connection conn = dbutils.getConnection();
           	   PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM StudentSubject WHERE centerId = ? AND studentId = ? AND subjectId =? ;");       
        		){
        	 pstmt.setString(1, centerId);
        	 pstmt.setString(2, studentId);
        	 pstmt.setString(3, subjectId);
        	 
	         rset = pstmt.executeQuery();
	          while(rset.next()){
	        	 studentSubject  = beanProcessor.toBean(rset, StudentSubject.class);
	             }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when getting a StudentSubject with centerId " + centerId + " and studentId " + studentId + " and subjectId" + subjectId);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return studentSubject; 
	}
	
	

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#getComibationId(java.lang.String, java.lang.String)
	 */
	@Override
	public StudentSubject getComibationId(String centerId, String studentId) {
		StudentSubject studentSubject = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM StudentSubject WHERE centerId =? AND studentId = ?;");) {

			pstmt.setString(1, centerId);
			pstmt.setString(2, studentId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				studentSubject = beanProcessor.toBean(rset, StudentSubject.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting StudentSubject with centerId " + centerId + " and studentId " + studentId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return studentSubject;
	}

	

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#getSubjects(java.lang.String, java.lang.String)
	 */
	@Override
	public List<StudentSubject> getSubjects(String centerId, String studentId) {
		List<StudentSubject> studentSubjectList = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM studentsubject WHERE centerId = ? AND studentId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   pstmt.setString(2, studentId); 
         	   
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  studentSubjectList = beanProcessor.toBeanList(rset, StudentSubject.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting Student Subject List for centerId " + centerId +" and studentId " +studentId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
        
		return studentSubjectList;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#putStudentSubject(com.appletech.bean.center.stm.StudentSubject)
	 */
	@Override
	public boolean putStudentSubject(StudentSubject studentSubject) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO StudentSubject"
						+ "(uuid,centerId,studentId,subjectId)" + " VALUES (?,?,?,?);");) {

			pstmt.setString(1, studentSubject.getUuid());
			pstmt.setString(2, studentSubject.getCenterId());
			pstmt.setString(3, studentSubject.getStudentId());
			pstmt.setString(4, studentSubject.getSubjectId());
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to add StudentSubject " + studentSubject);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#updateStudentSubject(com.appletech.bean.center.stm.StudentSubject)
	 */
	@Override
	public boolean updateStudentSubject(StudentSubject studentSubject) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE StudentSubject SET SubjectId = ?"
						+ " WHERE uuid = ? AND CenterId =? AND StudentId =?");) {

			pstmt.setString(1, studentSubject.getSubjectId());
			pstmt.setString(2, studentSubject.getUuid());
			pstmt.setString(3, studentSubject.getCenterId());
			pstmt.setString(4, studentSubject.getStudentId());
		
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to update StudentSubject " + studentSubject);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.stm.AppleStudentSubjectDAO#deleteStudentSubject(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteStudentSubject(String centerId, String studentId, String subjectId) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM StudentSubject WHERE centerId =? AND studentId = ? "
						+ "AND subjectId = ?;");

		) {
			pstmt.setString(1, centerId);
			pstmt.setString(2, studentId);
			pstmt.setString(3, subjectId);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when deleting Stream with centerId " + centerId + " and studentId" + studentId +"and subjectId " + subjectId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

}

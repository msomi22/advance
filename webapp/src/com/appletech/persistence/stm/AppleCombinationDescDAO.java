/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.CombinationDesc;

/**
 * @author peter
 *
 */
public interface AppleCombinationDescDAO {
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public CombinationDesc getCombinationDescByID(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @param combinationId
	 * @param subjectId
	 * @return
	 */
	public CombinationDesc getCombinationDescBySubjectId(String centerId,String combinationId,String subjectId);
	/**
	 * 
	 * @param centerId
	 * @param combinationId
	 * @return
	 */
	public int CombinationDesCount(String centerId,String combinationId);
	/**
	 * 
	 * @param centerId
	 * @param combinationId
	 * @return
	 */
	public List<CombinationDesc> getCombinationDesc(String centerId,String combinationId);
	/**
	 * 
	 * @param combinationDesc
	 * @return
	 */
	public boolean putCombinationDesc(CombinationDesc combinationDesc);
	/**
	 * 
	 * @param combinationDesc
	 * @return
	 */
	public boolean updateCombinationDesc(CombinationDesc combinationDesc);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteCombinationDesc(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @param combinationId
	 * @return
	 */
	public boolean deleteCombination(String centerId,String combinationId);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<CombinationDesc> getCombinationDesc(String centerId);

}

/**
 * 
 */
package com.appletech.persistence.stm;

import java.util.List;

import com.appletech.bean.center.stm.Subject;

/**
 * @author peter
 *
 */
public interface AppleSubjectDAO {
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public Subject getSubjectByUUID(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @param subjectCode
	 * @return
	 */
	public Subject getSubjectByCODE(String centerId,String subjectCode);
	/**
	 * 
	 * @param subject
	 * @return
	 */
	public boolean putSubject(Subject subject);
	/**
	 * 
	 * @param subject
	 * @return
	 */
	public boolean updateSubject(Subject subject);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteSubject(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Subject> getAllSubjects(String centerId);
	

}

/**
 * 
 */
package com.appletech.persistence.student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.bean.center.student.Student;
import com.appletech.persistence.GenericDAO;

/** 
 * @author peter
 *
 */
public class StudentDAO  extends GenericDAO implements AppleStudentDAO {

	public static StudentDAO studentDAO;
	private Logger logger = Logger.getLogger(this.getClass());
	private BeanProcessor beanProcessor = new BeanProcessor();
	
	
	/**
	 * @return studentDAO Instance
	 */
	public static StudentDAO getInstance(){
		if(studentDAO == null){
			studentDAO = new StudentDAO();
		}
		return studentDAO;
	}

	/**
	 * 
	 */
	public StudentDAO() {
		super();
	}
	
    public StudentDAO(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
    	super(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getStudentByindexNo(java.lang.String, java.lang.String)
	 */
	@Override
	public Student getStudentByindexNo(String centerId, String indexNo) {
		Student student = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE indexNo = ? AND centerId = ?;");) {

			pstmt.setString(1, indexNo);
			pstmt.setString(2, centerId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				student = beanProcessor.toBean(rset, Student.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting student with indexNo " + indexNo + "from centre with id -" + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return student;
	}
	
	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getNextIndexNo(java.lang.String)
	 */
	@Override
	public String getNextIndexNo(String centerId) {
		Student student = null;
        ResultSet rset = null;
        try(
        		  Connection conn = dbutils.getConnection();
           	      PreparedStatement pstmt = conn.prepareStatement("SELECT indexNo FROM Student WHERE centerId = ? "
           	      		+ "ORDER BY indexNo ASC;");       
        		
        		){
        	
        	 pstmt.setString(1, centerId);
	         rset = pstmt.executeQuery();
	     while(rset.next()){
	
	    	 student  = beanProcessor.toBean(rset,Student.class);
	   }
        		
        }catch(SQLException e){
        	 logger.error("SQL Exception when next indexNo for centerId " + centerId);
             logger.error(ExceptionUtils.getStackTrace(e));
             System.out.println(ExceptionUtils.getStackTrace(e));
        }
        int index = 0;
        if(student != null){
        	 index = Integer.parseInt(student.getIndexNo());
        }
        
		return String.valueOf((index + 1));  
	}


	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getStudentByUUID(java.lang.String, java.lang.String)
	 */
	@Override
	public Student getStudentByUUID(String centerId, String uuid) {
		Student student = null;
		ResultSet rset = null;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE uuid = ? AND centerId = ?;");) {

			pstmt.setString(1, uuid);
			pstmt.setString(2, centerId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				student = beanProcessor.toBean(rset, Student.class);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception when getting student with uuid " + uuid + "from centre with id -" + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return student;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getStudentByStreamId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Student> getStudentByStreamId(String centerId, String currentStreamId) {
		List<Student> studentList = null;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE centerId = ? AND currentStreamId = ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);      
         	   pstmt.setString(2, currentStreamId); 
         	   try( ResultSet rset = pstmt.executeQuery();){
         		  studentList = beanProcessor.toBeanList(rset, Student.class);
         		  
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when getting class performance List for centerId " + centerId +" and currentStreamId " +currentStreamId); 
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
		return studentList;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#putStudent(com.appletech.bean.center.student.Student)
	 */
	@Override
	public boolean putStudent(Student student) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO student"
						+ "(uuid,centerId,admmitedStreamId,currentStreamId,combinationId,indexNo,firstname"
						+ ",middlename,lastname,gender,levelId,isActive,isAlumnus,photoUri,admissionDate)" +
						" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");) {

			pstmt.setString(1, student.getUuid());
			pstmt.setString(2, student.getCenterId());
			pstmt.setString(3, student.getAdmmitedStreamId());
			pstmt.setString(4, student.getCurrentStreamId());
			pstmt.setString(5, student.getCombinationId());
			pstmt.setString(6, student.getIndexNo());
			pstmt.setString(7, student.getFirstname());
			pstmt.setString(8, student.getMiddlename());
			pstmt.setString(9, student.getLastname());
			pstmt.setString(10, student.getGender());
			pstmt.setString(11, student.getLevelId());
			pstmt.setString(12, student.getIsActive());
			pstmt.setString(13, student.getIsAlumnus());
			pstmt.setString(14, student.getPhotoUri());
			pstmt.setTimestamp(15, student.getAdmissionDate());

			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to putting student " + student);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#updateStudent(com.appletech.bean.center.student.Student)
	 */
	@Override
	public boolean updateStudent(Student student) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("UPDATE student SET centerId = ?,admmitedStreamId = ?,currentStreamId = ?,"
						+ "combinationId = ?,indexNo = ?,firstname = ?,middlename = ?,lastname = ?,"
						+ "gender = ?,levelId = ?,isActive = ?,isAlumnus = ?,photoUri=?"
						+ " WHERE uuid = ?");) {

			pstmt.setString(1, student.getCenterId());
			pstmt.setString(2, student.getAdmmitedStreamId());
			pstmt.setString(3, student.getCurrentStreamId());
			pstmt.setString(4, student.getCombinationId());
			pstmt.setString(5, student.getIndexNo());
			pstmt.setString(6, student.getFirstname());
			pstmt.setString(7, student.getMiddlename());
			pstmt.setString(8, student.getLastname());
			pstmt.setString(9, student.getGender());
			pstmt.setString(10, student.getLevelId());
			pstmt.setString(11, student.getIsActive());
			pstmt.setString(12, student.getIsAlumnus());
			pstmt.setString(13, student.getPhotoUri());
			pstmt.setString(14, student.getUuid());

			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQL Exception trying to updating student " + student);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
			success = false;
		}

		return success;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#deleteStudent(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteStudent(String centerId, String uuid) {
		boolean success = true;
		try (Connection conn = dbutils.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("DELETE  FROM student WHERE uuid = ? AND centerId = ? ;");

		) {
			pstmt.setString(1, uuid);
			pstmt.setString(2, centerId);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
			logger.error("SQL Exception when getting staff with uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		return success;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getStudentCountPerStream(java.lang.String, java.lang.String)
	 */
	@Override
	public int getStudentCountPerStream(String centerId, String currentStreamId) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE centerId = ? AND currentStreamId =?");    		   
 	    ) {
        	pstmt.setString(1, centerId);       
        	pstmt.setString(2, currentStreamId);       
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting student count for centerId " + centerId + "and currentStreamId " + currentStreamId);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }
	
	@Override
	public int studentCount(String centerId) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE centerId = ?;");    		   
 	    ) {
        	pstmt.setString(1, centerId);          
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting isActive student count for centerId " + centerId);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }


	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getTotalStudentCount(java.lang.String, int)
	 */
	@Override
	public int isActiveCount(String centerId, String isActive) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE centerId = ? AND isActive =?;");    		   
 	    ) {
        	pstmt.setString(1, centerId);       
        	pstmt.setString(2, isActive);       
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting isActive student count for centerId " + centerId + "and isActive " + isActive);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getTotalAlumnusCount(java.lang.String, int)
	 */
	@Override
	public int isAlumnusCount(String centerId, String isAlumnus) {
        int count = 0;
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM student WHERE centerId = ? AND isAlumnus =?;");    		   
 	    ) {
        	pstmt.setString(1, centerId);       
        	pstmt.setString(2, isAlumnus);       
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting isAlumnus student count for centerId " + centerId + "and isAlumnus " + isAlumnus);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#searchStudent(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Student> searchStudent(String centerId, String query) {
		List<Student> list = null;

        try (
        		 Connection conn = dbutils.getConnection();
     	       PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Student WHERE centerId = ? "
     	       		+ "AND indexNo ILIKE ? OR firstname ILIKE ? OR middlename ILIKE ? OR lastname ILIKE ? OR gender ILIKE ?"
     	       		+ "ORDER BY currentStreamId ASC LIMIT ? OFFSET ?;");    		   
     	   ) {
         	   pstmt.setString(1, centerId);           
         	   pstmt.setString(2, "%" + query + "%");
         	   pstmt.setString(3, "%" + query + "%");
         	   pstmt.setString(4, "%" + query + "%");
         	   pstmt.setString(5, "%" + query + "%");
         	   pstmt.setString(6, "%" + query + "%");
         	   pstmt.setInt(7, 15);
         	   pstmt.setInt(8, 0);
         	   try( ResultSet rset = pstmt.executeQuery();){
     	       
     	       list = beanProcessor.toBeanList(rset, Student.class);
         	   }
        } catch (SQLException e) {
            logger.error("SQLException when searching Student for centerId " + centerId  +
            " and query '" + query +  "'");
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e));
        }
                
        Collections.sort(list);
        return list;
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getActiveStudents(java.lang.String, int, int, int)
	 */
	@Override
	public List<Student> getActiveStudents(String centerId, String isActive, int startIndex, int endIndex) {
		List<Student> studentList = new ArrayList<>();
		
		try(
				Connection conn = dbutils.getConnection();
				PreparedStatement psmt= conn.prepareStatement("SELECT * FROM Student WHERE "
						+ "centerId = ? AND isActive = ? ORDER BY currentStreamId , indexNo DESC LIMIT ? OFFSET ? ;");
				) {
			psmt.setString(1, centerId);
			psmt.setString(2, isActive);
			psmt.setInt(3, endIndex - startIndex);
			psmt.setInt(4, startIndex);
			
			try(ResultSet rset = psmt.executeQuery();){
			
				studentList = beanProcessor.toBeanList(rset, Student.class);
			}
		} catch (SQLException e) {
			logger.error("SQLException when trying to get a Student List for centerId" + centerId + " with startIndex " 
		+ startIndex + " and endIndex " + endIndex);
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e)); 
	    }
		
		return studentList;		
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getAlumnusStudents(java.lang.String, int, int, int)
	 */
	@Override
	public List<Student> getAlumnusStudents(String centerId, String isAlumnus, int startIndex, int endIndex) {
		List<Student> studentList = new ArrayList<>();
		
		try(
				Connection conn = dbutils.getConnection();
				PreparedStatement psmt= conn.prepareStatement("SELECT * FROM Student WHERE "
						+ "centerId = ? AND isAlumnus = ? ORDER BY currentStreamId , indexNo DESC LIMIT ? OFFSET ? ;");
				) {
			psmt.setString(1, centerId);
			psmt.setString(2, isAlumnus);
			psmt.setInt(3, endIndex - startIndex);
			psmt.setInt(4, startIndex);
			
			try(ResultSet rset = psmt.executeQuery();){
			
				studentList = beanProcessor.toBeanList(rset, Student.class);
			}
		} catch (SQLException e) {
			logger.error("SQLException when trying to get a Student List for centerId" + centerId + " with startIndex " 
		+ startIndex + " and endIndex " + endIndex);
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e)); 
	    }
		
		return studentList;		
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getAllStudents(java.lang.String, int, int)
	 */
	@Override
	public List<Student> getAllStudents(String centerId, int startIndex, int endIndex) {
		List<Student> studentList = new ArrayList<>();
		
		try(
				Connection conn = dbutils.getConnection();
				PreparedStatement psmt= conn.prepareStatement("SELECT * FROM Student WHERE "
						+ "centerId = ? ORDER BY indexNo DESC LIMIT ? OFFSET ? ;");// IsActive, 
				) {
			psmt.setString(1, centerId);
			psmt.setInt(2, endIndex - startIndex);
			psmt.setInt(3, startIndex);
			
			try(ResultSet rset = psmt.executeQuery();){
			
				studentList = beanProcessor.toBeanList(rset, Student.class);
			}
		} catch (SQLException e) {
			logger.error("SQLException when trying to get a Student List for centerId" + centerId + " with startIndex " 
		+ startIndex + " and endIndex " + endIndex);
            logger.error(ExceptionUtils.getStackTrace(e));
            System.out.println(ExceptionUtils.getStackTrace(e)); 
	    }
		
		return studentList;		
	}

	/**
	 * @see com.appletech.persistence.student.AppleStudentDAO#getAllStudent(java.lang.String)
	 */
	@Override
	public List<Student> getAllStudent(String centerId) {
		List<Student> list = null;
		try(   
				Connection conn = dbutils.getConnection();
				PreparedStatement  pstmt = conn.prepareStatement("SELECT * FROM Student WHERE centerId = ?;");   
				) {
			pstmt.setString(1, centerId);
			try(ResultSet rset = pstmt.executeQuery();){
				list = beanProcessor.toBeanList(rset, Student.class);
			}

		} catch(SQLException e){
			logger.error("SQL Exception when getting all Students for centerId " + centerId);
			logger.error(ExceptionUtils.getStackTrace(e));
			System.out.println(ExceptionUtils.getStackTrace(e)); 
		}
		return list;
	}

	
	
}

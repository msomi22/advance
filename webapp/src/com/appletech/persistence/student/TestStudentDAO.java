/**
 * 
 */
package com.appletech.persistence.student;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author peter
 *
 */
public class TestStudentDAO {
	
	private final String databaseName = "advancedb";
	private final String Host = "localhost";
	private final String databaseUsername = "advance";
	private final String databasePassword = "xP*h12_^p12_";
	private final int databasePort = 5432;
	
	private final String CENTER_ID = "6EB51B7A-A4C2-4600-8539-2AFACD2BE43A";
	
	StudentDAO storable = new StudentDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#StudentDAO(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 */
	@Ignore
	@Test
	public void testStudentDAOStringStringStringStringInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getStudentByindexNo(java.lang.String, java.lang.String)}.
	 */
	//@Ignore
	@Test
	public void testGetStudentByindexNo() {
		String indexno = storable.getNextIndexNo(CENTER_ID);
		
		System.out.println("indexno : " + indexno);
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getNextIndexNo(java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetNextIndexNo() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getStudentByUUID(java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudentByUUID() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getStudentByStreamId(java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudentByStreamId() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#putStudent(com.appletech.bean.center.student.Student)}.
	 */
	@Ignore
	@Test
	public void testPutStudent() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#updateStudent(com.appletech.bean.center.student.Student)}.
	 */
	@Ignore
	@Test
	public void testUpdateStudent() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#deleteStudent(java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testDeleteStudent() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getStudentCountPerStream(java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudentCountPerStream() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getTotalStudentCount(java.lang.String, int)}.
	 */
	@Ignore
	@Test
	public void testGetTotalStudentCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getTotalAlumnusCount(java.lang.String, int)}.
	 */
	@Ignore
	@Test
	public void testGetTotalAlumnusCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#searchStudent(java.lang.String, java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testSearchStudent() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getActiveStudents(java.lang.String, int, int, int)}.
	 */
	@Ignore
	@Test
	public void testGetActiveStudents() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getAlumnusStudents(java.lang.String, int, int, int)}.
	 */
	@Test
	public void testGetAlumnusStudents() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getAllStudents(java.lang.String, int, int)}.
	 */
	@Ignore
	@Test
	public void testGetAllStudents() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.appletech.persistence.student.StudentDAO#getAllStudent(java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetAllStudent() {
		fail("Not yet implemented");
	}

}

/**
 * 
 */
package com.appletech.persistence.student;

import java.util.*;

import com.appletech.bean.center.student.Student;

/**
 * @author peter
 *
 */
public interface AppleStudentDAO {
	/**
	 * 
	 * @param centerId
	 * @param indexNo
	 * @return
	 */
	public Student getStudentByindexNo(String centerId,String indexNo); 
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public Student getStudentByUUID(String centerId,String uuid); 
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public String getNextIndexNo(String centerId); 
	/**
	 * 
	 * @param centerId
	 * @param currentStreamId
	 * @return
	 */
	public List<Student> getStudentByStreamId(String centerId,String currentStreamId);
	/**
	 * 
	 * @param student
	 * @return
	 */
	public boolean putStudent(Student student);
	/**
	 * 
	 * @param student
	 * @return
	 */
	public boolean updateStudent(Student student);
	/**
	 * 
	 * @param centerId
	 * @param uuid
	 * @return
	 */
	public boolean deleteStudent(String centerId,String uuid);
	/**
	 * 
	 * @param centerId
	 * @param currentStreamId
	 * @return
	 */
	public int getStudentCountPerStream(String centerId,String currentStreamId);
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public int studentCount(String centerId);
	/**
	 * 
	 * @param centerId
	 * @param isActive
	 * @return
	 */
	public int isActiveCount(String centerId,String isActive);
	/**
	 * 
	 * @param centerId
	 * @param isAlumnus
	 * @return
	 */
	public int isAlumnusCount(String centerId,String isAlumnus);
	/**
	 * 
	 * @param centerId
	 * @param query
	 * @return
	 */
	public List<Student> searchStudent(String centerId,String query); 
	/**
	 * 
	 * @param centerId
	 * @param isActive
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<Student> getActiveStudents(String centerId,String isActive,int offset,int limit);
	/**
	 * 
	 * @param centerId
	 * @param isAlumnus
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<Student> getAlumnusStudents(String centerId,String isAlumnus,int offset,int limit);
	/**
	 * 
	 * @param centerId
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<Student> getAllStudents(String centerId,int offset,int limit);
	
	/**
	 * 
	 * @param centerId
	 * @return
	 */
	public List<Student> getAllStudent(String centerId);
	
	
	
	

}

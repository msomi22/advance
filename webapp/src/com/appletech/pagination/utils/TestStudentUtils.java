/**
 * 
 */
package com.appletech.pagination.utils;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author peter
 *
 */
public class TestStudentUtils {
	
	private final String databaseName = "advancedb";
	private final String Host = "localhost";
	private final String databaseUsername = "advance";
	private final String databasePassword = "xP*h12_^p12_";
	private final int databasePort = 5432;
	
	final String CENTERID = "6EB51B7A-A4C2-4600-8539-2AFACD2BE43A";
	
	private StudentUtils store;

	/**
	 * Test method for {@link com.yahoo.petermwenda83.persistence.utils.StudentUtils#getStudents(java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetStudents() {
		store = new StudentUtils(databaseName, Host, databaseUsername, databasePassword, databasePort);
		equals(store.getStudentCount(CENTERID));
		
	}

	/**
	 * Test method for {@link com.yahoo.petermwenda83.persistence.utils.StudentUtils#getIncomingCount(java.lang.String)}.
	 */
	//@Ignore
	@Test
	public void testGetIncomingCount() {
		store = new StudentUtils(databaseName, Host, databaseUsername, databasePassword, databasePort);
		equals(store.getStudentCount2(CENTERID));
	}

}

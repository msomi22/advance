/**
 * 
 */
package com.appletech.pagination.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.appletech.persistence.GenericDAO;

/**
 * @author peter
 *
 */
public class CommonUtils extends GenericDAO{
	
	private static CommonUtils commonUtils;
	  private final Logger logger = Logger.getLogger(this.getClass());

	  public static CommonUtils getInstance() {
	        if (commonUtils == null) {
	        	commonUtils = new CommonUtils();
	        }
	        return commonUtils;
	    }

	/**
	 * 
	 */
	public CommonUtils() {
		super();
	}
	
	public CommonUtils(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
        super(databaseName, Host, databaseUsername, databasePassword, databasePort);
    }
	
	
	/**
	 * @param centerId
	 * @return
	 */
	public int getStaffCount(String centerId) {
        int count = 0;
        
        try (
        		 Connection conn = dbutils.getConnection();
     	         PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Staff WHERE centerId = ?");    		   
 	    ) {
        	pstmt.setString(1, centerId);           
 	       	ResultSet rset = pstmt.executeQuery();
 	       	
 	       	while(rset.next()){
 	       		count = count + 1;
 	       	}

 	       
        } catch (SQLException e) {
            logger.error("SQLException when getting staff count for centerId " + centerId);
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        
        return count;
    }
	
	
}


package com.appletech.pagination.staff;

import java.util.List;

import com.appletech.bean.center.staff.Staff;
import com.appletech.pagination.utils.CommonUtils;
import com.appletech.persistence.staff.StaffDAO;

/** 
 * For Staff pagination
 * 
 *  @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class StaffPaginator {

	public final int PAGESIZE = 10;
	private static CommonUtils commonUtils;
	private static StaffDAO staffDAO;
	private String centerId;
	/**
	 * @param SchoolAccountUuid 
	 * 
	 */
	public StaffPaginator(String centerId) {
		commonUtils = CommonUtils.getInstance();
		staffDAO = StaffDAO.getInstance();
		this.centerId = centerId;
	}

	/**
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public StaffPaginator(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {

		//initialize the DAOs
		commonUtils = new CommonUtils(databaseName, Host, databaseUsername, databasePassword, databasePort);
		staffDAO = new StaffDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 *
	 * @return the first page
	 */
	public StaffPage getFirstPage() {
		StaffPage page = new StaffPage();
		List<Staff> staffList = staffDAO.getStaffOffset(centerId , 0, PAGESIZE);
		page = new StaffPage(1, getTotalPage(), PAGESIZE, staffList);	    
		return page;
	}


	/**
	 * @return
	 */
	public StaffPage getLastPage() {

		StaffPage page = new StaffPage();

		List<Staff> staffList = null;
		int  startIndex,sessionCount;
		int totalPage = getTotalPage();
		startIndex = (totalPage - 1) * PAGESIZE;
		sessionCount = commonUtils.getStaffCount(centerId);
		staffList = staffDAO.getStaffOffset(centerId, startIndex, sessionCount); 
		page = new StaffPage(totalPage, totalPage, PAGESIZE, staffList);
		return page;
	}


	/**
	 * @param currentPage
	 * @return
	 */
	public StaffPage getNextPage(final StaffPage currentPage) {
		int totalPage = getTotalPage();

		StaffPage page = new StaffPage();
		List<Staff> staffList = staffDAO.getStaffOffset(centerId, currentPage.getPageNum() * PAGESIZE, 
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new StaffPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, staffList);

		return page;
	}



	/**
	 * @param currentPage
	 * @return
	 */
	public StaffPage getPrevPage(final StaffPage currentPage) {
		int totalPage = getTotalPage();

		StaffPage page = new StaffPage();

		List<Staff> staffList = staffDAO.getStaffOffset(centerId, (currentPage.getPageNum() - 2) * PAGESIZE, 
				((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new StaffPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, staffList);

		return page;
	}



	/**
	 * @return
	 */
	public int getTotalPage() {
		int totalSize = 0;
		totalSize = commonUtils.getStaffCount(centerId);
		return ((totalSize - 1) / PAGESIZE) + 1;
	}


}

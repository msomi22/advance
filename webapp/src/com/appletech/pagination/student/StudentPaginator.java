
package com.appletech.pagination.student;

import java.util.List;

import com.appletech.bean.center.student.Student;
import com.appletech.pagination.utils.StudentUtils;
import com.appletech.persistence.student.StudentDAO;


/** 
 * For Student pagination
 * 
 *  @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class StudentPaginator {

	public final int PAGESIZE = 15;
	private static StudentUtils studentUtils;
	private static StudentDAO studentDAO;
	private String centerId;
	/**
	 * @param centerId 
	 * 
	 */
	public StudentPaginator(String centerId) {
		studentUtils = StudentUtils.getInstance();
		studentDAO = StudentDAO.getInstance();
		this.centerId = centerId;
	}

	/**
	 * @param databaseName
	 * @param Host
	 * @param databaseUsername
	 * @param databasePassword
	 * @param databasePort
	 */
	public StudentPaginator(String databaseName, String Host, String databaseUsername, String databasePassword, int databasePort) {
		//initialize the DAOs
		studentUtils = new StudentUtils(databaseName, Host, databaseUsername, databasePassword, databasePort);
		studentDAO = new StudentDAO(databaseName, Host, databaseUsername, databasePassword, databasePort);
	}

	/**
	 *
	 * @return the first page
	 */
	public StudentPage getFirstPage() {
		StudentPage page = new StudentPage();
		List<Student> stuList = studentDAO.getAllStudents(centerId , 0, PAGESIZE);
		page = new StudentPage(1, getTotalPage(), PAGESIZE, stuList);	    
		return page;
	}



	/**
	 * @return
	 */
	public StudentPage getLastPage() {

		StudentPage page = new StudentPage();

		List<Student> stuList = null;
		int  startIndex,sessionCount;
		int totalPage = getTotalPage();
		startIndex = (totalPage - 1) * PAGESIZE;
		sessionCount = studentUtils.getStudentCount2(centerId);
		stuList = studentDAO.getAllStudents(centerId, startIndex, sessionCount); 
		page = new StudentPage(totalPage, totalPage, PAGESIZE, stuList);
		return page;
	}


	/**
	 * @param currentPage
	 * @return
	 */
	public StudentPage getNextPage(final StudentPage currentPage) {
		int totalPage = getTotalPage();

		StudentPage page = new StudentPage();
		List<Student> smsList = studentDAO.getAllStudents(centerId, currentPage.getPageNum() * PAGESIZE, 
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new StudentPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, smsList);

		return page;
	}


	/**
	 * @param currentPage
	 * @return
	 */
	public StudentPage getPrevPage(final StudentPage currentPage) {
		int totalPage = getTotalPage();

		StudentPage page = new StudentPage();

		List<Student> smsList = studentDAO.getAllStudents(centerId, (currentPage.getPageNum() - 2) * PAGESIZE, 
				((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new StudentPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, smsList);

		return page;
	}



	/**
	 * @return
	 */
	public int getTotalPage() {
		int totalSize = 0;
		totalSize = studentUtils.getStudentCount2(centerId);
		return ((totalSize - 1) / PAGESIZE) + 1;
	}


}

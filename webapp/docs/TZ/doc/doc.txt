*********************
School Management
*********************
#center/school
#accessLevel
#grade
#division

**************************
Student Teacher Management
**************************
#classroom
#stream
#student
#subject
#teacher
*********************
Teacher Management
*********************
#studentSubject
#teacherSubjectStream
#teacherStream

*********************
*********************
#performance
********************


***************************************************************
1) table center
 -id
 -uuid
 -centerNo
 -centerRegion
 -centerName

2) table accessLevel
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -description

3) table grade
  -id
  -uuid
  -centerId REFERENCES center(uuid) 
  -lowerMark
  -upperMark
  -description 
  -point

4) table division
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -lowerMark
 -upperMark
 -description 


5) table classroom
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -description  (FORM 1) 

6) table stream
-id
-uuid
-centerId REFERENCES center(uuid) 
-classroomId REFERENCES classroom(uuid)
-description   (A) 

**) table cycle
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -description 

**) table config 
-id
-uuid
-centerId REFERENCES center(uuid) 
-cycleId REFERENCES cycle(uuid)
-term
-year


************************************************************
7) table student
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -admmitedStreamId REFERENCES stream(uuid)
 -currentStreamId REFERENCES stream(uuid)
 -indexNo
 -firstname
 -middlename
 -lastname
 -gender
 -admissionDate


8) table subject
  -id
  -uuid
  -centerId REFERENCES center(uuid) 
  -subjectCode
  -subjectDescr

9) table teacher
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -accessLevelId REFERENCES accessLevel(uuid)
 -name
 -gender
 -mobile
 -email

*******************************************************

10) table studentSubject
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -studentId REFERENCES student(uuid)
 -subjectId REFERENCES subject(uuid)
 -isaMain 


11) table teacherSubjectStream
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -teacherId REFERENCES teacher(uuid)
 -subjectId REFERENCES subject(uuid)
 -streamId REFERENCES stream(uuid)


12) table teacherStream
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -teacherId REFERENCES teacher(uuid)
 -streamId REFERENCES stream(uuid)


*******************************************
13) table performance
 -id
 -uuid
 -centerId REFERENCES center(uuid) 
 -studentId REFERENCES student(uuid)
 -subjectId REFERENCES subject(uuid)
 -classroomId REFERENCES classroom(uuid)
 -streamId REFERENCES stream(uuid)
 -configId REFERENCES config(uuid)
 -score 

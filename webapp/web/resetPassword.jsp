<!DOCTYPE html>

<%@page import="com.appletech.server.session.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="java.util.Calendar" %>


<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Advance Port | Reset Password </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Advance</b>Portal</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">


    <%
  String resetErr = "";
  String resetSuccess = "";
  
  if(session != null) {
	  resetErr = (String) session.getAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR);
	  resetSuccess = (String) session.getAttribute(SessionConstants.USER_FORGOT_PASSWORD_SUCCESS);
  } 
  if (StringUtils.isNotEmpty(resetErr)) {
     %>
  <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
    <%
            out.println(resetErr);
             %>
  </div>

  <%                                 
      session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_ERROR, null);
    } 
     else if (StringUtils.isNotEmpty(resetSuccess)) {
     %>
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
    <%
             out.println(resetSuccess);
             %>
  </div>

  <%                                
      session.setAttribute(SessionConstants.USER_FORGOT_PASSWORD_SUCCESS, null);
    } 
  
  %>


    <p class="login-box-msg">Reset Password here.</p>

    <form action="forgotPassword" method="POST">

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="CenterNo/CenterName/Email/CenterCode" name="centerCredential" value="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Mobile/Email/IdNo" name="mobileORemail" value="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>


      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="index.jsp">Back to login!</a><br> 
    
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

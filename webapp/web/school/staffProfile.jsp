<%@page import="com.appletech.persistence.staff.StaffDAO"%>
<%@page import="com.appletech.bean.center.staff.Staff"%>

<%@page import="com.appletech.persistence.stm.TeacherSubjectStreamDAO"%>
<%@page import="com.appletech.bean.center.stm.TeacherSubjectStream"%>

<%@page import="com.appletech.persistence.stm.TeacherStreamDAO"%>
<%@page import="com.appletech.bean.center.stm.TeacherStream"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.stm.SubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.Subject"%>

<%@page import="com.appletech.persistence.center.AccessLevelDAO"%>
<%@page import="com.appletech.bean.center.AccessLevel"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>



<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.text.SimpleDateFormat"%>


<%
	 
	 String centerId = "";
	 String teacherId = ""; 
	 String sessionaccessId = ""; 
	 
	 centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
	 sessionaccessId = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_ACCESS_LEVEL);
	
	 HashMap<String, String> staffIdHash = (HashMap<String, String>) session.getAttribute(SessionConstants.STAFF_UPDATE_PARAM);
	 if (staffIdHash == null) {
	   staffIdHash = new HashMap<String, String>();
	 }
	
	 if(request.getParameter("id") != null){
	    teacherId = (String) request.getParameter("id");
	 }else{
	   teacherId = StringUtils.trimToEmpty(staffIdHash.get("teacherId"));
	 }
	
	
	 StreamDAO streamDAO = StreamDAO.getInstance();
	 ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
	 SubjectDAO subjectDAO = SubjectDAO.getInstance();
	 StaffDAO staffDAO = StaffDAO.getInstance();
	 TeacherSubjectStreamDAO teacherSubStreamDAO = TeacherSubjectStreamDAO.getInstance();
	 TeacherStreamDAO teacherStreamDAO = TeacherStreamDAO.getInstance();
	 AccessLevelDAO accessLevelDAO = AccessLevelDAO.getInstance();
	 ConfigDAO configDAO = ConfigDAO.getInstance();
	 
	 List<TeacherSubjectStream> tsublist = new ArrayList<>();
	 if(teacherSubStreamDAO.getTeacherSubjectStreamByTeacherId(centerId,teacherId) != null){
		 tsublist = teacherSubStreamDAO.getTeacherSubjectStreamByTeacherId(centerId,teacherId);
	 }
	 
	
	 List<AccessLevel> accessLevelList =   new ArrayList<>();
	 if(accessLevelDAO.getAccessLevelsByCenter(centerId) != null){
		 accessLevelList = accessLevelDAO.getAccessLevelsByCenter(centerId);
	 }
	 
	 
	 Map<String,String> alevelMap = new HashMap<>();
	 for(AccessLevel alevel : accessLevelList){
		 alevelMap.put(alevel.getUuid(), alevel.getDescription());  
	 }
	
	 List<Classroom> classlist = new ArrayList<>();
	 if(classroomDAO.getClassroomList(centerId) != null){
		 classlist = classroomDAO.getClassroomList(centerId);
	 }
	 
	
	
	 Map<String,Classroom> classMap = new HashMap<>();
	 for(Classroom cl : classlist){
		 classMap.put(cl.getUuid(), cl); 
	 }
	 
	 List<Stream> streamlist = new ArrayList<>();
	 if(streamDAO.getAllStreams(centerId) != null){
		 streamlist = streamDAO.getAllStreams(centerId);
	 }
	 
	 
	 Map<String,Stream> streamMap = new HashMap<>();
	 for(Stream stream : streamlist){
		 streamMap.put(stream.getUuid(), stream); 
	 }
	 
	 
	 
	  List<Subject> subjects = new ArrayList<>();
	  
	  if(subjectDAO.getAllSubjects(centerId) != null){
	    subjects = subjectDAO.getAllSubjects(centerId);
	  }
	  
	  Map<String,Subject> subjectMap = new HashMap<String,Subject>();
	  for(Subject subject : subjects){
	    subjectMap.put(subject.getUuid(), subject); 
	  }
	  

	Staff staff = staffDAO.getStaffByuuid(centerId, teacherId);

	
	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();

    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

    
   // Stream stream = new Stream();
    //Classroom classroom = new Classroom();
    String a_l_Principal = "AF066A82-B397-4213-AA1A-D9BF03FE0152";
%>



<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Staff Profile,
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="staff.jsp"><i class="fa fa-dashboard"></i>
					Back</a></li>
			<li class="active">Staff profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="alert alert-info alert-dismissible hidden"
			id="alertMessage"></div>

		<div class="row">
			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle"
							src="../dist/img/avatar.png" alt="User profile picture">

						<h3 class="profile-username text-center">
							Name:
							<%=staff.getName() %>
						</h3>

						<p class="text-muted text-center">Subjects</p>


						<button class="btn btn-xs btn-danger" data-toggle="modal"
							data-target="#subclassModal">Add</button>


						<div class="table-responsive ">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Code</th>
										<th>Class</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<%
             int tsubCount = 1;
              for(TeacherSubjectStream tsub : tsublist){
                Subject sub = new Subject();
                if(subjectMap.get(tsub.getSubjectId()) != null){
                  sub = subjectMap.get(tsub.getSubjectId());
                }
                
                Stream stream = new Stream();
                Classroom classroom = new Classroom();
                          stream = streamMap.get(tsub.getStreamId());
                          classroom = classMap.get(stream.getClassroomId());
                 
                
              %>

									<tr>
										<td width="3%"><%=tsubCount %></td>
										<td class="center"><%=sub.getSubjectCode() %></td>
										<td class="center"><%=classroom.getDescription() + " " +stream.getDescription() %></td>
										<td><input id="subjectIdDel" type="hidden"
											value="<%=tsub.getSubjectId()%>"> <input
											id="classroomIdDel" type="hidden"
											value="<%=tsub.getStreamId()%>">
											<button class="btn btn-xs btn-danger removeSub">Remove</button>
										</td>
									</tr>

									<%
              tsubCount++;
              }
             %>


								</tbody>
							</table>
						</div>

						<hr>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

				<!-- About Me Box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">About the Staff</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<strong><i class="fa fa-book margin-r-5"></i> About</strong>

						<p class="text-muted">
							A humble guy from :
							<%=staff.getNationality() %>
							, very hard-working
						</p>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>








			<!-- Modal -->
			<div id="subclassModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Assign subject</h4>

						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="Subjects" class="col-sm-2 control-label">
									Subjects </label>

								<div class="col-sm-10">
									<select class="form-control" id="classSelectedId">
										<% 
                int sCount = 1;
                for(Subject sub : subjects){
                  %>
										<option value="<%=sub.getUuid()%>">
											<%=sub.getSubjectCode() + " - " + sub.getSubjectDescr() %>
										</option>
										<%
                  sCount++;
                  }
                %>


									</select>
								</div>
							</div>

							<hr>

							<div class="form-group">
								<label for="Classroom" class="col-sm-2 control-label">
									Classroom </label>

								<div class="col-sm-10">
									<select class="form-control" id="classSelected" multiple>
							<% 
				            int stmcount = 1;
				            for(Stream stream1 : streamlist){ 
					                Classroom classroom = classMap.get(stream1.getClassroomId());
					                 
				              %>
								<option value="<%=stream1.getUuid() %>">
								<%=classroom.getDescription() + " " +  stream1.getDescription() %>
								</option>
							  <%
				                 stmcount++;
				          	 
				              } 
				            %>

									</select>
								</div>
							</div>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

							<button type="button" class="btn btn-danger" id="addSubclass">
								Add</button>
						</div>
					</div>

				</div>
			</div>










			<!-- /.col -->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<p>Staff Profile</p>
					</ul>



					<%
	String updateErr = "";
	String udtsuccess = "";
	
	if(session != null) {
	  updateErr = (String) session.getAttribute(SessionConstants.STAFF_UPDATE_ERROR);
	  udtsuccess = (String) session.getAttribute(SessionConstants.STAFF_UPDATE_SUCCESS);
	} 
	if (StringUtils.isNotEmpty(updateErr)) {
	   %>
					<div class="alert alert-warning">
						<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
						<%
	          out.println(updateErr);
	           %>
					</div>

					<%                                 
	    session.setAttribute(SessionConstants.STAFF_UPDATE_ERROR, null);
	  } 
	   else if (StringUtils.isNotEmpty(udtsuccess)) {
	   %>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
						<%
	           out.println(udtsuccess);
	           %>
					</div>

					<%                                
	    session.setAttribute(SessionConstants.STAFF_UPDATE_SUCCESS, null);
	  } 
	
	%>




					<div class="tab-content">



						<!--  form starts here -->
						<form class="form-horizontal" method="POST" action="updateStaff">

							<div class="form-group">
								<label for="Name" class="col-sm-2 control-label"> Name </label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="name"
										value='<%=staff.getName() %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Mobile" class="col-sm-2 control-label">Mobile</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="mobile"
										value='<%=staff.getMobile() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="Email" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="email"
										value='<%=staff.getEmail() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="IdNumber" class="col-sm-2 control-label">IdNumber</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="idNumber"
										value='<%=staff.getIdNumber() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="AccessLevel" class="col-sm-2 control-label">
									AccessLevel </label>

								<div class="col-sm-10">
									<select class="form-control" name="accessLevel">
										<option value="<%=staff.getAccessLevelId() %>">
											<%=alevelMap.get(staff.getAccessLevelId()) %>
										</option>
										<%
                           
                            if(StringUtils.equals(sessionaccessId, a_l_Principal)){
                            	 if(!StringUtils.equals(staff.getAccessLevelId(), a_l_Principal)){
                            		 for(AccessLevel level : accessLevelList){
                            			 if(!StringUtils.equals(level.getUuid(), a_l_Principal) && !StringUtils.equals(staff.getAccessLevelId(), level.getUuid())){
                            			 %>
										<option value="<%=level.getUuid() %>">
											<%=level.getDescription() %></option>
										<%
                            		 }
                            		 }
                            	 }
                              }
                              
                             %>

										<% 
                              if(!StringUtils.equals(sessionaccessId, a_l_Principal)){                            	  
                            	  for(AccessLevel level : accessLevelList){
                         			 if(!StringUtils.equals(level.getUuid(), a_l_Principal) && !StringUtils.equals(staff.getAccessLevelId(), level.getUuid())){
                         			 %>
										<option value="<%=level.getUuid() %>">
											<%=level.getDescription() %></option>
										<%
                         		 }
                                }
                              }
                        	
	                          %>
									</select>
								</div>
							</div>



							<div class="form-group">
								<label for="Nationality" class="col-sm-2 control-label">
									Nationality </label>

								<div class="col-sm-10">
									<select class="form-control" name="nationality">
										<option value="<%=staff.getNationality() %>">
											<%=staff.getNationality() %>
										</option>
										<option value="TZ">TZ</option>
										<option value="UG">UG</option>
										<option value="KE">KE</option>
										<option value="EL">ElseWhere</option>
									</select>
								</div>
							</div>


							<div class="form-group">
								<label for="Gender" class="col-sm-2 control-label">
									Gender </label>

								<div class="col-sm-10">

									<%
                          if(staff.getGender().equalsIgnoreCase("M")) {
                        %>
									<div class="radio">
										<label> <input type="radio" name="gender" value="M"
											checked> Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="gender" value="F">
											Female
										</label>
									</div>

									<%}else { %>
									<div class="radio">
										<label> <input type="radio" name="gender" value="M">
											Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="gender" value="F"
											checked> Female
										</label>
									</div>

									<%} %>

								</div>
							</div>


							<div class="form-group">
								<label for="IsActive" class="col-sm-2 control-label">
									IsActive </label>

								<div class="col-sm-10">

									<%
                       if(StringUtils.equals(staff.getIsActive(), "1")){
                      %>
									<div class="radio">
										<label> <input type="radio" name="isActive" value="1"
											checked> Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="isActive" value="0">
											No
										</label>
									</div>
									<%}else{ %>
									<div class="radio">
										<label> <input type="radio" name="isActive" value="1">
											Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="isActive" value="0"
											checked> No
										</label>
									</div>

									<%} %>
								</div>
							</div>


							<%
                   //not a class teacher
                  if(teacherStreamDAO.getTeacherStream(centerId, teacherId) == null){
                  
                  %>

							<!--Is not a  class-Teacher, assign now -->
							<div class="form-group">
								<label class="col-sm-2 control-label"> Assign class </label>

								<div class="col-sm-10">
									<select class="form-control" name="classroom">

										<option value="">select one</option>
										<% 
                          int strmCount1 = 1;
                          for(Stream stream2 : streamlist){                         		 
                        	  Classroom  classroom = classMap.get(stream2.getClassroomId());
              	                  
                        		  
                            %>
							<option value="<%=stream2.getUuid() %>">
								<%=classroom.getDescription() + " " + stream2.getDescription() %>
							</option>
										<%
                               strmCount1++;
                        	 
                            } 
                          %>
									</select>
								</div>

							</div>

							<% } else {%>
							<!--  Is a class teacher   -->

							<div class="form-group">
								<label class="col-sm-2 control-label"> Change class </label>

								<%
	                TeacherStream teacherStream =  teacherStreamDAO.getTeacherStream(centerId, teacherId);
	                    Stream stream = streamMap.get(teacherStream.getStreamId());
	                    Classroom classroom = classMap.get(stream.getClassroomId());
	                
	                %>

								<div class="col-sm-10">
									<select class="form-control" name="classroom">
										<option value="<%=teacherStream.getStreamId() %>">
											<%=classroom.getDescription() + " " + stream.getDescription() %>
										</option>

										<% 
                          int strmCount = 1;
                          for(Stream stream3 : streamlist){ 
                        	  if(!StringUtils.equals(stream3.getUuid(), teacherStream.getStreamId())){
                        		 
              	                  classroom = classMap.get(stream3.getClassroomId());
              	                  
                        		  
                            %>
								<option value="<%=stream3.getUuid() %>">
											<%=classroom.getDescription() + " " + stream3.getDescription() %>
								</option>
										<%
                             strmCount++;
                        	  }
                            } 
                          %>
									</select>
								</div>

							</div>

							<% }  %>


							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="teacherId" value="<%=teacherId%>">
									<input type="hidden" name="centerId" value="<%=centerId%>">
									<button type="submit" class="btn btn-danger">Update</button>
								</div>
							</div>
						</form>

						<!--  form ends here -->


					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->





<!-- import footer -->
<jsp:include page="footer.jsp" />


<script type="text/javascript">
  
     $('#addSubclass').click(function(event) {

      var classIds = [];   // 
      var subjectId = jQuery.trim($('#classSelectedId :selected').val()); 
      var teacherId = "<%=teacherId%>";
      var centerId = "<%=centerId%>";  

      subjectId =  jQuery.trim(subjectId); 
      
      $("#classSelected option:selected").each(function () {
         var $this = $(this);
         if ($this.length) {
          var sub = $this.val();
          classIds.push(sub);
         }
      });

      var classesJSON = JSON.stringify(classIds);

      $.post("assignSubClass",
      {
        classrooms: classesJSON,
        subjectId: subjectId,
        teacherId: teacherId,
        centerId: centerId
      },

      function(data,status){
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.responseMessage;

        $('#alertMessage').removeClass('hidden'); 
        $('#subclassModal').modal('toggle');            
        $("#alertMessage").html(alert); 
          
        setTimeout(function(){
          location.reload(true);
         },5000); 

         
      });

    });





     $('.removeSub').click(function(event) { 
     
      var subjectId = jQuery.trim($("#subjectIdDel").val()); 
      var classroomId = jQuery.trim($("#classroomIdDel").val()); 
      var teacherId = "<%=teacherId%>";
      var centerId = "<%=centerId%>"; 

      $.post("removeTSubject",
      {
        subjectId: subjectId,
        classroomId: classroomId,
        teacherId: teacherId,
        centerId: centerId
      },

      function(data,status){
        var alert =  '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.responseMessage;

        $('#alertMessage').removeClass('hidden');          
        $("#alertMessage").html(alert); 

        setTimeout(function(){
          location.reload(true);
         },5000); 
 
         
      });

    });


</script>

<%@page import="com.appletech.persistence.center.CenterDAO"%>
<%@page import="com.appletech.bean.center.Center"%>

<%@page import="com.appletech.persistence.center.ContactDAO"%>
<%@page import="com.appletech.bean.center.Contact"%>



<%@page import="java.util.*"%>

<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%
	 
String centerId = ""; 
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

if (StringUtils.isEmpty(centerId)) {
    response.sendRedirect("../index.jsp");
   
}

session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");



CenterDAO centerDAO = CenterDAO.getInstance();
ContactDAO contactDAO = ContactDAO.getInstance();

Center center = centerDAO.getCenterById(centerId);

List<Contact> contacts = new ArrayList<Contact>(); 
contacts = contactDAO.getContactList(centerId);




%>





<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			School Profile,
		</h1>
		<ol class="breadcrumb">
			<li><a href="adminIndex.jsp"><i class="fa fa-dashboard"></i>
					Back</a></li>
			<li class="active">School profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		

		<div class="row">
			<div class="col-md-3">

				<!-- Profile  -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle"
							src="../dist/img/avatar.png" alt="User profile picture">

                       
                       <div class="alert alert-info alert-dismissible hidden"
			            id="alertMessage2"></div>
						<p class="text-muted text-center">School Contacts</p>


						<button class="btn btn-xs btn-danger" data-toggle="modal"
							data-target="#contactsModal">Add</button>


						<div class="table-responsive ">
							<table class="table table-bordered" onkeypress="return event.keyCode != 13;" id="contactsTable">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Mobile</th>
										<th>Description</th>
										<th>Action</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<%
						             int contactsCount = 1;
						              for(Contact contact : contacts){
                                    %>

									<tr>
										<td width="3%"><%=contactsCount %></td>
										<td contenteditable='true'><%=contact.getMobile() %></td>
										<td contenteditable='true'><%=contact.getDescription() %></td>
										
										<td>
											<button class="btn btn-xs btn-danger updateMobile">Update</button>
										</td>

										<td>
											<button class="btn btn-xs btn-danger removeMobile">Remove</button>
										</td>
									</tr>

									<%
									contactsCount++;
                                 }
                         %>


								</tbody>
							</table>
						</div>

						<hr>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				
				
				

			</div>








			<!-- Modal -->
			<div id="contactsModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Contacts</h4>

						</div>
						
						<div class="modal-body">

							<div class="form-group">
								<label for="Mobile" class="col-sm-2 control-label">
									Mobile:</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="mobileId">
								</div>
							</div>
							
							<hr>
							
							<div class="form-group">
								<label for="Description" class="col-sm-2 control-label">
									Description:</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="descriptionId"> 
								</div>
							</div>

						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

							<button type="button" class="btn btn-danger" id="addContactBtn">
								Add
							</button>
						</div>
					</div>

				</div>
			</div>





			<!-- /.col -->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<p> School Profile</p>
					</ul>



					<%
				String updateErr = "";
				String udtsuccess = "";
				
				if(session != null) {
				  updateErr = (String) session.getAttribute(SessionConstants.STAFF_CENTER_UPDATE_ERROR);
				  udtsuccess = (String) session.getAttribute(SessionConstants.STAFF_CENTER_UPDATE_SUCCESS);
				} 
				if (StringUtils.isNotEmpty(updateErr)) {
				   %>
								<div class="alert alert-warning">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
									<%
				          out.println(updateErr);
				           %>
								</div>
			
								<%                                 
				    session.setAttribute(SessionConstants.STAFF_CENTER_UPDATE_ERROR, null);
				  } 
				   else if (StringUtils.isNotEmpty(udtsuccess)) {
				   %>
								<div class="alert alert-success">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
									<%
				           out.println(udtsuccess);
				           %>
								</div>
			
								<%                                
				    session.setAttribute(SessionConstants.STAFF_CENTER_UPDATE_SUCCESS, null);
				  } 
				
				%>


				
         <div class="alert alert-info alert-dismissible hidden"
			id="alertMessage"></div>





					<div class="tab-content">



						<!--  form starts here -->
						<form class="form-horizontal" method="POST" action="staffUpdateCenter">

							<div class="form-group">
								<label for="centerNo" class="col-sm-2 control-label">CenterNo </label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerNo"
										value='<%=center.getCenterNo() %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="centerCode" class="col-sm-2 control-label">CenterCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerCode"
										value='<%=center.getCenterCode() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="centerName" class="col-sm-2 control-label">CenterName</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerName"
										value='<%=center.getCenterName() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="centerRegion" class="col-sm-2 control-label">CenterRegion</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerRegion"
										value='<%=center.getCenterRegion() %>'>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="website" class="col-sm-2 control-label">Web site</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="website"
										value='<%=center.getWebsite() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="website" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="email" class="form-control" name="email"
										value='<%=center.getEmail() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalCode" class="col-sm-2 control-label">PostalCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalCode"
										value='<%=center.getPostalCode() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalAddress" class="col-sm-2 control-label">PostalAddress</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalAddress"
										value='<%=center.getPostalAddress() %>'>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="town" class="col-sm-2 control-label">Town</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="town"
										value='<%=center.getTown() %>'>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="centerId" value="<%=centerId%>">
									<button type="submit" class="btn btn-danger">Update</button>
								</div>
							</div>
						</form>

						<!--  form ends here -->


					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->





<!-- import footer -->
<jsp:include page="footer.jsp" />


<script type="text/javascript">
  
     $('#addContactBtn').click(function(event) {
      
      var mobile = jQuery.trim($("#mobileId").val());
      var description = jQuery.trim($("#descriptionId").val());
      var centerId = "<%=centerId%>";   

      $.post("staffCenterContact",
      {
    	mobile: mobile,
    	description: description,
    	decision: 'addContact',
        centerId: centerId
      },

      function(data,status){
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.messageResponse;

        $('#alertMessage2').removeClass('hidden'); 
        $('#contactsModal').modal('toggle');            
        $("#alertMessage2").html(alert); 
          
        setTimeout(function(){
          location.reload(true);
         },5000); 

         
      });

    });




     //*************************************************

      var clicked1 = 0;
      $('.removeMobile').click(function(event) {
          clicked1 = 1;
       });
  
    $("#contactsTable").on("click", "tr", function(e) {
      var mobile = $.trim(this.cells[1].innerHTML);
 
      if(clicked1 === 1){

         clicked1 = 0;
              $.post("staffCenterContact",
              {
                mobile: mobile,
                decision: 'removeContact',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage2').removeClass('hidden');        
                $("#alertMessage2").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

   //*************************************************

   //*************************************************

      var clicked2 = 0;
      $('.updateMobile').click(function(event) {
          clicked2 = 1;
       });
  
    $("#contactsTable").on("click", "tr", function(e) {
      var mobile = $.trim(this.cells[1].innerHTML);
      var description = $.trim(this.cells[2].innerHTML);
     
      if(clicked2 === 1){

         clicked2 = 0;
              $.post("staffCenterContact",
              {
                mobile: mobile,
                description: description,
                decision: 'updateContact',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage2').removeClass('hidden');        
                $("#alertMessage2").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

    
</script>
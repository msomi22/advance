
<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.persistence.staff.StaffDAO"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="com.appletech.persistence.center.DivisionDAO"%>
<%@page import="com.appletech.bean.center.Division"%>

<%@page import="com.appletech.persistence.center.GradeDAO"%>
<%@page import="com.appletech.bean.center.Grade"%>

<%@page import="com.appletech.persistence.center.CycleDAO"%>
<%@page import="com.appletech.bean.center.Cycle"%>

<%@page import="com.appletech.persistence.stm.CombinationDAO"%>
<%@page import="com.appletech.bean.center.stm.Combination"%>

<%@page import="com.appletech.persistence.stm.CombinationDescDAO"%>
<%@page import="com.appletech.bean.center.stm.CombinationDesc"%>

<%@page import="com.appletech.persistence.stm.SubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.Subject"%>


<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%
DivisionDAO divisionDAO = DivisionDAO.getInstance();
GradeDAO gradeDAO = GradeDAO.getInstance();
ConfigDAO configDAO = ConfigDAO.getInstance();
CycleDAO cycleDAO = CycleDAO.getInstance();
StudentDAO studentDAO = StudentDAO.getInstance();
StaffDAO staffDAO = StaffDAO.getInstance();
SubjectDAO subjectDAO = SubjectDAO.getInstance();

CombinationDAO combinationDAO = CombinationDAO.getInstance();
CombinationDescDAO combinationDescDAO = CombinationDescDAO.getInstance();

String centerId = ""; 
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);


List<CombinationDesc> combDescriList = new ArrayList<>();
String combinationId = (String) request.getParameter("id");  
if(combinationDescDAO.getCombinationDesc(centerId, combinationId) != null){
	 combDescriList = combinationDescDAO.getCombinationDesc(centerId, combinationId);
}


List<Division> divisionsAdvance = new ArrayList<>();
List<Division> divisionsOlevel = new ArrayList<>();

List<Grade> grades  = new ArrayList<>();
List<Cycle> cycles  = new ArrayList<>();

if(divisionDAO.getDivisionList(centerId,"D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97") != null){
	divisionsAdvance = divisionDAO.getDivisionList(centerId,"D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97"); 
}

if(divisionDAO.getDivisionList(centerId,"5ACC1957-46F3-43E2-A994-A6127A79DDEE") != null){
	divisionsOlevel = divisionDAO.getDivisionList(centerId,"5ACC1957-46F3-43E2-A994-A6127A79DDEE"); 
}


grades = gradeDAO.getGradeList(centerId);
cycles = cycleDAO.getAllCyclesPerSchool(centerId);


 List<Combination> comblist = new ArrayList<>();
 if(combinationDAO.getCombinationList(centerId) != null){
	 comblist = combinationDAO.getCombinationList(centerId);
 }
 
 List<Subject> subjects = new ArrayList<>();
 
 if(subjectDAO.getAllSubjects(centerId) != null){
   subjects = subjectDAO.getAllSubjects(centerId);
 }

String term = "";
String year = "";
Config schconfig = new Config();
Cycle cycle = new Cycle();

if(configDAO.getConfig(centerId) != null){
	schconfig = configDAO.getConfig(centerId);
	cycle = cycleDAO.getCycleByid(centerId, schconfig.getCycleId()); 
}

term = schconfig.getTerm(); 
year = schconfig.getYear();

int totalStudents = 0;
int isActiveStudents = 0;
int totalStaff = 0;
int isActiveStaff = 0;

totalStudents = studentDAO.studentCount(centerId);
isActiveStudents = studentDAO.isActiveCount(centerId, "1");
totalStaff = staffDAO.staffCount(centerId);
isActiveStaff = staffDAO.isActiveCount(centerId, "1"); 

 if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
  }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>


<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Control panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>



            <style type="text/css">
            	#combinationMessage{
            		color: red;
            		font-size: 24px;

            	}

            	 #showremoveMessage{
            	 	color: green;
            		font-size: 24px;
            		text-decoration: underline red; 
                 }
            </style>




	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->

		<div class="row">
		<!-- Small boxes (Stat box) -->

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<p>Total Students</p>
						<h3><%=totalStudents %></h3>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
				</div>
			</div>

			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<p>Active Students</p>
						<h3><%=isActiveStudents %></h3>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
				</div>
			</div>
			<!-- ./col -->


			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<p>Total Staff</p>
						<h3><%=totalStaff %></h3>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
				</div>
			</div>

			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<p>Active Staff</p>
						<h3><%=isActiveStaff %></h3>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
				</div>
			</div>
			<!-- ./col -->
		</div>











		<%
String updateErr = "";
String udtsuccess = "";

if(session != null) {
  updateErr = (String) session.getAttribute(SessionConstants.CONFIG_UPDATE_ERROR);
  udtsuccess = (String) session.getAttribute(SessionConstants.CONFIG_UPDATE_SUCCESS);
} 
if (StringUtils.isNotEmpty(updateErr)) {
   %>
		<div class="alert alert-warning">
			<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
			<%
          out.println(updateErr);
           %>
		</div>

		<%                                 
    session.setAttribute(SessionConstants.CONFIG_UPDATE_ERROR, null);
  } 
   else if (StringUtils.isNotEmpty(udtsuccess)) {
   %>
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
			<%
           out.println(udtsuccess);
           %>
		</div>

		<%                                
    session.setAttribute(SessionConstants.CONFIG_UPDATE_SUCCESS, null);
  } 

%>


		<div class="row">
			<!-- Left col -->
			<section class="col-lg-7 connectedSortable">
				<!-- quick email widget -->
				<div class="box box-info">
					<div class="box-header">
						<i class="fa fa-gears"></i>
						<h3 class="box-title">Cycles</h3>
					</div>

					<div class="box-body">

						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" action="updateConfig" method="POST">
							<div class="box-body">

								<div class="form-group">
									<label for="Current Cycle" class="col-sm-3 control-label">
										Current Cycle </label>

									<div class="col-sm-6">
										<select class="form-control" name="currentCycle">
											<option value="<%=schconfig.getCycleId()%>">
												<%=cycle.getDescription() %></option>

											<% 
                          int cCount = 1;
                          for(Cycle cyc : cycles){
                        	  if(!StringUtils.equals(schconfig.getCycleId(), cyc.getUuid())){ 
	                          %>
											<option value="<%=cyc.getUuid()%>">
												<%=cyc.getDescription() %></option>
											<%
	                          cCount++;
                        	   }
	                          }
                          %>



										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="Term" class="col-sm-3 control-label"> Term
									</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" name="term"
											value="<%=schconfig.getTerm() %>">
									</div>
								</div>

								<div class="form-group">
									<label for="Year" class="col-sm-3 control-label"> Year
									</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" name="year"
											value="<%=schconfig.getYear() %>">
									</div>
								</div>

								<div class="form-group">
									<label for="Closing Date" class="col-sm-3 control-label">
										Closing Date </label>

									<div class="col-sm-6 date">
										<!-- <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div> -->
										<input type="text" class="form-control" name="closingDate"
											id="closingDate" value="<%=schconfig.getClosingdate() %>">
									</div>
								</div>


								<div class="form-group">
									<label for="Opening Date" class="col-sm-3 control-label">
										Opening Date </label>

									<div class="col-sm-6 date">
										<!-- <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div> -->
										<input type="text" class="form-control" name="openingDate"
											id="openingDate" value="<%=schconfig.getOppeningdate() %>">
									</div>
								</div>





								<div class="form-group">
									<label for="HeadTeacher's Comment"
										class="col-sm-3 control-label"> HeadTeacher's Comment
									</label>

									<div class="col-sm-6">
										<input type="text" class="form-control"
											name="headTeacherComment"
											value="<%=schconfig.getHeadTeacherComment() %>">
									</div>
								</div>

							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<input type="hidden" name="centerId" value="<%=centerId%>">
								<button type="submit" class="btn btn-info">Update</button>
							</div>
							<!-- /.box-footer -->
						</form>

					</div>


				</div>





				<!-- quick email widget -->
				<div class="box box-info">
					<div class="box-header">
						<i class="fa fa-gears"></i>
						<h3 class="box-title">Combinations</h3> 
						<button class="btn btn-xs btn-danger" data-toggle="modal"
							data-target="#addCombinationModal">Add</button> 
							<h1 id="showremoveMessage"></h1>
					</div>

					

					<div class="box-body">
                   
						
                   <div class="table-responsive ">
						<table class="table table-bordered" id="combinationTab"
							onkeypress="return event.keyCode != 13;">

							<thead>
								<tr>
									<th style="width: 10px">#</th>
									<th>Combination</th>
									<th>Action</th> 
									<th>Action</th>
									<th>Action</th>
								</tr>
							</thead>

						<tbody>

							<%  
						int cmbCount = 1;
						for (Combination comb : comblist) {
						
						%>
							<tr>
								<td><%=cmbCount%>.</td>  
								<td contenteditable='true'><%=comb.getDescription() %></td>
								<td>
									<button class="btn btn-info btn-sm combinationUpdate" value="<%=comb.getUuid() %>">
										Update 
								   </button>
								</td>

								<td> 
								   <button class="btn btn-info btn-sm showcombinationModal" value="<%=comb.getUuid() %>">
								   	  View
								   	</button>
								</td> 

								<td> 
								   <button class="btn btn-info btn-sm removeComb" value="<%=comb.getUuid() %>">
								   	  Remove 
								   	</button>
								</td>  

							</tr>
							<%
							cmbCount++;
						}
						%>
						</tbody>
						</table>
						</div>






					</div>


				</div>

			</section>
			<!-- /.Left col -->
            



            <!-- Modal -->
			<div id="combinationDescModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">
							  Combination Subjects for : 
							  <u><span id="showCombinationDesc"></span></u> 
							 </h4>

						</div>
						<div class="modal-body" id="subjectsCombTable">

                           

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close
							</button>
						</div>
					</div>

				</div>
			</div>


			<!-- Modal -->
			<div id="addCombinationModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">
							Add Combination
							<span id="combinationMessage"></span>  
							</h4>

							

						</div>
						<div class="modal-body">

                        <div class="table-responsive ">

                        <div class="form-group">
								<label for="Description" class="col-sm-2 control-label">Description</label>

								<div class="col-sm-6">
									<input type="text" class="form-control" id="description"
										placeholder="Description">
								</div>
							</div>

							<br>


				<table class="table table-bordered col-sm-6" id="subjetsTable">

								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Subject Code</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>
						<%
			               int subcount = 1;
			               for(Subject subject : subjects){
			             %>
							<tr>
								<td><%=subcount%>.</td>

								<td><%=subject.getSubjectCode() %></td>

								<td><%=subject.getSubjectDescr() %></td>
								
								<td>
	                               <input type="checkbox" class="subject-checkbox" value="<%=subject.getUuid() %>">
								</td>
							</tr>
											<%
							subcount++;
					   }
					   %>
								</tbody>
							</table>
						</div>
               

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close
							</button>
							<button type="button" class="btn btn-danger" id="addSubComb">
								Add
							</button>
						</div>
					</div>

				</div>
			</div>

























			<!-- right col -->
			<section class="col-lg-5 connectedSortable">

				<div class="alert alert-info alert-dismissible hidden"
					id="alertMessage1"></div>

				<!-- Divisions -->
				<div class="box box-solid bg-green-gradient">
					<div class="box-header">
						<i class="fa fa-calendar"></i>
						<h3 class="box-title">Divisions</h3>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-black">
						<div class="row">
							<div class="col-sm-12">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Advance Divisions</h3>
									</div>

									<div class="table-responsive ">
										<table class="table table-bordered" id="divTab"
											onkeypress="return event.keyCode != 13;">

											<thead>
												<tr>
													<th style="width: 10px">#</th>
													<th>LowerMark</th>
													<th>UpperMark</th>
													<th>Points</th>
													<th style="width: 40px">Action</th>
												</tr>
											</thead>

											<tbody>

												<%  
											
                    int dcount = 1;
                    for (Division div : divisionsAdvance) {
                          %>
												<tr>
													<td><%=dcount%>.</td>

													<td contenteditable='true'><%=div.getLowerMark() %></td>

													<td contenteditable='true'><%=div.getUpperMark() %></td>

													<td><%=div.getDescription() %></td>

													<td class="hidden"><%=div.getUuid() %></td>

													<td>
														<button type="submit"
															class="btn btn-info btn-sm divisionUpdate">
															Update</button>
													</td>
												</tr>
												<%
                          dcount++;
                           }
                           %>
											</tbody>
										</table>
									</div>


								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					
					<!-- o level division -->
					
					
					
					<!-- /.box-body -->
					<div class="box-footer text-black">
						<div class="row">
							<div class="col-sm-12">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">O Level Divisions</h3>
									</div>

									<div class="table-responsive ">
										<table class="table table-bordered" id="divTab2"
											onkeypress="return event.keyCode != 13;">

											<thead>
												<tr>
													<th style="width: 10px">#</th>
													<th>LowerMark</th>
													<th>UpperMark</th>
													<th>Points</th>
													<th style="width: 40px">Action</th>
												</tr>
											</thead>

											<tbody>

												<%  
												
                    int dcount2 = 1;
                    for (Division div : divisionsOlevel) {
                          %>
												<tr>
													<td><%=dcount2%>.</td>

													<td contenteditable='true'><%=div.getLowerMark() %></td>

													<td contenteditable='true'><%=div.getUpperMark() %></td>

													<td><%=div.getDescription() %></td>

													<td class="hidden"><%=div.getUuid() %></td>

													<td>
														<button type="submit"
															class="btn btn-info btn-sm divisionUpdate2">
															Update</button>
													</td>
												</tr>
												<%
												dcount2++;
                           }
                           %>
											</tbody>
										</table>
									</div>


								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					
					
					
					
					
				</div>
				<!-- Divisions -->





				<div class="alert alert-info alert-dismissible hidden"
					id="alertMessage2"></div>
				<!-- Grades -->
				<div class="box box-solid bg-green-gradient">
					<div class="box-header">
						<i class="fa fa-calendar"></i>
						<h3 class="box-title">Grades</h3>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-black">
						<div class="row">
							<div class="col-sm-12">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Grades</h3>
									</div>

									<div class="table-responsive ">
										<table class="table table-bordered" id="gradeTab"
											onkeypress="return event.keyCode != 13;">

											<thead>
												<tr>
													<th style="width: 10px">#</th>
													<th>LowerMark</th>
													<th>UpperMark</th>
													<th>Description</th>
													<th>Points</th>
													<th style="width: 40px">Action</th>
												</tr>
											</thead>

											<tbody>

												<%  
                    int gcount = 1;
                    for (Grade grds : grades) {
                          %>
												<tr>
													<td><%=gcount%>.</td>
													<td contenteditable='true'><%=grds.getLowerMark() %></td>
													<td contenteditable='true'><%=grds.getUpperMark() %></td>
													<td><%=grds.getDescription() %></td>
													<td><%=grds.getPoint() %></td>

													<td class="hidden"><%=grds.getUuid() %></td>

													<td>
														<button type="submit"
															class="btn btn-info btn-sm gradeUpdate">Update</button>
													</td>
												</tr>
												<%
                           gcount++;
                           }
                           %>
											</tbody>
										</table>
									</div>


								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
				</div>
				<!-- Grades -->

			</section>
			<!-- /.content -->
		</div>
	</section>
</div>






<!-- import footer -->
<jsp:include page="footer.jsp" />

<script type="text/javascript">
  
    $('#openingDate').datepicker({
      autoclose: true
    });

     $('#closingDate').datepicker({
      autoclose: true
    });





    //*************************************************

      var clicked2 = 0;
      $('.divisionUpdate').click(function(event) {
          clicked2 = 1;
       });
  
    $("#divTab").on("click", "tr", function(e) {

      
      var lMark = $.trim(this.cells[1].innerHTML);
      var uMark = $.trim(this.cells[2].innerHTML);
      var point = $.trim(this.cells[3].innerHTML);
      var uuid = $.trim(this.cells[4].innerHTML);

      if(clicked2 === 1){
         clicked2 = 0;
              $.post("updateCpanel",
              {
                lowerMark: lMark,
                upperMark: uMark,
                points: point,
                uuid: uuid,
                toUpdate: 'division',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage1').removeClass('hidden');        
                $("#alertMessage1").html(alert); 
                location.reload(true);
                 
              });


      }

      

    });

    
    
   //#############################################
    
    var clicked22 = 0;
    $('.divisionUpdate2').click(function(event) {
        clicked22 = 1;
     });

  $("#divTab2").on("click", "tr", function(e) {

    
    var lMark = $.trim(this.cells[1].innerHTML);
    var uMark = $.trim(this.cells[2].innerHTML);
    var point = $.trim(this.cells[3].innerHTML);
    var uuid = $.trim(this.cells[4].innerHTML);

    if(clicked22 === 1){
       clicked22 = 0;
            $.post("updateCpanel",
            {
              lowerMark: lMark,
              upperMark: uMark,
              points: point,
              uuid: uuid,
              toUpdate: 'division',
              centerId: '<%=centerId%>' 
            },

            function(data,status){
             var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                       + data.messageResponse;

              $('#alertMessage1').removeClass('hidden');        
              $("#alertMessage1").html(alert); 
              location.reload(true);
               
            });


    }

    

  });

    
    
    
    
    
    
   //*************************************************

   var clicked = 0;
   $('.gradeUpdate').click(function(event) {
          clicked = 1;
       });


    $("#gradeTab").on("click", "tr", function(e) {

      var lMark = $.trim(this.cells[1].innerHTML);
      var uMark = $.trim(this.cells[2].innerHTML);
      var descr = $.trim(this.cells[3].innerHTML);
      var point = $.trim(this.cells[4].innerHTML);
      var uuid = $.trim(this.cells[5].innerHTML);

      if(clicked === 1){
             clicked = 0;

              $.post("updateCpanel",
              {
                lowerMark: lMark,
                upperMark: uMark,
                description: descr,
                points: point,
                uuid: uuid,
                toUpdate: 'grade',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage2').removeClass('hidden');        
                $("#alertMessage2").html(alert); 
                location.reload(true);
                 
              });

      }
  

    });



     




   $('.showcombinationModal').click(function(event) { 
     var combinationId =  this.value; 
     var centerId = "<%=centerId%>"; 
     
     $.post("gestSubjects",
      {
        combinationId: combinationId,
        decision: 'loadSubjects',
        centerId: centerId
      },

      function(data,status){
       $("#subjectsCombTable").empty(); 
       displayeSubjects(data.subjects); 
       
      });

    });





   function displayeSubjects(object){
   	   var jsonObject = JSON.parse(object); 

       var comibation = jsonObject.comibation;
       var subjects = jsonObject.subjects;

       $("#showCombinationDesc").html(comibation);


       var table = '<div class="content table-responsive table-full-width">';
           table += '<table class="table  table-bordered">';
           table += '<thead><tr> <th>#</th><th>SubjectCode</th><th>SubjectDescr</th>';
           table +=  '</tr></thead><tbody>';
           $.each(subjects, function(i, item) {
                              
                 i++;
                 table += '</tr>';
                 table += '<td>' + i + '</td>';
                 table += '<td>' + item.subjectCode + '</td>';
                 table += '<td>' + item.subjectDescr + '</td>';
                 table += '<tr>';
          
                   });

                  table += '</tbody></table></div>';

                  $('#combinationDescModal').modal('show');
                  //$("#subjectsCombTable").show(); 
                  $("#subjectsCombTable").append(table);


       
   }








    jQuery(document).ready(function(){

        $("#addSubComb").prop("disabled", true);


        $('input[type=checkbox]').on('change', function (e) {

	        if ($('input[type=checkbox]:checked').length > 3) {
		        $(this).prop('checked', false);
		        //alert("allowed only 3");		     
		        $("#combinationMessage").html('Only 3 subjects are allowed.');
	         }
         });


        $('input[type=checkbox]').on('change', function (e) {

	        if ($('input[type=checkbox]:checked').length === 3) {
		        $("#addSubComb").prop("disabled", false); 

	         }else{
	         	 $("#addSubComb").prop("disabled", true);
	         }
         });



        var subjectsIDs = [];
        $('#addSubComb').click(function(event) {

        	var description = jQuery.trim($("#description").val()); 

        	if(description === ''){
        		$("#combinationMessage").html('Please provide a Description.');      		
        	}else{

        		//subjetsTable
        	$("#subjetsTable input:checkbox:checked").map(function(){
              subjectsIDs.push($(this).val());
             });

        	 var subjectsId = JSON.stringify(subjectsIDs);
        	 var centerId = "<%=centerId%>"; 

        	  $.post("gestSubjects",
		      {
		        subjectsId: subjectsId,
		        description: description,
		        decision: 'addCombination',
		        centerId: centerId
		      },

		      function(data,status){
		       $('#addCombinationModal').modal('toggle');   
		       $("#showremoveMessage").html(data.addMessage); 
		        blink('#showremoveMessage'); 

		         setTimeout(function(){
                  location.reload(true);
                 },5000); 
		       
		      });
         



        	}

       
         });




   $('.removeComb').click(function(event) {
         
     var combinationId =  this.value; 
     var centerId = "<%=centerId%>"; 

    // console.log('combinationId: ' + combinationId);
     
	     $.post("gestSubjects",
	      {
	        combinationId: combinationId,
	        decision: 'removeCombination',
	        centerId: centerId
	      },

	      function(data,status){
	       $("#showremoveMessage").html(data.removeMessage); 
	        blink('#showremoveMessage'); 
	        setTimeout(function(){
              location.reload(true);
             },5000); 
	       
	      });
       
         });






     //*************************************************

      var clicked1 = 0;
      var uuid = '';
      $('.combinationUpdate').click(function(event) {
          clicked1 = 1;
          uuid = this.value; 
       });
  
       $("#combinationTab").on("click", "tr", function(e) {

      
      var description = $.trim(this.cells[1].innerHTML);
      
      console.log('description: ' + description);
      console.log('uuid : ' + uuid);

      if(clicked1 === 1){
         clicked1 = 0;
              $.post("gestSubjects",
              {
                description: description,
                combinationId: uuid,
                decision: 'combinationUpdate',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
              	uuid = '';
               $("#showremoveMessage").html(data.updateMessage); 
		        blink('#showremoveMessage'); 
		        setTimeout(function(){
	              location.reload(true);
	             },5000); 
		       
              });


      }

      

    });

   //*************************************************






    function blink(selector){
        $(selector).fadeOut('slow', function(){
            $(this).fadeIn('slow', function(){
        blink(this);
          });
       });
      }



    });

     
    





</script>





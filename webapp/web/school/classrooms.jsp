<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>




<%
String centerId = ""; 
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

ConfigDAO configDAO = ConfigDAO.getInstance();
StreamDAO streamDAO = StreamDAO.getInstance();
ClassroomDAO classroomDAO = ClassroomDAO.getInstance();

List<Classroom> classlist = new ArrayList<Classroom>();
if(classroomDAO.getClassroomList(centerId) != null){
	classlist = classroomDAO.getClassroomList(centerId);
}


Map<String,Classroom> clmap = new HashMap<>();
if(classlist != null){
	for(Classroom cl : classlist){
		 clmap.put(cl.getUuid(), cl); 
	}
}

List<Stream> streamlist = new ArrayList<Stream>();
if(streamDAO.getAllStreams(centerId) != null){
	streamlist = streamDAO.getAllStreams(centerId);
}


	
	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();



    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");



%>

<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Exam panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		
		
		<div class="row">
			<!-- right column -->
			<div class="col-md-12">

			<div class="alert alert-info alert-dismissible hidden"
					id="alertMessage"></div>


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Classes Registration Form</h3>
					</div>



              <button class="btn btn-xs btn-danger" data-toggle="modal"
					data-target="#addNewClassModal">Add Class</button>

                     <div class="table-responsive ">
							<table class="table table-bordered" id="classroomsTab" onkeypress="return event.keyCode != 13;">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Class</th>
										<th>Stream</th>
										<th>Action</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

													<%
				             int count = 1;
								if(streamlist != null){
				              for(Stream stream : streamlist){
				            	  Classroom cl = clmap.get(stream.getClassroomId());
				              %>

									<tr>
										<td width="3%"><%=count %></td>
										<td><%=cl.getDescription() %></td>
										<td contenteditable='true'><%=stream.getDescription() %></td>
										<td class="hidden">
										  <%=stream.getUuid() %>
										 </td>
										<td>
										   <button class="btn btn-xs btn-danger updateClassBtn">Update </button>
										</td>
										<td>										
                                          <button class="btn btn-xs btn-danger removeClassBtn">Remove </button>
										    
										</td>
									</tr>

									<%
									count++;
				                 }
				              }
				             %>


								</tbody>
							</table>
						</div>
						
						
						
						
						
			<!-- Modal -->
			<div id="addNewClassModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add new Classroom</h4>

						</div>
						
						<div class="modal-body">
						<div class="form-group">
								<label for="Class" class="col-sm-4 control-label">
									Class </label>

								<div class="col-sm-8">
									<select class="form-control" id="newclassroomId">
									   <%
									   int stmcount = 1;
										if(classlist != null){
									   for(Classroom clss : classlist){ 
									   %>
										<option value="<%=clss.getUuid() %>"> <%=clss.getDescription() %></option>
										<% 
										stmcount++;
									    }
									   }
										%>
									</select>
								</div>
							</div>
						
						 <hr>
						
                          <div class="form-group">
								<label for="Stream" class="col-sm-4 control-label">Stream</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" id="newstreamId">
								</div>
							</div>

						</div>
						
						 <hr>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

							<button type="button" class="btn btn-danger" id="addNewClassBtn">
								Add</button>
						</div>
					</div>

				</div>
			</div>
						
						
						
		</div>
		</div>
		</div>				
		

	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />




<script type="text/javascript">
  
     $('#addNewClassBtn').click(function(event) {

      var classroomId = jQuery.trim($('#newclassroomId :selected').val()); 
      var newstream = jQuery.trim($("#newstreamId").val());  
      var centerId = "<%=centerId%>"; 

      $.post("auClassroom",
      {
    	classroomId: classroomId,
    	newstream: newstream,
        centerId: centerId,
        decision: 'addClassroom'
      },

      function(data,status){
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.messageResponse;

        $('#alertMessage').removeClass('hidden'); 
        $('#addNewClassModal').modal('toggle');            
        $("#alertMessage").html(alert); 
          
        setTimeout(function(){
          location.reload(true);
         },5000); 

         
      });

    });


     //*************************************************

      var clicked1 = 0;
      $('.removeClassBtn').click(function(event) {
          clicked1 = 1;
       });
  
    $("#classroomsTab").on("click", "tr", function(e) {
      var centerId = "<%=centerId%>"; 
      var streamId = $.trim(this.cells[3].innerHTML);
     
      if(clicked1 === 1){

         clicked1 = 0;
              $.post("auClassroom",
              {
                streamId: streamId,
                decision: 'removeClassroom',
                centerId: centerId
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage').removeClass('hidden');        
                $("#alertMessage").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

   //*************************************************
     
     
     


     //*************************************************

      var clicked2 = 0;
      $('.updateClassBtn').click(function(event) {
          clicked2 = 1;
       });
  
    $("#classroomsTab").on("click", "tr", function(e) {
      var newstreamName = $.trim(this.cells[2].innerHTML);
      var newstreamId = $.trim(this.cells[3].innerHTML);
     
      if(clicked2 === 1){

         clicked2 = 0;
              $.post("auClassroom",
              {
                newstreamName: newstreamName,
                newstreamId: newstreamId,
                decision: 'updateClassroom',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage').removeClass('hidden');        
                $("#alertMessage").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

   //*************************************************
     

</script>




<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.bean.center.student.Student"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="com.appletech.persistence.center.CycleDAO"%>
<%@page import="com.appletech.bean.center.Cycle"%>

<%@page import="com.appletech.persistence.performance.PerformanceDAO"%>
<%@page import="com.appletech.bean.center.performance.Performance"%>

<%@page import="com.appletech.persistence.stm.StudentSubjectDAO"%>
<%@page import="com.appletech.persistence.stm.CombinationDescDAO"%>
<%@page import="com.appletech.persistence.stm.SubjectDAO"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%

  String centerId = ""; 
  centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

  StudentDAO studentDAO = StudentDAO.getInstance();
  ConfigDAO configDAO = ConfigDAO.getInstance();
  SubjectDAO subjectDAO = SubjectDAO.getInstance();
  StudentSubjectDAO studentSubjectDAO = StudentSubjectDAO.getInstance();
  CombinationDescDAO combinationDescDAO = CombinationDescDAO.getInstance();
  PerformanceDAO performanceDAO = PerformanceDAO.getInstance();
  CycleDAO cycleDAO = CycleDAO.getInstance();

  List<Student> studentList = new ArrayList<Student>(); 

	String currentStreamId = request.getParameter("streamId");
	String subjectId = request.getParameter("subjectId"); 
	
	if(studentDAO.getStudentByStreamId(centerId, currentStreamId) != null){
		studentList = studentDAO.getStudentByStreamId(centerId, currentStreamId);
	}

	String subjectCode = "";
	subjectCode = subjectDAO.getSubjectByUUID(centerId, subjectId).getSubjectCode();
	
	    String term = "";
	    String year = "";
	    String thiscycle = "";
	    Config conf = configDAO.getConfig(centerId);
	    term = conf.getTerm(); year = conf.getYear();
	
	    Cycle cycle = cycleDAO.getCycleByid(centerId, conf.getCycleId()); 
	    thiscycle = cycle.getDescription().toUpperCase();
	
    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>


<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Stuent-Subject panel</small>
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="staff.jsp"><i class="fa fa-dashboard"></i> Back
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<style type="text/css">
		#errorMessage {
			color: red;
			font-size: 25px;
		}
		
		#succesMessage {
			color: green;
			font-size: 25px;
		}
		
		.scoreCell {
			min-height: 25px;
		}
		</style>


		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">
							List of Students per stream taking subject:
							<%=subjectCode%> 
						</h3> 
						<h1 class="pull-right" style="color: #ed1111;"> Cycle : <%=thiscycle %></h1> 
						
						<p id="errorMessage"></p>
					    <p id="succesMessage"></p>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered" onkeyup="pushScore(event)"
								onkeypress="return event.keyCode != 13;">
								<!-- onkeypress="return event.keyCode != 13;" -->

								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th style="width: 80px">IndexNo</th>
										<th style="width: 80px">Student</th>
										<th style="width: 10px">Cycle 1</th>
										<th style="width: 10px">Cycle 2</th>
										<th style="width: 10px">Cycle 3</th>
										<th style="width: 20px">Score</th>
									</tr>
								</thead>

								<tbody>

									<%  
              //submitScore
              int studentCount = 1;
              for (Student student : studentList) {
            	   String name = "";
            	   String cycle1 = "";
            	   String cycle2 = "";
            	   String cycle3 = "";
            	   
            	   if(StringUtils.equals(student.getIsActive(), "1") && StringUtils.equals(student.getIsAlumnus(),"0")){
            
            	   name = student.getFirstname() + " " + student.getMiddlename() + " " + student.getLastname(); 
            	   Performance performance = performanceDAO.getPerformance(centerId, student.getUuid(), subjectId, conf.getTerm(), conf.getYear());
            	   
            	   if(performance != null){
            		   cycle1 = String.valueOf(performance.getCycleOneScore()); 
            		   cycle2 = String.valueOf(performance.getCycleTwoScore());
            		   cycle3 = String.valueOf(performance.getCycleThreeScore());
            		   if(StringUtils.equals(cycle1, "0")){
            			   cycle1 = "";
            		   }if(StringUtils.equals(cycle2, "0")){
            			   cycle2 = "";
            		   }if(StringUtils.equals(cycle3, "0")){
            			   cycle3 = "";
            		   }
            	   }
            	   
            	   
                   if(combinationDescDAO.getCombinationDescBySubjectId(centerId, student.getCombinationId(), subjectId) != null ||
                		   studentSubjectDAO.getStudentSubject(centerId, student.getUuid(), subjectId) != null  ){
                    %>
									<tr>
										<td><%=studentCount%>.</td>
										<td><%=student.getIndexNo()%></td>
										<td><%=name%></td>
										<td><%=cycle1%></td>
										<td><%=cycle2%></td>
										<td><%=cycle3%></td>
										<td contenteditable='true'></td>
										<!--  contenteditable='true' class="scoreCell"  <input class="inputs">-->
									</tr>
									<%
                          studentCount++;
                         }									
                       }
                     }
              %>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">&laquo;</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>



	</section>
	<!-- /.content -->
</div>

<!--
<script src="../dist/js/submitscore.js"></script>
-->


<script type="text/javascript">
  
var streamId = "<%=currentStreamId%>";
var subjectId = "<%=subjectId%>";

function pushScore(e){
    var score = e.target.innerText;
    var currentRow = e.target.parentNode.innerText;
    var parts = currentRow.split(/[ \t]+/);
    var indexNo = parts[1];
    //console.log(score);

    if(e.keyCode === 9){ 

        $("#errorMessage").html(""); 
        $("#succesMessage").html(""); 
        
    }
     
     e.preventDefault();
     if(e.keyCode === 13){
        e.keycode=9;
        return e.keycode; 

    }else{

    if($.trim(score) !=''){
      // ajax call
      $.ajax({
         url:"submitScore",
         method:"POST",
         data:{indexNo:indexNo,subjectId:subjectId,streamId:streamId,score:score},
         dataType:"text",
         success:function(data){
          var object = JSON.parse(data); 
           $("#errorMessage").html(object.errorMessage);
           $("#succesMessage").html(object.succesMessage);
           //location.reload(true); 
         }
      });
     }

}

}

</script>
<!-- import footer -->
<jsp:include page="footer.jsp" />







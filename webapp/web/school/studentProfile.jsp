<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.bean.center.student.Student"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.stm.CombinationDAO"%>
<%@page import="com.appletech.bean.center.stm.Combination"%>

<%@page import="com.appletech.persistence.stm.CombinationDescDAO"%>
<%@page import="com.appletech.bean.center.stm.CombinationDesc"%>

<%@page import="com.appletech.persistence.stm.SubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.Subject"%>

<%@page import="com.appletech.persistence.stm.StudentSubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.StudentSubject"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>




<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.text.SimpleDateFormat"%>


<%
 
 String centerId = ""; 
 String studentId = "";
 
 HashMap<String, String> studentIdHash = (HashMap<String, String>) session.getAttribute(SessionConstants.STUDENT_PARAM);
 if (studentIdHash == null) {
	 studentIdHash = new HashMap<String, String>();
 }

 if(request.getParameter("id") != null){
	  studentId = (String) request.getParameter("id");
 }else{
	 studentId = StringUtils.trimToEmpty(studentIdHash.get("studentId"));
 }
 
 centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

 StudentDAO studentDAO = StudentDAO.getInstance();
 StreamDAO streamDAO = StreamDAO.getInstance();
 ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
 CombinationDAO combinationDAO = CombinationDAO.getInstance();
 CombinationDescDAO combinationDescDAO = CombinationDescDAO.getInstance();
 SubjectDAO subjectDAO = SubjectDAO.getInstance();
 StudentSubjectDAO studentSubjectDAO = StudentSubjectDAO.getInstance();
 ConfigDAO configDAO = ConfigDAO.getInstance();
 
 Student student = new Student();
 if(studentDAO.getStudentByUUID(centerId, studentId) != null){
	 student = studentDAO.getStudentByUUID(centerId, studentId);
 }
 
 String combination = "";
 String studentname = "";
 
 String admissionClass = "";
 String currentClass = "";
 
 List<Classroom> classlist = new ArrayList<>();
 if(classroomDAO.getClassroomList(centerId) != null){
	 classlist = classroomDAO.getClassroomList(centerId);
 }
 

 List<StudentSubject> stusublist = new ArrayList<>();
 if(studentSubjectDAO.getSubjects(centerId,studentId) != null){
	 stusublist = studentSubjectDAO.getSubjects(centerId,studentId);
 }
 
 
 Map<String,Classroom> clmap = new HashMap<>();
 for(Classroom cl : classlist){
	 clmap.put(cl.getUuid(), cl); 
 }
 
 List<Stream> streamlist = new ArrayList<>();
 if(streamDAO.getAllStreams(centerId) != null){
	 streamlist = streamDAO.getAllStreams(centerId);
 }
 
 
 
 List<Combination> comblist = new ArrayList<>();
 if(combinationDAO.getCombinationList(centerId) != null){
	 comblist = combinationDAO.getCombinationList(centerId);
 }
 
 
 List<CombinationDesc> combinations = new ArrayList<>();
 
  if(combinationDescDAO.getCombinationDesc(centerId, student.getCombinationId()) != null){
	  combinations = combinationDescDAO.getCombinationDesc(centerId, student.getCombinationId());
  }
  
  combination = combinationDAO.getCombinationBYUUID(centerId, student.getCombinationId()).getDescription();
  studentname = student.getFirstname() + " " + student.getMiddlename() + " " + student.getLastname();
  
  String admclass = "";
  String currntclass = ""; 
  Stream admitted = streamDAO.getStreambyUuid(centerId, student.getAdmmitedStreamId());
  	
  admclass = classroomDAO.getClassroomById(centerId, admitted.getClassroomId()).getDescription();
  admissionClass = admclass + " " + admitted.getDescription();
  
  Stream current = streamDAO.getStreambyUuid(centerId, student.getCurrentStreamId());
  currntclass = classroomDAO.getClassroomById(centerId, current.getClassroomId()).getDescription(); 
  currentClass = currntclass + " " + current.getDescription();
  		  
  
  List<Subject> subjects = new ArrayList<>();
  
  if(subjectDAO.getAllSubjects(centerId) != null){
	  subjects = subjectDAO.getAllSubjects(centerId);
  }
  
  Map<String,Subject> subjectMap = new HashMap<String,Subject>();
  for(Subject subject : subjects){
	  subjectMap.put(subject.getUuid(), subject); 
  }

	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();

    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>



<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Student Profile,
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i>
					Back</a></li>
			<li class="active">Student profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle"
							src="../dist/img/avatar.png" alt="User profile picture">

						<h3 class="profile-username text-center">
							Name:
							<%=studentname%>
						</h3>

						<p class="text-muted text-center">
							Combination:
							<%=combination%>
						</p>

						<div class="table-responsive ">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Code</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>

									<%
             int combCount = 1;
              for(CombinationDesc combdecs : combinations){
            	  Subject sub = new Subject();
            	  if(subjectMap.get(combdecs.getSubjectId()) != null){
            		  sub = subjectMap.get(combdecs.getSubjectId());
            	  }
            	   
            	  
              %>

									<tr>
										<td width="3%"><%=combCount %></td>
										<td class="center"><%=sub.getSubjectCode() %></td>
										<td class="center"><%=sub.getSubjectDescr() %></td>
									</tr>

									<%
              combCount++;
              }
             %>


								</tbody>
							</table>
						</div>




						<div class="alert alert-info alert-dismissible hidden"
							id="alertMessage"></div>

						<hr>

						<strong> <i class="fa fa-file-text-o margin-r-5"></i>
							Other Subjects
						</strong>
						<button class="btn btn-xs btn-danger" data-toggle="modal"
							data-target="#subModal">Add</button>
						<p></p>
						<div class="table-responsive ">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Code</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<%
             int subCount = 1;
              for(StudentSubject stusub : stusublist){
                Subject sub = new Subject();
                if(subjectMap.get(stusub.getSubjectId()) != null){
                  sub = subjectMap.get(stusub.getSubjectId());
                }
                 
                
              %>

									<tr>
										<td width="3%"><%=subCount %></td>
										<td class="center"><%=sub.getSubjectCode() %></td>
										<td>
											<button class="btn btn-xs btn-danger removeSub">
												Remove <input type="hidden" value="<%=sub.getUuid()%>">
											</button>
										</td>
									</tr>

									<%
              subCount++;
              }
             %>


								</tbody>
							</table>
						</div>


						<p></p>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

				<!-- About Me Box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">About the student</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<strong><i class="fa fa-book margin-r-5"></i> About</strong>

						<p class="text-muted">
							Admission Class:
							<%=admissionClass %>
						</p>
						<hr>
						Current Class:
						<%=currentClass %>
						<p></p>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>








			<!-- Modal -->
			<div id="subModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Assign subject</h4>
						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="Lastname" class="col-sm-2 control-label">
									Subjects </label>

								<div class="col-sm-10">
									<select class="form-control" id="subSelected" multiple>
										<% 
                int sCount = 1;
                for(Subject sub : subjects){
                  %>
										<option value="<%=sub.getUuid()%>">
											<%=sub.getSubjectCode() + " - " + sub.getSubjectDescr() %>
										</option>
										<%
                  sCount++;
                  }
                %>


									</select>
								</div>
							</div>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

							<button type="button" class="btn btn-danger" id="addSub">
								Add</button>
						</div>
					</div>

				</div>
			</div>










			<!-- /.col -->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<p>Student Profile</p>
					</ul>



					<%
String updateErr = "";
String udtsuccess = "";

if(session != null) {
  updateErr = (String) session.getAttribute(SessionConstants.STUDENT_UPDATE_ERROR);
  udtsuccess = (String) session.getAttribute(SessionConstants.STUDENT_UPDATE_SUCCESS);
} 
if (StringUtils.isNotEmpty(updateErr)) {
   %>
					<div class="alert alert-warning">
						<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
						<%
          out.println(updateErr);
           %>
					</div>

					<%                                 
    session.setAttribute(SessionConstants.STUDENT_UPDATE_ERROR, null);
  } 
   else if (StringUtils.isNotEmpty(udtsuccess)) {
   %>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
						<%
           out.println(udtsuccess);
           %>
					</div>

					<%                                
    session.setAttribute(SessionConstants.STUDENT_UPDATE_SUCCESS, null);
  } 

%>




					<div class="tab-content">



						<!--  form starts here -->
						<form class="form-horizontal" method="POST" action="updateStudent">

							<div class="form-group">
								<label for="IndexNo" class="col-sm-2 control-label">
									IndexNo </label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="IndexNo"
										value="<%=student.getIndexNo() %>">
								</div>
							</div>

							<div class="form-group">
								<label for="Firstname" class="col-sm-2 control-label">Firstname</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Firstname"
										value="<%=student.getFirstname() %>">
								</div>
							</div>


							<div class="form-group">
								<label for="Middlename" class="col-sm-2 control-label">Middlename</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Middlename"
										value="<%=student.getMiddlename() %>">
								</div>
							</div>


							<div class="form-group">
								<label for="Lastname" class="col-sm-2 control-label">Lastname</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Lastname"
										value="<%=student.getLastname() %>">
								</div>
							</div>


							<div class="form-group">
								<label for="Lastname" class="col-sm-2 control-label">
									Combination </label>

								<div class="col-sm-10">
									<select class="form-control" name="Combination">
										<option value="<%=student.getCombinationId()%>">
											<%=combination %></option>
										<% 
                          int cmbCount = 1;
                          for(Combination comb : comblist){
                        	  if(!StringUtils.equals(student.getCombinationId(), comb.getUuid())){
	                          %>
										<option value="<%=comb.getUuid()%>">
											<%=comb.getDescription() %></option>
										<%
	                          cmbCount++;
                        	  }
	                          }
                          %>



									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="Lastname" class="col-sm-2 control-label">
									Class </label>

								<div class="col-sm-10">
									<select class="form-control" name="streamId">
										<option value="<%=current.getUuid()%>">
											<%=currentClass %>
										</option>
										<% 
                           int strmCount = 1;
                    		for(Stream strm : streamlist){
                    			Classroom cl = clmap.get(strm.getClassroomId());
                    			
                    			if(!StringUtils.equals(current.getUuid(), strm.getUuid())){
                          %>
										<option value="<%=strm.getUuid()%>">
											<%=cl.getDescription() + " " + strm.getDescription() %>
										</option>
										<% 
                             strmCount++;
                    			}
                    		}
                          %>
									</select>
								</div>
							</div>


							<div class="form-group">
								<label for="Gender" class="col-sm-2 control-label">
									Gender </label>

								<div class="col-sm-10">

									<%
                          if(student.getGender().equalsIgnoreCase("M")) {
                        %>
									<div class="radio">
										<label> <input type="radio" name="gender" value="M"
											checked> Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="gender" value="F">
											Female
										</label>
									</div>

									<%}else { %>
									<div class="radio">
										<label> <input type="radio" name="gender" value="M">
											Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="gender" value="F"
											checked> Female
										</label>
									</div>

									<%} %>

								</div>
							</div>


							<div class="form-group">
								<label for="IsActive" class="col-sm-2 control-label">
									IsActive </label>

								<div class="col-sm-10">

									<%
                       if(StringUtils.equals(student.getIsActive(), "1")){
                      %>
									<div class="radio">
										<label> <input type="radio" name="IsActive" value="1"
											checked> Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="IsActive" value="0">
											No
										</label>
									</div>
									<%}else{ %>
									<div class="radio">
										<label> <input type="radio" name="IsActive" value="1">
											Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="IsActive" value="0"
											checked> No
										</label>
									</div>

									<%} %>
								</div>
							</div>


							<div class="form-group">
								<label for="IsAlumni" class="col-sm-2 control-label">
									IsAlumni </label>

								<div class="col-sm-10">
									<%
					if(StringUtils.equals(student.getIsAlumnus(), "1")){		
                       
                       %>
									<div class="radio">
										<label> <input type="radio" name="IsAlumni" value="1"
											checked> Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="IsAlumni" value="0">
											No
										</label>
									</div>
									<%}else{ %>
									<div class="radio">
										<label> <input type="radio" name="IsAlumni" value="1">
											Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="IsAlumni" value="0"
											checked> No
										</label>
									</div>
									<%} %>
								</div>
							</div>
							
							
							<% 
							if(StringUtils.equals(student.getLevelId(), "D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97")){//advance
								%>
								
								<div class="form-group">
								<label for="Level" class="col-sm-2 control-label">Level</label>

								<div class="col-sm-10">
									<select class="form-control" name="Level">
										<option value="D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97"> Advance </option>
										<option value="5ACC1957-46F3-43E2-A994-A6127A79DDEE"> O </option> 
									</select>
								</div>
							</div>
							
							
							<% } else if(StringUtils.equals(student.getLevelId(), "5ACC1957-46F3-43E2-A994-A6127A79DDEE")){ // O level
							
							%>
							
							<div class="form-group">
								<label for="Level" class="col-sm-2 control-label">Level</label>

								<div class="col-sm-10">
									<select class="form-control" name="Level">
									   <option value="5ACC1957-46F3-43E2-A994-A6127A79DDEE"> O </option> 
										<option value="D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97"> Advance </option>
										
									</select>
								</div>
							</div>
							
							<% } else{ %>
							
							<div class="form-group">
								<label for="Level" class="col-sm-2 control-label">Level</label>

								<div class="col-sm-10">
									<select class="form-control" name="Level">
										<option value="D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97"> Advance </option>
										<option value="5ACC1957-46F3-43E2-A994-A6127A79DDEE"> O </option> 
									</select>
								</div>
							</div>
							
							<% } %>
							

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="studentId" value="<%=studentId%>">
									<input type="hidden" name="centerId" value="<%=centerId%>">
									<button type="submit" class="btn btn-danger">Update</button>
								</div>
							</div>
						</form>

						<!--  form ends here -->





					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->





<!-- import footer -->
<jsp:include page="footer.jsp" />


<script type="text/javascript">
  
     $('#addSub').click(function(event) {

      var subids = [];  
      var stuid = "<%=studentId%>";
      var centerid = "<%=centerId%>";   
      
      $("#subSelected option:selected").each(function () {
         var $this = $(this);
         if ($this.length) {
          var sub = $this.val();
          subids.push(sub);
         }
      });

      var subsJSON = JSON.stringify(subids);
    
      $.post("addMoreSubjects",
      {
        subids: subsJSON,
        stuid: stuid,
        centerid: centerid
      },

      function(data,status){
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.responseMessage;

        $('#alertMessage').removeClass('hidden'); 
        $('#subModal').modal('toggle');            
        $("#alertMessage").html(alert); 
         
         setTimeout(function(){
          location.reload(true);
         },5000); 
 
         
      });

    });





     $('.removeSub').click(function(event) { 
     
      var subid = jQuery.trim($('input[type=hidden]', $(this).closest("td")).val()); 
      var stuid = "<%=studentId%>";
      var centerid = "<%=centerId%>"; 

      $.post("removeSubject",
      {
        subid: subid,
        stuid: stuid,
        centerid: centerid
      },

      function(data,status){
        var alert =  '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.responseMessage;

        $('#alertMessage').removeClass('hidden');          
        $("#alertMessage").html(alert); 
        
        setTimeout(function(){
          location.reload(true);
         },5000); 
 
         
      });

    });


</script>
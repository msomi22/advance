
<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.stm.TeacherStreamDAO"%>
<%@page import="com.appletech.bean.center.stm.TeacherStream"%>

<%@page import="com.appletech.persistence.staff.StaffDAO"%>
<%@page import="com.appletech.bean.center.staff.Staff"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%


	String centerId = ""; 
	
	StreamDAO streamDAO = StreamDAO.getInstance();
	ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
	TeacherStreamDAO teacherStreamDAO = TeacherStreamDAO.getInstance();
	StaffDAO staffDAO = StaffDAO.getInstance();
	ConfigDAO configDAO = ConfigDAO.getInstance();
	
	centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
	
	Map<String, Classroom> classroomMap = new HashMap<String, Classroom>();
	List<Classroom> classlist = new ArrayList<Classroom>();
	
	if(classroomDAO.getClassroomList(centerId) != null){
		classlist = classroomDAO.getClassroomList(centerId);
		for(Classroom classroom : classlist){
			classroomMap.put(classroom.getUuid(), classroom); 
		}
	}
	
	Map<String, Stream> streamMap = new HashMap<String, Stream>();
	List<Stream> streamlist = new ArrayList<Stream>();
	
	if(streamDAO.getAllStreams(centerId) != null){
		streamlist = streamDAO.getAllStreams(centerId);
		for(Stream stream : streamlist){
			streamMap.put(stream.getUuid(), stream); 
		}
	}
	
	
	
	List<Staff> staffList = new ArrayList<Staff>();
	Map<String, Staff> StaffMap = new HashMap<>();
	
	if(staffList != null){
		if(staffDAO.getAllStaff(centerId) != null){
		       staffList = staffDAO.getAllStaff(centerId);
		}
	}
	
	if(staffList != null){
		for(Staff staff : staffList){
			StaffMap.put(staff.getUuid(), staff); 
		}
	}
	
	

	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();

	

	List<TeacherStream> tsList = new ArrayList<TeacherStream>();
	if(teacherStreamDAO.getTeacherStream(centerId) != null){
		tsList = teacherStreamDAO.getTeacherStream(centerId);
	}
	

/*
  if (session == null) {
       response.sendRedirect("../index.jsp");
      
    }
*/

    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>


<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Class-teacher panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->

		<div class="alert alert-info alert-dismissible hidden"
					id="alertMessage"></div>




		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">List of Class Teachers</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered">

								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Name</th>
										<th>Id Number</th>
										<th>Class</th>
										<th>Exam</th>
										<th>Exam</th>
										<th>Exam</th>
										<th style="width: 40px">Action</th>
									</tr>
								</thead>

								<tbody>

									<%  
                int count = 1;
                for (TeacherStream ts : tsList) {
                	
                	String class_stream = "";
                	Staff staff = StaffMap.get(ts.getTeacherId());
                	Stream stream = streamMap.get(ts.getStreamId());
                	Classroom classroom = classroomMap.get(stream.getClassroomId());
                	
                	class_stream = classroom.getDescription() + " " + stream.getDescription();
                
                      %>
									<tr>
										<td><%=count %>.</td>
										<td><%=staff.getName() %></td>
										<td><%=staff.getIdNumber() %></td>
										<td><%=class_stream %></td>

										<td><a
											href="rpcCombined?ClassroomId=<%=stream.getClassroomId()%>"
											target="_blank"> Class MeritList </a></td>

										<td><a href="rc?ClassroomId=<%=stream.getClassroomId()%>"
											target="_blank"> Class R.cards </a></td>

										<td><a href="rpsCombined?StreamId=<%=ts.getStreamId()%>"
											target="_blank"> Stream MeritList </a></td>

										<td>
											<button type="submit" class="btn btn-info removeThisTeacher" value="<%=ts.getUuid() %>">Remove</button>
										</td>


									</tr>



									<%
                       count++;
                       }
                %>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">&laquo;</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>


















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />





<script type="text/javascript">
	
	$('.removeThisTeacher').click(function(event) {
         
     var uuid =  this.value; 
     var centerId = "<%=centerId%>"; 

    // console.log('uuid: ' + uuid);
     
	     $.post("removeClassTeacher",
	      {
	        uuid: uuid,
	        centerId: centerId
	      },

	      function(data,status){

	      	var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.responseMessage;

                $('#alertMessage').removeClass('hidden');        
                $("#alertMessage").html(alert); 
               
               setTimeout(function(){
                  location.reload(true);
                 },5000); 
	       
	      });
       
         });

</script>



<%@page import="com.appletech.persistence.center.AccessLevelDAO"%>
<%@page import="com.appletech.bean.center.AccessLevel"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>


<%

   String centerId = ""; 
   centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
   
   AccessLevelDAO accessLevelDAO = AccessLevelDAO.getInstance();
   ConfigDAO configDAO = ConfigDAO.getInstance();
   
   
   List<AccessLevel> accessLevelList = new ArrayList<>();
   if(accessLevelDAO.getAccessLevelsByCenter(centerId) != null){
	   accessLevelList =  accessLevelDAO.getAccessLevelsByCenter(centerId);
   }
     
   

	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();

   
    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");
    
    

    HashMap<String, String> staffMap = (HashMap<String, String>) session.getAttribute(SessionConstants.STAFF_ADD_PARAM);
    if (staffMap == null) {
    	staffMap = new HashMap<String, String>();
    }
    
    
%>
<!-- import header -->
<jsp:include page="header.jsp" />>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>New Staff panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>


	<%
	String addErr = "";
	String addsuccess = "";
	
	if(session != null) {
		addErr = (String) session.getAttribute(SessionConstants.STAFF_ADD_ERROR);
		addsuccess = (String) session.getAttribute(SessionConstants.STAFF_ADD_SUCCESS);
	} 
	if (StringUtils.isNotEmpty(addErr)) {
	   %>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
		<%
	          out.println(addErr);
	           %>
	</div>

	<%                                 
	    session.setAttribute(SessionConstants.STAFF_ADD_ERROR, null);
	  } 
	   else if (StringUtils.isNotEmpty(addsuccess)) {
	   %>
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
		<%
	           out.println(addsuccess);
	           %>
	</div>

	<%                                
	    session.setAttribute(SessionConstants.STAFF_ADD_SUCCESS, null);
	  } 
	
	%>


	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Staff Registration Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form class="form-horizontal" method="POST" action="newStaff">
						<div class="box-body">

							<div class="form-group">
								<label for="Name" class="col-sm-2 control-label">Name</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="name"
										placeholder="Name"
										value='<%= StringUtils.trimToEmpty(staffMap.get("name")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Mobile" class="col-sm-2 control-label">Mobile</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="mobile"
										placeholder="Mobile"
										value='<%= StringUtils.trimToEmpty(staffMap.get("mobile")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Email" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="email" class="form-control" name="email"
										placeholder="Email"
										value='<%= StringUtils.trimToEmpty(staffMap.get("email")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="IdNumber" class="col-sm-2 control-label">Id
									Number</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="idNumber"
										placeholder="Id Number"
										value='<%= StringUtils.trimToEmpty(staffMap.get("idNumber")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Nationality" class="col-sm-2 control-label">Nationality</label>
								<div class="col-sm-10">
									<select class="form-control" name="nationality">
										<option value="TZ">TZ</option>
										<option value="UG">UG</option>
										<option value="KE">KE</option>
										<option value="EL">ElseWhere</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="Access Level" class="col-sm-2 control-label">Access
									Level</label>
								<div class="col-sm-10">
									<select class="form-control" name="accessLevel">
										<%
	                            int lcount = 1;
	                           for(AccessLevel level : accessLevelList){
	                        	   if(!StringUtils.equals(level.getDescription(), "Principal")){
	                         %>
										<option value="<%=level.getUuid() %>">
											<%=level.getDescription() %></option>
										<%
	                            lcount++;
	                        	   }
	                           }
	                          %>
									</select>
								</div>
							</div>


							<!-- radio -->
							<div class="form-group">
								<label for="Gender" class="col-sm-2 control-label">Gender</label>

								<div class="radio">
									<label> <input type="radio" name="gender" value="M"
										checked> Male
									</label> <label> <input type="radio" name="gender" value="F">
										Female
									</label>
								</div>

							</div>


						</div>

						<!-- /.box-body -->
						<div class="box-footer">
							<input type="hidden" name="centerId" value="<%=centerId%>">
							<button type="submit" class="btn btn-default">Cancel</button>
							<button type="submit" class="btn btn-info pull-right">Register</button>
						</div>

						<!-- /.box-footer -->
					</form>

				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>

















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />





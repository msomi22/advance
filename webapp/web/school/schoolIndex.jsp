
<%@page import="com.appletech.pagination.student.StudentPaginator"%>
<%@page import="com.appletech.pagination.student.StudentPage"%>

<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.bean.center.student.Student"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.center.LevelDAO"%>
<%@page import="com.appletech.bean.center.Level"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.text.SimpleDateFormat"%>



<%

StudentDAO studentDAO = StudentDAO.getInstance();
StreamDAO streamDAO = StreamDAO.getInstance();
ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
ConfigDAO configDAO = ConfigDAO.getInstance();
LevelDAO levelDAO = LevelDAO.getInstance();

List<Student> studentList = new ArrayList<Student>();  
List<Level> levelList = new ArrayList<Level>();  

SimpleDateFormat stuformater = new SimpleDateFormat("MMM','dd','yyyy"); 
String center = "";
center = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

if(studentDAO.getAllStudents(center , 0 , 15) != null) {
	studentList = studentDAO.getAllStudents(center , 0 , 15); 
}


if(!levelDAO.getLevels(center).isEmpty()){
	levelList = levelDAO.getLevels(center); 
}
Map<String,String> levelMap = new HashMap<String,String>(); 
for(Level level : levelList){
	levelMap.put(level.getUuid(), level.getDescription()); 
}

int studentsCount = 0;
StudentPaginator paginator = new StudentPaginator(center);
StudentPage studentpage;

studentpage = (StudentPage) session.getAttribute("currentPage");
  String referrer = request.getHeader("referer");
  String pageParam = (String) request.getParameter("page");

  // We are to give the first page
  if (studentpage == null
          || !StringUtils.endsWith(referrer, "schoolIndex.jsp")
          || StringUtils.equalsIgnoreCase(pageParam, "first")) {
        studentpage = paginator.getFirstPage();

      //We are to give the last page
  } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
       studentpage = paginator.getLastPage();

      // We are to give the previous page
  } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
      studentpage = paginator.getPrevPage(studentpage);

      // We are to give the next page 
  } else if (StringUtils.equalsIgnoreCase(pageParam, "next"))  {
     studentpage = paginator.getNextPage(studentpage);
  }

  session.setAttribute("currentPage", studentpage);
  studentList = studentpage.getContents();
  studentsCount = (studentpage.getPageNum() - 1) * studentpage.getPagesize() + 1;

  String term = "";
  String year = "";
  Config conf = new Config();
  if(configDAO.getConfig(center) != null){
    conf = configDAO.getConfig(center);
  }
  term = conf.getTerm(); year = conf.getYear();

  /*
  if (session == null) {
       response.sendRedirect("../index.jsp");
      
    }
*/

    if (StringUtils.isEmpty(center)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>

<!-- import header -->
<jsp:include page="header.jsp" />
%>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Students panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>

		<div id="search_box">
           <form action="#" method="get">
             <input type="text" placeholder="Search Students" 
              size="20" onkeyup="searchStu(this.value)" />
           </form>
        </div>


		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>

	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Bordered Table</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>IndexNo</th>
										<th>Firstname</th>
										<th>Middlename</th>
										<th>Lastname</th>
										<th>Gender</th>
										<th>IsActive</th>
										<th>Class</th>  
										<th>Level</th> 
										<th>AdmissionDate</th>
										<th style="width: 40px">Action</th>
									</tr>
								</thead>

								<tbody class='tablebody'>

									<%
                  if(studentList !=null){
                    for(Student student : studentList){
                    	
                    	//streamDAO,classroomDAO,student.getCurrentStreamId()

                       String isactive = "";
                       String admDate = "";
                       String classStr = "";
                       String slevel = "";
                       
                       Stream stream = new Stream();
                       Classroom classroom = new Classroom();
                       if(streamDAO.getStreambyUuid(center, student.getCurrentStreamId()) != null){
                    	   stream = streamDAO.getStreambyUuid(center, student.getCurrentStreamId());
                       }
                      
                        if(classroomDAO.getClassroomById(center, stream.getClassroomId()) != null){
                        	classroom = classroomDAO.getClassroomById(center, stream.getClassroomId());
                        }
                       
                      
                      classStr = classroom.getDescription() + " : " + stream.getDescription();

                       if(StringUtils.equals(student.getIsActive(), "1")){
                          isactive = "YES";
                       }else{
                         isactive = "NO";
                       }

                       admDate = stuformater.format(student.getAdmissionDate());
                       slevel = levelMap.get(student.getLevelId());
                       %>
									<tr class="tabledit">
										<td width="3%"><%=studentsCount%></td>
										<td class="center"><%=student.getIndexNo()%></td>
										<td class="center"><%=student.getFirstname()%></td>
										<td class="center"><%=student.getMiddlename()%></td>
										<td class="center"><%=student.getLastname()%></td>
										<td class="center"><%=student.getGender()%></td>
										<td class="center"><%=isactive%></td>
										<td class="center"><%=classStr%></td>
										<td class="center"><%=slevel%></td>
										<td class="center"><%=admDate%></td>
										<td class="center"><a
											href="studentProfile.jsp?id=<%=student.getUuid()%>">
												Profile </a></td>
									</tr>
									<%
                       studentsCount++;

                     }
                    }

                  %>

								</tbody>
							</table>

							<!-- -->

							<div id="pagination">
								<form name="pageForm" method="post" action="schoolIndex.jsp">
									<%                                            
                        if (!studentpage.isFirstPage()) {
                    %>
									<input class="toolbarBtn" type="submit" name="page"
										value="First" /> <input class="toolbarBtn" type="submit"
										name="page" value="Previous" />
									<%
                        }
                    %>
									<span class="pageInfo">Page <span
										class="pagePosition currentPage"><%= studentpage.getPageNum()%></span>
										of <span class="pagePosition"><%= studentpage.getTotalPage()%></span>
									</span>
									<%
                        if (!studentpage.isLastPage()) {                        
                    %>
									<input class="toolbarBtn" type="submit" name="page"
										value="Next"> <input class="toolbarBtn" type="submit"
										name="page" value="Last">
									<%
                       }
                    %>
								</form>
							</div>




							<!-- -->


						</div>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>

	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />

<script src="../dist/js/searchStudent.js" type="text/javascript"></script>










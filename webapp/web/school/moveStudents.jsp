<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>


<%

   String centerId = ""; 
   centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
   
   StreamDAO streamDAO = StreamDAO.getInstance();
   ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
   ConfigDAO configDAO = ConfigDAO.getInstance();
   

   List<Classroom> classlist = new ArrayList<>();
	 if(classroomDAO.getClassroomList(centerId) != null){
		 classlist = classroomDAO.getClassroomList(centerId);
	 }
	 
	
	
	 Map<String,Classroom> classMap = new HashMap<>();
	 for(Classroom classroom : classlist){
		 classMap.put(classroom.getUuid(), classroom); 
	 }
	 
	 List<Stream> streamlist = new ArrayList<>();
	 if(streamDAO.getAllStreams(centerId) != null){
		 streamlist = streamDAO.getAllStreams(centerId);
	 }
     
   

	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();

   
    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");
    
    

    HashMap<String, String> staffMap = (HashMap<String, String>) session.getAttribute(SessionConstants.STAFF_ADD_PARAM);
    if (staffMap == null) {
    	staffMap = new HashMap<String, String>();
    }
    
    
%>
<!-- import header -->
<jsp:include page="header.jsp" />>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Students panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>


	<%
	String addErr = "";
	String addsuccess = "";
	
	if(session != null) {
		addErr = (String) session.getAttribute(SessionConstants.STAFF_ADD_ERROR);
		addsuccess = (String) session.getAttribute(SessionConstants.STAFF_ADD_SUCCESS);
	} 
	if (StringUtils.isNotEmpty(addErr)) {
	   %>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
		<%
	          out.println(addErr);
	           %>
	</div>

	<%                                 
	    session.setAttribute(SessionConstants.STAFF_ADD_ERROR, null);
	  } 
	   else if (StringUtils.isNotEmpty(addsuccess)) {
	   %>
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
		<%
	           out.println(addsuccess);
	           %>
	</div>

	<%                                
	    session.setAttribute(SessionConstants.STAFF_ADD_SUCCESS, null);
	  } 
	
	%>


	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<!-- right column -->
			<div class="col-md-12">

			<div class="alert alert-info alert-dismissible hidden" id="alertMessage">
				
			</div>


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title pull-left" >Move Students</h3>
                       
					</div>
					<!-- /.box-header -->
					<!-- form start -->
		<div class="col-sm-12">
			<div class="form-horizontal col-sm-6">
			  <div class="box-body"> 

			  	<button class="btn btn-info" id="displayActive">ShowActive</button>
			  	<button class="btn btn-info" id="displayInactive">ShowInActive</button>


				<div class="form-group">
					<label for="Class" class="col-sm-2 control-label">MoveFrom:</label>
					<div class="col-sm-4">
						<select class="form-control" id="sel_streamId">
							<% 
					        int mvFromCount = 1;
					        for(Stream stream : streamlist){ 
					                 Classroom classroom = classMap.get(stream.getClassroomId());
					                 
					      		  
					          %>
							<option value="<%=stream.getUuid() %>">
							  <%=classroom.getDescription() + " " + stream.getDescription() %>
						    </option>
														<%
					             mvFromCount++;
					      	 
					          } 
					        %>
						</select>
					</div>
				</div>             

			</div>
		</div>



		<div class="form-horizontal col-sm-6">
			  <div class="box-body">

			  <button class="btn btn-info" id="moveStudents">Move</button>
			  <button class="btn btn-info" id="IsAlumni">IsAlumni</button>
			  <button class="btn btn-info" id="IsNotAlumni">IsNotAlumni</button>


				<div class="form-group">
					<label for="Class" class="col-sm-2 control-label">MoveTo:</label>
					<div class="col-sm-4">
						<select class="form-control" id="moveto_streamId">
							<% 
					        int mvTocount = 1;
					        for(Stream stream : streamlist){ 
					                 Classroom classroom = classMap.get(stream.getClassroomId());
					                 
					      		  
					          %>
							<option value="<%=stream.getUuid() %>">
							  <%=classroom.getDescription() + " " + stream.getDescription() %>
						    </option>
														<%
					             mvTocount++;
					      	 
					          } 
					        %>
						</select>
					</div>
				</div>             

			</div>
		</div>
		</div>


				<!-- load table here -->
              <div id="student_display_div">
              	
              </div>






			</div>
			<!-- /.box -->
			</div>
			<!--/.col -->
		</div>
















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />




<script type="text/javascript">
	
	//
	//

	$('#displayActive').click(function(event) {
         
     var streamId = jQuery.trim($('#sel_streamId :selected').val()); 
     var centerId = "<%=centerId%>"; 

    
     
	     $.post("loadStudents",
	      {
	        streamId: streamId,
	        decision: 'loadActive', 
	        centerId: centerId
	      },

	      function(data,status){
	       $("#student_display_div").empty(); 
	       loadStudents(data.students);
	       
	      });
       
         });




	$('#displayInactive').click(function(event) {
         
     var streamId = jQuery.trim($('#sel_streamId :selected').val()); 
     var centerId = "<%=centerId%>"; 

    
     
	     $.post("loadStudents",
	      {
	        streamId: streamId,
	        decision: 'loadInactive', 
	        centerId: centerId
	      },

	      function(data,status){
	       $("#student_display_div").empty(); 
	       loadStudents(data.students);
	       
	      });
       
         });


	








	function loadStudents(object){ 
       var studentsList = JSON.parse(object);
       var students = studentsList.students;
       //console.log(students);

       var table = '<div class="content table-responsive table-full-width" id="movestudentTable">';
           table += '<table class="table  table-bordered">';
           table += '<thead><tr> <th>#</th><th>IndexNo</th><th>Firstname</th>';
           table += '<th>Middlename</th><th>Lastname</th><th><input type="checkbox" onchange="checkAll(this)" name="checkbox"></th>';
           table +=  '</tr></thead><tbody>';
           $.each(students, function(i, item) {
                              
                 i++;
                 table += '</tr>';
                 table += '<td>' + i + '</td>';
                 table += '<td>' + item.indexNo + '</td>';
                 table += '<td>' + item.firstname + '</td>';
                 table += '<td>' + item.middlename + '</td>';
                 table += '<td>' + item.lastname + '</td>';
                 table += '<td>'+'<input type="checkbox" name="studentcheckbox" value="'+item.uuid+'"> Check'+'</td>'; 
                 table += '<tr>';
          
                   });

                  table += '</tbody></table></div>';

                  $("#student_display_div").append(table);


	}


//*************************************************
	

$("#moveStudents").click(function(event){
  event.preventDefault();

  var studentIds = [];
  var tostreamId = jQuery.trim($('#moveto_streamId :selected').val()); 

  $("#movestudentTable input:checkbox:checked").map(function(){
     studentIds.push($(this).val());
  });

  if(jQuery.isEmptyObject(studentIds)){
  	 alert('Please select a student.');
  }else{

      //console.log(studentIds);
      var centerId = "<%=centerId%>"; 
      var studentIdsJSON = JSON.stringify(studentIds);


      $.post("loadStudents",
      {
        studentIds: studentIdsJSON,
        tostreamId: tostreamId,
        decision: 'moveStudent', 
        centerId: centerId
      },

      function(data,status){
        
        var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.responseMessage;

         $('#alertMessage').removeClass('hidden'); 
         $('#alertMessage').html(alert); 

         setTimeout(function(){
             location.reload(true);
          },5000); 
       
      });
  }
});

//*************************************************


$("#IsNotAlumni").click(function(event){
  event.preventDefault();

  var studentIds = [];

  $("#movestudentTable input:checkbox:checked").map(function(){
     studentIds.push($(this).val());
  });

  if(jQuery.isEmptyObject(studentIds)){
  	 alert('Please select a student.');
  }else{

      //console.log(studentIds);
      var centerId = "<%=centerId%>"; 
      var studentIdsJSON = JSON.stringify(studentIds);


      $.post("loadStudents",
      {
        studentIds: studentIdsJSON,
        decision: 'isNotAlumni', 
        centerId: centerId
      },

      function(data,status){
       
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.responseMessage;

         $('#alertMessage').removeClass('hidden'); 
         $('#alertMessage').html(alert); 

         setTimeout(function(){
             location.reload(true);
          },5000); 
       
      });
  }
});

//******************************************

$("#IsAlumni").click(function(event){
  event.preventDefault();

  var studentIds = [];

  $("#movestudentTable input:checkbox:checked").map(function(){
     studentIds.push($(this).val());
  });

  if(jQuery.isEmptyObject(studentIds)){
  	 alert('Please select a student.');
  }else{

      //console.log(studentIds);
      var centerId = "<%=centerId%>"; 
      var studentIdsJSON = JSON.stringify(studentIds);


      $.post("loadStudents",
      {
        studentIds: studentIdsJSON,
        decision: 'isAlumni', 
        centerId: centerId
      },

      function(data,status){
         var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.responseMessage;

         $('#alertMessage').removeClass('hidden'); 
         $('#alertMessage').html(alert); 

         setTimeout(function(){
             location.reload(true);
          },5000); 
       
      });
  }
});
























	function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             //console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }


	




</script>
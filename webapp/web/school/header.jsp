<!DOCTYPE html>

<%@page import="com.appletech.persistence.center.AccessLevelDAO"%>
<%@page import="com.appletech.persistence.staff.StaffDAO"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%

AccessLevelDAO accessLevelDAO = AccessLevelDAO.getInstance();
StaffDAO staffDAO = StaffDAO.getInstance();

SimpleDateFormat formater = new SimpleDateFormat("MMM','dd','yyyy");  

String centerId = "";
String staff = "";
String mobile = "";
String level = "";
String accesslevel = "";
String since = "";

mobile = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_MOBILE);
level = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_ACCESS_LEVEL);
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
staff = (String) session.getAttribute(SessionConstants.CENTER_STAFF_SIGN_ID);
String a_l_Principal = "AF066A82-B397-4213-AA1A-D9BF03FE0152";

if(accessLevelDAO.getAccessLevelByuuid(centerId, level) != null){
	accesslevel = accessLevelDAO.getAccessLevelByuuid(centerId, level).getDescription();
}

if(staffDAO.getStaffByuuid(centerId, staff) != null){
    since = formater.format(staffDAO.getStaffByuuid(centerId, staff).getDateRegistered()); 
}



if (StringUtils.isEmpty(centerId)) {
  response.sendRedirect("../index.jsp");

}


session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Advance Portal</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">

<link rel="stylesheet"
	href="../dist/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../dist/font-awesome-4.7.0/css/font-awesome.css">

<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="../plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

</head>
<!-- class="hold-transition skin-blue fixed sidebar-mini" -->
<body class="hold-transition skin-blue fixed sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="schoolIndex.jsp" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>P</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Advance</b>Portal</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="../dist/img/avatar.png" class="user-image" alt="User Image">
								<span class="hidden-xs"><%=mobile%></span>
						</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header"><img src="../dist/img/avatar.png"
									class="img-circle" alt="User Image">

									<p>
										<%=mobile%>
										-
										<%=accesslevel%>
										<small>Since : <%=since%></small>
									</p></li>

								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="userProfile.jsp" class="btn btn-default btn-flat">Profile</a>
										<a href="../dist/img/resources/user_m.pdf" target="_blank" class="btn btn-default btn-flat">User Manual</a>

									</div>
									<div class="pull-right">
										<a href="../staffLogout" class="btn btn-default btn-flat">
											Sign out </a>
									</div>
								</li>
							</ul></li>
						<!-- Control Sidebar Toggle Button -->
						<li><a href="#" data-toggle="control-sidebar"><i
								class="fa fa-gears"></i></a></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="../dist/img/avatar.png" class="img-circle"
							alt="User Image">
					</div>
					<div class="pull-left info">
						<p><%=mobile%></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control"
							placeholder="Search..."> <span class="input-group-btn">
							<button type="submit" name="search" id="search-btn"
								class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->



				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="active treeview"><a href="schoolIndex.jsp"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="schoolIndex.jsp"><i
									class="fa fa-circle-o"></i> Dashboard</a></li>
						</ul></li>

					<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
							<span>Students</span> <span class="pull-right-container">
								<span class="label label-primary pull-right">new</span>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="newstudent.jsp"> <i class="fa fa-circle-o"></i>
									Registration
							</a></li>

						</ul></li>

					<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Staff</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="staff.jsp"><i class="fa fa-circle-o"></i>View</a></li>
							<li><a href="newstaff.jsp"><i class="fa fa-circle-o"></i>
									Registration</a></li>

						</ul></li>


					<li class="treeview"><a href="#"> <i class="fa fa-laptop"></i>
							<span>Control Panel</span> <span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li>
							  <a href="cpanel.jsp"><i class="fa fa-circle-o"></i>
									General
							   </a>
							 </li>

							<li>
							  <a href="classteacher.jsp"> 
							    <i class="fa fa-circle-o"></i>ClassTeacher
							  </a>
							</li>

							<li>
							 <a href="classrooms.jsp"> 
							   <i class="fa fa-circle-o"></i>Classrooms
							 </a>
							</li>

							<li>
							 <a href="moveStudents.jsp"> 
							   <i class="fa fa-circle-o"></i>MoveStudents
							 </a>
							</li>
							
							<%if(StringUtils.equals(a_l_Principal,level)){ %>
							
							<li>
							 <a href="schoolProfile.jsp">  
							   <i class="fa fa-circle-o"></i>School Profile
							 </a>
							</li>
							
							<% } %>


						</ul>
					 </li>


					<li class="treeview"><a href="#"> <i class="fa fa-edit"></i>
							<span>Reports</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="examreport.jsp"><i class="fa fa-circle-o"></i>
									Exam</a></li>
						</ul></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
	</div>
</body>
</html>
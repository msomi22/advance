<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>



<%
String centerId = ""; 
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

ConfigDAO configDAO = ConfigDAO.getInstance();
StreamDAO streamDAO = StreamDAO.getInstance();
ClassroomDAO classroomDAO = ClassroomDAO.getInstance();

String term = "";
String year = "";
Config conf = new Config();
if(configDAO.getConfig(centerId) != null){
	 conf = configDAO.getConfig(centerId);
}

term = conf.getTerm(); 
year = conf.getYear();

List<Classroom> classlist = new ArrayList<>();
	 if(classroomDAO.getClassroomList(centerId) != null){
		 classlist = classroomDAO.getClassroomList(centerId);
	 }
	 
	
	
Map<String,Classroom> classMap = new HashMap<>();
	 for(Classroom cl : classlist){
		 classMap.put(cl.getUuid(), cl); 
	 }
	 
List<Stream> streamlist = new ArrayList<>();
	 if(streamDAO.getAllStreams(centerId) != null){
		 streamlist = streamDAO.getAllStreams(centerId);
	 }


if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");



%>
<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Reports panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->



		

		<div class="row">
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Examination Reports</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

	 <form class="form-horizontal" action="" method="POST" id="reportGenForm" target="_blank"> 
	    <div class="box-body">

              <div class="form-group">
					<label for="Cycle" class="col-sm-2 control-label">
					  Per Class
					 </label>
					<div class="col-sm-10">
						<div class="radio">
	                      <input type="radio" name="classOpt" value="perClass" checked>
	                   </div>
					</div>

					<label for="Cycle" class="col-sm-2 control-label">
					 Per Stream
					</label>
					<div class="col-sm-10">
						<div class="radio">
	                      <input type="radio" name="classOpt" value="PerStream">
	                   </div>
					</div>
			    </div>


				<div class="form-group" id="showClassOpt">
				  <label for="Stream" class="col-sm-2 control-label">Class</label>
					  <div class="col-sm-10">
						<select class="form-control" id="classroomID" name="classroomId">
							<% 
			            int count1 = 1;
			            for(Classroom clss : classlist){ 			          		
			
			              %>
						<option value="<%=clss.getUuid() %>">
							<%=clss.getDescription() %>
						</option>
								<%
			                 count1++;
			          	 
			              } 
			            %>

						</select>
					</div>
				</div>



				<div class="form-group" id="showStreamOpt">
				  <label for="Stream" class="col-sm-2 control-label">Stream</label>
					  <div class="col-sm-10">
						<select class="form-control" id="streamId" name="streamId">
							<% 
			            int count2 = 1;
			            for(Stream stream : streamlist){ 			          		
			            	Classroom classroom = classMap.get(stream.getClassroomId());
			          		  
			              %>
						<option value="<%=stream.getUuid() %>">
							<%=classroom.getDescription() + " - " + stream.getDescription() %>
						</option>
								<%
			                 count2++;
			          	 
			              } 
			            %>

						</select>
					</div>
				</div>





                <div class="form-group">
					<label for="Cycle" class="col-sm-2 control-label">Cycle</label>
					<div class="col-sm-10">
						<select class="form-control" id="cycleId" name="cycle">
							<option value="One">One</option>
							<option value="Two">Two</option>
							<option value="Three">Three</option>
						</select>
					</div>
			    </div>
			


						</div>

						<!-- /.box-body -->
						<div class="box-footer">
							<button class="btn btn-info pull-right" id="generateReport">Generate!</button>
						</div>

						<!-- /.box-footer -->
		      </form>

				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>

     <style type="text/css">
     	.modal-content{
     		background-color: #18d6f5 !important;
     		width: 900px !important;
     		height: 500px !important;
     	}
     	
     </style>


		<!-- Modal -->
			<div id="pdfFileModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">
							 PDF file
							  
							 </h4>

						</div>
						<div class="modal-body">

                           <iframe id="loadPdfIframe" src="" width="100%" height="300">
                           </iframe>

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close
							</button>
						</div>
					</div>

				</div>
			</div>




	</section>
	<!-- /.content -->
</div>

<!-- import footer -->
<jsp:include page="footer.jsp" />




<script type='text/javascript'>

jQuery(document).ready(function(){

	$("#showStreamOpt").hide();
	$('#reportGenForm').attr('action', 'rpc');


   $('input:radio[name="classOpt"]').change(
    function(){
        if ($(this).is(':checked')) {
            var whatClass =  $(this).val();
            
            if(whatClass === 'perClass'){//default
            	 $("#showStreamOpt").hide();
            	 $("#showClassOpt").show();
            	 $('#reportGenForm').attr('action', 'rpc');


            }else if(whatClass === 'PerStream'){
            	 $("#showClassOpt").hide();
            	 $("#showStreamOpt").show();
            	 $('#reportGenForm').attr('action', 'rps');


            }
           
        }
    });





});



</script>


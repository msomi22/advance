<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>



<%
String centerId = ""; 
centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

ConfigDAO configDAO = ConfigDAO.getInstance();

String term = "";
String year = "";
Config conf = configDAO.getConfig(centerId);
term = conf.getTerm(); year = conf.getYear();

if (StringUtils.isEmpty(centerId)) {
    response.sendRedirect("../index.jsp");
       
}


session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");



%>

<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>User Profile</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<%
  String resetErr = "";
  String resetSuccess = "";
	
	if(session != null) {
		resetErr = (String) session.getAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR);
		resetSuccess = (String) session.getAttribute(SessionConstants.STAFF_RESET_PASSWORD_SUCCESS);
	} 
	if (StringUtils.isNotEmpty(resetErr)) {
	   %>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
		<%
	          out.println(resetErr);
	           %>
	</div>

	<%                                 
	    session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_ERROR, null);
	  } 
	   else if (StringUtils.isNotEmpty(resetSuccess)) {
	   %>
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
		<%
	           out.println(resetSuccess);
	           %>
	</div>

	<%                                
	    session.setAttribute(SessionConstants.STAFF_RESET_PASSWORD_SUCCESS, null);
	  } 
	
	%>





  	<div class="row">
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">User Profile</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
			<form class="form-horizontal" method="POST" action="resetPassword">
				<div class="box-body">

					<div class="form-group">
						<label for="CurrentPassword" class="col-sm-2 control-label">Current Password</label>

						<div class="col-sm-10">
							<input type="password" class="form-control" name="currentPassword" placeholder="Current Password">
						</div>
					</div>


					<div class="form-group">
						<label for="New Password" class="col-sm-2 control-label">New Password</label>

						<div class="col-sm-10">
							<input type="password" class="form-control" name="newPassword"
								placeholder="New Password">
						</div>
					</div>

					<div class="form-group">
						<label for="Confirm Password" class="col-sm-2 control-label">Confirm Password</label>

					  <div class="col-sm-10">
						<input type="password" class="form-control" name="confirmPassword"
								placeholder="Confirm Password">
					  </div>
					</div>

					
				</div>

				<!-- /.box-body -->
				<div class="box-footer">
					<input type="hidden" name="centerId" value="<%=centerId%>">
					<button type="submit" class="btn btn-info pull-right">Submit</button>
				</div>

				<!-- /.box-footer -->
			</form>

				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>














	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />








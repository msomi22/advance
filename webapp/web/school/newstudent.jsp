<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.bean.center.student.Student"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.stm.CombinationDAO"%>
<%@page import="com.appletech.bean.center.stm.Combination"%>

<%@page import="com.appletech.persistence.stm.SubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.Subject"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>


<%
	StudentDAO studentDAO = StudentDAO.getInstance();
	StreamDAO streamDAO = StreamDAO.getInstance();
	ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
	CombinationDAO combinationDAO = CombinationDAO.getInstance();
	SubjectDAO subjectDAO = SubjectDAO.getInstance();
	ConfigDAO configDAO = ConfigDAO.getInstance();

	String centerId = "";

	centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

	String gender = "";
	String classid = "";
	String combId = "";
	String subs = "";

	HashMap<String, String> studentHash = (HashMap<String, String>) session
			.getAttribute(SessionConstants.STUDENT_PARAM);
	
	if (studentHash == null) {
		studentHash = new HashMap<String, String>();

		gender = StringUtils.trimToEmpty(studentHash.get("gender"));
		classid = StringUtils.trimToEmpty(studentHash.get("classroom"));
		combId = StringUtils.trimToEmpty(studentHash.get("combination"));
		subs = StringUtils.trimToEmpty(studentHash.get("subjects"));
	}

	List<Classroom> classlist = new ArrayList<>();
	List<Stream> streamlist = new ArrayList<>();
	List<Combination> comblist = new ArrayList<>();
	List<Subject> subjects = new ArrayList<>();
	Map<String, Classroom> clmap = new HashMap<>();

	if (classroomDAO.getClassroomList(centerId) != null) {
		classlist = classroomDAO.getClassroomList(centerId);
	}

	if (streamDAO.getAllStreams(centerId) != null) {
		streamlist = streamDAO.getAllStreams(centerId);
	}

	if (combinationDAO.getCombinationList(centerId) != null) {
		comblist = combinationDAO.getCombinationList(centerId);
	}

	if (subjectDAO.getAllSubjects(centerId) != null) {
		subjects = subjectDAO.getAllSubjects(centerId);
	}

	if (classlist != null) {
		for (Classroom cl : classlist) {
			clmap.put(cl.getUuid(), cl);
		}
	}

	String nextIndexNo = "";
	//nextIndexNo = studentDAO.getNextIndexNo(centerId);

	String term = "";
	String year = "";
	Config conf = new Config();

	if (configDAO.getConfig(centerId) != null) {
		conf = configDAO.getConfig(centerId);
	}

	term = conf.getTerm();
	year = conf.getYear();

	if (StringUtils.isEmpty(centerId)) {
		response.sendRedirect("../index.jsp");

	}

	session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
	response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");
%>







<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->




   <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>New Student panel</small>  <%=term %> Year: <%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li><a href="importStudent.jsp"><i class="fa fa-dashboard"></i>
					Import</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>




	<%
String addErr = "";
String addsuccess = "";

if(session != null) {
	addErr = (String) session.getAttribute(SessionConstants.STUDENT_ADD_ERROR);
	addsuccess = (String) session.getAttribute(SessionConstants.STUDENT_ADD_SUCCESS);
} 
if (StringUtils.isNotEmpty(addErr)) {
   %>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
		<%
          out.println(addErr);
           %>
	</div>

	<%                                 
    session.setAttribute(SessionConstants.STUDENT_ADD_ERROR, null);
  } 
   else if (StringUtils.isNotEmpty(addsuccess)) {
   %>
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
		<%
           out.println(addsuccess);
           %>
	</div>

	<%                                
    session.setAttribute(SessionConstants.STUDENT_ADD_SUCCESS, null);
  } 

%>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->




		<div class="row">
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Student Registration Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form class="form-horizontal" method="POST" action="addStudent">
						<div class="box-body">

							<div class="form-group">
								<label for="IndexNo" class="col-sm-2 control-label">IndexNo</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="IndexNo"
										value="<%=nextIndexNo %>">
								</div>
							</div>


							<div class="form-group">
								<label for="Firstname" class="col-sm-2 control-label">Firstname</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Firstname"
										placeholder="Firstname"
										value='<%= StringUtils.trimToEmpty(studentHash.get("firstname")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Middlename" class="col-sm-2 control-label">Middlename</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Middlename"
										placeholder="Middlename"
										value='<%= StringUtils.trimToEmpty(studentHash.get("middlename")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Lastname" class="col-sm-2 control-label">Lastname</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="Lastname"
										placeholder="Lastname"
										value='<%= StringUtils.trimToEmpty(studentHash.get("lastname")) %>'>
								</div>
							</div>


							<!-- radio -->
							<div class="form-group">
								<label for="Gender" class="col-sm-2 control-label">Gender</label>


								<%
                       if(StringUtils.isEmpty(gender)){
                     %>
								<div class="radio">
									<label> <input type="radio" name="Gender" value="M"
										checked> Male
									</label> <label> <input type="radio" name="Gender" value="F">
										Female
									</label>
								</div>

								<%} else { %>

								<% if(StringUtils.equals(gender, "M")) { %>

								<div class="radio">
									<label> <input type="radio" name="Gender" value="M"
										checked> Male
									</label> <label> <input type="radio" name="Gender" value="F">
										Female
									</label>
								</div>

								<%}else { %>

								<div class="radio">
									<label> <input type="radio" name="Gender" value="M">
										Male
									</label> <label> <input type="radio" name="Gender" value="F"
										checked> Female
									</label>
								</div>

								<%} %>

								<%} %>

							</div>

							<div class="form-group">
								<label for="Class" class="col-sm-2 control-label">Class</label>

								<div class="col-sm-10">
									<select class="form-control" name="Classroom">
										<% 
                           int strmCount = 1;
						
                        for(Stream strm : streamlist){
                        	 Classroom classroom = new Classroom();
                             classroom = clmap.get(strm.getClassroomId());
                          %>
										<option value="<%=strm.getUuid()%>">
											<%=classroom.getDescription() + " " + strm.getDescription() %>
										</option>
										<% 
                             strmCount++;
                         
                        }
                          %>
									</select>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="Level" class="col-sm-2 control-label">Level</label>

								<div class="col-sm-10">
									<select class="form-control" name="Level">
										<option value="D15FDEF1-5D34-4FE6-B17F-E7F3067D2B97"> Advance </option>
										<option value="5ACC1957-46F3-43E2-A994-A6127A79DDEE"> O </option> 
									</select>
								</div>
							</div>
							
							
							
							

							<div class="form-group">
								<label for="Combination" class="col-sm-2 control-label">Combination</label>

								<div class="col-sm-10">
									<select class="form-control" name="Combination">
										<% 
                          int cmbCount = 1;
						
                          for(Combination comb : comblist){
                            %>
										<option value="<%=comb.getUuid()%>">
											<%=comb.getDescription() %></option>
										<%
                            cmbCount++;
                              
                            }
                          %>
									</select>
								</div>
							</div>


							<div class="form-group">
								<label for="subjects" class="col-sm-2 control-label">Subjects</label>
								<div class="col-sm-10">
									<select class="form-control" multiple name="subjects[]">
														<% 
				                    int sCount = 1;
										
				                    for(Subject sub : subjects){
				                      %>
														<option value="<%=sub.getUuid()%>">
															<%=sub.getSubjectCode() + " - " + sub.getSubjectDescr() %>
														</option>
														<%
				                      sCount++;
				                        
				                      }
				                    %>
									</select>
								</div>
							</div>





						</div>

						<!-- /.box-body -->
						<div class="box-footer">
							<input type="hidden" name="centerId" value="<%=centerId %>"> 
							<button type="submit" class="btn btn-default">Cancel</button>
							<button type="submit" class="btn btn-info pull-right">Register</button>
						</div>

						<!-- /.box-footer -->
					</form>

				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>


	</section>
	<!-- /.content -->
</div>
   









<!-- import footer -->
<jsp:include page="footer.jsp" />








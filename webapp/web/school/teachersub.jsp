
<%@page import="com.appletech.persistence.stm.TeacherSubjectStreamDAO"%>
<%@page import="com.appletech.bean.center.stm.TeacherSubjectStream"%>

<%@page import="com.appletech.persistence.stm.SubjectDAO"%>
<%@page import="com.appletech.bean.center.stm.Subject"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%

    String centerId = ""; 

	TeacherSubjectStreamDAO teacherSubStreamDAO = TeacherSubjectStreamDAO.getInstance();
	SubjectDAO subjectDAO = SubjectDAO.getInstance();
	StreamDAO streamDAO = StreamDAO.getInstance();
	ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
	ConfigDAO configDAO = ConfigDAO.getInstance();
	
	List<TeacherSubjectStream> tsubstList = new ArrayList<>(); 
	List<Subject> subjects = new ArrayList<>(); 
	List<Stream> streams = new ArrayList<>(); 
	
	Map<String,String> subjectMap = new HashMap<>();
	Map<String,Stream> streamMap = new HashMap<>();
	
	centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
  
	String teacherId = request.getParameter("staffId");
	
	if(teacherSubStreamDAO.getTeacherSubjectStreamByTeacherId(centerId,teacherId) != null){
	      tsubstList = teacherSubStreamDAO.getTeacherSubjectStreamByTeacherId(centerId,teacherId);
	}
	
	if(subjectDAO.getAllSubjects(centerId) != null){
		subjects = subjectDAO.getAllSubjects(centerId);
	}
	
	if(streamDAO.getAllStreams(centerId) != null){
		streams = streamDAO.getAllStreams(centerId);
	}
	
	for(Subject subject : subjects){
		subjectMap.put(subject.getUuid(), subject.getSubjectCode());
	}
	
    for(Stream stream : streams){
    	streamMap.put(stream.getUuid(), stream);
	}
    
	
	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();


    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>


<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Teacher-Subjects panel</small>
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="staff.jsp"><i class="fa fa-dashboard"></i> Back</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">List of Subjects per class</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered">

								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Subject</th>
										<th>Stream</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>

									<%  
              int stsCount = 1;
              for (TeacherSubjectStream sts : tsubstList) {
            	  
            	  String subject = "";
            	  String streamstr = "";
            	  Stream stream;
            	  String classroom = "";
            	  String streamId = "";
            	 
            	  
            	  if(subjectMap.get(sts.getSubjectId()) != null){
            		  subject = subjectMap.get(sts.getSubjectId());
            	  }
            	  
            	  if(streamMap.get(sts.getStreamId()) != null){
            		  stream = streamMap.get(sts.getStreamId());
            		  streamstr = stream.getDescription();
            		  classroom = classroomDAO.getClassroomById(centerId, stream.getClassroomId()).getDescription(); 
            		  streamId = stream.getUuid();
            	  }
            	  
            	  
                    %>
									<tr>
										<td><%=stsCount%>.</td>
										<td><%=subject%></td>
										<td><%=classroom + " " + streamstr%></td>
										<td>
											<form action="studentsub.jsp" method="POST">
												<input type="hidden" name="streamId" value="<%=streamId%>">
												<input type="hidden" name="subjectId"
													value="<%=sts.getSubjectId()%>">
												<button class="btn btn-primary">View Students</button>
											</form>
										</td>
									</tr>
									<%
                     stsCount++;
                     }
              %>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">&laquo;</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>




	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<%@ include file="footer.jsp"%>








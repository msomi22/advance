
<%@page import="com.appletech.persistence.staff.StaffDAO"%>
<%@page import="com.appletech.bean.center.staff.Staff"%>

<%@page import="com.appletech.persistence.center.AccessLevelDAO"%>
<%@page import="com.appletech.bean.center.AccessLevel"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.util.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%

String centerId = ""; 

StaffDAO staffDAO = StaffDAO.getInstance();
AccessLevelDAO accessLevelDAO = AccessLevelDAO.getInstance();
ConfigDAO configDAO = ConfigDAO.getInstance();

List<AccessLevel> accessLevelList = new ArrayList<>();
List<Staff> staffList = new ArrayList<>();

centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

if(staffDAO.getAllStaff(centerId) != null){
       staffList = staffDAO.getAllStaff(centerId);
}

if(accessLevelDAO.getAccessLevelsByCenter(centerId) != null){
	accessLevelList = accessLevelDAO.getAccessLevelsByCenter(centerId);
}

Map<String,String> accesslMap = new HashMap<>();
for(AccessLevel level : accessLevelList){
	accesslMap.put(level.getUuid(), level.getDescription()); 
}

	
	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();
	

    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");

%>


<!-- import header 
/*@ include file="header.jsp" */
-->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Staff panel</small>
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="schoolIndex.jsp"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">List of Staff</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered">

								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Name</th>
										<th>Mobile</th>
										<th>Email</th>
										<th>Gender</th>
										<th>Access_Level</th>
										<th>Reg_Date</th>
										<th style="width: 40px">Action</th>
										<th style="width: 40px">Action</th>
									</tr>
								</thead>

								<tbody>

									<%  
                int staffCount = 1;
                for (Staff staff : staffList) {
                      %>
									<tr>
										<td><%=staffCount%>.</td>
										<td><%=staff.getName()%></td>
										<td><%=staff.getMobile()%></td>
										<td><%=staff.getEmail()%></td>
										<td><%=staff.getGender()%></td>
										<td><%=accesslMap.get(staff.getAccessLevelId())%></td>
										<td><%=staff.getDateRegistered()%></td>
										<td>
											<form method="POST" action="teachersub.jsp">
												<input type="hidden" name="staffId"
													value="<%=staff.getUuid()%>">
												<button class="btn btn-primary">Subjects</button>
											</form>
										</td>
										<td><a href="staffProfile.jsp?id=<%=staff.getUuid()%>">
												Profile </a></td>
									</tr>



									<%
                      staffCount++;
                       }
                %>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">&laquo;</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>

	</section>
	<!-- /.content -->
</div>

<!-- import footer -->
<jsp:include page="footer.jsp" />p" %>








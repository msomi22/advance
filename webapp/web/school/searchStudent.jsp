
<%@page import="com.appletech.persistence.student.StudentDAO"%>
<%@page import="com.appletech.bean.center.student.Student"%>

<%@page import="com.appletech.persistence.stm.StreamDAO"%>
<%@page import="com.appletech.bean.center.stm.Stream"%>

<%@page import="com.appletech.persistence.stm.ClassroomDAO"%>
<%@page import="com.appletech.bean.center.stm.Classroom"%>

<%@page import="com.appletech.persistence.center.LevelDAO"%>
<%@page import="com.appletech.bean.center.Level"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%@page import="java.text.SimpleDateFormat"%>



<%

StudentDAO studentDAO = StudentDAO.getInstance();
StreamDAO streamDAO = StreamDAO.getInstance();
ClassroomDAO classroomDAO = ClassroomDAO.getInstance();
LevelDAO levelDAO = LevelDAO.getInstance();

List<Student> studentList = new ArrayList<Student>();  
List<Level> levelList = new ArrayList<Level>(); 

SimpleDateFormat stuformater = new SimpleDateFormat("MMM','dd','yyyy"); 
String center = "";
center = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);

String query = request.getParameter("query"); 


if(!levelDAO.getLevels(center).isEmpty()){
	levelList = levelDAO.getLevels(center); 
}
Map<String,String> levelMap = new HashMap<String,String>(); 
for(Level level : levelList){
	levelMap.put(level.getUuid(), level.getDescription()); 
}


if(studentDAO.getAllStudents(center , 0 , 15) != null) {
	studentList = studentDAO.searchStudent(center , query); 
}


                  if(studentList !=null){
                	  int studentsCount = 1;
                    for(Student student : studentList){
                    
                       String isactive = "";
                       String admDate = "";
                       String classStr = "";
                       String slevel = "";
                       
                       Stream stream = new Stream();
                       Classroom classroom = new Classroom();
                       if(streamDAO.getStreambyUuid(center, student.getCurrentStreamId()) != null){
                    	   stream = streamDAO.getStreambyUuid(center, student.getCurrentStreamId());
                       }
                      
                        if(classroomDAO.getClassroomById(center, stream.getClassroomId()) != null){
                        	classroom = classroomDAO.getClassroomById(center, stream.getClassroomId());
                        }
                       
                      
                      classStr = classroom.getDescription() + " : " + stream.getDescription();

                       if(StringUtils.equals(student.getIsActive(), "1")){
                          isactive = "YES";
                       }else{
                         isactive = "NO";
                       }

                       admDate = stuformater.format(student.getAdmissionDate());
                       slevel = levelMap.get(student.getLevelId());

                       %>
									<tr class="tabledit">
										<td width="3%"><%=studentsCount%></td>
										<td class="center"><%=student.getIndexNo()%></td>
										<td class="center"><%=student.getFirstname()%></td>
										<td class="center"><%=student.getMiddlename()%></td>
										<td class="center"><%=student.getLastname()%></td>
										<td class="center"><%=student.getGender()%></td>
										<td class="center"><%=isactive%></td>
										<td class="center"><%=classStr%></td>
										<td class="center"><%=slevel%></td>
										<td class="center"><%=admDate%></td>
										<td class="center"><a
											href="studentProfile.jsp?id=<%=student.getUuid()%>">
												Profile </a></td>
									</tr>
									<%
                       studentsCount++;

                     }
                    }

                  %>





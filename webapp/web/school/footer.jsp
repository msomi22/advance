<%@ page import="java.util.Calendar"%>


<!-- /.content-wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 1.0
	</div>

	<strong> Copyright &copy; <%= Calendar.getInstance().get(Calendar.YEAR)%>
		<a href="#">AppleTech</a>.
	</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
<div></div>
<!-- ./wrapper -->


<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"
	type="text/javascript"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../dist/js/jquery-ui.min.js" type="text/javascript"></script>


<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="text/javascript">
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Sparkline -->
<script src="../plugins/sparkline/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
	type="text/javascript"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
	type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/knob/jquery.knob.js" type="text/javascript"></script>

<!-- daterangepicker -->

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"
	type="text/javascript"></script>




<script src="../dist/js/moment.min.js" type="text/javascript"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"
	type="text/javascript"></script>
<!-- datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"
	type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script
	src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"
	type="text/javascript"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js" type="text/javascript"></script>

<script src="../dist/js/app.min.js" type="text/javascript"></script>




<body></body>
<html></html>

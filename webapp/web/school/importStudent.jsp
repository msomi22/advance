<%@page
	import="com.appletech.server.servlet.student.upload.StudentExcel"%>

<%@page import="com.appletech.persistence.center.ConfigDAO"%>
<%@page import="com.appletech.bean.center.Config"%>

<%@page import="java.util.*"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.SessionConstants"%>

<%



 String notNull=null;

 String centerId = ""; 
 centerId = (String) session.getAttribute(SessionConstants.CENTER_SIGN_IN_ID);
 
 ConfigDAO configDAO = ConfigDAO.getInstance();
 
	
	String term = "";
	String year = "";
	Config conf = new Config();
	if(configDAO.getConfig(centerId) != null){
		 conf = configDAO.getConfig(centerId);
	}
	
	term = conf.getTerm(); 
	year = conf.getYear();


    if (StringUtils.isEmpty(centerId)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=../staffLogout");


%>

<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>Import student panel</small> Term :
			<%=term %>
			Year:
			<%=year %>
		</h1>
		<ol class="breadcrumb">
			<li><a href="newstudent.jsp"><i class="fa fa-dashboard"></i> Back</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->













		<div class="row">


			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Student Registration</h3>
					</div>


					<p>
						Upload Excel file with format <br>
						<code>IndexNo,Firstname,Middlename,Gender,Combination,Class</code>
						<br> Saved as
						<code>filename.xlsx</code>
					</p>
					<br>

					<p>
						Download this sample excel file <a
							href="../dist/img/resources/students.xlsx">Download</a>

					</p>



					<form class="form-horizontal" method="POST" action="studentExcel"
						enctype="multipart/form-data">
						<fieldset>

							<div class="form-group" id="javascript"
								javaScriptCheck="<%=notNull%>">

								<div class="col-sm-10">
									<input type="file" name="file" required="true" multiple
										accept=".csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" />
								</div>

								<div class="form-group">
									<div class="col-sm-9 col-sm-offset-3">
										<button type="submit" class="btn btn-primary">Upload
											File</button>
									</div>
								</div>
							</div>
						</fieldset>
					</form>




					<%
  if(StringUtils.isNotBlank((String)session.getAttribute(StudentExcel.UPLOAD_FEEDBACK ))) {
  String servletResponse =(String)session.getAttribute(StudentExcel.UPLOAD_FEEDBACK );
                   %>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<%out.println(servletResponse);%>
					</div>
					<%    
      if(servletResponse!=null){
        notNull=servletResponse.substring(0,10);
        }                    
      session.setAttribute(StudentExcel.UPLOAD_FEEDBACK, null);
      }
                %>






				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>

















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />


<!--scroll to the bottom the page if file upload is done -->
<script type="text/javascript">
$("document").ready(function() {   

        var check1 = $("#javascript").attr("javaScriptCheck");
        if(check1.length>2){
           $("html, body").animate({ scrollTop: $(document).height() });
        }   

    });
</script>





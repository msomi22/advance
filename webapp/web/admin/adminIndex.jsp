


<%@page import="com.appletech.persistence.center.CenterDAO"%>
<%@page import="com.appletech.bean.center.Center"%>


<%@page import="java.util.*"%>

<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.AdminSessionConstants"%>


<%

String username = (String) session.getAttribute(AdminSessionConstants.ADMIN_SIGN_IN_KEY);

    if (StringUtils.isEmpty(username)) {
        response.sendRedirect("../index.jsp");
       
    }


    session.setMaxInactiveInterval(AdminSessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", AdminSessionConstants.SESSION_TIMEOUT + "; url=adminLogout");
   
	CenterDAO centerDAO = CenterDAO.getInstance();
	
	List<Center> centers = new ArrayList<Center>(); 
	centers = centerDAO.getAllCenters();
	
	SimpleDateFormat formater = new SimpleDateFormat("MMM','dd','yyyy");  



%>

<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			School Profile,
		</h1>
		<ol class="breadcrumb">
			<li><a href="adminIndex.jsp"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Staff profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->



         
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Bordered Table</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive ">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>CenterNo</th>
										<th>CenterName</th>
										<th>CenterCode</th>
										<th>Email</th>
										<th>CenterRegion</th>
										<th>Status</th>
										<th style="width: 40px">Action</th>
									</tr>
								</thead>

								<tbody>

												<%
			                  if(centers !=null){
			                	  int count =1;
			                    for(Center center : centers){
			                    	
			                       %>
									<tr>
										<td width="3%"><%=count%></td>
										<td class="center"><%=center.getCenterNo() %></td>
										<td class="center"><%=center.getCenterName() %></td>
										<td class="center"><%=center.getCenterCode() %></td>
										<td class="center"><%=center.getEmail() %></td>
										<td class="center"><%=center.getCenterRegion() %></td>
										<td class="center"><%=center.getIsActive() %></td>
										<td class="center"><a
											href="schoolProfile.jsp?id=<%=center.getUuid()%>">
												Profile </a>
										</td>
									</tr>
									<%
									count++;

			                     }
			                    }

                  %>

								</tbody>
							</table>

							<!-- -->

							

						</div>
					</div>
				</div>
				<!-- /.box -->
			</div>

			<!-- /.col -->
		</div>
















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />








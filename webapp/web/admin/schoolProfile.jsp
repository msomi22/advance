
<%@page import="com.appletech.persistence.staff.StaffDAO"%>
<%@page import="com.appletech.bean.center.staff.Staff"%>

<%@page import="com.appletech.persistence.center.CenterDAO"%>
<%@page import="com.appletech.bean.center.Center"%>

<%@page import="com.appletech.persistence.center.ContactDAO"%>
<%@page import="com.appletech.bean.center.Contact"%>



<%@page import="java.util.*"%>

<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.AdminSessionConstants"%>

<%
	 
String username = (String) session.getAttribute(AdminSessionConstants.ADMIN_SIGN_IN_KEY);

if (StringUtils.isEmpty(username)) {
    response.sendRedirect("../index.jsp");
   
}


session.setMaxInactiveInterval(AdminSessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", AdminSessionConstants.SESSION_TIMEOUT + "; url=adminLogout");

CenterDAO centerDAO = CenterDAO.getInstance();
ContactDAO contactDAO = ContactDAO.getInstance();
StaffDAO staffDAO = StaffDAO.getInstance();

HashMap<String, String> centerIdMap = (HashMap<String, String>) session.getAttribute(AdminSessionConstants.SCHOOL_UPDATE_PARAM);
if (centerIdMap == null) {
	centerIdMap = new HashMap<String, String>();
}

String centerId = "";

if(request.getParameter("id") != null){
	centerId = (String) request.getParameter("id");
	
 }else{
	 centerId = StringUtils.trimToEmpty(centerIdMap.get("centerId"));
	 
 }

String acess_lelev_principal = "AF066A82-B397-4213-AA1A-D9BF03FE0152";

Center center = centerDAO.getCenterById(centerId);
Staff staff = new Staff();
if(staffDAO.getStaff(centerId, acess_lelev_principal) != null){
	staff = staffDAO.getStaff(centerId, acess_lelev_principal); 
}
 

List<Contact> contacts = new ArrayList<Contact>(); 
contacts = contactDAO.getContactList(centerId);




%>





<!-- import header -->
<jsp:include page="header.jsp" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			School Profile,
		</h1>
		<ol class="breadcrumb">
			<li><a href="adminIndex.jsp"><i class="fa fa-dashboard"></i>
					Back</a></li>
			<li class="active">School profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		

		<div class="row">
			<div class="col-md-3">

				<!-- Profile  -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle"
							src="../dist/img/avatar.png" alt="User profile picture">

						<h3 class="profile-username text-center">
							Name:
							<%=staff.getName() %>
						</h3>
						
						
						
						
						
				<!-- About the principal -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">About the Principal</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">


						<!--  form starts here -->
						
							<div class="form-group">
								<label for="Name" class="col-sm-4 control-label"> Name </label>

								<div class="col-sm-8">
									<input type="text" class="form-control" id="priName"
										value='<%=staff.getName() %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Mobile" class="col-sm-4 control-label">Mobile</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" id="priMobile"
										value='<%=staff.getMobile() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="Email" class="col-sm-4 control-label">Email</label>

								<div class="col-sm-8">
									<input type="email" class="form-control" id="priEmail"
										value='<%=staff.getEmail() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="IdNumber" class="col-sm-4 control-label">IdNumber</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" id="priIdNumber"
										value='<%=staff.getIdNumber() %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="Password" class="col-sm-4 control-label">Password</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" id="priPassword"
										value="">
								</div>
							</div>


							<div class="form-group">
								<label for="Nationality" class="col-sm-4 control-label">
									Nationality </label>

								<div class="col-sm-8">
									<select class="form-control" id="priNationality">
										<option value="<%=staff.getNationality() %>">
											<%=staff.getNationality() %>
										</option>
										<option value="TZ">TZ</option>
										<option value="UG">UG</option>
										<option value="KE">KE</option>
										<option value="EL">ElseWhere</option>
									</select>
								</div>
							</div>


							<div class="form-group">
								<label for="Gender" class="col-sm-4 control-label">
									Gender </label>

								<div class="col-sm-8">

									<%
                          if(staff.getGender().equalsIgnoreCase("M")) {
                        %>
									<div class="radio">
										<label> <input type="radio" name="priGender" value="M"
											checked> Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="priGender" value="F">
											Female
										</label>
									</div>

						<%}else if(staff.getGender().equalsIgnoreCase("F")) { %>
									<div class="radio">
										<label> <input type="radio" name="priGender" value="M">
											Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="priGender" value="F"
											checked> Female
										</label>
									</div>

									<%} else { %>


									<div class="radio">
										<label> <input type="radio" name="priGender" value="M">
											Male
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="priGender" value="F"> Female
										</label>
									</div>


									<% } %>

								</div>
							</div>


							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button class="btn btn-danger" id="principalBtn">Update</button>
								</div>
							</div>
						
						<!--  form ends here -->
						
						
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				
						
                       
                       <div class="alert alert-info alert-dismissible hidden"
			            id="alertMessage2"></div>
						<p class="text-muted text-center">School Contacts</p>


						<button class="btn btn-xs btn-danger" data-toggle="modal"
							data-target="#contactsModal">Add</button>


						<div class="table-responsive ">
							<table class="table table-bordered" onkeypress="return event.keyCode != 13;" id="contactsTable">
								<thead>
									<tr>
										<th style="width: 10px">#</th>
										<th>Mobile</th>
										<th>Description</th>
										<th>Action</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<%
						             int contactsCount = 1;
						              for(Contact contact : contacts){
                                    %>

									<tr>
										<td width="3%"><%=contactsCount %></td>
										<td contenteditable='true'><%=contact.getMobile() %></td>
										<td contenteditable='true'><%=contact.getDescription() %></td>
										
										<td>
											<button class="btn btn-xs btn-danger updateMobile">Update</button>
										</td>

										<td>
											<button class="btn btn-xs btn-danger removeMobile">Remove</button>
										</td>
									</tr>

									<%
									contactsCount++;
                                 }
                         %>


								</tbody>
							</table>
						</div>

						<hr>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				
				
				

			</div>








			<!-- Modal -->
			<div id="contactsModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Contacts</h4>

						</div>
						
						<div class="modal-body">

							<div class="form-group">
								<label for="Mobile" class="col-sm-2 control-label">
									Mobile:</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="mobileId">
								</div>
							</div>
							
							<hr>
							
							<div class="form-group">
								<label for="Description" class="col-sm-2 control-label">
									Description:</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="descriptionId"> 
								</div>
							</div>

						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

							<button type="button" class="btn btn-danger" id="addContactBtn">
								Add
							</button>
						</div>
					</div>

				</div>
			</div>





			<!-- /.col -->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<p> School Profile</p>
					</ul>



					<%
				String updateErr = "";
				String udtsuccess = "";
				
				if(session != null) {
				  updateErr = (String) session.getAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR);
				  udtsuccess = (String) session.getAttribute(AdminSessionConstants.SCHOOL_UPDATE_SUCCESS);
				} 
				if (StringUtils.isNotEmpty(updateErr)) {
				   %>
								<div class="alert alert-warning">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
									<%
				          out.println(updateErr);
				           %>
								</div>
			
								<%                                 
				    session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_ERROR, null);
				  } 
				   else if (StringUtils.isNotEmpty(udtsuccess)) {
				   %>
								<div class="alert alert-success">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
									<%
				           out.println(udtsuccess);
				           %>
								</div>
			
								<%                                
				    session.setAttribute(AdminSessionConstants.SCHOOL_UPDATE_SUCCESS, null);
				  } 
				
				%>


				
         <div class="alert alert-info alert-dismissible hidden"
			id="alertMessage"></div>





					<div class="tab-content">



						<!--  form starts here -->
						<form class="form-horizontal" method="POST" action="updateCenter">

							<div class="form-group">
								<label for="centerNo" class="col-sm-2 control-label">CenterNo </label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerNo"
										value='<%=center.getCenterNo() %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="centerCode" class="col-sm-2 control-label">CenterCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerCode"
										value='<%=center.getCenterCode() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="centerName" class="col-sm-2 control-label">CenterName</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerName"
										value='<%=center.getCenterName() %>'>
								</div>
							</div>


							<div class="form-group">
								<label for="centerRegion" class="col-sm-2 control-label">CenterRegion</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerRegion"
										value='<%=center.getCenterRegion() %>'>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="website" class="col-sm-2 control-label">Web site</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="website"
										value='<%=center.getWebsite() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="website" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="email" class="form-control" name="email"
										value='<%=center.getEmail() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalCode" class="col-sm-2 control-label">PostalCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalCode"
										value='<%=center.getPostalCode() %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalAddress" class="col-sm-2 control-label">PostalAddress</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalAddress"
										value='<%=center.getPostalAddress() %>'>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="town" class="col-sm-2 control-label">Town</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="town"
										value='<%=center.getTown() %>'>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="IsActive" class="col-sm-2 control-label">
									IsActive </label>

								<div class="col-sm-10">

									<%
                       if(StringUtils.equals(center.getIsActive(), "1")){
                      %>
									<div class="radio">
										<label> <input type="radio" name="isActive" value="1"
											checked> Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="isActive" value="0">
											No
										</label>
									</div>
									<%}else{ %>
									<div class="radio">
										<label> <input type="radio" name="isActive" value="1">
											Yes
										</label>
									</div>

									<div class="radio">
										<label> <input type="radio" name="isActive" value="0"
											checked> No
										</label>
									</div>

									<%} %>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="centerId" value="<%=centerId%>">
									<button type="submit" class="btn btn-danger">Update</button>
								</div>
							</div>
						</form>

						<!--  form ends here -->


					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->





<!-- import footer -->
<jsp:include page="footer.jsp" />


<script type="text/javascript">
  
     $('#addContactBtn').click(function(event) {
      
      var mobile = jQuery.trim($("#mobileId").val());
      var description = jQuery.trim($("#descriptionId").val());
      var centerId = "<%=centerId%>";   

      $.post("centerContact",
      {
    	mobile: mobile,
    	description: description,
    	decision: 'addContact',
        centerId: centerId
      },

      function(data,status){
       var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.messageResponse;

        $('#alertMessage2').removeClass('hidden'); 
        $('#contactsModal').modal('toggle');            
        $("#alertMessage2").html(alert); 
          
        setTimeout(function(){
          location.reload(true);
         },5000); 

         
      });

    });




     //*************************************************

      var clicked1 = 0;
      $('.removeMobile').click(function(event) {
          clicked1 = 1;
       });
  
    $("#contactsTable").on("click", "tr", function(e) {
      var mobile = $.trim(this.cells[1].innerHTML);
 
      if(clicked1 === 1){

         clicked1 = 0;
              $.post("centerContact",
              {
                mobile: mobile,
                decision: 'removeContact',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage2').removeClass('hidden');        
                $("#alertMessage2").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

   //*************************************************

   //*************************************************

      var clicked2 = 0;
      $('.updateMobile').click(function(event) {
          clicked2 = 1;
       });
  
    $("#contactsTable").on("click", "tr", function(e) {
      var mobile = $.trim(this.cells[1].innerHTML);
      var description = $.trim(this.cells[2].innerHTML);
     
      if(clicked2 === 1){

         clicked2 = 0;
              $.post("centerContact",
              {
                mobile: mobile,
                description: description,
                decision: 'updateContact',
                centerId: '<%=centerId%>' 
              },

              function(data,status){
               var alert = '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                         + data.messageResponse;

                $('#alertMessage2').removeClass('hidden');        
                $("#alertMessage2").html(alert); 
                
                setTimeout(function(){
                     location.reload(true);
                  },5000); 
                 
              });


      }

      

    });

    //****************************************************************************





     $('#principalBtn').click(function(event) { 
     
      var name = jQuery.trim($("#priName").val());
      var mobile = jQuery.trim($("#priMobile").val());
      var email = jQuery.trim($("#priEmail").val());
      var idNumber = jQuery.trim($("#priIdNumber").val());
      var nationality =  jQuery.trim($('#priNationality :selected').val());
      var gender = jQuery.trim($('input[name=priGender]:checked').val());
      var password = jQuery.trim($("#priPassword").val());
      var centerId = "<%=centerId%>"; 

      $.post("centerPrincipal",
      {
    	  name: name,
    	  mobile: mobile,
    	  email: email,
    	  idNumber: idNumber,
    	  nationality: nationality,
    	  gender: gender,
    	  password: password,
          centerId: centerId
      },

      function(data,status){
        var alert =  '<h4><i class="icon fa fa-info"></i> Alert!</h4> '
                 + data.messageResponse;

        $('#alertMessage').removeClass('hidden');          
        $('#alertMessage').html(alert); 
        
        setTimeout(function(){
          location.reload(true);
         },5000);  
 
         
      });

    });


</script>
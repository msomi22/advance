<!DOCTYPE html>

<%@page import="com.appletech.server.session.AdminSessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="java.util.Calendar" %>


<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin-Advance Port | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="../dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../dist/font-awesome-4.7.0/css/font-awesome.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Advance</b>Portal</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">


    <%
        String loginErrStr = "";
        session = request.getSession(false);

        if(session != null) {
            loginErrStr = (String) session.getAttribute(AdminSessionConstants.ADMIN_LOGIN_ERROR);
        }                        

        if (StringUtils.isNotEmpty(loginErrStr)) {
            %>
            <div class="alert alert-warning">
              <a href="#" class="close" data-dismiss="alert">
                    &times;
              </a>
                 <strong>Login error!</strong> 
                   <%
                   out.println(loginErrStr); 
                   %>
            </div>

    <%                                 
    session.setAttribute(AdminSessionConstants.ADMIN_LOGIN_ERROR, null);
  } 

       %>


    <p class="login-box-msg">Sign in below</p>

    <form action="adminLogin" method="POST">

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" value="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#">I forgot my password</a><br>
    
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

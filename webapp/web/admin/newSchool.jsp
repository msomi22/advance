

<%@page import="java.util.*"%>

<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.apache.commons.lang3.StringUtils"%>

<%@page import="com.appletech.server.session.AdminSessionConstants"%>



<%
String username = (String) session.getAttribute(AdminSessionConstants.ADMIN_SIGN_IN_KEY);

if (StringUtils.isEmpty(username)) {
    response.sendRedirect("../index.jsp");
   
}


session.setMaxInactiveInterval(AdminSessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", AdminSessionConstants.SESSION_TIMEOUT + "; url=adminLogout");
    

    HashMap<String, String> schoolMap = (HashMap<String, String>) session.getAttribute(AdminSessionConstants.SCHOOL_PARAM);
    if (schoolMap == null) {
    	schoolMap = new HashMap<String, String>();
    }
    
    
%>
<!-- import header -->
<jsp:include page="header.jsp" />>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard <small>New Staff panel</small> Term :
		</h1>
		<ol class="breadcrumb">
			<li><a href="adminIndex.jsp"><i class="fa fa-dashboard"></i> Back</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>


	<%
				String addErr = "";
				String addsuccess = "";
				
				if(session != null) {
					addErr = (String) session.getAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR);
					addsuccess = (String) session.getAttribute(AdminSessionConstants.SCHOOL_ADD_SUCCESS);
				} 
				if (StringUtils.isNotEmpty(addErr)) {
				   %>
								<div class="alert alert-warning">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Warning!</strong>
									<%
				          out.println(addErr);
				           %>
								</div>
			
								<%                                 
				    session.setAttribute(AdminSessionConstants.SCHOOL_ADD_ERROR, null);
				  } 
				   else if (StringUtils.isNotEmpty(addsuccess)) {
				   %>
								<div class="alert alert-success">
									<a href="#" class="close" data-dismiss="alert"> &times; </a> <strong>Success!</strong>
									<%
				           out.println(addsuccess);
				           %>
								</div>
			
								<%                                
				    session.setAttribute(AdminSessionConstants.SCHOOL_ADD_SUCCESS, null);
				  } 
				
				%>


	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->


		<div class="row">
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">School Registration Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form class="form-horizontal" method="POST" action="addCenter">
						<div class="box-body">

							<div class="form-group">
								<label for="centerNo" class="col-sm-2 control-label">CenterNo</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerNo"
										placeholder="CenterNo"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("centerNo")) %>'>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="centerCode" class="col-sm-2 control-label">CenterCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerCode"
										placeholder="CenterCode"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("centerCode")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="centerName" class="col-sm-2 control-label">CenterName</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerName"
										placeholder="CenterName"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("centerName")) %>'>
								</div>
							</div>

							<div class="form-group">
								<label for="centerRegion" class="col-sm-2 control-label">CenterRegion</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="centerRegion"
										placeholder="CenterRegion"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("centerRegion")) %>'>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="Email" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="email" class="form-control" name="email"
										placeholder="Email"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("email")) %>'>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="website" class="col-sm-2 control-label">Web site</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="website"
										placeholder="Web site"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("website")) %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalCode" class="col-sm-2 control-label">PostalCode</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalCode"
										placeholder="PostalCode"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("postalCode")) %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="postalAddress" class="col-sm-2 control-label">PostalAddress</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="postalAddress"
										placeholder="PostalAddress"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("postalAddress")) %>'>
								</div>
							</div>
							
							<div class="form-group">
								<label for="town" class="col-sm-2 control-label">Town</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="town"
										placeholder="Town"
										value='<%= StringUtils.trimToEmpty(schoolMap.get("town")) %>'>
								</div>
							</div>
							
							
						</div>

						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-default">Cancel</button>
							<button type="submit" class="btn btn-info pull-right">Register</button>
						</div>

						<!-- /.box-footer -->
					</form>

				</div>
				<!-- /.box -->

			</div>
			<!--/.col -->
		</div>

















	</section>
	<!-- /.content -->
</div>


<!-- import footer -->
<jsp:include page="footer.jsp" />





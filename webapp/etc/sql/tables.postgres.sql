
-- Schema Name: advancedb
-- Username: advance
-- Password: xP*h12_^p12_

\c postgres

-- Then execute the following:
DROP DATABASE IF EXISTS advancedb; -- To drop a database you can't be logged into it. Drops if it exists.
CREATE DATABASE advancedb;

-- Connect with the database on the username
\c advancedb advance


-----------------------------------------------
-- Table center
-----------------------------------------------
CREATE TABLE  center (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerNo text UNIQUE NOT NULL, 
    centerRegion text,
    centerName text UNIQUE NOT NULL,
    email text UNIQUE,
    website  text UNIQUE,
    centerCode  text UNIQUE,
    postalCode  text UNIQUE,
    postalAddress  text,
    town  text,
    logoUri  text UNIQUE,
    signatureUri  text UNIQUE,
    isActive text,
    dateRegistered timestamp with time zone DEFAULT now()
);
\COPY center(uuid,centerNo,centerRegion,centerName,email,website,centerCode,postalCode,postalAddress,town,logoUri,signatureUri,isActive) FROM '/tmp/center.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE center OWNER TO advance;


-----------------------------------------------
-- Table contact
-----------------------------------------------
CREATE TABLE  contact (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    centerId text REFERENCES center(uuid), 
    mobile text UNIQUE,
    description text
);
\COPY contact(uuid,centerId,mobile,description) FROM '/tmp/contact.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE contact OWNER TO advance;


-----------------------------------------------
-- Table level
-----------------------------------------------
CREATE TABLE  level (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    centerId text REFERENCES center(uuid), 
    levelId text UNIQUE,
    description text UNIQUE
);
\COPY level(uuid,centerId,levelId,description) FROM '/tmp/level.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE level OWNER TO advance;




-----------------------------------------------
-- Table accessLevel
-----------------------------------------------
CREATE TABLE  accessLevel (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    centerId text REFERENCES center(uuid), 
    description text UNIQUE
);
\COPY accessLevel(uuid,centerId,description) FROM '/tmp/accessLevel.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE accessLevel OWNER TO advance;





-----------------------------------------------
-- Table grade
-----------------------------------------------
CREATE TABLE  grade (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid), 
    lowerMark text UNIQUE NOT NULL,
    upperMark text UNIQUE NOT NULL,
    description text UNIQUE NOT NULL,
    point text UNIQUE NOT NULL  
);
\COPY grade(uuid,centerId,lowerMark,upperMark,description,point) FROM '/tmp/grade.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE grade OWNER TO advance;



-- -------------------
-- Table division
-- -------------------
CREATE TABLE  division (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    levelId text REFERENCES level(uuid), 
    lowerMark text,
    upperMark text,
    description text,
    grade text
);
\COPY division(uuid,centerId,levelId,lowerMark,upperMark,description,grade) FROM '/tmp/division.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE division OWNER TO advance;


-- -------------------
-- Table classroom
-- -------------------
CREATE TABLE  classroom (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text UNIQUE NOT NULL
);
\COPY classroom(uuid,centerId,description) FROM '/tmp/classroom.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE classroom OWNER TO advance;




-- -------------------
-- Table stream
-- -------------------
CREATE TABLE  stream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    classroomId text REFERENCES classroom(uuid),
    description text
);
\COPY stream(uuid,centerId,classroomId,description) FROM '/tmp/stream.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE stream OWNER TO advance;


-- -------------------
-- Table cycle
-- -------------------
CREATE TABLE  cycle (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text UNIQUE
);
\COPY cycle(uuid,centerId,description) FROM '/tmp/cycle.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE cycle OWNER TO advance;




-- -------------------
-- Table config
-- -------------------
CREATE TABLE  config (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    cycleId text REFERENCES cycle(uuid),
    term text,
    year text UNIQUE NOT NULL, 
    closingdate text,
    oppeningdate text,
    headTeacherComment text
);
\COPY config(uuid,centerId,cycleId,term,year,closingdate,oppeningdate,headTeacherComment) FROM '/tmp/config.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE config OWNER TO advance;



-- -------------------
-- Table subject
-- -------------------
CREATE TABLE  subject (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    subjectCode text UNIQUE NOT NULL,
    subjectNumCode text UNIQUE,
    subjectDescr text 
);
\COPY subject(uuid,centerId,subjectCode,subjectNumCode,subjectDescr) FROM '/tmp/subject.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE subject OWNER TO advance;



-------------------------------
--- Table combination
-------------------------------
CREATE TABLE  combination (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text 
);
\COPY combination(uuid,centerId,description) FROM '/tmp/combination.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE combination OWNER TO advance;

--------------------------------
-- Table combinationDesc
--------------------------------

CREATE TABLE  combinationDesc (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    combinationId text REFERENCES combination(uuid),
    subjectId text REFERENCES subject(uuid)
);
\COPY combinationDesc(uuid,centerId,combinationId,subjectId) FROM '/tmp/combinationDesc.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE combinationDesc OWNER TO advance;



-- -------------------
-- Table student
-- -------------------
CREATE TABLE  student (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    admmitedStreamId text REFERENCES stream(uuid),
	currentStreamId text REFERENCES stream(uuid),
    combinationId text REFERENCES combination(uuid),
	indexNo text UNIQUE NOT NULL,
	firstname text,
	middlename text,
	lastname text,
	gender text,
    levelId text REFERENCES level(uuid), 
    isActive text,
    isAlumnus text,
    photoUri text ,
	admissionDate timestamp with time zone DEFAULT now()
);
\COPY student(uuid,centerId,admmitedStreamId,currentStreamId,combinationId,indexNo,firstname,middlename,lastname,gender,levelId,isActive,isAlumnus,photoUri) FROM '/tmp/student.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE student OWNER TO advance;



-- -------------------
-- Table staff
-- -------------------
CREATE TABLE  staff (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    accessLevelId text REFERENCES accessLevel(uuid),
	name text,
	gender text,
	mobile text UNIQUE NOT NULL,
    password text,
	email text UNIQUE,
    idNumber text UNIQUE,
    nationality text,
    isActive text,
    dateRegistered timestamp with time zone DEFAULT now()
);
\COPY staff(uuid,centerId,accessLevelId,name,gender,mobile,password,email,idNumber,nationality,isActive) FROM '/tmp/staff.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE staff OWNER TO advance;


-- -------------------
-- Table studentSubject
-- -------------------
CREATE TABLE  studentSubject (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    studentId text REFERENCES student(uuid),
    subjectId text REFERENCES subject(uuid)
);
\COPY studentSubject(uuid,centerId,studentId,subjectId) FROM '/tmp/studentSubject.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE studentSubject OWNER TO advance;

-- -------------------
-- Table teacherSubjectStream
-- -------------------
CREATE TABLE  teacherSubjectStream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    teacherId text REFERENCES staff(uuid),
    subjectId text REFERENCES subject(uuid),
    streamId text REFERENCES stream(uuid)
);
\COPY teacherSubjectStream(uuid,centerId,teacherId,subjectId,streamId) FROM '/tmp/teacherSubjectStream.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE teacherSubjectStream OWNER TO advance;

-- -------------------
-- Table teacherStream
-- -------------------
CREATE TABLE  teacherStream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    teacherId text REFERENCES staff(uuid),
    streamId text REFERENCES stream(uuid)
);
\COPY teacherStream(uuid,centerId,teacherId,streamId) FROM '/tmp/teacherStream.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE teacherStream OWNER TO advance;

-- -------------------
-- Table performance
-- -------------------
CREATE TABLE  performance (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    studentId text REFERENCES student(uuid),
	subjectId text REFERENCES subject(uuid),
	classroomId text REFERENCES classroom(uuid),
	streamId text REFERENCES stream(uuid),
    cycleOneScore integer,
    cycleTwoScore integer,
    cycleThreeScore integer,
    term text,
    year text
);
\COPY performance(uuid,centerId,studentId,subjectId,classroomId,streamId,cycleOneScore,cycleTwoScore,cycleThreeScore,term,year) FROM '/tmp/performance.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE performance OWNER TO advance;


-- Schema Name: advancedb
-- Username: advance
-- Password: xP*h12_^p12_

\c postgres

-- Then execute the following:
DROP DATABASE IF EXISTS advancedb; -- To drop a database you can't be logged into it. Drops if it exists.
CREATE DATABASE advancedb;

-- Connect with the database on the username
\c advancedb advance


-----------------------------------------------
-- Table center
-----------------------------------------------
CREATE TABLE  center (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerNo text UNIQUE NOT NULL, 
    centerRegion text,
    centerName text UNIQUE NOT NULL,
    email text UNIQUE,
    website  text UNIQUE,
    centerCode  text UNIQUE,
    postalCode  text UNIQUE,
    postalAddress  text,
    town  text,
    logoUri  text UNIQUE,
    signatureUri  text UNIQUE,
    isActive text,
    dateRegistered timestamp with time zone DEFAULT now()
);
ALTER TABLE center OWNER TO advance;


-----------------------------------------------
-- Table contact
-----------------------------------------------
CREATE TABLE  contact (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    centerId text REFERENCES center(uuid), 
    mobile text UNIQUE,
    description text
);
ALTER TABLE contact OWNER TO advance;




-----------------------------------------------
-- Table accessLevel
-----------------------------------------------
CREATE TABLE  accessLevel (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    centerId text REFERENCES center(uuid), 
    description text UNIQUE
);
ALTER TABLE accessLevel OWNER TO advance;





-----------------------------------------------
-- Table grade
-----------------------------------------------
CREATE TABLE  grade (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid), 
    lowerMark text UNIQUE NOT NULL,
    upperMark text UNIQUE NOT NULL,
    description text UNIQUE NOT NULL,
    point text UNIQUE NOT NULL  
);
ALTER TABLE grade OWNER TO advance;



-- -------------------
-- Table division
-- -------------------
CREATE TABLE  division (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    lowerMark text UNIQUE NOT NULL,
    upperMark text UNIQUE NOT NULL,
    description text UNIQUE NOT NULL
);
ALTER TABLE division OWNER TO advance;


-- -------------------
-- Table classroom
-- -------------------
CREATE TABLE  classroom (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text UNIQUE NOT NULL
);
ALTER TABLE classroom OWNER TO advance;




-- -------------------
-- Table stream
-- -------------------
CREATE TABLE  stream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    classroomId text REFERENCES classroom(uuid),
    description text
);
ALTER TABLE stream OWNER TO advance;


-- -------------------
-- Table cycle
-- -------------------
CREATE TABLE  cycle (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text UNIQUE
);
ALTER TABLE cycle OWNER TO advance;




-- -------------------
-- Table config
-- -------------------
CREATE TABLE  config (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    cycleId text REFERENCES cycle(uuid),
    term text,
    year text UNIQUE NOT NULL, 
    closingdate text,
    oppeningdate text,
    headTeacherComment text
);
ALTER TABLE config OWNER TO advance;



-- -------------------
-- Table subject
-- -------------------
CREATE TABLE  subject (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    subjectCode text UNIQUE NOT NULL,
    subjectNumCode text UNIQUE,
    subjectDescr text 
);
ALTER TABLE subject OWNER TO advance;



-------------------------------
--- Table combination
-------------------------------
CREATE TABLE  combination (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    description text
);
ALTER TABLE combination OWNER TO advance;

--------------------------------
-- Table combinationDesc
--------------------------------

CREATE TABLE  combinationDesc (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    combinationId text REFERENCES combination(uuid),
    subjectId text REFERENCES subject(uuid)
);
ALTER TABLE combinationDesc OWNER TO advance;



-- -------------------
-- Table student
-- -------------------
CREATE TABLE  student (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    admmitedStreamId text REFERENCES stream(uuid),
	currentStreamId text REFERENCES stream(uuid),
    combinationId text REFERENCES combination(uuid),
	indexNo text UNIQUE NOT NULL,
	firstname text,
	middlename text,
	lastname text,
	gender text,
    isActive text,
    isAlumnus text,
    photoUri text ,
	admissionDate timestamp with time zone DEFAULT now()
);
ALTER TABLE student OWNER TO advance;



-- -------------------
-- Table staff
-- -------------------
CREATE TABLE  staff (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    accessLevelId text REFERENCES accessLevel(uuid),
	name text,
	gender text,
	mobile text UNIQUE NOT NULL,
    password text,
	email text UNIQUE,
    idNumber text UNIQUE,
    nationality text,
    isActive text,
    dateRegistered timestamp with time zone DEFAULT now()
);
ALTER TABLE staff OWNER TO advance;


-- -------------------
-- Table studentSubject
-- -------------------
CREATE TABLE  studentSubject (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    studentId text REFERENCES student(uuid),
    subjectId text REFERENCES subject(uuid)
);
ALTER TABLE studentSubject OWNER TO advance;

-- -------------------
-- Table teacherSubjectStream
-- -------------------
CREATE TABLE  teacherSubjectStream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    teacherId text REFERENCES staff(uuid),
    subjectId text REFERENCES subject(uuid),
    streamId text REFERENCES stream(uuid)
);
ALTER TABLE teacherSubjectStream OWNER TO advance;

-- -------------------
-- Table teacherStream
-- -------------------
CREATE TABLE  teacherStream (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    teacherId text REFERENCES staff(uuid),
    streamId text REFERENCES stream(uuid)
);
ALTER TABLE teacherStream OWNER TO advance;

-- -------------------
-- Table performance
-- -------------------
CREATE TABLE  performance (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    centerId text REFERENCES center(uuid),
    studentId text REFERENCES student(uuid),
	subjectId text REFERENCES subject(uuid),
	classroomId text REFERENCES classroom(uuid),
	streamId text REFERENCES stream(uuid),
    cycleOneScore integer,
    cycleTwoScore integer,
    cycleThreeScore integer,
    term text,
    year text
);
ALTER TABLE performance OWNER TO advance;
